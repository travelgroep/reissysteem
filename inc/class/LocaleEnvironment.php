<?php
namespace Travelgroep; 
 
class LocaleEnvironment
{
    protected $shortLongLocaleCodes;
    
    public function __construct( )
    {
        $this->shortLongLocaleCodes = array ( 
            "dk" => "da_DK.UTF-8",
            "nl" => "nl_NL.UTF-8",
            "de" => "de_DE.UTF-8",
            "se" => "sv_SE.UTF-8",
            "en" => "en_GB.UTF-8",
            "no" => "no_NO.UTF-8"
        );
    }
    
    public function getLongLocaleCodes ($locale)
    {
    	return $this->shortLongLocaleCodes[$locale];
    }
    
    public function setLocale ($locale)
    {
    	$longLocaleCodes = $this->getLongLocaleCodes($locale);
    	setlocale(LC_MESSAGES, $longLocaleCodes	);
    }
    public function setPutenv ($locale)
    {
        $longLocaleCodes = $this->getLongLocaleCodes($locale); 
    	putenv('LANG='.$longLocaleCodes);
    }
    public function setBindtextdomain ($domain , $path )
    {
         bindtextdomain($domain, $path);
    }
    public function textdomain($domain)
    {
         textdomain($domain);
    }
    
    public function setLocaleEnvironment($locale,$domain , $path )
    {
    	 $this->setlocale($locale);
         $this->setPutenv($locale);
         $this->setBindtextdomain($domain , $path );
         $this->textdomain($domain);
         
    }
   
    
}  
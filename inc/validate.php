<?

function generateResultXML( $result )
{
  // XML genereren ahv resultaatset
  header('content-type:text/xml');
  echo '<xml>';
  $keys = array_keys($result);
  for ($x = 0; $x<count($result); $x++)
  {
    echo '<error>';
    echo '<field>' . $keys[ $x ] . '</field>';
    switch ($result[ $keys[$x] ])
    {
      case "EMPTY":
        echo '<msg>Niks ingevuld!</msg>';
      break;    
      case "NOINT":
        echo '<msg>Vul alleen getallen in!</msg>';
      break;
      case "UNIQUE";
        echo '<msg>Naam bestaat al. Vul een unieke naam in.</msg>';
      break;
      case "MASK";
        echo '<msg>Foutieve invoer!</msg>';
      break;
    }
    echo '</error>';
  }
  echo '</xml>';
}

?>
<?php 
      require_once( ROOT.DBLIB.'db.php' );
      require_once( ROOT.DBLIB.'DBRecord.php');
  
      require_once( ROOT . 'class/Locale2.php');
      require_once( ROOT . 'class/Beschrijving.php');
      require_once( ROOT . 'class/Reisoptie.php');
      require_once( ROOT . 'class/ReisoptieWaarde.php');
      require_once( ROOT . 'class/LocaleReisoptieWaarde.php');
      require_once( ROOT . 'class/Webentiteit.php');
      require_once( ROOT . 'class/Land.php');
      require_once( ROOT . 'class/Stad.php');
      require_once( ROOT . 'class/Locatie.php');
      require_once( ROOT . 'class/Event.php');
      require_once( ROOT . 'class/Hotel.php');
      require_once( ROOT . 'class/Arrangement.php');
      require_once( ROOT . 'class/EventType.php');
      require_once( ROOT . 'class/Foto.php');
      require_once( ROOT . 'class/Layout.php');
      require_once( ROOT . 'class/Tariefwijziging.php');
      require_once( ROOT . 'class/Valuta.php');
      require_once( ROOT . 'class/LocaleValue.php');
      require_once( ROOT . 'class/Vliegveld.php');
      require_once( ROOT . 'inc/validate.php');

    require_once( ROOT . 'class/Controller/OptaMatchController.php');
      
    require_once( ROOT . 'class/Team.php');
    require_once( ROOT . 'class/Match.php');
    require_once( ROOT . 'class/Competition.php');
    require_once( ROOT . 'class/MatchChangelog.php');
    require_once( ROOT . 'class/ArrangementChangelog.php');
    require_once( ROOT . 'class/Venue.php');
    
    require_once( ROOT . 'class/ModelManager/TeamModelManager.php');
    require_once( ROOT . 'class/ModelManager/MatchModelManager.php');
    require_once( ROOT . 'class/ModelManager/MatchChangelogModelManager.php');
    require_once( ROOT . 'class/ModelManager/ArrangementChangelogModelManager.php');
    require_once( ROOT . 'class/ModelManager/CompetitionModelManager.php');
    require_once( ROOT . 'class/ModelManager/VenueModelManager.php');
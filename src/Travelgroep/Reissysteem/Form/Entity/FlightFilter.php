<?php

namespace Travelgroep\Reissysteem\Form\Entity;

class FlightFilter
{
    private $id;

    private $departure;

    private $arrive;

    private $departureAirportIata;

    private $departureAirportName;
    
    private $arriveAirportIata;

    private $arriveAirportName;

    private $airlineCode;

    private $airlineName;

    private $flightNumber;

    private $costPerPerson;
    
    private $currency;

    private $enabled;
    
    private $createdAt;    

    private $updatedAt;
        
    public function getId()
    {
        return $this->id;
    }

    public function getDeparture()
    {
        return $this->departure;
    }

    public function setDeparture($departure)
    {
        $this->departure = $departure;

        return $this;
    }

    public function getArrive()
    {
        return $this->arrive;
    }

    public function setArrive($arrive)
    {
        $this->arrive = $arrive;

        return $this;
    }
    
    public function getDepartureAirportIata()
    {
        return $this->departureAirportIata;
    }

    public function setDepartureAirportIata($departureAirportIata)
    {
        $this->departureAirportIata = $departureAirportIata;

        return $this;
    }

    public function getDepartureAirportName()
    {
        return $this->departureAirportName;
    }

    public function setDepartureAirportName($departureAirportName)
    {
        $this->departureAirportName = $departureAirportName;

        return $this;
    }
    
    public function getArriveAirportIata()
    {
        return $this->arriveAirportIata;
    }

    public function setArriveAirportIata($arriveAirportIata)
    {
        $this->arriveAirportIata = $arriveAirportIata;

        return $this;
    }

    public function getArriveAirportName()
    {
        return $this->arriveAirportName;
    }

    public function setArriveAirportName($arriveAirportIata)
    {
        $this->arriveAirportName = $arriveAirportIata;

        return $this;
    }
    
    public function getAirlineCode()
    {
        return $this->airlineCode;
    }

    public function setAirlineCode($airlineCode)
    {
        $this->airlineCode = $airlineCode;

        return $this;
    }
    
    public function getAirlineName()
    {
        return $this->airlineName;
    }

    public function setAirlineName($airlineName)
    {
        $this->airlineName = $airlineName;

        return $this;
    }
    
    public function getFlightNumber()
    {
        return $this->flightNumber;
    }

    public function setFlightNumber($flightNumber)
    {
        $this->flightNumber = $flightNumber;

        return $this;
    }
    
    public function getCostPerPerson()
    {
        return $this->costPerPerson;
    }

    public function setCostPerPerson($costPerPerson)
    {
        $this->costPerPerson = $costPerPerson;

        return $this;
    }
    
    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }
    
    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
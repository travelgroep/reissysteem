<?php
namespace Travelgroep\Reissysteem\Form\Type\Filter;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FlightFilterType extends AbstractType
{
    const CHOICE_ENABLED_ALL = 'all';
    const CHOICE_ENABLED_ENABLED = 'enabled';
    const CHOICE_ENABLED_DISABLED = 'disabled';
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enabled', 'choice', array(
                'choices'   => array(
                    self::CHOICE_ENABLED_ALL => self::CHOICE_ENABLED_ALL,
                    self::CHOICE_ENABLED_ENABLED => self::CHOICE_ENABLED_ENABLED,
                    self::CHOICE_ENABLED_DISABLED => self::CHOICE_ENABLED_DISABLED),
                    'required' => false
                )
            )
            ->add('flightNumber', 'text', array('required' => false))
            ->add('airlineCode', 'text', array('required' => false))
            ->add('airlineName', 'text', array('required' => false))
            ->add('departureAirportIata', 'text', array('required' => false))
            ->add('departureAirportName', 'text', array('required' => false))
            ->add('arriveAirportIata', 'text', array('required' => false))
            ->add('arriveAirportName', 'text', array('required' => false))
            ->add('departure', 'datetime',
                array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false
                )
            )
            ->add('arrive', 'datetime', 
                array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false
                )
            )
            ->add('currency', 'text', array('required' => false))
            ->add('filter', 'submit');
    }

    public function getName()
    {
        return 'flight_filter';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => '\Travelgroep\Reissysteem\Form\Entity\FlightFilter',
        ));
    }    
}
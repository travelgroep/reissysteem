<?php
namespace Travelgroep\Reissysteem\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class FlightType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enabled', 'checkbox', array('required'  => false))
            ->add('flightNumber', 'text')
            ->add('airlineCode', 'text')
            ->add('airlineName', 'text')
            ->add('departureAirportIata', 'text')
            ->add('departureAirportName', 'text')
            ->add('arriveAirportIata', 'text')
            ->add('arriveAirportName', 'text')
            ->add('departure', 'datetime',
                array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm'                
                )
            )
            ->add('arrive', 'datetime', 
                array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm'                
                )
            )
            ->add('costPerPerson', 'text')
            ->add('currency', 'text')
            ->add('save', 'submit');
    }

    public function getName()
    {
        return 'flight';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => '\Travelgroep\Bundle\OldSystem\RBSBundle\Entity\Flight',
        ));
    }    
}
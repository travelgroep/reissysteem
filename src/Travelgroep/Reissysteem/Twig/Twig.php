<?php 
namespace Travelgroep\Reissysteem\Twig;

use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Twig_Environment;
use Twig_Loader_Filesystem;

class Twig
{
    public static function getTwig()
    {
        // the Twig file that holds all the default markup for rendering forms
        // this file comes with TwigBridge
        $defaultFormTheme = 'form_div_layout.html.twig';

        $vendorDir = realpath(ROOT.'vendor');
        $vendorFormDir = $vendorDir.'/symfony/form/Symfony/Component/Form';

        // the path to TwigBridge so Twig can locate the form_div_layout.html.twig file
        $vendorTwigBridgeDir = $vendorDir . '/symfony/twig-bridge/Symfony/Bridge/Twig';

        // the path to your other templates
        $viewsDir = realpath(ROOT.'src/Travelgroep/Reissysteem/Resources/view');

        $twig = new Twig_Environment(new Twig_Loader_Filesystem(array(
            $viewsDir,
            $vendorTwigBridgeDir . '/Resources/views/Form',
        )));
        
        // create the Translator
        $translator = new Translator('en');
        // somehow load some translations into it
        $translator->addLoader('xlf', new XliffFileLoader());
        $translator->addResource(
            'xlf',
            $vendorFormDir.'/Resources/translations/validators.en.xlf',
            'en'
        );
        
        // add the TranslationExtension (gives us trans and transChoice filters)
        $twig->addExtension(new TranslationExtension($translator));        
        
        
        $formEngine = new TwigRendererEngine(array($defaultFormTheme));
        $formEngine->setEnvironment($twig);
        // add the FormExtension to Twig
        $twig->addExtension(new FormExtension(new TwigRenderer($formEngine, $csrfProvider)));
        
        
//        $templatePath = ROOT.'/src/Travelgroep/Reissysteem/Resources/view';
//        $loader = new \Twig_Loader_Filesystem($templatePath);
//        $twig = new \Twig_Environment($loader);

        return $twig;
    }
}

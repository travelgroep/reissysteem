<?php
namespace Travelgroep\Reissysteem\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="dummy_flight")
 */
class Flight
{
    /** @ORM\Id
     *  @ORM\Column(type="integer")
     *  @ORM\GeneratedValue */
    private $id;

    /** @ORM\Column(type="string") */
    private $currency;
}

<?php 
namespace Travelgroep\Reissysteem\Controller;

use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\DefaultView;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\Request;
use Travelgroep\Reissysteem\Doctrine\Doctrine;
use Travelgroep\Reissysteem\Form\Type\FlightType;
use Travelgroep\Reissysteem\Form\Type\Filter\FlightFilterType;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;

class FlightController
{
    public function indexAction()
    {
        $request = Request::createFromGlobals();
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getEntityManager();
        $flightRepository = $em->getRepository('\Travelgroep\Bundle\OldSystem\RBSBundle\Entity\Flight');

        $filterForm = $this->getFilterForm();        
        $filterForm->submit($request->query->get($filterForm->getName()));

        $qb = $flightRepository->getAll();
        $flightRepository->filterByFilterForm($qb, $filterForm);
        $qb->addOrderBy('flight.departure', 'DESC');
        
        $pagerfanta = $this->getPaginator($qb);
        $paginatorView = $this->getPaginatorView($pagerfanta);
        $flights = $pagerfanta->getCurrentPageResults($pagerfanta);

        $this->render(
            'Flight/index.html.twig',
            array(
                'flights' => $flights,
                'paginatorView' => $paginatorView,
                'filterForm' => $filterForm->createView()
            )        
        );
    }

    public function newAction()
    {
        $request = Request::createFromGlobals();
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getEntityManager();
        $flightRepository = $em->getRepository('\Travelgroep\Bundle\OldSystem\RBSBundle\Entity\Flight');
        
        $id = $request->query->get('id');
        if ($id) {
            $existingFlight = $flightRepository->find($id);
            if ($existingFlight) {
                $flight = clone $existingFlight;
            }
            else {
                $flight = new \Travelgroep\Bundle\OldSystem\RBSBundle\Entity\Flight();
            }
        }
        $form = $this->createForm(new FlightType(), $flight);

        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            
            if ($form->isValid()) {
                $data = $form->getData();
                $em->persist($data);
                $em->flush();
                $this->redirect('index.php');
                
            }
        }
                
        $this->render(
            'Flight/new.html.twig',
            array(
                'form' => $form->createView()
            )
        );        
    }

    public function editAction()
    {
        $request = Request::createFromGlobals();
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getEntityManager();
        
        $id = $request->query->get('id');
        
        $flightRepository = $em->getRepository('\Travelgroep\Bundle\OldSystem\RBSBundle\Entity\Flight');
        $flight = $flightRepository->find($id);
        $form = $this->createForm(new FlightType(), $flight);

        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));
            
            if ($form->isValid()) {
                $data = $form->getData();
                $em->persist($data);
                $em->flush();
            }
        }        
        $this->render(
            'Flight/edit.html.twig',
            array(
                'form' => $form->createView()
            )
        );        
    }

    public function deleteAction()
    {
        $request = Request::createFromGlobals();
        
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getEntityManager();
        
        $id = $request->query->get('id');
        
        $flightRepository = $em->getRepository('\Travelgroep\Bundle\OldSystem\RBSBundle\Entity\Flight');
        $flight = $flightRepository->find($id);

        if ($flight) {
            $em->remove($flight);
            $em->flush();
        }

        $referer = $request->headers->get('referer');
        $this->redirect($referer);
    }
    
    protected function getDoctrine()
    {
        $doctrineOptions = array(
            'user' => DBUSER ,
            'password' => DBPASS,
            'dbname' => DBNAME,
            'proxy' => array(
                'path' => ROOT.'cachedir',
                'ns' => 'Proxy'
            ),
            'gedmo' => array(
                'timestampable' => array(
                    'annotation_path' => ROOT.'vendor/gedmo/doctrine-extensions/lib' 
                )
            )
        );

        
        
        $doctrine = new Doctrine($doctrineOptions);
        \Doctrine\Common\Annotations\AnnotationRegistry::registerAutoloadNamespace('Symfony\Component\Validator\Constraint', ROOT.'vendor/symfony/validator');    
        
        return $doctrine;
    }
    
    protected function getPaginator($qb)
    {
        $adapter = new DoctrineORMAdapter($qb);
        $pagerfanta = new Pagerfanta($adapter);
        $maxPerPage = 20;
        if (isset($_GET['page'])) {
            $currentPage = $_GET['page'];
        } else {
            $currentPage = 1;
        }
        $pagerfanta->setMaxPerPage($maxPerPage);
        $pagerfanta->setCurrentPage($currentPage);
        
        return $pagerfanta;
    }
    
    protected function getPaginatorView($pagerfanta)
    {
        $view = new DefaultView();
        $options = array('proximity' => 10);
        $routeGenerator = function($page) 
        {
            return '?page=' . $page;
        };
        
        return $view->render($pagerfanta, $routeGenerator, $options);
    }
    
    protected function getFormFactory()
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();
        $formFactory = Forms::createFormFactoryBuilder()
            ->addExtension(new ValidatorExtension($validator))
            ->getFormFactory();
            
        return $formFactory;
    }
    
    protected function createForm($type, $data = null)
    {
        $formFactory = $this->getFormFactory();
        return $formFactory->create($type, $data);
    }
    
    protected function render($templateName, $parameters)
    {
        $twig = \Travelgroep\Reissysteem\Twig\Twig::getTwig();
        
        echo $twig->render($templateName, $parameters);        
    }
    
    protected function redirect($url)
    {
        header( 'Location: '.$url);
        exit;        
    }
    
    protected function getFilterForm()
    {
        $form = $this->createForm(new FlightFilterType());
        
        return $form;
    }
}
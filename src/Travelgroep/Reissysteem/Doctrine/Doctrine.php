<?php
namespace Travelgroep\Reissysteem\Doctrine;

use Doctrine\Common\ClassLoader;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\DBAL\Logging\EchoSQLLogger;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\EventManager;
use Gedmo\Timestampable\TimestampableListener;
use Doctrine\Common\Annotations\AnnotationRegistry; 

class Doctrine
{
  protected $em = null;

  public function __construct($options)
  {
    $user = $options['user'];
    $password = $options['password'];
    $dbname = $options['dbname'];
    
    $entityDir = $options['entity']['path'];
    $entityNs = $options['entity']['ns'];

    $proxyDir = $options['proxy']['path'];
    $proxyNs = $options['proxy']['ns'];

    $proxiesClassLoader = new ClassLoader($proxyDir, $entityDir);
    $proxiesClassLoader->register();

    $annotationDir = $options['annotation']['path'];
    
    // Set up caches
    $config = new Configuration;
    $evm = new EventManager();
    
    $cache = new ArrayCache;
    $config->setMetadataCacheImpl($cache);
    
    $driverImpl = $config->newDefaultAnnotationDriver(null, false);
    $chainDriverImpl = new \Doctrine\ORM\Mapping\Driver\DriverChain();
    
    $chainDriverImpl->addDriver($driverImpl, 'Travelgroep\Bundle\OldSystem\RBSBundle\Entity');
    
    if (isset($options['gedmo']['timestampable']['annotation_path'])) {
        AnnotationRegistry::registerAutoloadNamespace('Gedmo\Mapping\Annotation', $options['gedmo']['timestampable']['annotation_path']);
        $translatableDriverImpl = $config->newDefaultAnnotationDriver();
    
        $chainDriverImpl->addDriver($translatableDriverImpl, 'Gedmo\Timestampable');    

        // timestampable
        $timestampableListener = new TimestampableListener;
        $timestampableListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($timestampableListener);
        
    }
    
    $config->setMetadataDriverImpl($chainDriverImpl);
    $config->setQueryCacheImpl($cache);

    $config->setQueryCacheImpl($cache);

    // Proxy configuration
    $config->setProxyDir($proxyDir);
    $config->setProxyNamespace($proxyNs);

    // Set up logger
//    $logger = new EchoSQLLogger;
//    $config->setSQLLogger($logger);

    $config->setAutoGenerateProxyClasses( TRUE );

    // Database connection information
    $connectionOptions = array(
        'driver' => 'pdo_pgsql',
        'user' =>     $user,
        'password' => $password,
        'host' =>     'localhost',
        'dbname' =>   $dbname
    );

    // Create EntityManager
    $this->em = EntityManager::create($connectionOptions, $config, $evm);
    
    
  }
  
  public function getEntityManager()
  {
      return $this->em;
  }
}
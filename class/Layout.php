<?
class Layout
{
  var $extraJs;
  var $title;
  var $onload;
    
  function Layout($title = null, $extraJs = null, $onload = null)
  {
    $this->title = $title;
    $this->extraJs = $extraJs;
    $this->onload = $onload;
  }
  function header()
  {
    header("Content-Type:text/html;charset=utf-8");
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
    echo '<html>';
      echo '<head>';
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
			
			echo '<link rel="stylesheet" type="text/css" href="'.PATH.'/stylesheets/layout.css" /> ';
            echo '<link rel="stylesheet" type="text/css" href="'.PATH.'/stylesheets/pagerfanta.css" /> ';
            echo '<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /> ';
            echo '<link rel="stylesheet" type="text/css" href="'.PATH.'/stylesheets/datetimepicker.css" /> ';
            echo '<link rel="stylesheet" type="text/css" href="'.PATH.'/stylesheets/flight.css" /> ';
            
            echo '<script type="text/javascript" src="' . PATH . '/backoffice/js/menu.js' . '"></script>'; 
			echo '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>';
            echo '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>';
            echo '<script type="text/javascript" src="' . PATH . 'backoffice/js/jquery-ui-timepicker-addon.js"></script>';
			echo '<script type="text/javascript" src="' . PATH . 'backoffice/js/tiny_mce/tiny_mce.js"></script>';
			
 	echo '
 	<script type="text/javascript">
        tinyMCE.init({
            mode : "textareas",
            plugins : "save,paste,fullscreen",
            theme_advanced_buttons1 : "save,bold,italic,cut,copy,paste,pastetext,bullist,undo,redo,link,unlink,image,code,fullscreen",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_resizing : true,
            width: "398",
            height: "500"        
        });
        </script>
 	';
 	
 		if ($this->extraJs) {
          foreach ($this->extraJs as $js)
          {
            echo '<script type="text/javascript" src="' . PATH . $js . '"></script>';
          }
        }
        
        echo '<title>';
        echo 'backoffice :: ' . $this->title;
        echo '</title>';
      echo '</head>';
      echo '<body onload="'.$this->onload.'">';
			echo '<div class="container">';
  }
  
  function footer()
  {
?>
		</div> <!-- einde container -->
	</body>
</html>
<?	
  }
  

function topMenu( $location = "" )
{
?>
			<div class="topBar">	
				<div class="topMenu">
					<ul>
<?
							// kijk of er een order id is
							if ( isset( $_SESSION['orderid'] ) && $_SESSION['orderid'] != "" )
							{
								$selectedOrderId = "#".$_SESSION[ 'orderid' ];
							}
							
							$links = array(//label 					=> 	url
															'EventType'	  =>	'../eventtype/',
															'Land' 				=>	'../land/',
															'Stad'				=>	'../stad/',
															'Locatie'			=>	'../locatie/',
															'Event'		=>	'../event/',
															'Arrangement'				=>	'../arrangement/',
															'Hotel'				=>	'../hotels/',
                                                            'Flights'  =>  '../flights/',
															'Valuta'				=>	'../valuta/',
															'Vliegveld'				=>	'../vliegveld/',
															'Opta'				=>	'../opta/',
																			);
												
							$i = 1;
							
							
							foreach( $links as $label => $url ){
								if ( $location == $label )
									print "						<li><a class=\"active\" id=\"topLink".$i."\" href=\"$url\" >$label</a></li>\n";
								else
									print "						<li><a id=\"topLink".$i."\" href=\"$url\" >$label</a></li>\n";
								$i ++;
							}
?>
			</ul>
		</div> <!-- einde topMenu -->
	</div> <!-- einde topBar -->
<?
	 
	$talen = getTalen();
	//echo "<pre>";var_dump($talen,true);echo "</pre>";
	echo('<div class="top_bar_site">');
 	echo '&nbsp;taalkeuze: <select onchange="document.location=\'../system/change_taalcode.php?ref=' 
 			. $_SERVER['PHP_SELF'] . '+' . $_SERVER['QUERY_STRING'] . '&taalcode=\'+this.options[selectedIndex].value" class="top_select">';

    $talen[] = ''; // root
	foreach( $talen as $taal ){
 	  	$s = ( $_SESSION['taalcode'] == $taal['taalcode'] ) ? ' selected' : '';
    	echo '<option' . $s . '>' . $taal['taalcode'] . '</option>';
	}
  echo '</select>';
  
  
  echo '&nbsp;<span>';
  echo $_SESSION['taalcode'];
  echo '</span>&nbsp;';
  
  
    $aEventype = getEventTypes();
  
	
 	echo '&nbsp;Eventype: <select onchange="document.location=\'../system/change_eventype.php?ref=' 
 			. $_SERVER['PHP_SELF'] . '+' . $_SERVER['QUERY_STRING'] . '&eventype=\'+this.options[selectedIndex].value" class="top_select">';

	foreach( $aEventype as $aItem ){
 	  	$s = ( $_SESSION['eventypeId'] == $aItem->id ) ? ' selected' : '';
    	echo '<option' . $s . ' value="'.$aItem->id.'">' . $aItem->naam . '</option>';
	}
  echo '</select>&nbsp;';
  echo (NEW_SERVER?"NEW":"OLD"); 
  echo('</div>');
}

function contentHeader()
{
?>
			<div class="content">			
				<h1><?=$this->title ?></h1>
<?
}

function contentFooter() {
?>
			</div><!-- einde content -->
			<div class="contentFooter" >
			</div>
<?
}

}
<?

/*

  Query pronto booking

*/

@$id = isset($_GET['id'])?$_GET['id']:null;

$id = 37363764;
require_once("../../inc/config.php");
require_once("Hotelpronto.php");
$db = new DB();
$myPronto = new Pronto();
$bookings = $myPronto->bookingQuery( $id );

?>
<html>
  <head>
    <title>overzicht huidige boekingen (HotelPronto)</title>
  </head>
  <body>

<h1>Overzicht huidige boekingen (HotelPronto) <?= $id ?></h1>

Aan het annuleren zijn mogelijk kosten verbonden. Deze worden momenteel nog niet berekend!

<table border>
  <tr><td>id</td><td>boekingsdatum</td><td>status</td><td>annuleren</td></tr>
<?

foreach ( $bookings as $booking )
{
  $status = $booking['status'];
  echo '<tr';
  if ( $status == 'cancelled' )
  {
    echo ' style="background-color:#f33"';
  }
  echo '>';
  echo '<td>' . $booking['id'] . '</td>';
  echo '<td>' . $booking['creationdate'] . '</td>';
  echo '<td>' . $booking['status'] . '</td>';
  echo '<td>';

  if ( $status != 'cancelled' )
  {
    echo '<a href="pronto_bookingcancel.php?id=' . $booking['id'] . '">annuleer</a>';
  }
  else
    echo '&nbsp;';
     
  echo '</td>';
  echo '</tr>';
}

?>
</table>

  </body>
</html>
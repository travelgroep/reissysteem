<?php

/*
Org: 1546
User:XML; 
password: xmlvoet
*/

define ( 'PRONTO_TESTMODE', false );

if ( DEBUGMODUS ) {
  define ( 'PRONTO_ORG', 'voetbaltravel' );
  define ( 'PRONTO_USERNAME', 'xmltest' );
  define ( 'PRONTO_PASSWORD', 'voetxml' );
  define ( 'PRONTO_URL', 'http://test.hotelpronto.com/HPTestServices/ASMX/XmlService.asmx' );
	// i think that this test url
  
	
	/*
  define ( 'PRONTO_ORG', 'xmlVoetbaltravel' );
  define ( 'PRONTO_USERNAME', 'xml' );
  define ( 'PRONTO_PASSWORD', 'xml' );
  define ( 'PRONTO_URL', 'http://totalstaytestxml.ivector.co.uk/soap/bookhotelpronto.asmx' );
  */
  
} else {
  
  define ( 'PRONTO_ORG', 'voetbaltravel' );
  define ( 'PRONTO_USERNAME', 'xml' );
  define ( 'PRONTO_PASSWORD', 'xml' );
  //define ( 'PRONTO_URL', 'https://www.hotelpronto.com/HPServices/ASMX/XmlService.asmx' );
  define ( 'PRONTO_URL', 'http://tsxmlclone.ivector.co.uk/soap/bookhotelpronto.asmx' );
  
}

define ( 'IMAGES_URL', 'http://www.hotelpronto.com' );

define ( 'ERR_BOOKING_FAILED', 4004 );

include_once('XMLTransactionHander.class.php'); 

class Pronto {
  var $vendorKey = 'pronto'; // vendor identifier
	var $requestURL = PRONTO_URL; 
	var $org;
	var $username;
	var $password;
	var	$language = 'nl';
	var $currency = 'EUR';
	var $auth;
	var $db;
	protected $logger;
	
	function __construct( )
	{
    $this->db = $GLOBALS[ "rsdb" ];

		$this->org = PRONTO_ORG;
		$this->username = PRONTO_USERNAME;		
		$this->password = PRONTO_PASSWORD;		
    $this->auth = "<Authority>\n";
    $this->auth .= "<Org>" . $this->org . "</Org>\n";
    $this->auth .= "<User>" . $this->username . "</User>\n";
    $this->auth .= "<Password>" . $this->password . "</Password>\n";
    $this->auth .= "<Language>" . $this->language . "</Language>\n";
    
    //$this->auth .= "<TestDebug>true</TestDebug>\n";
    //$this->auth .= "<Timeout>300</Timeout>\n"; // 5 min
    
    $this->auth .= "<Currency>" . $this->currency . "</Currency>\n";
    $this->auth .= "</Authority>\n";
	}

	public function setLogger($logger)
	{
		$this->logger = $logger;
	}
	
	 protected function PaymentDetails(   )  {
	 	
	  $data .=  "<PaymentDetails>\n";
	    $data .=  "<PaymentType>\n";
	    $data .=  "<Code>VISA</Code>\n";
	    $data .=  "<Text></Text>\n";
	    $data .=  "</PaymentType>\n";
	    $data .=  "<CreditCard>\n";
	    
	    $data .=  "<CardNumber>4111111111111111</CardNumber>\n";
	    $data .=  "<StartDate>2006-01-01</StartDate>\n";
	    $data .=  "<EndDate>2010-01-01</EndDate>\n";
	    $data .=  "<IssueNumber>123</IssueNumber>\n";
	    $data .=  "<SecurityCode>123</SecurityCode>\n";
	    
	    $data .=  "</CreditCard>\n";
	    $data .=  "</PaymentDetails>\n";
	 	return $data;
	 }

/*  RW 
 * Geeft een xml AvailabilitySearch terug.
 * $stadProntoId -> alle accomodaties van een stad met de beschikbare kamers.
 * $hotelId -> alle gegevens van de accomdatie met alle beschikbare kamers.
 * 
 * $nights -> aantal nachten
 * $arrivalDate -> $arrivalDate wanneer je aankomt
 * $rooms-> aantal gewenste kamers plus aantal volwassen en kinderen.
 */
   public function makeXMLAvailabilitySearch( $arrivalDate, $nights, $bedden , $hotelId, $stadProntoId )  {
	  	/* || maak een Availability Search xml */
	    $data =  "<AvailabilitySearch>\n";
	    $data .= $this->auth;
	    	
	    if ( $hotelId ) {
		    $data .= "<HotelId>" . $hotelId . "</HotelId>\n";
		    $data .= "<CustomDetailLevel>basic,address,allphotos</CustomDetailLevel>\n";
		    //$data .= "<CustomDetailLevel>basic,address,allphotos,amenities</CustomDetailLevel>\n";
		    $data .= "<DetailLevel>full</DetailLevel>\n";
	    } else {
		    $data .= "<RegionId>" . $stadProntoId . "</RegionId>\n"	;
		    $data .= "<CustomDetailLevel>basic,address,allphotos</CustomDetailLevel>\n";
		    $data .= "<DetailLevel>custom</DetailLevel>\n";
	    	//$data .= "<DetailLevel>custom, full,summary,basic</DetailLevel>\n";
		    $data .= "<MaxHotels>150</MaxHotels>\n"; 
	    	//$data .= "<MaxHotels>0</MaxHotels>\n"; 0 = alle hotels
	    }
	    	$data .= "<HotelStayDetails>\n";
	    	$data .= "<ArrivalDate>" . $arrivalDate . "</ArrivalDate>\n";
	    	$data .= "<Nights>" . $nights . "</Nights>\n";
	    	
   foreach ( $bedden as $bed )    {
      switch ( $bed )      {
        case 'single':        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>SINGLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'twin': {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TWIN</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'double': {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>DOUBLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }

        case 'singledouble':
        case 'triplesingle': {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TRIPLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
      }
    }
			$data .= "</HotelStayDetails>\n";
			$data .= "</AvailabilitySearch>\n";
			
	 	/* || sluit de Availability Search xml */
       //echo "<pre>"; echo print_r( $data ,true); echo "</pre>";
       
       
      return $data; 
   }
	public function hotelAvailableNw($arrivalDate, $duration, $regionId, $rooms)
    {
        $url = HOTEL_FEED_URL;
        $user = HOTEL_FEED_USER;
        $password = HOTEL_FEED_PASSWORD;
		
		$httpHeader = array(
			"Content-Type: application/x-www-form-urlencoded",
			"Content-Encoding: UTF-8"
		);

		$curlOptions = array(
			'options' => array(
				CURLOPT_URL => $url,
				CURLOPT_TIMEOUT => 180,
				CURLOPT_HEADER => 0,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_POST => 1,
				CURLOPT_HTTPHEADER => $httpHeader
			)
		);


		$xmlRequestLoginOptions = array(
			'login' => $user,
			'password' => $password
			//'locale' => 'DE'
		);

		$roomRequests = array();
		
		foreach($rooms as $roomBedType)
		{
			switch ($roomBedType)
			{
				case 'single':
					$roomRequests[]['adults'] = 1;
					break;
				case 'twin':
					$roomRequests[]['adults'] = 2;
					break;
				case 'double':
					$roomRequests[]['adults'] = 2;
					break;
				case 'singledouble':
				case 'triplesingle':
					$roomRequests[]['adults'] = 3;
					break;
					
			}
		}
		
		$searchRequestOptions = array(
			'arrival_date' => $arrivalDate,
			'duration' => $duration,
			'region_id' => $regionId,
			'room_requests' => $roomRequests
		);
		$searchRequestOptions = array_merge($xmlRequestLoginOptions, $searchRequestOptions);
		
		$response = $this->search($curlOptions, $searchRequestOptions);

		return $response;
	}
	
	
public function hotelDetails( $propertyId )
    {
        //echo "\n [  hotelDetails  ]"; 
    	$url = HOTEL_FEED_URL;
        $user = HOTEL_FEED_USER;
        $password = HOTEL_FEED_PASSWORD;
        
        $httpHeader = array(
            "Content-Type: application/x-www-form-urlencoded",
            "Content-Encoding: UTF-8"
        );

        $curlOptions = array(
            'options' => array(
                CURLOPT_URL => $url,
                CURLOPT_TIMEOUT => 180,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HTTPHEADER => $httpHeader
            )
        );

        $xmlRequestLoginOptions = array(
            'login' => $user,
            'password' => $password
            //'locale' => 'DE'
        );
        
        $propertyRequestOptions = array(
            'PropertyID' => $propertyId,
        );
        //var_dump($propertyRequestOptions);
        $propertyRequestOptions = array_merge($xmlRequestLoginOptions, $propertyRequestOptions);
        
        //echo print_r ($propertyRequestOptions,true );
        
        $response = $this->propertyDetails($curlOptions, $propertyRequestOptions);

        return $response;
    }
    protected function propertyDetails($curlOptions, $propertyRequestOptions)
    {
        //echo "\n [  propertyDetails  ]"; 
    	$curlTransferClient = new \UniversalServiceHandler\TransferClient\CurlTransferClient($curlOptions);
        $propertyMapper = new \Travelgroep\TotalStay\Mapper\Xml\PropertyDetailsMapper();
        $xmlRequestMapper = new \Travelgroep\TotalStay\Mapper\Xml\PropertyDetailsRequestMapper();
        $requestMapper = new \Travelgroep\TotalStay\Mapper\XmlRequestMapper($xmlRequestMapper);

        $errorOptions = array(
            'x_path' => 'ReturnStatus/Exception'
        );
        $errorMapper = new \UniversalServiceHandler\Mapper\XPathMapper($errorOptions);

        $statusOptions = array(
            'x_path' => 'ReturnStatus/Success'
        );
        $statusMapper = new \UniversalServiceHandler\Mapper\XPathMapper($statusOptions);

        $propertyDetailsResponseOptions = array(
            'mapper' => $propertyMapper
        );
        $dataMapper = new \Travelgroep\TotalStay\Mapper\PropertyDetailsResponseMapper($propertyDetailsResponseOptions);
        
        $requestProcessorOptions = array(
            'request_mapper' => $requestMapper
        );
        $requestProcessor = new \UniversalServiceHandler\Processor\RequestProcessor($requestProcessorOptions);

        $responseProcessorOptions = array(
            'error_mapper' => $errorMapper,
            'data_mapper' => $dataMapper,
            'status_mapper' => $statusMapper
        );

        $responseProcessor = new \UniversalServiceHandler\Processor\ResponseProcessor($responseProcessorOptions);

        $serviceOptions = array(
            'transfer_client' => $curlTransferClient,
            'request_processor' => $requestProcessor,
            'response_processor' => $responseProcessor
        );

        $totalStayService = new \Travelgroep\TotalStay\TotalStayService($serviceOptions);
        
        //var_dump($propertyRequestOptions);
        
        $response = $totalStayService->callService($propertyRequestOptions);
        //var_dump($response);
        return $response;
    }
	
	protected function search($curlOptions, $searchRequestOptions)
	{
		$curlTransferClient = new \UniversalServiceHandler\TransferClient\CurlTransferClient($curlOptions);
		$propertyMapper = new \Travelgroep\TotalStay\Mapper\Xml\PropertyMapper();
		$xmlRequestMapper = new \Travelgroep\TotalStay\Mapper\Xml\SearchRequestMapper();
		$requestMapper = new \Travelgroep\TotalStay\Mapper\XmlRequestMapper($xmlRequestMapper);

		$errorOptions = array(
			'x_path' => 'ReturnStatus/Exception'
		);

		$errorMapper = new \UniversalServiceHandler\Mapper\XPathMapper($errorOptions);

		$statusOptions = array(
			'x_path' => 'ReturnStatus/Success'
		);

		$statusMapper = new \UniversalServiceHandler\Mapper\XPathMapper($statusOptions);

		$searchResponseOptions = array(
			'mapper' => $propertyMapper
		);

		$dataMapper = new \Travelgroep\TotalStay\Mapper\SearchResponseMapper($searchResponseOptions);

		$requestProcessorOptions = array(
			'request_mapper' => $requestMapper
		);

		$requestProcessor = new \UniversalServiceHandler\Processor\RequestProcessor($requestProcessorOptions);

		$responseProcessorOptions = array(
			'error_mapper' => $errorMapper,
			'data_mapper' => $dataMapper,
			'status_mapper' => $statusMapper
		);

		$responseProcessor = new \UniversalServiceHandler\Processor\ResponseProcessor($responseProcessorOptions);

		$serviceOptions = array(
			'transfer_client' => $curlTransferClient,
			'request_processor' => $requestProcessor,
			'response_processor' => $responseProcessor
		);

		$totalStayService = new \Travelgroep\TotalStay\TotalStayService($serviceOptions);

		$response = $totalStayService->callService($searchRequestOptions);

		return $response;
	}
   
	/**
   * public static function gets passenger count array from passengers object
   *
   * @param passengers object
   * @return array with passenger counts
   */
  public static function getPassengerCount( $bedden )  {
    $no_adults = 0;
    $no_children = 0;
    $no_infants = 0;
    if ( is_array( $bedden) ) {
		  foreach ( $bedden as $bed ){
		      switch ( $bed ) {
		        case 'single':	{ $no_adults++;  break; }
		        case 'twin': 		{ $no_adults+=2; break; }
		        case 'double': 	{ $no_adults+=2; break; }
		        case 'singledouble':
		        case 'triplesingle': { $no_adults+=3; break; }
		      }
	    }
	    return array( $no_adults, $no_children, $no_infants );
    } else {
      return ( false );
    }
  }
  
 /**
   *  Maakt een pakket van available rooms
   */
  public function responseAvailableRooms ( $xpath , $resultArr, $bedden  )  {
		
  	    
    $results 			= $resultArr;
    $countPassenger 	= $this->getPassengerCount($bedden ) ;
	$passenger_count 	= $countPassenger[0] + $countPassenger[1] ;
	$x 					= 0;
      
	$resultArray			= array();
	foreach ( $results as $result ) {
	       
		$rooms 					= $xpath->query( 'Room', $result );
        $totalPrice 		= 0;
        $roomPrice 			= 0;
        $y 					= 0;
        $match 				= 0;
      	$roomCodeDubble = array();
		
        foreach ( $rooms as $room )   {
      		$noDubbleRoom 	= true;
        	$roomCode 			= $xpath->query( 'RoomType/Code', $room )->item(0)->textContent;
	        	
        	if ( $roomCodeDubble [$roomCode] ) {
        		$roomCodeDubble [$roomCode] = $roomCodeDubble [$roomCode] +1;
        		$noDubbleRoom = false;
        	} else {
        		$roomCodeDubble [$roomCode] = 1;
        	}
        	$roomType 		= $xpath->query( 'RoomType/Text', $room )->item(0)->textContent;
	        $roomPrice 		= $xpath->query( 'SellingPrice/Amount', $room )->item(0)->textContent ;
	        $totalPrice 	+= $roomPrice;
	          
			if ( $noDubbleRoom ) {
						
		          $adults = $xpath->query( 'Guests/Adult', $room );
		          foreach ( $adults as $adult ) {
			          	$resultArray[$x]['rooms'][$y]['noAdults']++;
		          }
		          
		          $children = $xpath->query( 'Guests/Child', $room );
		          foreach ( $children as $child )          {
			          	$resultArray[$x]['rooms'][$y]['noChildren']++;
		          }
						
		          // plaat het in de resultArray.
		        $resultArray[$x]['rooms'][$y]['price'] 	= $roomPrice; 
		        $resultArray[$x]['rooms'][$y]['type'] 	= $roomType;          
		        $resultArray[$x]['rooms'][$y]['code'] 	= $roomCode;          
		        $mealType 			= $xpath->query( 'MealType/Code', $room )->item(0)->textContent;
	         	$mealTypeDesc 		= $xpath->query( 'MealType/Text', $room )->item(0)->textContent;
		        if ( $mealType ){
		            $resultArray[$x]['rooms'][$y]['mealtype'] 		= $mealType; 
		            $resultArray[$x]['rooms'][$y]['mealtypeDesc'] 	= $mealTypeDesc; 
		        }
		        $y++;
			}
		}
        
		$buyingTotalPrice   = $totalPrice;
        $quoteId 			= $xpath->query( 'QuoteId', $result )->item(0)->textContent;
        $totalPrice 		= $this->getTotalPriceWithMargin ( $totalPrice); 
        $price_p_p 			= $this->getPricePerPersoon ( $passenger_count , $totalPrice );
        $totalPrice 		= $passenger_count * $price_p_p;
        
        $resultArray[$x]['dubbleRoom'] 		= $roomCodeDubble;     
        $resultArray[$x]['price_p_p'] 		= $price_p_p;     
        $resultArray[$x]['passenger_count'] = $passenger_count;     
        $resultArray[$x]['totalPrice'] 		= $totalPrice;     
        $resultArray[$x]['buyingTotalPrice']= $buyingTotalPrice;     
        $resultArray[$x]['quoteId'] 		= $quoteId;
        $x++;     
		}
		return ( $resultArray );
  
  }
  
  public function getTotalPriceWithMargin ( $totalPrice) {
  	$totalPrice += ( $totalPrice / 100 ) * HOTEL_MARGIN;
  	$totalPrice = (( ceil( $totalPrice / 10 ) * 10 ) -1 );
  	return $totalPrice;
  }
  public function getPricePerPersoon ( $totaalAdults ,$price ) {
  	$price = ( $price / $totaalAdults ) ;
  	$price = (( ceil( $price / 10 ) * 10 ) -1 );
  	return $price;
  }
  
   /** end rw */

  public function setErrorOutputMethod( $errorOutputMethod ) { $this->errorOutputMethod = $errorOutputMethod; }

	function sendRequest( $data )	{
		$requestData  = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
    $requestData .= $data;

/*
    echo '<pre>'; 
    echo htmlspecialchars($requestData);
    echo '</pre>';
*/
		/*********************************************/
		/*         Execute Search Transaction        */
		/*********************************************/
		
//		print '<pre>'.htmlentities( $requestData ).'</pre>';
		
		$XMLTransactionHander = new XMLTransactionHander;
		return $XMLTransactionHander->executeRequest( $this->requestURL, $requestData );
		
		
	}
	

  function checkForErrors( &$responseDoc )
  {
    $responseElement = $responseDoc->documentElement;
    $xpath = new DOMXPath( $responseDoc );
    $errorElements = $xpath->query( "//Error", $responseElement );
    $errors = array();

    foreach ($errorElements as $error)
    {
      $errorText = $xpath->query( 'Code', $error );
      $code = $errorText->item(0)->textContent;

      $errorText = $xpath->query( 'Description', $error );
      $description = $errorText->item(0)->textContent;

      $errorText = $xpath->query( 'Details', $error );
      $details = $errorText->item(0)->textContent;
      
      $errors[] = array("Code"=>$code, "Description"=>$description, "Details"=>$details );
    }
    return $errors;
  }
  
  private function outputErrors( $errors, $errorOutputMethod )
  {
    return;
    switch ( $errorOutputMethod )
    {
      case 'HTML':

        foreach ( $errors as $error )
        {
          echo "<b>FOUT</b><br/>";
          echo "code: " . $error['Code'] . "<br />";
          echo "beschrijving: " . $error['Description'] . "<br />";
        }
        break;

      case 'XML':
        foreach ( $errors as $error )
        {
          echo "<error>";
          echo "<code>" . $error['Code'] . "</code>";
          echo "<description>" . $error['Description'] . "</description>";
          echo "</error>";
        }
        break;

      default:
        foreach ( $errors as $error )
        {
          echo "FOUT\n";
          echo "code: " . $error['Code'] . "\n";
          echo "beschrijving: " . $error['Description'] . "\n";
        }

    }
  }

  public function regionDownload( $regionId, $regionName, $depthChildren )
  {
    $data =  "<RegionSearch>\n";
    $data .= $this->auth;
    $data .= "<DepthChildren>" . $depthChildren . "</DepthChildren>\n";
    $data .= "</RegionSearch>\n";
    
    
    $outdata = print_r ($data, true);
    mail( "ronald@travelgroep.nl", "971::REsponse regionDownload",  $outdata );
    
    
    $responseDoc = $this->sendRequest( $data );
	    
    $out = print_r ($responseDoc->saveXML(), true);
    mail( "ronald@travelgroep.nl", "971::REsponse regionDownload",  $out );
     
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
 
      $regionElements = $xpath->query( '//Region', $responseElement );

      foreach ($regionElements as $region)
      {
        //$pushElements = array();
        $item = $xpath->query( 'Name', $region );
        $pushElements['name'] = $item->item(0)->textContent;
  
        $item = $xpath->query( 'Id', $region );
        $pushElements['id'] = $item->item(0)->textContent;
  
        $item = $xpath->query( 'Type', $region );
        $pushElements['type'] = $item->item(0)->textContent;
        $this->db->replace( 'pronto_region', $pushElements );
      }
    }
  }	

/*
  private function recurseRegionTree( $level, $tree, $xpath )
  {
    $elements = $xpath->query( $level, $tree );

    foreach ($elements as $region)
    {
      //$pushElements = array();
      $item = $xpath->query( 'Name', $region );
      $pushElements['name'] = $item->item(0)->textContent;

      $item = $xpath->query( 'Id', $region );
      $pushElements['id'] = $item->item(0)->textContent;

      $item = $xpath->query( 'Type', $region );
      $pushElements['type'] = $item->item(0)->textContent;

      //$this->db->replace( 'pronto_region', $pushElements );
      $this->recurseRegionTree ( 'Children/Region/', $region, $xpath ) ;
    }
  }
*/

  public function getHotelData( $hotelId )
  {
    $data =  "<HotelSearch>\n";
    $data .= $this->auth;
    $data .= "<HotelId>" . $hotelId . "</HotelId>\n";
    $data .= "<DetailLevel>full</DetailLevel>\n";
    $data .= "<CustomDetailLevel>full</CustomDetailLevel>\n";
//    $data .= "<CustomDetailLevel>basic,address,allphotos</CustomDetailLevel>\n";

    $data .= "</HotelSearch>\n";


//    $r = $this->sendRequest( $data );

    //$out = print_r ($data, true);
   //echo "<pre>"; print_r( $out ); echo "</pre>";

    $responseDoc = $this->sendRequest( $data );
    
    // $out = print_r ($data, true);
   //echo "<pre>"; print_r( $responseDoc->saveXML() ); echo "</pre>";
    
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//HotelElement', $responseElement );


      foreach ( $elements as $element )
      {
        $item = $xpath->query( 'Id', $element );
        $hotelData['id'] = $item->item(0)->textContent; 
        
        $item = $xpath->query( 'Name', $element );
        $hotelData['name'] = $item->item(0)->textContent;   

        $item = $xpath->query( 'Stars', $element );
        $hotelData['rating'] = $item->item(0)->textContent; 

        $item = $xpath->query( 'Address/Address1', $element );
        $hotelData['address'] = $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Address2', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\n" . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Address3', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\n" . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/City', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\n" . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Country', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\n" . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Tel', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\nTel. " . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Fax', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\nFax. " . $item->item(0)->textContent; 

        $descriptions = $xpath->query( 'Description', $element );
        foreach ( $descriptions as $description )
        {
          $item = $xpath->query( 'Type', $description );
          $type = $item->item(0)->textContent;
          $content = $xpath->query( 'Text', $description );
          $hotelData['description_' . $type] = $content->item(0)->textContent;  
        }        

        $photos = $xpath->query( 'Photo', $element );
        foreach ( $photos as $photo )
        {
          $item = $xpath->query( 'Url', $photo );
          $hotelData['photo'][] = $item->item(0)->textContent; 
          $item = $xpath->query( 'ThumbnailUrl', $photo );
          $hotelData['thumbnail'][] = $item->item(0)->textContent; 
        }
      }
      return ( $hotelData );
    }
  }
  
  public function getHotelDataByCityId( $cityId, $order, $ascdesc, $limit )
  {
    $q = $this->db->query( "SELECT id, name, rating, address, photo, thumbnail FROM pronto_hotel WHERE city_id={$cityId} ORDER BY {$order} {$ascdesc} LIMIT {$limit}" );

  	for( $i=0; $i<pg_num_rows( $q ); $i++ )
  	{
  		$array = pg_fetch_array( $q, $i, PGSQL_ASSOC );
  		
  		$thumbnailArray = explode( '|', $array['thumbnail'] );
  		$array['thumbnail'] = $thumbnailArray;

  		$photoArray = explode( '|', $array['photo'] );
  		$array['photo'] = $photoArray;

			$result[ $i ]= $array;
  	}
  	return ( $result );
  }
  
  public function hotelDownload( $regionId )
  {
    $data =  "<HotelSearch>\n";
    $data .= $this->auth;
    $data .= "<RegionId>" . $regionId . "</RegionId>\n";

    $data .= '
      <HotelSearchCriteria>
        <MinStars>2</MinStars>
      </HotelSearchCriteria>
    ';

    //$data .= "<CustomDetailLevel>basic</CustomDetailLevel>\n";   
    $data .= "</HotelSearch>\n";
//    $r = $this->sendRequest( $data );

//    $outData = print_r ($data, true);
//    mail( "ronald@travelgroep.nl", "651::data hotelDownload",  $outData );
//    
    $responseDoc = $this->sendRequest( $data );
    
//        $out = print_r ($responseDoc->saveXML(), true);
//    mail( "ronald@travelgroep.nl", "651::REsponse hotelDownload",  $out );
    
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//HotelElement', $responseElement );

     
      $arrList = array();
      foreach ($elements as $element)
      {      
        $item = $xpath->query( 'Name', $element );
        //echo $pushElements['name'] = $item->item(0)->textContent;
        //echo "\n";

        $item = $xpath->query( 'Id', $element );
        $pushElements['id'] = $item->item(0)->textContent;
				$hotelId = $pushElements['id'];        

        $item = $xpath->query( 'Region/Id', $element );
        $pushElements['region_id'] = $item->item(0)->textContent;

        $item = $xpath->query( 'Stars', $element );
        $pushElements['rating'] = $item->item(0)->textContent;

        $pushElements['city_id'] = $regionId;

        $this->db->replace( 'pronto_hotel', $pushElements );
        
        $arrList[]=$pushElements;
        
        
		    $hotelData = $this->getHotelData( $hotelId );
				
				$photo = "";
				$thumbnail = "";
			if (empty ($hotelData)){
					
	            foreach ( $hotelData['thumbnail'] as $tn )
	            {
			               $thumbnail .= $tn . '|';
	            }
			
	            foreach ( $hotelData['photo'] as $pic )
	            {
	                $photo .= $pic . '|';
	            }
			}
            $update = array ( 'rating'=>$hotelData['rating'],
				'address'=>(string)$hotelData['address'],
				'thumbnail'=>(string)$thumbnail,
				'photo'=>(string)$photo );
            
	      // update table with extra info
	      $this->db->update( 'pronto_hotel', $update, array( 'id'=>$hotelId ) );
      }
    }
    
//    $out1 = print_r ($arrList, true);
//    mail( "ronald@travelgroep.nl", "651::list hotelDownload",  $out1 );
    
  }
  
  public function hotelAvailable( $hotelId, $arrivalDate, $nights, $bedden )
  {
    $data =  "<AvailabilitySearch>\n";
    $data .= $this->auth;
    $data .= "<HotelId>" . $hotelId . "</HotelId>\n";
//    $data .= "<RegionId>256</RegionId>\n";
    //$data .= "<DetailLevel>custom</DetailLevel>\n";
   // $data .= "<CustomDetailLevel>basic,address,allphotos,amenities</CustomDetailLevel>\n";
    $data .= "<HotelStayDetails>\n";
    $data .= "<ArrivalDate>" . $arrivalDate . "</ArrivalDate>\n";
    $data .= "<Nights>" . $nights . "</Nights>\n";

    foreach ( $bedden as $bed )
    {
      switch ( $bed )
      {
        case 'single':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>SINGLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'twin':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TWIN</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'double':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>DOUBLE</BasicRoomType>\n";
           $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }

        case 'singledouble':
        case 'triplesingle':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TRIPLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
      }
    }
    
    $data .= "</HotelStayDetails>\n";
    $data .= "</AvailabilitySearch>\n";

    //echo $data;
    
//    $r = $this->sendRequest( $data );
    
    $responseDoc = $this->sendRequest( $data );
    
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        return;
      }

      $responseElement = $responseDoc->documentElement;
      
      //echo "<pre> responseDoc = ";print_r ($responseDoc);echo "</pre>";	
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//Result', $responseElement );
	
      $x = 0;
      $prev = 0;
      $found = false;
      
      foreach ( $elements as $element )
      {
        $beddenTest = $bedden;
        
        $totalPrice = 0;
        $item = $xpath->query( 'QuoteId', $element );
        $quoteId = $item->item(0)->textContent;

        $rooms = $xpath->query( 'Room', $element );
        $roomPrice = 0;
        
        $y = 0;
        $match = 0;
        
        // build result
        foreach ( $rooms as $room )
        {
          $item = $xpath->query( 'SellingPrice/Amount', $room );
          $roomPrice = $item->item(0)->textContent;
          $resultArray[$x]['rooms'][$y]['price'] = $roomPrice;
          $totalPrice += $roomPrice;

          $adults = $xpath->query( 'Guests/Adult', $room );
          foreach ( $adults as $adult )
          {
            $resultArray[$x]['rooms'][$y]['noAdults']++;
          }

          $item = $xpath->query( 'Guests/Adult/SellingPrice/Amount', $room );
          // determine adult price, even if it's not stated by the XML feed
          if ( !$adultPrice = $item->item(0)->textContent )
          {
            $adultPrice = $roomPrice / $resultArray[$x]['rooms'][$y]['noAdults'];
          }
          $adultPrice /= $nights;
          $resultArray[$x]['rooms'][$y]['adultPrice'] = $adultPrice;

          $item = $xpath->query( 'RoomType/Text', $room );
          $roomType = $item->item(0)->textContent;

          $item = $xpath->query( 'MealType/Code', $room );
          $mealType = $item->item(0)->textContent;

          $item = $xpath->query( 'MealType/Text', $room );
          $mealTypeDesc = $item->item(0)->textContent;

          $z = 0 ;

        // count no matches for this result
          foreach ( $beddenTest as $bed )
          {          
            if ( $bed == strtolower( $roomType ) )
            {
              unset ( $beddenTest[$z] );
              $match += 1;
            }
            $z++;
          }
          
          $resultArray[$x]['rooms'][$y]['type'] = $roomType;          
          if ( $mealType )
          {
            $resultArray[$x]['rooms'][$y]['mealtype'] = $mealType; 
            $resultArray[$x]['rooms'][$y]['mealtypeDesc'] = $mealTypeDesc; 
          }
          
          $y++;
        }

        // determine and set best match
        if ( $match > $prev && !$found )
        {
          $resultArray['bestMatch'] = $x;  
        }
        $prev = $match;

        // best match already found?
        if ( count( $bedden ) == $match )
        {
          $found = true;
        }

        $resultArray[$x]['totalPrice'] = $totalPrice;     
        $resultArray[$x]['quoteId'] = $quoteId;
        $resultArray[$x]['roomPrice'] = $roomPrice;
        $x++;     
      }

      return ( $resultArray );

    }
  }

  public function hotelsAvailableByRegionId( $regionId, $arrivalDate, $nights, $bedden ) {
    $data =  "<AvailabilitySearch>\n";
    $data .= $this->auth;
    $data .= "<RegionId>" . $regionId . "</RegionId>\n";
    //$data .= "<DetailLevel>custom</DetailLevel>\n";
   // $data .= "<CustomDetailLevel>basic,address,allphotos,amenities</CustomDetailLevel>\n";
    //$data .= "<MaxHotels>10</MaxHotels>";
    $data .= '
      <HotelSearchCriteria>
        <MinStars>2</MinStars>
      </HotelSearchCriteria>
    ';
    $data .= "<HotelStayDetails>\n";
    $data .= "<ArrivalDate>" . $arrivalDate . "</ArrivalDate>\n";
    $data .= "<Nights>" . $nights . "</Nights>\n";
    
    foreach ( $bedden as $bed )
    {
      switch ( $bed )
      {
        case 'single':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>SINGLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'twin':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TWIN</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'double':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>DOUBLE</BasicRoomType>\n";
           $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }

        case 'singledouble':
        case 'triplesingle':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TRIPLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
      }
    }
    
    $data .= "</HotelStayDetails>\n";
    $data .= "</AvailabilitySearch>\n";

     //$out = print_r ($data, true);
	//mail( "ronald@travelgroep.nl", "request AvailabilitySearch",  $out );
    
     
     
     
	  $responseDoc = $this->sendRequest( $data );
    
	  //$out = print_r ($responseDoc->saveXML() , true);
	  //echo "<pre>"; print_r( $out ); echo "</pre>";
	  //mail( "ronald@travelgroep.nl", "request AvailabilitySearch",  " stop oke" );
	  //die ("stop");   
	 //echo "<pre>";  
    //echo " hotelsAvailableByRegionId :: = " .  $responseDoc->saveXML() . "\n"; // test
    //echo "</pre>";
    //mail( "ronald@travelgroep.nl", "Log 2 hotelsAvailableByRegionId AvailabilitySearch",  $responseDoc->saveXML() );
    
      //$out = print_r ($responseDoc->saveXML(), true);
     //mail( "ronald@travelgroep.nl", "971::REsponse AvailabilitySearch",  $out );
    
	if( $responseDoc != NULL ) {
      if ( $errors = $this->checkForErrors( $responseDoc ) ) {
        $this->outputErrors( $errors, "HTML" );
        return;
      }
      
      //$out = print_r ($data, true);
      //mail( "ronald@travelgroep.nl", "request AvailabilitySearch",  $out );
      
      $responseElement 	= $responseDoc->documentElement;
      $xpath 			= new DOMXPath( $responseDoc );
      $elements 		= $xpath->query( '//HotelAvailability', $responseElement );
      
      //$out = print_r ($responseDoc->saveXML(), true);
      //mail( "ronald@travelgroep.nl", "REsponse AvailabilitySearch",  $out );
      
		//echo "<pre> hotelsAvailableByRegionId::responseDoc = ";print_r ($responseElement);echo "</pre>";
	//echo "<pre>";print_r ( $responseElement );echo "</pre>"; test	
      $i = 0;
      foreach ( $elements as $availibityElement ) {
      	$item = $xpath->query( 'Hotel/Name', $availibityElement );
        $name = $item->item(0)->textContent;
        
        $item = $xpath->query( 'Hotel/Id', $availibityElement );
        $hotelId = $item->item(0)->textContent;
        
        $hotelArray[$hotelId]['name'] = $name;
        $hotelArray[$hotelId]['pronto_id'] = $hotelId;        

// hier de kamerinfo parsen!  
		$output = "";     
        $x = 0;
        $prev = 0;
        $found = false;
        
        $resultElements = $xpath->query( 'Result', $availibityElement );
        foreach ( $resultElements as $element )
        {
          $beddenTest = $bedden;
          
          $totalPrice 	= 0;
          $volwassen 	= 0;
          $item = $xpath->query( 'QuoteId', $element );
          $quoteId = $item->item(0)->textContent;
  
          $rooms = $xpath->query( 'Room', $element );
          $roomPrice = 0;
          
          $y = 0;
          $match = 0;
          
          // build result
          foreach ( $rooms as $room )
          {
            $item = $xpath->query( 'SellingPrice/Amount', $room );
            $roomPrice = $item->item(0)->textContent;
            $resultArray[$x]['rooms'][$y]['price'] = $roomPrice;
            $totalPrice += $roomPrice;
  
            $adults = $xpath->query( 'Guests/Adult', $room );
            
            if (isset ($adults) && count($adults) > 0){
	            $resultArray[$x]['rooms'][$y]['noAdults'] = count($adults)+1;
            } 
            $volwassen +=  $resultArray[$x]['rooms'][$y]['noAdults'];
            
            $item = $xpath->query( 'Guests/Adult/SellingPrice/Amount', $room );
            // determine adult price, even if it's not stated by the XML feed
            if ( !$adultPrice = $item->item(0)->textContent )
            {
              $adultPrice = $roomPrice / $resultArray[$x]['rooms'][$y]['noAdults'];
            }
            $adultPrice /= $nights;
            $resultArray[$x]['rooms'][$y]['adultPrice'] = $adultPrice;
  
            $item = $xpath->query( 'RoomType/Text', $room );
            $roomType = $item->item(0)->textContent;

            // language conversions
            if ( $this->language=='nl' )
            {
              if ( $roomType == '2pers kamer (2 aparte bedden)' )
                $roomType = 'Twin';
                
              if ( $roomType == 'Dubbel (1 bed voor 2pers)' )
                $roomType = 'Double';

              if ( $roomType == 'Een-persoons' )
                $roomType = 'Single';
            }

            $item = $xpath->query( 'MealType/Code', $room );
            $mealType = @$item->item(0)->textContent;
  
            $item = $xpath->query( 'MealType/Text', $room );
            $mealTypeDesc = @$item->item(0)->textContent;
            
            $z = 0 ;
  
          // count no matches for this result
            foreach ( $beddenTest as $bed )
            {          
              if ( $bed == strtolower( $roomType ) )
              {
                unset ( $beddenTest[$z] );
                $match += 1;
              }
              $z++;
            }
            
            $resultArray[$x]['rooms'][$y]['type'] = $roomType;          
            if ( $mealType ) {
              $resultArray[$x]['rooms'][$y]['mealtype'] = $mealType; 
              $resultArray[$x]['rooms'][$y]['mealtypeDesc'] = $mealTypeDesc; 
            }
            
            $y++;
          }
  
          // determine and set best match
          if ( $match > $prev && !$found ) { $resultArray['bestMatch'] = $x; }
          $prev = $match;
  
          // best match already found?
          if ( count( $bedden ) == $match ) { $found = true; }
            
          $buyingTotalPrice = $totalPrice;
          $totalPrice 		= $this->getTotalPriceWithMargin ( $totalPrice); 
          $resultArray[$x]['buyingTotalPrice']  = $buyingTotalPrice; 
          $resultArray[$x]['totalPrice'] 		= $totalPrice  ;  
          $resultArray[$x]['quoteId'] 			= $quoteId;
          $resultArray[$x]['volwassen'] 		= $volwassen;
          $resultArray[$x]['gemiddeldeAdult'] 	= $totalPrice / ($nights * $volwassen); 
          $resultArray[$x]['roomPrice'] 		= $roomPrice; 
          
		   //$output .= "<br/>- - - - - -  - -  - - ". $x ." ROOM:  " . $quoteId . " totalPrice = " . $totalPrice 
		  	 		//. " gemiddeldeAdult = " . $resultArray[$x]['gemiddeldeAdult'] . " nights  = " . $nights   ;		               
          $x++;
        }// alle kamers
       //echo "<br/>PRONTO -- " .  $i++ . " name = " . $name . " hotelId = " . $hotelId . " -- " . $x;  
      //echo $output;
        
        $hotelArray[$hotelId]['priceInfo'] = $resultArray;
        unset ( $resultArray );            
        
      }
      	
      //$out = print_r ($hotelArray, true);
      //mail ( "ronald@travelgroep.nl", "hotelArray|" , $out);
      return ( $hotelArray );

    }
  }
  
  // creates a booking. Returns the booking ID  
  public function bookingCreateByHotelInfo ( $orderno, $hotelInfo, $persons ) {
    $return = array();
    $quoteId = $hotelInfo['quoteId'];
    $data =  "<BookingCreate>\n";
    $data .= $this->auth;
    $data .=  "<QuoteId>" . $quoteId . "</QuoteId>\n";
    $data .=  "<AgentReference>" . $orderno . "</AgentReference>\n";
    $data .=  "<HotelStayDetails>\n";

    $index = 0;
    
    foreach ( $hotelInfo['rooms'] as $room )
    {
      $data .= "<Room>\n";
      $data .= "<Guests>\n";            
      for ( $x=0; $x<$room['noAdults']; $x++ )
      {
        $data .= "<Adult>\n";
        $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
        $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
        $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
        $data .= "</Adult>\n";
        $index++;
      }
      $data .= "</Guests>\n";
      $data .= "</Room>\n";
    }
    
    $data .=  "</HotelStayDetails>\n";
    $data .=  "<PaymentDetails>\n";
    $data .=  "<PaymentType>\n";
    $data .=  "<Code>VISA</Code>\n";
    $data .=  "<Text></Text>\n";
    $data .=  "</PaymentType>\n";
    $data .=  "<CreditCard>\n";
    $data .=  "<CardNumber>4111111111111111</CardNumber>\n";
    $data .=  "<StartDate>2006-01-01</StartDate>\n";
    $data .=  "<EndDate>2010-01-01</EndDate>\n";
    $data .=  "<IssueNumber>123</IssueNumber>\n";
    $data .=  "<SecurityCode>123</SecurityCode>\n";
    $data .=  "</CreditCard>\n";
    $data .=  "</PaymentDetails>\n";

/*
    <PaymentDetails>       (affiliate users only)
        <PaymentType>
        <CreditCard>
            <CardNumber>
            <StartDate>    (optional)
            <EndDate>
            <IssueNumber>  (optional)
            <SecurityCode> (optional)
        </CreditCard>
    </PaymentDetails>
*/

    $data .=  "<CommitLevel>confirm</CommitLevel>\n";
    $data .=  "</BookingCreate>\n";

    //$responseDoc = $this->sendRequest( $data );

		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $x = 0;
        // parse errors here
        foreach ( $errors as $error )
        {
          $return['errors'][$x]['code'] = $error['Code'];
          $return['errors'][$x]['description'] = $error['Description'];
          $x++;
        }
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '/BookingCreateResult/Booking/Id', $responseElement );

      $id = $elements->item(0)->textContent;

      $return['id'] = $id;
      
      return ( $return ) ;
    }
  }

  
     
/*    
    $data .=  "<PaymentDetails>\n";
    $data .=  "<PaymentType>\n";
    $data .=  "<Code>VISA</Code>\n";
    $data .=  "<Text></Text>\n";
    $data .=  "</PaymentType>\n";
    $data .=  "<CreditCard>\n";
    $data .=  "<CardNumber>4111111111111111</CardNumber>\n";
    $data .=  "<StartDate>2006-01-01</StartDate>\n";
    $data .=  "<EndDate>2010-01-01</EndDate>\n";
    $data .=  "<IssueNumber>123</IssueNumber>\n";
    $data .=  "<SecurityCode>123</SecurityCode>\n";
    $data .=  "</CreditCard>\n";
    $data .=  "</PaymentDetails>\n";
*/

/*
    <PaymentDetails>       (affiliate users only)
        <PaymentType>
        <CreditCard>
            <CardNumber>
            <StartDate>    (optional)
            <EndDate>
            <IssueNumber>  (optional)
            <SecurityCode> (optional)
        </CreditCard>
    </PaymentDetails>
    
    
<Booking>
    <Id>
    <CreationDate>
    <Customer>
    <Affiliate>             (optional; see notes below)
    <AgentReference>        (optional)
    <HotelBooking>          (1 or more)
        <Id>
        <HotelId>
        <HotelName>
        <Status>
        <SupplierInfo>      (broker users only)
            <Supplier>
            <HotelCode>
            <BookingRef>
        </SupplierInfo>
*/
  public function bookingCreate ( $orderno, $quoteId, $bedden, $persons )
  {
    
    $data =  "<BookingCreate>\n";
    $data .= $this->auth;
    $data .=  "<QuoteId>" . $quoteId . "</QuoteId>\n";
    $data .=  "<AgentReference>" . $orderno . "</AgentReference>\n";
    $data .=  "<HotelStayDetails>\n";

    $index = 0;
    
    foreach ( $bedden as $bed )
    {
      switch ( $bed )
      {
        case 'single':
        {
          $data .= "<Room>\n";
          $data .= "<Guests>\n";

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;
          
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'twin':
        case 'double':
        {
          $data .= "<Room>\n";
          $data .= "<Guests>\n";

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }

        case 'singledouble':
        case 'triplesingle':
        {
          $data .= "<Room>\n";
          $data .= "<Guests>\n";

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
      }
    }

    $data .=  "</HotelStayDetails>\n";
    
    $data .=  "<CommitLevel>confirm</CommitLevel>\n";
    $data .=  "</BookingCreate>\n";

    mail( "ronald@travelgroep.nl", "Log ($orderno) Hotelpronto (BC) DATA: ",  $data );
    $responseDoc = $this->sendRequest( $data );
    mail( "ronald@travelgroep.nl", "Log 2 ($orderno) Hotelpronto (BC)Response: ",  $responseDoc->saveXML() );

    $arr = array();
	if( $responseDoc != NULL ){  
     
		if ( $errors = $this->checkForErrors( $responseDoc ) ) {
	        	$x = 0;
		        // parse errors here
		        foreach ( $errors as $error ) {
		          $arr['errors'][$x]['code'] = $error['Code'];
		          $arr['errors'][$x]['description'] = $error['Description'];
		          $error_msg .= $error['Description'] . "(" . $error['Code'] . ")\n";
		          $x++;
		        }
		        
		        /*
				1001	The operation completed but there was an error generating the response. Resubmitting a request in this case may result in a duplicate booking
				1002	The operation completed with some non-fatal errors. Please contact the site operator to confirm the status of the booking
				2001	The request was in an invalid format
				2002	The request was missing a required parameter / element
				2003	The request contained a parameter / element with an illegal value
				2004	The operation requested is not available to this user type
				3001	The feature requested is not supported on this deployment
				4001	General system error
				4002	General user error. The error message should give sufficient details of the problem to enable it to be corrected
				4003	Could not access requested booking. You may not have permission to view this booking ID
				4004	Operation failed
				4005	Operation previously performed. Most likely you have attempted to submit the same request twice
				4006	The details you have provided when attempting to book do not match those on the quote. 
						For example � the pax counts differ. The booking cannot be made in this situation
				4007	This means the operator has disabled the XML interface, and generally indicates the site is undergoing maintenance or is temporarily offline. Please check with the site operator for further information
				4008	Timeout: the request exceeded the time-limit for generating a response. If the request was a search, you might like to retry it with more restrictive parameters.
				5001	The credit card type is unrecognised or not supported on this deployment
				5002	The credit card number is invalid
				5003	Credit card authorisation failed
				5004	The date on the credit card is invalid
				6002	You attempted to make a booking with duplicate extras
				6004	The extra specified for the booking is invalid
				6006	The upgrade specified for the booking is invalid
				6007	The price for the upgrade could not be calculated
				
				7001	The price of a quote changed when booking. See section 4.5.1.3
				7002	Could not access requested quote. Either you do not have permission, or the quote ID was incorrect. See section 4.5.1.3
				7003	The specified quote is no longer available. See section 4.5.1.3
				*/
		   
		        
		     $out = print_r( $arr , true);  
			 mail( "ronald@travelgroep.nl", "ERROR ($orderno) Hotelpronto (out) => ", $out  );  
		} else {
	      	
	      	$responseElement 	= $responseDoc->documentElement;
	      	$xpath 				= new DOMXPath( $responseDoc );    
	      	
	      	$elements 		= $xpath->query( '//BookingCreateResult/Booking/Id', $responseElement );  
	        $bookingID 		= $elements->item(0)->textContent;
	     	
		    
	        $hotelBookingen		= $xpath->query( '//BookingCreateResult/Booking/HotelBooking', $responseElement ); 
	        $bookingTotalSellingPrice = 0;
	      	foreach ( $hotelBookingen as $hotelBooking ) {
	      		$item = $xpath->query( 'TotalSellingPrice/Amount', $hotelBooking );
		        $bookingTotalSellingPrice += $item->item(0)->textContent;
	      	} 
	      	$arr['bookingId'] = $bookingID;
	      	$arr['bookingTotaalSellingPrice'] = $bookingTotalSellingPrice;
	      
			$out = print_r( $arr , true);  	       
	        mail( "ronald@travelgroep.nl", "Log ($orderno) Hotelpronto (BookingCreate)  ",  " arr =" . $out );
	      }
		return ( $arr ) ;
	      
    } else {
	 	  mail( "ronald@travelgroep.nl", "ERROR Hotelpronto (Boeking) $orderno ", "booking result\n\nquote-id: " . $quoteId . "\nresponsedoc was NULL" );
    }
  }

/***
    <Authority>
    <DetailLevel>
    <DataSource>
    <QueryParams>
        <BookingId>                   (optional)
        <BookingItemId>               (optional)
        <HotelStayDate>               (optional)
        <HotelStayDateEndRange>       (optional)
        <BookingCreatedDate>          (optional)
        <BookingCreatedDateEndRange>  (optional)
        <BookingModifiedDate>         (optional)
        <BookingModifiedDateEndRange> (optional)
        <CustomerName>                (optional)
        <GuestName>                   (optional)
        <HotelName> or <HotelId>      (both optional)
        <RegionName> or <RegionId>    (both optional)
        <Supplier>                    (optional)
        <SupplierBookingRef>          (optional)
	  <Affiliate>                 (optional; see notes)   
    <QueryParams>

*/ 
  public function bookingQuery( $id ) {
    $result = array();
    
    $data .= "<BookingQuery>\n";
    $data .= $this->auth;    
    $data .= "<DetailLevel>full</DetailLevel>\n";
    $data .= "<DataSource>cache</DataSource>\n";
    $data .= "<QueryParams>\n";
    //$data .= "<HotelName>Gutenberg</HotelName>\n";
    
    if ( $id ) { $data .= "<BookingId>" . $id . "</BookingId>\n"; } 
    else {
      $data .= "<BookingCreatedDate>2012-01-30</BookingCreatedDate>\n";
      $data .= "<BookingCreatedDateEndRange>2012-02-14</BookingCreatedDateEndRange>\n";
    }
    
    $data .= "</QueryParams>\n";
    $data .= "</BookingQuery>\n";
     	 
    $responseDoc = $this->sendRequest( $data );  // ElementenDOMDocument 
    
     //$out  =  print_r( $responseDoc->saveXML() , true );
     //mail( "ronald@travelgroep.nl", "Log Hotelpronto (bookingQuery)  ",  $out );
    
    
	if( $responseDoc != NULL ) {    
      if ( $errors = $this->checkForErrors( $responseDoc ) ) {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//Booking', $responseElement );
      
      
      //var_dump($elements);
      //echo " <br/> length => "  . $elements->length;
      //print_r($elements->item());
     
      $x = 0;
      foreach ( $elements as $element )
      {
      	$item = $xpath->query( 'Id', $element );
        $result[$x]['id'] = $item->item(0)->textContent;
        echo "<br/> if =".$item->item(0)->textContent ;

        $item = $xpath->query( 'CreationDate', $element );
        $result[$x]['creationdate'] = $item->item(0)->textContent;
         echo "<br/> creationdate =".$item->item(0)->textContent ;

        $item = $xpath->query( 'HotelBooking/Status', $element );
        $result[$x]['status'] = $item->item(0)->textContent;
         echo "<br/> status =".$item->item(0)->textContent ;
        
        $x++;
      }
    }
      $outResult  = print_r ($result, true);
     mail( "ronald@travelgroep.nl", "Log 3 Hotelpronto (bookingQuery)  ", " Elementen" . $outResult );
    
    return ( $result );
    
  }
  
  public function bookingCancel( $id ) {
    $result = array();
    
    $data .= "<BookingCancel>\n";
    $data .= $this->auth;    
    $data .= "<BookingId>" . $id . "</BookingId>\n";
    $data .= "<CommitLevel>confirm</CommitLevel>\n";
    $data .= "</BookingCancel>\n";

    $responseDoc = $this->sendRequest( $data );

	if( $responseDoc != NULL ) 	{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )      {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//BookingQueryResult/Booking', $responseElement );

      $x = 0;
      foreach ( $elements as $element ) {
      	
        $item = $xpath->query( 'Id', $element );
        $result[$x]['id'] = $item->item(0)->textContent;

        $item = $xpath->query( 'CreationDate', $element );
        $result[$x]['creationdate'] = $item->item(0)->textContent;
        
        $x++;
      }
    }
  }
}

?>

<?php

/*
Org: 1546
User:XML; 
password: xmlvoet
*/

define ( 'PRONTO_TESTMODE', false );

if ( PRONTO_TESTMODE )
{
  define ( 'PRONTO_ORG', '1546' );
  define ( 'PRONTO_USERNAME', 'XML' );
  define ( 'PRONTO_PASSWORD', 'xmlvoet' );
  define ( 'PRONTO_URL', 'http://85.232.37.13/HPStagingServices/ASMX/XmlService.asmx' );
}
else
{
  define ( 'PRONTO_ORG', 'voetbaltravel' );
  define ( 'PRONTO_USERNAME', 'xml' );
  define ( 'PRONTO_PASSWORD', 'xml' );
  define ( 'PRONTO_URL', 'https://www.hotelpronto.com/HPServices/ASMX/XmlService.asmx' );
}

define ( 'IMAGES_URL', 'http://www.hotelpronto.com' );

define ( 'ERR_BOOKING_FAILED', 4004 );

include_once('XMLTransactionHander.class.php'); 

class Pronto
{
  var $vendorKey = 'pronto'; // vendor identifier
	var $requestURL = PRONTO_URL; 
	var $password;
	var	$language = 'nl';
	var $currency = 'EUR';
	var $auth;
	var $db;
	
	function __construct( )
	{
    $this->db = $GLOBALS[ DBIDENT ];

		$this->org = PRONTO_ORG;
		$this->username = PRONTO_USERNAME;		
		$this->password = PRONTO_PASSWORD;		
    $this->auth = "<Authority>\n";
    $this->auth .= "<Org>" . $this->org . "</Org>\n";
    $this->auth .= "<User>" . $this->username . "</User>\n";
    $this->auth .= "<Password>" . $this->password . "</Password>\n";
    $this->auth .= "<Language>" . $this->language . "</Language>\n";
    $this->auth .= "<Currency>" . $this->currency . "</Currency>\n";
    $this->auth .= "</Authority>\n";
	}
	

/*  RW 
 * Geeft een xml AvailabilitySearch terug.
 * $stadProntoId -> alle accomodaties van een stad met de beschikbare kamers.
 * $hotelId -> alle gegevens van de accomdatie met alle beschikbare kamers.
 * 
 * $nights -> aantal nachten
 * $arrivalDate -> $arrivalDate wanneer je aankomt
 * $rooms-> aantal gewenste kamers plus aantal volwassen en kinderen.
 */
   public function makeXMLAvailabilitySearch( $arrivalDate, $nights, $bedden , $hotelId, $stadProntoId )  {
	  	/* || maak een Availability Search xml */
	    $data =  "<AvailabilitySearch>\n";
	    $data .= $this->auth;
	    	
	    if ( $hotelId ) {
		    $data .= "<HotelId>" . $hotelId . "</HotelId>\n";
		    $data .= "<CustomDetailLevel>basic,address,allphotos</CustomDetailLevel>\n";
		    //$data .= "<CustomDetailLevel>basic,address,allphotos,amenities</CustomDetailLevel>\n";
		    $data .= "<DetailLevel>full</DetailLevel>\n";
	    } else {
		    $data .= "<RegionId>" . $stadProntoId . "</RegionId>\n"	;
		    $data .= "<CustomDetailLevel>basic,address,allphotos</CustomDetailLevel>\n";
		    $data .= "<DetailLevel>custom</DetailLevel>\n";
	    	//$data .= "<DetailLevel>custom, full,summary,basic</DetailLevel>\n";
		    $data .= "<MaxHotels>150</MaxHotels>\n"; 
	    	//$data .= "<MaxHotels>0</MaxHotels>\n"; 0 = alle hotels
	    }
	    	$data .= "<HotelStayDetails>\n";
	    	$data .= "<ArrivalDate>" . $arrivalDate . "</ArrivalDate>\n";
	    	$data .= "<Nights>" . $nights . "</Nights>\n";
	    	
   foreach ( $bedden as $bed )    {
      switch ( $bed )      {
        case 'single':        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>SINGLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'twin': {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TWIN</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'double': {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>DOUBLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }

        case 'singledouble':
        case 'triplesingle': {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TRIPLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "<Adult><Title/></Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
      }
    }
			$data .= "</HotelStayDetails>\n";
			$data .= "</AvailabilitySearch>\n";
			
	 	/* || sluit de Availability Search xml */
   
      return $data; 
   }
   
 /* geeft accomoddaties terug doormiddel van een regionID||stadprontoId.  */  
	public function hotelAvailableNw(  $arrivalDate, $nights, $bedden, $hotelId , $regionId  )  {
		 $data = $this->makeXMLAvailabilitySearch( $arrivalDate, $nights, $bedden , $hotelId, $regionId );

		 //print_r ( $data ); // test
		 
		 /* || zorg voor het uitlezen van de response xml */
		if ( isset( $data) ) {
		 $responseDoc = $this->sendRequest( $data );
				//echo $responseDoc->saveXML() . "\n"; // test
		} 
		 //echo ' - als hier geen echo staat, treed er een  - ';
		 //echo '<b>Catchable fatal error</b>:  Object of class stdClass could not be converted to string in <b>/usr/home/ronald/public_html/rbs/class/gta/Hotelpronto.php</b> on line <b>577</b><br />';
		if( $responseDoc != NULL ) {  
			if ( $errors = $this->checkForErrors( $responseDoc ) )  {
				$this->outputErrors( $errors, "HTML" );
				return;
			} 
			 
      $responseElement 	= $responseDoc->documentElement;
      
      $xpath 						= new DOMXPath( $responseDoc );    
      $hotel_elements 	= $xpath->query( '//HotelAvailability', $responseElement );
      
      $x 						= 0;
      $prev 				= 0;
      $found 				= false;
      $resultArray 	= array();

      /* || Lees HotelElement uit van de response xml   */
      foreach ( $hotel_elements as $hotel_element )   {
        // Xml gegevens plaatsen.
        $hotel_id 			= $xpath->query( 'Hotel/Id', $hotel_element )->item(0)->textContent;
        $name 					= $xpath->query( 'Hotel/Name', $hotel_element )->item(0)->textContent;
        $regionId 			= $xpath->query( 'Hotel/Region/Id', $hotel_element )->item(0)->textContent;
        $regionName 		= $xpath->query( 'Hotel/Region/Name', $hotel_element )->item(0)->textContent;
        $stars 					= $xpath->query( 'Hotel/Stars', $hotel_element )->item(0)->textContent;
        $generalInfoLength	= $xpath->query( 'Hotel/GeneralInfo', $hotel_element )->length;
        $generalInfo 		= $xpath->query( 'Hotel/GeneralInfo', $hotel_element )->item(0)->textContent;
        $supplierInfo 	= $xpath->query( 'Hotel/SupplierInfo', $hotel_element )->item(0)->textContent;
        $isRecommended 	= $xpath->query( 'Hotel/IsRecommended', $hotel_element )->item(0)->textContent;
        
        $photoLength		= $xpath->query( 'Hotel/Photo', $hotel_element )->length;
        $photo 					= $xpath->query( 'Hotel/Photo', $hotel_element );
        $photoArr 			= array();
        
        for ( $i = 0 ; $i < $photoLength ; $i++) {
        	$photoArr[$i]['url'] 					= $xpath->query( 'Hotel/Photo/Url', $hotel_element )->item($i)->textContent;
        	$photoArr[$i]['width'] 				= $xpath->query( 'Hotel/Photo/Width', $hotel_element )->item($i)->textContent;
        	$photoArr[$i]['height'] 			= $xpath->query( 'Hotel/Photo/Height', $hotel_element )->item($i)->textContent;
        	$photoArr[$i]['thumbnailUrl'] = $xpath->query( 'Hotel/Photo/ThumbnailUrl', $hotel_element )->item($i)->textContent;
        }
        
        $descriptionLength	= $xpath->query( 'Hotel/Description', $hotel_element )->length;
        $description  			= $xpath->query( 'Hotel/Description', $hotel_element )->item(0)->textContent;
        $descriptionArr 		= array();
        
        for ( $i = 0 ; $i < $descriptionLength ; $i++) {
        	$descriptionArr['Type'] 	= $xpath->query( 'Hotel/Description/Type', $hotel_element )->item($i)->textContent;
        	$descriptionArr['Text'] 	= $xpath->query( 'Hotel/Description/Text', $hotel_element )->item($i)->textContent;
        }
         
				$resultArray[ $hotel_id ]['name'] 					= $name;
				$resultArray[ $hotel_id ]['regionId'] 			= $regionId;
				$resultArray[ $hotel_id ]['regionName'] 		= $regionName;
				$resultArray[ $hotel_id ]['stars'] 					= $stars;
				$resultArray[ $hotel_id ]['generalInfo'] 		= $generalInfo;
				$resultArray[ $hotel_id ]['supplierInfo'] 	= $supplierInfo;
				$resultArray[ $hotel_id ]['isRecommended'] 	= $isRecommended;
				$resultArray[ $hotel_id ]['photo'] 					= $photoArr;
				$resultArray[ $hotel_id ]['description'] 		= $descriptionArr;
				/*
					echo '<pre>'; echo '------------------- ';  echo '<br/> name = ' . $name ; echo '<br/> $regionId = ' . $regionId ; echo '<br/> $regionId = ' . $regionName ; echo '<br/> stars = ' . $stars ; echo '<br/> generalInfo = ' . $generalInfo ; echo '<br/> isRecommended = ' . $isRecommended ; echo '<br/> $photo = ' . $photo ; echo '</pre>';
				*/
      	
      	/* || Lees HotelAvailabilityResultElement uit van de response xml   */
         
				 $resultArr 															= $xpath->query( 'Result', $hotel_element );
         $resultArray[ $hotel_id ]['roomsPakket'] = $this->responseAvailableRooms( $xpath , $resultArr , $bedden );
      }
      
      return  $resultArray  ;
    }
  }
  
	/**
   * public static function gets passenger count array from passengers object
   *
   * @param passengers object
   * @return array with passenger counts
   */
  public static function getPassengerCount( $bedden )  {
    $no_adults = 0;
    $no_children = 0;
    $no_infants = 0;
    if ( is_array( $bedden) ) {
		  foreach ( $bedden as $bed ){
		      switch ( $bed ) {
		        case 'single':	{ $no_adults++;  break; }
		        case 'twin': 		{ $no_adults+=2; break; }
		        case 'double': 	{ $no_adults+=2; break; }
		        case 'singledouble':
		        case 'triplesingle': { $no_adults+=3; break; }
		      }
	    }
	    return array( $no_adults, $no_children, $no_infants );
    } else {
      return ( false );
    }
  }
  
 /**
   *  Maakt een pakket van available rooms
   */
  public function responseAvailableRooms ( $xpath , $resultArr, $bedden  )  {
		
  	    
    $results 					= $resultArr;
    $countPassenger 	= $this->getPassengerCount($bedden ) ;
		$passenger_count 	= $countPassenger[0] + $countPassenger[1] ;
		$x 								= 0;
      
		$resultArray			= array();
		foreach ( $results as $result ) {
	       
				$rooms 					= $xpath->query( 'Room', $result );
        $totalPrice 		= 0;
        $roomPrice 			= 0;
        $y 							= 0;
        $match 					= 0;
      	$roomCodeDubble = array();
		
        foreach ( $rooms as $room )   {
	      		$noDubbleRoom 	= true;
	        	$roomCode 			= $xpath->query( 'RoomType/Code', $room )->item(0)->textContent;
	        	
	        	if ( $roomCodeDubble [$roomCode] ) {
	        		$roomCodeDubble [$roomCode] = $roomCodeDubble [$roomCode] +1;
	        		$noDubbleRoom = false;
	        	} else {
	        		$roomCodeDubble [$roomCode] = 1;
	        	}
	        	$roomType 		= $xpath->query( 'RoomType/Text', $room )->item(0)->textContent;
	          $roomPrice 		= $xpath->query( 'SellingPrice/Amount', $room )->item(0)->textContent ;
	          $totalPrice 	+= $roomPrice;
	          
						if ( $noDubbleRoom ) {
						
		          $adults = $xpath->query( 'Guests/Adult', $room );
		          foreach ( $adults as $adult ) {
			          	$resultArray[$x]['rooms'][$y]['noAdults']++;
		          }
		          
		          $children = $xpath->query( 'Guests/Child', $room );
		          foreach ( $children as $child )          {
			          	$resultArray[$x]['rooms'][$y]['noChildren']++;
		          }
						
		          // plaat het in de resultArray.
		          $resultArray[$x]['rooms'][$y]['price'] = $roomPrice; 
		          $resultArray[$x]['rooms'][$y]['type'] = $roomType;          
		          $resultArray[$x]['rooms'][$y]['code'] = $roomCode;          
		           $mealType 			= $xpath->query( 'MealType/Code', $room )->item(0)->textContent;
	         		 $mealTypeDesc 	= $xpath->query( 'MealType/Text', $room )->item(0)->textContent;
		          if ( $mealType ){
		            $resultArray[$x]['rooms'][$y]['mealtype'] = $mealType; 
		            $resultArray[$x]['rooms'][$y]['mealtypeDesc'] = $mealTypeDesc; 
		          }
		          $y++;
						}
				}
        
        $quoteId 				= $xpath->query( 'QuoteId', $result )->item(0)->textContent;
				$totalPrice 		= $this->getTotalPriceWithMargin ( $totalPrice); 
        $price_p_p 			= $this->getPricePerPersoon ( $passenger_count , $totalPrice );
        $totalPrice 		= $passenger_count * $price_p_p;
        
        $resultArray[$x]['dubbleRoom'] 	= $roomCodeDubble;     
        $resultArray[$x]['price_p_p'] 	= $price_p_p;     
        $resultArray[$x]['passenger_count'] = $passenger_count;     
        $resultArray[$x]['totalPrice'] 	= $totalPrice;     
        $resultArray[$x]['quoteId'] 		= $quoteId;
        $x++;     
		}
		return ( $resultArray );
  
  }
  
  public function getTotalPriceWithMargin ( $totalPrice) {
  	$totalPrice += ( $totalPrice / 100 ) * HOTEL_MARGIN;
  	$totalPrice = (( ceil( $totalPrice / 10 ) * 10 ) -1 );
  	return $totalPrice;
  }
  public function getPricePerPersoon ( $totaalAdults ,$price ) {
  	$price = ( $price / $totaalAdults ) ;
  	$price = (( ceil( $price / 10 ) * 10 ) -1 );
  	return $price;
  }
  
   /** end rw */

  public function setErrorOutputMethod( $errorOutputMethod ) { $this->errorOutputMethod = $errorOutputMethod; }

	function sendRequest( $data )	{
		$requestData  = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
    $requestData .= $data;

/*
    echo '<pre>'; 
    echo htmlspecialchars($requestData);
    echo '</pre>';
*/
		/*********************************************/
		/*         Execute Search Transaction        */
		/*********************************************/
		
//		print '<pre>'.htmlentities( $requestData ).'</pre>';
		
		$XMLTransactionHander = new XMLTransactionHander;
		return $XMLTransactionHander->executeRequest( $this->requestURL, $requestData );
	}
	

  function checkForErrors( &$responseDoc )
  {
    $responseElement = $responseDoc->documentElement;
    $xpath = new DOMXPath( $responseDoc );
    $errorElements = $xpath->query( "//Error", $responseElement );
    $errors = array();

    foreach ($errorElements as $error)
    {
      $errorText = $xpath->query( 'Code', $error );
      $code = $errorText->item(0)->textContent;

      $errorText = $xpath->query( 'Description', $error );
      $description = $errorText->item(0)->textContent;

      $errorText = $xpath->query( 'Details', $error );
      $details = $errorText->item(0)->textContent;
      
      $errors[] = array("Code"=>$code, "Description"=>$description, "Details"=>$details );
      //return $array;
    }
    return $errors;
  }
  
  private function outputErrors( $errors, $errorOutputMethod )
  {
    return;
    switch ( $errorOutputMethod )
    {
      case 'HTML':

        foreach ( $errors as $error )
        {
          echo "<b>FOUT</b><br/>";
          echo "code: " . $error['Code'] . "<br />";
          echo "beschrijving: " . $error['Description'] . "<br />";
        }
        break;

      case 'XML':
        foreach ( $errors as $error )
        {
          echo "<error>";
          echo "<code>" . $error['Code'] . "</code>";
          echo "<description>" . $error['Description'] . "</description>";
          echo "</error>";
        }
        break;

      default:
        foreach ( $errors as $error )
        {
          echo "FOUT\n";
          echo "code: " . $error['Code'] . "\n";
          echo "beschrijving: " . $error['Description'] . "\n";
        }

    }
  }

  public function regionDownload( $regionId, $regionName, $depthChildren )
  {
    $data =  "<RegionSearch>\n";
    $data .= $this->auth;
    $data .= "<DepthChildren>" . $depthChildren . "</DepthChildren>\n";
    $data .= "</RegionSearch>\n";
    $responseDoc = $this->sendRequest( $data );
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
 
      $regionElements = $xpath->query( '//Region', $responseElement );

      foreach ($regionElements as $region)
      {
        //$pushElements = array();
        $item = $xpath->query( 'Name', $region );
        $pushElements['name'] = $item->item(0)->textContent;
  
        $item = $xpath->query( 'Id', $region );
        $pushElements['id'] = $item->item(0)->textContent;
  
        $item = $xpath->query( 'Type', $region );
        $pushElements['type'] = $item->item(0)->textContent;
        $this->db->replace( 'pronto_region', $pushElements );
      }
    }
  }	

/*
  private function recurseRegionTree( $level, $tree, $xpath )
  {
    $elements = $xpath->query( $level, $tree );

    foreach ($elements as $region)
    {
      //$pushElements = array();
      $item = $xpath->query( 'Name', $region );
      $pushElements['name'] = $item->item(0)->textContent;

      $item = $xpath->query( 'Id', $region );
      $pushElements['id'] = $item->item(0)->textContent;

      $item = $xpath->query( 'Type', $region );
      $pushElements['type'] = $item->item(0)->textContent;

      //$this->db->replace( 'pronto_region', $pushElements );
      $this->recurseRegionTree ( 'Children/Region/', $region, $xpath ) ;
    }
  }
*/

  public function getHotelData( $hotelId )
  {
    $data =  "<HotelSearch>\n";
    $data .= $this->auth;
    $data .= "<HotelId>" . $hotelId . "</HotelId>\n";
    $data .= "<DetailLevel>full</DetailLevel>\n";
    //$data .= "<CustomDetailLevel>full</CustomDetailLevel>\n";
//    $data .= "<CustomDetailLevel>basic,address,allphotos</CustomDetailLevel>\n";

    $data .= "</HotelSearch>\n";


//    $r = $this->sendRequest( $data );
    $responseDoc = $this->sendRequest( $data );
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//HotelElement', $responseElement );


      foreach ( $elements as $element )
      {
        $item = $xpath->query( 'Id', $element );
        $hotelData['id'] = $item->item(0)->textContent; 
        
        $item = $xpath->query( 'Name', $element );
        $hotelData['name'] = $item->item(0)->textContent;   

        $item = $xpath->query( 'Stars', $element );
        $hotelData['rating'] = $item->item(0)->textContent; 

        $item = $xpath->query( 'Address/Address1', $element );
        $hotelData['address'] = $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Address2', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\n" . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Address3', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\n" . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/City', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\n" . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Country', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\n" . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Tel', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\nTel. " . $item->item(0)->textContent; 
        $item = $xpath->query( 'Address/Fax', $element );
        if ( $item->item(0)->textContent ) $hotelData['address'] .= "\nFax. " . $item->item(0)->textContent; 

        $descriptions = $xpath->query( 'Description', $element );
        foreach ( $descriptions as $description )
        {
          $item = $xpath->query( 'Type', $description );
          $type = $item->item(0)->textContent;
          $content = $xpath->query( 'Text', $description );
          $hotelData['description_' . $type] = $content->item(0)->textContent;  
        }        

        $photos = $xpath->query( 'Photo', $element );
        foreach ( $photos as $photo )
        {
          $item = $xpath->query( 'Url', $photo );
          $hotelData['photo'][] = $item->item(0)->textContent; 
          $item = $xpath->query( 'ThumbnailUrl', $photo );
          $hotelData['thumbnail'][] = $item->item(0)->textContent; 
        }
      }
      return ( $hotelData );
    }
  }
  
  public function getHotelDataByCityId( $cityId, $order, $ascdesc, $limit )
  {
    $q = $this->db->query( "SELECT id, name, rating, address, photo, thumbnail FROM pronto_hotel WHERE city_id={$cityId} ORDER BY {$order} {$ascdesc} LIMIT {$limit}" );

  	for( $i=0; $i<pg_num_rows( $q ); $i++ )
  	{
  		$array = pg_fetch_array( $q, $i, PGSQL_ASSOC );
  		
  		$thumbnailArray = explode( '|', $array['thumbnail'] );
  		$array['thumbnail'] = $thumbnailArray;

  		$photoArray = explode( '|', $array['photo'] );
  		$array['photo'] = $photoArray;

			$result[ $i ]= $array;
  	}
  	return ( $result );
  }
  
  public function hotelDownload( $regionId )
  {
    $data =  "<HotelSearch>\n";
    $data .= $this->auth;
    $data .= "<RegionId>" . $regionId . "</RegionId>\n";

    $data .= '
      <HotelSearchCriteria>
        <MinStars>2</MinStars>
      </HotelSearchCriteria>
    ';

    //$data .= "<CustomDetailLevel>basic</CustomDetailLevel>\n";   
    $data .= "</HotelSearch>\n";
//    $r = $this->sendRequest( $data );
    $responseDoc = $this->sendRequest( $data );
    
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//HotelElement', $responseElement );


      foreach ($elements as $element)
      {      
        $item = $xpath->query( 'Name', $element );
        echo $pushElements['name'] = $item->item(0)->textContent;
        echo "\n";

        $item = $xpath->query( 'Id', $element );
        $pushElements['id'] = $item->item(0)->textContent;
				$hotelId = $pushElements['id'];        

        $item = $xpath->query( 'Region/Id', $element );
        $pushElements['region_id'] = $item->item(0)->textContent;

        $item = $xpath->query( 'Stars', $element );
        $pushElements['rating'] = $item->item(0)->textContent;

        $pushElements['city_id'] = $regionId;

        $this->db->replace( 'pronto_hotel', $pushElements );
        
		    $hotelData = $this->getHotelData( $hotelId );
				
				$photo = "";
				$thumbnail = "";
				
		    foreach ( $hotelData['thumbnail'] as $tn )
		            {
		               $thumbnail .= $tn . '|';
		            }
		
        foreach ( $hotelData['photo'] as $pic )
        {
            $photo .= $pic . '|';
        }
		
				$update = array ( 'rating'=>$hotelData['rating'],
				'address'=>(string)$hotelData['address'],
				'thumbnail'=>(string)$thumbnail,
				'photo'=>(string)$photo );
	      // update table with extra info
	      $this->db->update( 'pronto_hotel', $update, array( 'id'=>$hotelId ) );

//				sleep(1);
      }
    }
  }
  
  public function hotelAvailable( $hotelId, $arrivalDate, $nights, $bedden )
  {
    $data =  "<AvailabilitySearch>\n";
    $data .= $this->auth;
    $data .= "<HotelId>" . $hotelId . "</HotelId>\n";
//    $data .= "<RegionId>256</RegionId>\n";
    //$data .= "<DetailLevel>custom</DetailLevel>\n";
   // $data .= "<CustomDetailLevel>basic,address,allphotos,amenities</CustomDetailLevel>\n";
    $data .= "<HotelStayDetails>\n";
    $data .= "<ArrivalDate>" . $arrivalDate . "</ArrivalDate>\n";
    $data .= "<Nights>" . $nights . "</Nights>\n";

    foreach ( $bedden as $bed )
    {
      switch ( $bed )
      {
        case 'single':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>SINGLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'twin':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TWIN</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'double':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>DOUBLE</BasicRoomType>\n";
           $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }

        case 'singledouble':
        case 'triplesingle':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TRIPLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
      }
    }
    
    $data .= "</HotelStayDetails>\n";
    $data .= "</AvailabilitySearch>\n";

    //echo $data;
    
//    $r = $this->sendRequest( $data );
    $responseDoc = $this->sendRequest( $data );
    
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        return;
      }

      $responseElement = $responseDoc->documentElement;
      
      //echo "<pre> responseDoc = ";print_r ($responseDoc);echo "</pre>";	
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//Result', $responseElement );

      $x = 0;
      $prev = 0;
      $found = false;
      
      foreach ( $elements as $element )
      {
        $beddenTest = $bedden;
        
        $totalPrice = 0;
        $item = $xpath->query( 'QuoteId', $element );
        $quoteId = $item->item(0)->textContent;

        $rooms = $xpath->query( 'Room', $element );
        $roomPrice = 0;
        
        $y = 0;
        $match = 0;
        
        // build result
        foreach ( $rooms as $room )
        {
          $item = $xpath->query( 'SellingPrice/Amount', $room );
          $roomPrice = $item->item(0)->textContent;
          $resultArray[$x]['rooms'][$y]['price'] = $roomPrice;
          $totalPrice += $roomPrice;

          $adults = $xpath->query( 'Guests/Adult', $room );
          foreach ( $adults as $adult )
          {
            $resultArray[$x]['rooms'][$y]['noAdults']++;
          }

          $item = $xpath->query( 'Guests/Adult/SellingPrice/Amount', $room );
          // determine adult price, even if it's not stated by the XML feed
          if ( !$adultPrice = $item->item(0)->textContent )
          {
            $adultPrice = $roomPrice / $resultArray[$x]['rooms'][$y]['noAdults'];
          }
          $adultPrice /= $nights;
          $resultArray[$x]['rooms'][$y]['adultPrice'] = $adultPrice;

          $item = $xpath->query( 'RoomType/Text', $room );
          $roomType = $item->item(0)->textContent;

          $item = $xpath->query( 'MealType/Code', $room );
          $mealType = $item->item(0)->textContent;

          $item = $xpath->query( 'MealType/Text', $room );
          $mealTypeDesc = $item->item(0)->textContent;

          $z = 0 ;

        // count no matches for this result
          foreach ( $beddenTest as $bed )
          {          
            if ( $bed == strtolower( $roomType ) )
            {
              unset ( $beddenTest[$z] );
              $match += 1;
            }
            $z++;
          }
          
          $resultArray[$x]['rooms'][$y]['type'] = $roomType;          
          if ( $mealType )
          {
            $resultArray[$x]['rooms'][$y]['mealtype'] = $mealType; 
            $resultArray[$x]['rooms'][$y]['mealtypeDesc'] = $mealTypeDesc; 
          }
          
          $y++;
        }

        // determine and set best match
        if ( $match > $prev && !$found )
        {
          $resultArray['bestMatch'] = $x;  
        }
        $prev = $match;

        // best match already found?
        if ( count( $bedden ) == $match )
        {
          $found = true;
        }

        $resultArray[$x]['totalPrice'] = $totalPrice;     
        $resultArray[$x]['quoteId'] = $quoteId;
        $resultArray[$x]['roomPrice'] = $roomPrice;
        $x++;     
      }

      return ( $resultArray );

    }
  }

  public function hotelsAvailableByRegionId( $regionId, $arrivalDate, $nights, $bedden )
  {
    $data =  "<AvailabilitySearch>\n";
    $data .= $this->auth;
    $data .= "<RegionId>" . $regionId . "</RegionId>\n";
    //$data .= "<DetailLevel>custom</DetailLevel>\n";
   // $data .= "<CustomDetailLevel>basic,address,allphotos,amenities</CustomDetailLevel>\n";
    //$data .= "<MaxHotels>10</MaxHotels>";
    $data .= '
      <HotelSearchCriteria>
        <MinStars>2</MinStars>
      </HotelSearchCriteria>
    ';
    $data .= "<HotelStayDetails>\n";
    $data .= "<ArrivalDate>" . $arrivalDate . "</ArrivalDate>\n";
    $data .= "<Nights>" . $nights . "</Nights>\n";
    
    foreach ( $bedden as $bed )
    {
      switch ( $bed )
      {
        case 'single':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>SINGLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'twin':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TWIN</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'double':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>DOUBLE</BasicRoomType>\n";
           $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }

        case 'singledouble':
        case 'triplesingle':
        {
          $data .= "<Room>\n";
          $data .= "<BasicRoomType>TRIPLE</BasicRoomType>\n";
          $data .= "<Guests>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "<Adult>\n";
          $data .= "<Title/>\n";
          $data .= "</Adult>\n";
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
      }
    }
    
    $data .= "</HotelStayDetails>\n";
    $data .= "</AvailabilitySearch>\n";

    //echo $data;
    
//    $r = $this->sendRequest( $data );
	 
    $responseDoc = $this->sendRequest( $data );
    //echo "<pre>"; 
    //echo " hotelsAvailableByRegionId :: = " .  $responseDoc->saveXML() . "\n"; // test
    //echo "</pre>";
    
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        return;
      }
      $responseElement 	= $responseDoc->documentElement;
      $xpath 			= new DOMXPath( $responseDoc );
      $elements 		= $xpath->query( '//HotelAvailability', $responseElement );
      
		//echo "<pre> hotelsAvailableByRegionId::responseDoc = ";print_r ($responseElement);echo "</pre>";
	//echo "<pre>";print_r ( $responseElement );echo "</pre>"; test	
      $i = 0;
      foreach ( $elements as $availibityElement ) {
      	$item = $xpath->query( 'Hotel/Name', $availibityElement );
        $name = $item->item(0)->textContent;
        
        $item = $xpath->query( 'Hotel/Id', $availibityElement );
        $hotelId = $item->item(0)->textContent;
        
        $hotelArray[$hotelId]['name'] = $name;
        $hotelArray[$hotelId]['pronto_id'] = $hotelId;        

// hier de kamerinfo parsen!  
		$output = "";     
        $x = 0;
        $prev = 0;
        $found = false;
        
        $resultElements = $xpath->query( 'Result', $availibityElement );
		
        foreach ( $resultElements as $element )
        {
          $beddenTest = $bedden;
          
          $totalPrice = 0;
          $item = $xpath->query( 'QuoteId', $element );
          $quoteId = $item->item(0)->textContent;
  
          $rooms = $xpath->query( 'Room', $element );
          $roomPrice = 0;
          
          $y = 0;
          $match = 0;
          
          // build result
          foreach ( $rooms as $room )
          {
            $item = $xpath->query( 'SellingPrice/Amount', $room );
            $roomPrice = $item->item(0)->textContent;
            $resultArray[$x]['rooms'][$y]['price'] = $roomPrice;
            $totalPrice += $roomPrice;
  
            $adults = $xpath->query( 'Guests/Adult', $room );
            
            foreach ( $adults as $adult )
            {
              $resultArray[$x]['rooms'][$y]['noAdults']++;
            }
            
            $item = $xpath->query( 'Guests/Adult/SellingPrice/Amount', $room );
            // determine adult price, even if it's not stated by the XML feed
            if ( !$adultPrice = $item->item(0)->textContent )
            {
              $adultPrice = $roomPrice / $resultArray[$x]['rooms'][$y]['noAdults'];
            }
            $adultPrice /= $nights;
            $resultArray[$x]['rooms'][$y]['adultPrice'] = $adultPrice;
  
            $item = $xpath->query( 'RoomType/Text', $room );
            $roomType = $item->item(0)->textContent;

            // language conversions
            if ( $this->language=='nl' )
            {
              if ( $roomType == '2pers kamer (2 aparte bedden)' )
                $roomType = 'Twin';
                
              if ( $roomType == 'Dubbel (1 bed voor 2pers)' )
                $roomType = 'Double';

              if ( $roomType == 'Een-persoons' )
                $roomType = 'Single';
            }

            $item = $xpath->query( 'MealType/Code', $room );
            $mealType = $item->item(0)->textContent;
  
            $item = $xpath->query( 'MealType/Text', $room );
            $mealTypeDesc = $item->item(0)->textContent;
            
            $z = 0 ;
  
          // count no matches for this result
            foreach ( $beddenTest as $bed )
            {          
              if ( $bed == strtolower( $roomType ) )
              {
                unset ( $beddenTest[$z] );
                $match += 1;
              }
              $z++;
            }
            
            $resultArray[$x]['rooms'][$y]['type'] = $roomType;          
            if ( $mealType )
            {
              $resultArray[$x]['rooms'][$y]['mealtype'] = $mealType; 
              $resultArray[$x]['rooms'][$y]['mealtypeDesc'] = $mealTypeDesc; 
            }
            
            $y++;
          }
  
          // determine and set best match
          if ( $match > $prev && !$found )
          {
            $resultArray['bestMatch'] = $x;  
          }
          $prev = $match;
  
          // best match already found?
          if ( count( $bedden ) == $match )
          {
            $found = true;
          }
  
          $resultArray[$x]['totalPrice'] = $totalPrice;     
          $resultArray[$x]['quoteId'] = $quoteId;
          $resultArray[$x]['roomPrice'] = $roomPrice;
          
		   //$output .= "<br/>- - - - - -  - -  - - ". $x ." ROOM:  " . $quoteId . " totalPrice = " . $totalPrice 
		  	// 		. " roomPrice = " . $roomPrice . " adultPrice = " . $adultPrice . " nights  = " . $nights  ;		               
          $x++;
        }// alle kamers
        // echo "<br/>PRONTO -- " .  $i++ . " name = " . $name . " hotelId = " . $hotelId . " -- " . $x;  
       // echo $output;
        
        $hotelArray[$hotelId]['priceInfo'] = $resultArray;
        unset ( $resultArray );            
        
      }

      return ( $hotelArray );

    }
  }
  
  // creates a booking. Returns the booking ID  
  public function bookingCreateByHotelInfo ( $orderno, $hotelInfo, $persons )
  {
    $return = array();
    $quoteId = $hotelInfo['quoteId'];
    $data =  "<BookingCreate>\n";
    $data .= $this->auth;
    $data .=  "<QuoteId>" . $quoteId . "</QuoteId>\n";
    $data .=  "<AgentReference>" . $orderno . "</AgentReference>\n";
    $data .=  "<HotelStayDetails>\n";

    $index = 0;

    $index = 0;
    
    foreach ( $hotelInfo['rooms'] as $room )
    {
      $data .= "<Room>\n";
      $data .= "<Guests>\n";            
      for ( $x=0; $x<$room['noAdults']; $x++ )
      {
        $data .= "<Adult>\n";
        $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
        $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
        $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
        $data .= "</Adult>\n";
        $index++;
      }
      $data .= "</Guests>\n";
      $data .= "</Room>\n";
    }
    
    $data .=  "</HotelStayDetails>\n";
    $data .=  "<PaymentDetails>\n";
    $data .=  "<PaymentType>\n";
    $data .=  "<Code>VISA</Code>\n";
    $data .=  "<Text></Text>\n";
    $data .=  "</PaymentType>\n";
    $data .=  "<CreditCard>\n";
    $data .=  "<CardNumber>4111111111111111</CardNumber>\n";
    $data .=  "<StartDate>2006-01-01</StartDate>\n";
    $data .=  "<EndDate>2010-01-01</EndDate>\n";
    $data .=  "<IssueNumber>123</IssueNumber>\n";
    $data .=  "<SecurityCode>123</SecurityCode>\n";
    $data .=  "</CreditCard>\n";
    $data .=  "</PaymentDetails>\n";

/*
    <PaymentDetails>       (affiliate users only)
        <PaymentType>
        <CreditCard>
            <CardNumber>
            <StartDate>    (optional)
            <EndDate>
            <IssueNumber>  (optional)
            <SecurityCode> (optional)
        </CreditCard>
    </PaymentDetails>
*/

    $data .=  "<CommitLevel>confirm</CommitLevel>\n";
    $data .=  "</BookingCreate>\n";

    //$responseDoc = $this->sendRequest( $data );

		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $x = 0;
        // parse errors here
        foreach ( $errors as $error )
        {
          $return['errors'][$x]['code'] = $error['Code'];
          $return['errors'][$x]['description'] = $error['Description'];
          $x++;
        }
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '/BookingCreateResult/Booking/Id', $responseElement );

      $id = $elements->item(0)->textContent;

      $return['id'] = $id;
      
      return ( $return ) ;
    }
  }

  public function bookingCreate ( $orderno, $quoteId, $bedden, $persons )
  {
    $return = array();
    $data =  "<BookingCreate>\n";
    $data .= $this->auth;
    $data .=  "<QuoteId>" . $quoteId . "</QuoteId>\n";
    $data .=  "<AgentReference>" . $orderno . "</AgentReference>\n";
    $data .=  "<HotelStayDetails>\n";

    $index = 0;
    
    foreach ( $bedden as $bed )
    {
      switch ( $bed )
      {
        case 'single':
        {
          $data .= "<Room>\n";
          $data .= "<Guests>\n";

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;
          
          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
        case 'twin':
        case 'double':
        {
          $data .= "<Room>\n";
          $data .= "<Guests>\n";

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }

        case 'singledouble':
        case 'triplesingle':
        {
          $data .= "<Room>\n";
          $data .= "<Guests>\n";

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "<Adult>\n";
          $data .= "<Forename>" . $persons[$index]['forename'] . "</Forename>\n";
          $data .= "<Surname>" . $persons[$index]['surname'] . "</Surname>\n";
          $data .= "<Title>" . $persons[$index]['title'] . "</Title>\n";
          $data .= "</Adult>\n";
          $index++;

          $data .= "</Guests>\n";
          $data .= "</Room>\n";
          break;
        }
      }
    }

    $data .=  "</HotelStayDetails>\n";
    
/*    
    $data .=  "<PaymentDetails>\n";
    $data .=  "<PaymentType>\n";
    $data .=  "<Code>VISA</Code>\n";
    $data .=  "<Text></Text>\n";
    $data .=  "</PaymentType>\n";
    $data .=  "<CreditCard>\n";
    $data .=  "<CardNumber>4111111111111111</CardNumber>\n";
    $data .=  "<StartDate>2006-01-01</StartDate>\n";
    $data .=  "<EndDate>2010-01-01</EndDate>\n";
    $data .=  "<IssueNumber>123</IssueNumber>\n";
    $data .=  "<SecurityCode>123</SecurityCode>\n";
    $data .=  "</CreditCard>\n";
    $data .=  "</PaymentDetails>\n";
*/

/*
    <PaymentDetails>       (affiliate users only)
        <PaymentType>
        <CreditCard>
            <CardNumber>
            <StartDate>    (optional)
            <EndDate>
            <IssueNumber>  (optional)
            <SecurityCode> (optional)
        </CreditCard>
    </PaymentDetails>
*/

    $data .=  "<CommitLevel>confirm</CommitLevel>\n";
    $data .=  "</BookingCreate>\n";

    $responseDoc = $this->sendRequest( $data );

 	  mail( "ronald@travelgroep.nl", "log van hotelpronto (boeking)", "booking result\n\nquote-id: " . $quoteId . "\n" );

		if( $responseDoc != NULL ) 
		{  
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $x = 0;
        // parse errors here
        foreach ( $errors as $error )
        {
          $return['errors'][$x]['code'] = $error['Code'];
          $return['errors'][$x]['description'] = $error['Description'];
          $error_msg .= $error['Description'] . "(" . $error['Code'] . ")\n";
          $x++;
        }
		 	  mail( "ronald@travelgroep.nl", "log van hotelpronto (boeking)", "booking result\n\nquote-id: " . $quoteId . "\n{$error_msg}" );
      }

/*
<Booking>
    <Id>
    <CreationDate>
    <Customer>
    <Affiliate>             (optional; see notes below)
    <AgentReference>        (optional)
    <HotelBooking>          (1 or more)
        <Id>
        <HotelId>
        <HotelName>
        <Status>
        <SupplierInfo>      (broker users only)
            <Supplier>
            <HotelCode>
            <BookingRef>
        </SupplierInfo>
*/


      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//BookingCreateResult/Booking/HotelBooking/SupplierInfo/BookingRef', $responseElement );
      $bookingRef = $elements->item(0)->textContent;

      $return['bookingRef'] = $bookingRef;
      
      return ( $return ) ;
    }
    else
    {
	 	  mail( "ronald@travelgroep.nl", "log van hotelpronto (boeking)", "booking result\n\nquote-id: " . $quoteId . "\nresponsedoc was NULL" );
    }
  }


  public function bookingQuery( $id )
  {
    $result = array();
    
    $data .= "<BookingQuery>\n";
    $data .= $this->auth;    
    $data .= "<DetailLevel>full</DetailLevel>\n";
    $data .= "<DataSource>cache</DataSource>\n";
    $data .= "<QueryParams>\n";
    $data .= "<HotelName>Ab Viladomat</HotelName>\n";
    
    if ( $id )
    { 
      $data .= "<BookingId>" . $id . "</BookingId>\n";
    }
    $data .= "</QueryParams>\n";
    $data .= "</BookingQuery>\n";

/*
    <Authority>
    <DetailLevel>
    <DataSource>
    <QueryParams>
        <BookingId>                   (optional)
        <BookingItemId>               (optional)
        <HotelStayDate>               (optional)
        <HotelStayDateEndRange>       (optional)
        <BookingCreatedDate>          (optional)
        <BookingCreatedDateEndRange>  (optional)
        <BookingModifiedDate>         (optional)
        <BookingModifiedDateEndRange> (optional)
        <CustomerName>                (optional)
        <GuestName>                   (optional)
        <HotelName> or <HotelId>      (both optional)
        <RegionName> or <RegionId>    (both optional)
        <Supplier>                    (optional)
        <SupplierBookingRef>          (optional)
	  <Affiliate>                 (optional; see notes)   
    <QueryParams>

*/    

//    $r = $this->sendRequest( $data );

    $responseDoc = $this->sendRequest( $data );
		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//Booking', $responseElement );

      $x = 0;
      foreach ( $elements as $element )
      {
        $item = $xpath->query( 'Id', $element );
        $result[$x]['id'] = $item->item(0)->textContent;

        $item = $xpath->query( 'CreationDate', $element );
        $result[$x]['creationdate'] = $item->item(0)->textContent;

        $item = $xpath->query( 'HotelBooking/Status', $element );
        $result[$x]['status'] = $item->item(0)->textContent;
        
        $x++;
      }
    }
    return ( $result );
    
  }
  
  public function bookingCancel( $id )
  {
    $result = array();
    
    $data .= "<BookingCancel>\n";
    $data .= $this->auth;    
    $data .= "<BookingId>" . $id . "</BookingId>\n";
    $data .= "<CommitLevel>confirm</CommitLevel>\n";
    $data .= "</BookingCancel>\n";

    $responseDoc = $this->sendRequest( $data );

		if( $responseDoc != NULL ) 
		{    
      if ( $errors = $this->checkForErrors( $responseDoc ) )
      {
        $this->outputErrors( $errors, "HTML" );
        die;
      }

      $responseElement = $responseDoc->documentElement;
      $xpath = new DOMXPath( $responseDoc );    
      $elements = $xpath->query( '//BookingQueryResult/Booking', $responseElement );

      $x = 0;
      foreach ( $elements as $element )
      {
        $item = $xpath->query( 'Id', $element );
        $result[$x]['id'] = $item->item(0)->textContent;

        $item = $xpath->query( 'CreationDate', $element );
        $result[$x]['creationdate'] = $item->item(0)->textContent;
        
        $x++;
      }
    }
    
  }
  
}

?>

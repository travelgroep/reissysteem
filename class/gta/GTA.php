<?php

include('XMLTransactionHander.class.php'); 

class GTA
{

	var $requestURL = 'https://interface.test.gta-travel.com/gtaapi/RequestListenerServlet';
	
	var $clientID;
	var $email;
	var $password;
	var	$language = 'en';
	var $currency = 'EUR';
	//var	$requestMode = 'SYNCHRONOUS'; // 'ASYNCHRONOUS';
	var	$responseCallbackURL =  'http://your/callback_url'; // 'http://localhost/php/CallbackResponseHandler.php';
	
	
	function GTA()
	{
		$this->clientID = GTA_CLIENTID;
		$this->email = GTA_EMAIL;
		$this->password = GTA_PASSWORD;		
	}

	function sendRequest( $RequestDetails, $requestMode = "SYNCHRONOUS" )
	{
		$requestData = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		$requestData .= '<Request>';
		$requestData .= '  <Source>';
		$requestData .= '<RequestorID Client="'.$this->clientID.'" EMailAddress="'.$this->email.'" Password="'.$this->password.'" />';
		$requestData .= '    <RequestorPreferences Language="'.$this->language.'" Currency="' . $this->currency . '">';
		$requestData .= '		  <RequestMode>' . $requestMode . '</RequestMode> ';
 
    if ( $requestMode == "ASYNCHRONOUS" )
      $requestData .= '<ResponseURL>http://wim.cj2.nl/~pheno/callback.php</ResponseURL>';

		$requestData .= '    </RequestorPreferences>';
		$requestData .= '  </Source>';
		$requestData .= '	<RequestDetails>';
		$requestData .= $RequestDetails;
		$requestData .= '	</RequestDetails>';
		$requestData .= '</Request>';

/*
    echo '<pre>'; 
    echo htmlspecialchars($requestData);
    echo '</pre>';
*/    

		/*********************************************/
		/*         Execute Search Transaction        */
		/*********************************************/
		
//		print '<pre>'.htmlentities( $requestData ).'</pre>';
		
		$XMLTransactionHander = new XMLTransactionHander;
		return $XMLTransactionHander->executeRequest( $this->requestURL, $requestData );
	}

	function findGTACityCode( $cityName='Bonn', $countryCode='de' )
	{
    $cityCode = findGTACityCodes( $cityName, $countryCode );

		if( count($cityCode)>1 )
			return $cityCode;						
		else
			return $cityCode[0];

	}

	function findGTACityCodes( $cityName='Bonn', $countryCode='de' )
	{
	
		$requestData  = '		<SearchCityRequest CountryCode="'.$countryCode.'">';
		$requestData .= '			<CityName><![CDATA['.$cityName.']]></CityName>';
		$requestData .= '		</SearchCityRequest>';

/*
 				print "<pre>\n";
				print htmlspecialchars( $requestData );
				print "</pre>\n";
*/

		$responseDoc = $this->sendRequest( $requestData, "SYNCHRONOUS" );

		 // Process Response XML Data
		if( $responseDoc != NULL ) 
		{
/*
				print "<pre>\n";
				print htmlspecialchars( $responseDoc->saveXML() );
				print "</pre>\n";
*/
		    $responseElement = $responseDoc->documentElement;
		    $xpath = new DOMXPath( $responseDoc );    
		
		    $errorsElements = $xpath->query( 'ResponseDetails/SearchCityResponse/Errors', $responseElement );
		    if( $errorsElements->length == 0 )
		    {

		        // Process Response Data
		        $searchCityReponseElements = $xpath->query( 'ResponseDetails/SearchCityResponse/CityDetails', $responseElement );

		        foreach( $searchCityReponseElements as $searchCityReponseElement ) 
		        {
				
							$cities = $xpath->query( 'City', $searchCityReponseElement);
							foreach( $cities as $city )
							{
								//print $city->getAttribute( 'Code' );
								//print $city->textContent;
								$cityCode[ $city->textContent ] = $city->getAttribute( 'Code' );
							}
						}
		      }
		}

		return $cityCode;
	}

	function findGTAHotels( $cityCode, $countryCode='nl' )
	{
		$requestData = '		<SearchItemInformationRequest ItemType="hotel">';
		$requestData .= '			<ItemDestination DestinationType="city" DestinationCode="'.$cityCode.'" />';
		//$requestData .= '			<ItemName>AP</ItemName>';
		$requestData .= '		</SearchItemInformationRequest>';
		
		
		$responseDoc = $this->sendRequest( $requestData, "SYNCHRONOUS" );
	
		
		$hotels = array();

		 // Process Response XML Data
		if( $responseDoc != NULL ) 
		{
		    $responseElement = $responseDoc->documentElement;
		    $xpath = new DOMXPath( $responseDoc );    
		
		    $errorsElements = $xpath->query( 'ResponseDetails/SearchItemInformationResponse/Errors', $responseElement );
		    if( $errorsElements->length == 0 ) 
		    {
		        // Process Response Data
		        $searchHotelPriceReponseElements = $xpath->query( 'ResponseDetails/SearchItemInformationResponse', $responseElement );
		        foreach( $searchHotelPriceReponseElements as $searchHotelPriceReponseElement ) 
		        {
		            $hotelElements = $xpath->query( 'ItemDetails/ItemDetail', $searchHotelPriceReponseElement );
		            foreach( $hotelElements as $hotelElement ) 
		            {
										$hotel = array();
										
		                // Process each Hotel
		                $city = $xpath->query( 'City', $hotelElement );
		                $item = $xpath->query( 'Item', $hotelElement );		                
		                $starrating = $xpath->query( 'HotelInformation/StarRating', $hotelElement );
		                

		                $addr1 = $xpath->query( 'HotelInformation/AddressLines/AddressLine1', $hotelElement );
		                $addr2 = $xpath->query( 'HotelInformation/AddressLines/AddressLine2', $hotelElement );
		                $addr3 = $xpath->query( 'HotelInformation/AddressLines/AddressLine3', $hotelElement );
		                
		                $hotel['city'] = $city->item(0)->textContent;
		                $hotel['naam'] = $item->item(0)->textContent;
		                $hotel['gta_code'] = $item->item(0)->getAttribute( 'Code' );
		                $hotel['gta_city_code'] = $cityCode;
		                $hotel['rating'] = $starrating->item(0)->textContent;
		                $hotel['addr1'] = $addr1->item(0)->textContent;
		                $hotel['addr2'] = $addr2->item(0)->textContent;
		                $hotel['addr3'] = $addr3->item(0)->textContent;

		                $images = $xpath->query( 'HotelInformation/Links/ImageLinks/ImageLink', $hotelElement );
		                $hotelimages = array();

		                foreach( $images as $image )
		                {
		                	$thumb_link = $xpath->query( 'ThumbNail', $image );
		                	$descr = $xpath->query( 'Text', $image );
			                $hotelimage['url'] = $thumb_link->item(0)->textContent;
			                $hotelimage['descr'] = $descr->item(0)->textContent;
			                
			                $hotelimages[] = $hotelimage;
		                }
		                
		                $hotel['images'] = $hotelimages;
		                
		                $hotels[] = $hotel;

		            }
		        }
		    }
		  }
							
			return $hotels;
		
	}

	function hotelAvailable( $CityCode, $ItemCode, $CheckInDate, $CheckOutDate, $NumberOfRooms, $NumberOfCots )
	{

/*
	  $CityCode = 'AMS';
	  $ItemCode = 'ACC';
	  $CheckInDate = '2007-11-10';
	  $CheckOutDate = '2007-11-12';
	  
    $NumberOfRooms = 2;
*/
	  
		$requestData = '		<SearchHotelAvailabilityRequest>';
		$requestData .= '			<ItemDestination DestinationType="city" DestinationCode="'.$CityCode.'" />';
    $requestData .= '     <ItemCode>' . $ItemCode . '</ItemCode>';
    $requestData .= '     <PeriodOfStay>';
    $requestData .= '       <CheckInDate>' . $CheckInDate . '</CheckInDate>';
    $requestData .= '       <CheckOutDate>' . $CheckOutDate . '</CheckOutDate>';
    $requestData .= '     </PeriodOfStay>';
    $requestData .= '     <Rooms>';
    $requestData .= '       <Room Code="TB" NumberOfRooms="' . $NumberOfRooms . '" NumberOfCots="' . $NumberOfCots. '" ExtraBed="true" />';
    $requestData .= '     </Rooms>';
    $requestData .= '		</SearchHotelAvailabilityRequest>';

/*
    echo '<pre>';
    echo $requestData ;
    echo '</pre>';
    
<SearchHotelAvailabilityResponse>
<HotelDetails>
<Hotel>
  <City Code="AMS"><![CDATA[Amsterdam]]></City>
  <Item Code="ACC"><![CDATA[ACCA]]></Item>
  <RoomAvailability>
    <Availability>
      <DateRange>
      <FromDate>2007-11-10</FromDate>
      <ToDate>2007-11-11</ToDate>
      </DateRange>
      <Confirmation Code="IM"><![CDATA[AVAILABLE]]>
      </Confirmation>
    </Availability>
  </RoomAvailability>
</Hotel>
</HotelDetails>
</SearchHotelAvailabilityResponse>
</ResponseDetails>
</Response>    
    
    
*/
		$responseDoc = $this->sendRequest( $requestData, "SYNCHRONOUS" );	
		if( $responseDoc != NULL ) 
		{
 		    $responseElement = $responseDoc->documentElement;
		    $xpath = new DOMXPath( $responseDoc );    
		    $errorsElements = $xpath->query( 'ResponseDetails/Errors', $responseElement );

		    if( $errorsElements->length == 0 ) 
		    {
		        // Process Response Data
		        $availabilityElements = $xpath->query( 'ResponseDetails/SearchHotelAvailabilityResponse/HotelDetails/Hotel/RoomAvailability/Availability', $responseElement );
		        foreach( $availabilityElements as $availabilityElement ) 
		        {
              $item = $xpath->query( 'Confirmation', $availabilityElement );

              // datum moet exact overeenkomen!              
              $fromDateElement = $xpath->query( 'DateRange/FromDate', $availabilityElement );
              $fromDate = $fromDateElement->item(0)->textContent;
              
              $toDateElement = $xpath->query( 'DateRange/ToDate', $availabilityElement );
              $toDate = $toDateElement->item(0)->textContent;
              
              // toDate moet opgehoogd worden met ꨮ dag omdat het gaat om nachten!
              $toDateCheck = new DateTime( $toDate );
              $toDateCheck->modify("+1 day");
              $toDate = $toDateCheck->format( "Y-m-d" );
              
              //echo "[$toDate][gevraagd $CheckOutDate] ";
              if ( $fromDate == $CheckInDate && $toDate == $CheckOutDate)
                return $item->item(0)->getAttribute( 'Code' );
		        }
        }
        else 
        {
          foreach ($errorsElements as $error)
          {
            $errorText = $xpath->query( 'Error', $error );
            return $errorText->item(0)->textContent;
          }
        }
		}
	}
	
	function hotelPriceAvailable( $CityCode, $ItemCode, $CheckInDate, $CheckOutDate, $verdeling )
	{
		$requestData = '		<SearchHotelPriceRequest>';
		$requestData .= '			<ItemDestination DestinationType="city" DestinationCode="'.$CityCode.'" />';
  //$requestData .= '     <ImmediateConfirmationOnly />';
    $requestData .= '     <ItemCode>' . $ItemCode . '</ItemCode>';
    $requestData .= '     <PeriodOfStay>';
    $requestData .= '       <CheckInDate>' . $CheckInDate . '</CheckInDate>';
    $requestData .= '       <CheckOutDate>' . $CheckOutDate . '</CheckOutDate>';
    $requestData .= '     </PeriodOfStay>';
    $requestData .= '     <Rooms>';

	  if ( is_array ( $verdeling ) )
	  {
     if ( $verdeling[0] ) $requestData .= '<Room Code="SB" NumberOfRooms="' . $verdeling[0] . '" />';
     if ( $verdeling[1] ) $requestData .= '<Room Code="TB" NumberOfRooms="' . $verdeling[1] . '" />';
     if ( $verdeling[2] ) $requestData .= '<Room Code="TB" NumberOfRooms="' . $verdeling[2] . '" ExtraBed="true" />';
    }
    else
      return false;

    $requestData .= '     </Rooms>';
    $requestData .= '		</SearchHotelPriceRequest>';

   // echo $requestData;


  	$responseDoc = $this->sendRequest( $requestData, "SYNCHRONOUS" );	
		if( $responseDoc != NULL ) 
		{
 		    $responseElement = $responseDoc->documentElement;
		    $xpath = new DOMXPath( $responseDoc );    
		    $errorsElements = $xpath->query( 'ResponseDetails/SearchHotelPriceResponse/Errors', $responseElement );

//<ResponseDetails Language="en"><SearchHotelPriceResponse>

		    if( $errorsElements->length == 0 ) 
		    {
		        // Process Response Data
		        $availabilityElements = $xpath->query( 'ResponseDetails/SearchHotelPriceResponse/HotelDetails/Hotel', $responseElement );
		        foreach( $availabilityElements as $availabilityElement ) 
		        {
              $confirmationCode = $xpath->query( 'Confirmation', $availabilityElement );
              $price = $xpath->query( 'ItemPrice', $availabilityElement );


              $array['confirmationCode'] = $confirmationCode->item(0)->getAttribute( 'Code' );
              $array['price'] = $price->item(0)->textContent;
              
              return $array;
		        }
        }
        else 
        {
          foreach ($errorsElements as $error)
          {
            $errorText = $xpath->query( 'Error/ErrorId', $error );
            $array['error'] = $errorText->item(0)->textContent;
            return $array;
          }

        }
		}
	}
	
	function bookHotel()
	{
    $requestData = '<AddBookingRequest Currency="GBP">
      <BookingReference>test2</BookingReference>
      <PaxNames>
				<PaxName PaxId="1"><![CDATA[Mr John Doe ]]></PaxName>
				<PaxName PaxId="2"><![CDATA[Mrs Sarah Doe     ]]></PaxName>
				<PaxName PaxId="3" PaxType="child" ChildAge="11"><![CDATA[Master Jim Doe]]></PaxName>
		  </PaxNames>
      <BookingItems>
				<BookingItem ItemType="hotel">
					<ItemReference>1</ItemReference>
					<ItemCity Code="LON" />
					<Item Code="BRE" />
					<HotelItem>
						<AlternativesAllowed>false</AlternativesAllowed>
						<PeriodOfStay>
							<CheckInDate>2007-11-25</CheckInDate>
							<CheckOutDate>2007-11-30</CheckOutDate>
						</PeriodOfStay>
						<HotelRooms>
							<HotelRoom Code="DB" ExtraBed="true" NumberOfCots="2">
								<PaxIds>
									<PaxId>1</PaxId>
									<PaxId>2</PaxId>
									<PaxId>3</PaxId>
								</PaxIds>
							</HotelRoom>
						</HotelRooms>
					</HotelItem>
				</BookingItem>
      </BookingItems>
    </AddBookingRequest>	
		';
		
		$responseDoc = $this->sendRequest( $requestData, "ASYNCHRONOUS" );

/*

		<AddBookingRequest Currency="GBP">
			<BookingName>MC20031301</BookingName>		
			<BookingReference>MC20031201a</BookingReference>
			<AgentReference>AG1</AgentReference>			
			<BookingDepartureDate>2004-01-01</BookingDepartureDate>
			<PaxNames>
				<PaxName PaxId="1"><![CDATA[Mr John Doe ]]></PaxName>
				<PaxName PaxId="2"><![CDATA[Mrs Sarah Doe     ]]></PaxName>
				<PaxName PaxId="3" PaxType="child" ChildAge="11"><![CDATA[Master Jim Doe]]></PaxName>
			</PaxNames>
			<BookingItems>
				<BookingItem ItemType="hotel">
					<ItemReference>1</ItemReference>
					<ItemCity Code="AMS" />
					<Item Code="NAD" />
					<ItemRemarks>
						<ItemRemark Code="LA"/>
                  <ItemRemark Code="LD" />
                  <ItemRemark Code="HM" />
                  <ItemRemark><![CDATA[PLS CONFIRM BOX OF CHOCOLATE]]></ItemRemark>
                  <ItemRemark><![CDATA[KINDLY PROVIDE WITH A BOTTLE OF CHAMPAGNE]]></ItemRemark>
                   <ItemRemark><![CDATA[IF POSSIBLE PROVIDE A ROOM ON THE TOP FLOOR]]></ItemRemark>
					</ItemRemarks>
					<HotelItem>
						<AlternativesAllowed>false</AlternativesAllowed>
						<PeriodOfStay>
							<CheckInDate>2004-01-01</CheckInDate>
							<CheckOutDate>2004-01-11</CheckOutDate>
						</PeriodOfStay>
						<HotelRooms>
							<HotelRoom Code="DB" ExtraBed="true" NumberOfCots="2">
								<PaxIds>
									<PaxId>1</PaxId>
									<PaxId>2</PaxId>
									<PaxId>3</PaxId>
								</PaxIds>
							</HotelRoom>
						</HotelRooms>
					</HotelItem>
				</BookingItem>
			</BookingItems>
		</AddBookingRequest>	


*/	  
	}		
	
		
}


?>

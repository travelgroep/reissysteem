<?

/*

Update hotel data for all Hotelpronto hotels in the database

*/

define( "PRONTO_PATH", "/data1/www/reissysteem/backoffice/hotels/daemon" );

require_once( PRONTO_PATH . '/../../../inc/config.php');
require_once( PRONTO_PATH . '/../../../class/gta/Hotelpronto.php');

$db = new DB();
$myPronto = new Pronto();

$q = $db->query( "SELECT id FROM pronto_hotel" );

for( $i=0; $i<pg_num_rows( $q ); $i++ )
{
  $id = pg_fetch_result( $q, $i, 0 );
  $hotelData = $myPronto->getHotelData( $id );

  if ( is_array ( $hotelData ) )
  {
    $thumbnail = '';
    $photo = '';

    foreach ( $hotelData['thumbnail'] as $tn )
    {
      $thumbnail .= $tn . '|';
    }

    foreach ( $hotelData['photo'] as $pic )
    {
      $photo .= $pic . '|';
    }

    /*
    Fetch the description
    
    possible descriptions are (case sensitive!):
    General
    RoomInformation
    AreaDetails
    Short
    DiningFacilities
    PropertyInformation
    RoomTypes
    SurroundingArea
    */

    $description = pg_escape_string( $hotelData['description_General'] );
    $description_short = pg_escape_string( $hotelData['description_Short'] );

    
    $update = array ( 
                      'rating'=>$hotelData['rating'],
                      'address'=>(string)$hotelData['address'],
                      'thumbnail'=>(string)$thumbnail,
                      'photo'=>(string)$photo,
                      'description'=>(string)$description,
                      'description_short'=>(string)$description_short
                    );

    // update table with extra info
    $db->update( 'pronto_hotel', $update, array( 'id'=>$hotelData['id'] ) );
    echo 'Updated: ' . $hotelData['name'] . '(' . $id . ')' . "\n";
  }
}

?>
<?php

Class XMLTransactionHander {
    public $URL;
    public $XMLRequest;
    public $XMLResponseRaw;
    public $XPath;
    public $errno;

    function curlRequest() 
    {
        $hotelLogger = $GLOBALS['loggers']['hotel'];
        
        if (!empty($hotelLogger))
        {
	        $hotelLogger->addDebug(sprintf('Request url: %s',  $this->URL));
	        $hotelLogger->addDebug(sprintf('Request: %s', $this->XMLRequest));      
        }
        
        // Configure headers, etc for request
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->XMLRequest);

        $httpHeader = array(
            "Content-Type: text/xml; charset=UTF-8",
            "Content-Encoding: UTF-8"
        );

        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);

        // Execute request, store response and HTTP response code
        $data=curl_exec($ch);
        
        if(curl_errno($ch))
		{
	        if (!empty($hotelLogger))
	        {
                $hotelLogger->addError(sprintf('Curl error: %s', curl_error($ch)));
	        }        
		}
		else
		{
            if (!empty($hotelLogger))
            {
                $hotelLogger->addDebug(sprintf('Response: %s', $data));
            }      
			
		}
        
        $this->errno = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        
        
        curl_close($ch);

				//xecho $data;
        // in case of error return, add missing xml header!
/*
        if ( !strstr( $data, '<?xml' ) )
        {
          $data = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<Container xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" . $data . "\n</Container>";
        }
*/
        return( $data );
    }

    function executeRequest($URL, $request) {
        $this->URL = $URL;
        $this->XMLRequest = $request;
        $this->getFeed();

        // Process response XML if a successful HTTP response returned
        if( $this->errno == 200  )  {
				 /*
					$prefix = defined( 'VENDOR_PREFIX' ) ? VENDOR_PREFIX : 'unknown';
					$filename = "/tmp/pronto_" . $prefix . ".log";
					$file = fopen($filename, "a+");
					$no = trim( fread($file, 100) );
					$no = $no == null ? 0 : $no;
					$no++;
					file_put_contents ( $filename , $no );
					fclose( $file );
        		*/
		// Convert to a DOMDocument object
        $inputDoc = new DOMDocument();
        $inputDoc->loadXML($this->XMLResponseRaw);
            
        	return $inputDoc;
        } else {
        return null;	
        	
        }
				

       //$this->simpleXML = simplexml_load_string($this->XMLResponseRaw);
       //return ($this->parseXPath($this->simpleXML));
       
       //return ($this->XMLResponseRaw);
    } 

    function getFeed() 
    {
        $rawData=$this->curlRequest();

        if ($rawData!=-1) {
            $this->XMLResponseRaw=$rawData;
        }
    } 
}

?>
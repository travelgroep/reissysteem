<?
/*

  LocaleValue.php

*/

function getLocaleValueByBeschrijvingId($beschrijving_id, $taalcode) {
	$locale = null;
	$q = "SELECT * FROM locale_value WHERE beschrijving_id={$beschrijving_id} AND taalcode='{$taalcode}'";
	$db = $GLOBALS ['rsdb'];
	$res = $db->query ( $q );
	$row = @pg_fetch_array ( $res, 0, PGSQL_ASSOC );
	if ($row) {
		$locale = new LocaleValue ( $row );
	}
	return $locale;
}

function getLocaleValueByNaam($beschrijving_id, $naam, $taalcode) {
	$locale = null;
	if (isset($beschrijving_id)){
		$q = "SELECT * FROM locale_value WHERE beschrijving_id={$beschrijving_id} AND naam='{$naam}' AND taalcode='{$taalcode}'";
		$res = $GLOBALS ['rsdb']->query ( $q );
		$row = @pg_fetch_array ( $res, 0, PGSQL_ASSOC );
		if ($row) {
			$locale = new LocaleValue ( $row );
		}
	} else {
		debug_print_backtrace();
	}
	return $locale;
}

class LocaleValue extends DBRecord {
	var $id;
	var $taalcode;
	var $beschrijving_id;
	var $naam;
	var $waarde;
	
	function LocaleValue($array = null) {
		DBRecord::DBRecord ( 'rsdb' );
		$this->tabelNaam = "locale_value";
		
		if ($array) {
			$this->loadArray ( $array );
		}
	}
	
	function loadArray($array) {
		$this->id = $array ['id'];
		$this->taalcode = $array ['taalcode'];
		$this->beschrijving_id = $array ['beschrijving_id'];
		$this->naam = $array ['naam'];
		$this->waarde = $array ['waarde'];
	}
	
	function post($array) {
		$resultArray = array ();
		$this->id = $array ['id'];
		$this->taalcode = $array ['taalcode'];
		$this->beschrijving_id = $array ['beschrijving_id'];
		$this->naam = $array ['naam'];
		$this->waarde = $array ['waarde'];
		return $resultArray;
	}
	
	function store($array = null) {
		$resultArray = array ();
		$storeArray ['id'] = $this->id;
		$storeArray ['taalcode'] = $this->taalcode;
		$storeArray ['beschrijving_id'] = $this->beschrijving_id;
		$storeArray ['naam'] = $this->naam;
		$storeArray ['waarde'] = $this->waarde;
		
		$resultArray = parent::store ( $storeArray ); //, array( "beschrijving_id"=>$this->beschrijving_id, "taalcode"=>$this->taalcode ) );
		

		return $resultArray;
	}
	
	function getWaarde() {
		return ($this->waarde);
	}
	
	function setWaarde($value) {
		$this->waarde = $value;
	}

}
?>
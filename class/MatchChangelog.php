<?php 

class MatchChangelog {
    
  var $id;
  var $matchId;
  var $oldValue;
  var $newValue;
  var $changedField;
  var $createdAt;
  
  function loadArray($array = null){
    $this->id = $array ['id']; 
    $this->matchId = $array ['match_id']; 
    $this->oldValue = $array ['old_value']; 
    $this->newValue = $array ['new_value']; 
    $this->changedField = $array ['changed_field']; 
    $this->createdAt = $array ['created_at']; 
  }
/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

/**
	 * @return the $matchId
	 */
	public function getMatchId() {
		return $this->matchId;
	}

/**
	 * @return the $oldValue
	 */
	public function getOldValue() {
		return $this->oldValue;
	}

/**
	 * @return the $newValue
	 */
	public function getNewValue() {
		return $this->newValue;
	}

/**
	 * @return the $changedField
	 */
	public function getChangedField() {
		return $this->changedField;
	}

/**
	 * @return the $createdAt
	 */
	public function getCreatedAt() {
		return $this->createdAt;
	}

/**
	 * @param $id the $id to set
	 */
	public function setId($id) {
		$this->id = $id;
	}

/**
	 * @param $matchId the $matchId to set
	 */
	public function setMatchId($matchId) {
		$this->matchId = $matchId;
	}

/**
	 * @param $oldValue the $oldValue to set
	 */
	public function setOldValue($oldValue) {
		$this->oldValue = $oldValue;
	}

/**
	 * @param $newValue the $newValue to set
	 */
	public function setNewValue($newValue) {
		$this->newValue = $newValue;
	}

/**
	 * @param $changedField the $changedField to set
	 */
	public function setChangedField($changedField) {
		$this->changedField = $changedField;
	}

/**
	 * @param $createdAt the $createdAt to set
	 */
	public function setCreatedAt($createdAt) {
		$this->createdAt = $createdAt;
	}

	 


  
}

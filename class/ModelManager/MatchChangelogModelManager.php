<?php

class MatchChangelogModelManager {
	
	protected $db;
	
	// rsdb 
	function __construct($name='rsdb') {
		$this->db = & $GLOBALS [$name];
	}
	
	function findAll() {
		$matchChangelogs = array ();
		$db = $this->db;
		
		$result = $db->query ( "SELECT * from rbs_opta_match_changelog order by created_at desc LIMIT 500" ); // no id
		while ($row = pg_fetch_assoc($result)) {
			$matchChangelog = new MatchChangelog ();
			$matchChangelog->loadArray ( $row );
			$matchChangelogs[] = $matchChangelog;
		}
		return $matchChangelogs;
	}

}
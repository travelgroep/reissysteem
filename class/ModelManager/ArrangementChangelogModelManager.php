<?php

class ArrangementChangelogModelManager {
	
	protected $db;
	protected $table;
	
	// rsdb 
	function __construct($name='rsdb') {
		$this->table = "rbs_arrangement_changelog";
		$this->db = & $GLOBALS [$name];
	}
	
	function findAll() {
		$models = array ();
		$db = $this->db;
	
		$result = $db->query ( "SELECT * from ".$this->table." order by created_at desc LIMIT 500" ); // no id
		while ($row = pg_fetch_assoc($result)) {
			$model = new ArrangementChangelog ();
			$model->loadArray ( $row );
			$models[] = $model;
		}
		return $models;
	}
	
    

}
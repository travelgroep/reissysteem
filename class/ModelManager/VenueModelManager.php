<?php

class VenueModelManager {
	
	protected $db;
	protected $table;
	
	// rsdb  : 
	function __construct($name='rsdb') {
		$this->table = "rbs_opta_venue";
		$this->db = & $GLOBALS [$name];
	}
	
    function find($id) {
        
        $db = $this->db;
        
        $result = pg_prepare($db->db, null ,"SELECT * from ".$this->table." where id = $1");
        if (!$result){ return false; }
        $result = pg_execute($db->db, null, array($id) );
        
        $venue = new Venue();
        $venue->loadArray( pg_fetch_assoc( $result ) );
         
        return $venue;
    }
    
	function findAll() {
		$models = array ();
		$db = $this->db;
	
		$result = $db->query ( "SELECT * from ".$this->table." order by created_at desc" ); // no id
		while ($row = pg_fetch_assoc($result)) {
			$model = new Venue ();
			$model->loadArray ( $row );
			$models[] = $model;
		}
		return $models;
	}
	
    

}
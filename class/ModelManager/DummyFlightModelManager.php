<?php

class DummyFlightModelManager
{
    protected $db;
    
    function __construct($name='rsdb')
    {
        $this->db = & $GLOBALS[$name];
    }
    
    /**
     * This function has not been tested yet!
     */
    function find($id)
    {
        
        $db = $this->db;
        
        $result = pg_prepare($db->db, null ,"SELECT * from dummy_flight where id = $1");
        if (!$result){ return false; }
        $result = pg_execute($db->db, null, array($id) );
        

        $flight= new Travelgroep\Multicom\FAB\Entity\Flight();
        $this->loadFlightFromDBRow($flight, $row);
        
        return $flight;
    }
    
    function findAll()
    {
        $flights = array ();
        $db = $this->db;
        
        $result = $db->query ( "SELECT * FROM dummy_flight ORDER BY id ASC" );
        while ($row = pg_fetch_assoc($result)) {
            $flight= new Travelgroep\Multicom\FAB\Entity\Flight();
            $this->loadFlightFromDBRow($flight, $row);
            $flights[] = $flight;
        }
        return $flights;
    }
    
    function findByTrip($departureDate, $departureAirports, $destinationAirports)
    {
        $flights = array ();
        $db = $this->db;

        $parameters= array($departureDate);
        $nextParameterNum = 2;

        $departureAirportPrepareString = $this->getAirportPrepareString($departureAirports, $nextParameterNum);
        $nextParameterNum += count($departureAirports);
        $parameters = array_merge($parameters, $departureAirports);
        $destinationAirportPrepareString = $this->getAirportPrepareString($destinationAirports, $nextParameterNum);
        $parameters = array_merge($parameters, $destinationAirports);
        $nextParameterNum += count($destinationAirports);
        
        $query = sprintf('SELECT * FROM dummy_flight WHERE CAST(departure AS Date) = $1 AND departure_airport_iata IN (%s) AND arrive_airport_iata IN (%s) ORDER BY id ASC', $departureAirportPrepareString, $destinationAirportPrepareString);

        $result = pg_prepare($db->db, null, $query);
        $result = pg_execute($db->db, null, $parameters);
        while ($row = pg_fetch_assoc($result)) {
            $flight= new Travelgroep\Multicom\FAB\Entity\Flight();
            $this->loadFlightFromDBRow($flight, $row);
            $flights[] = $flight;
        }
        
        return $flights;
    }
    
    public function getAirportPrepareString($airports, $nextParameterNum = 1)
    {
        $parameters = array();
        $prepareString = '';
        $i = $nextParameterNum;
        foreach($airports as $airport) {
            $parameters[] = '$'.$i;
            $i++;
        }
        
        $prepareString = implode(',', $parameters);        
        return $prepareString;
    }
    
    function loadFlightFromDBRow($dummyFlight, $dbRow)
    {
        if (isset($dbRow['id'])) {
            $dummyFlight->setItineraryId($dbRow['id']);
        }
        
        $depDateTime = new \DateTime($dbRow['departure']);
        $dummyFlight->setDepDateTime($depDateTime);
        $dummyFlight->setDepDate($depDateTime->format('Ymd'));
        $dummyFlight->setDepTime($depDateTime->format('hi'));
        $arrDateTime = new \DateTime($dbRow['arrive']);
        $dummyFlight->setArrDateTime($arrDateTime);
        $dummyFlight->setArrDate($arrDateTime->format('Ymd'));
        $dummyFlight->setArrTime($arrDateTime->format('hi'));
        $dummyFlight->setDepAirport($dbRow['departure_airport_iata']);
        $dummyFlight->setDepAirportName($dbRow['departure_airport_name']);
        $dummyFlight->setArrAirport($dbRow['arrive_airport_iata']);
        $dummyFlight->setArrAirportName($dbRow['arrive_airport_name']);
        $dummyFlight->setAirlineCode($dbRow['airline_code']);
        $dummyFlight->setAirlineName($dbRow['airline_name']);
        $dummyFlight->setFlightNo($dbRow['flight_number']);
        $dummyFlight->setCostPP($dbRow['cost_per_person']);
        $dummyFlight->setCurrency($dbRow['currency']);
        
        return $dummyFlight;
   }
}
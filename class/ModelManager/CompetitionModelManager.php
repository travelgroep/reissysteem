<?php

class CompetitionModelManager {
	
	protected $db;
	
	// rsdb 
	function __construct($name='rsdb') {
		$this->db = & $GLOBALS [$name];
	}
	
    function find($id) {
        
        $db = $this->db;
        
        $result = pg_prepare($db->db, null ,"SELECT * from rbs_opta_competition where id = $1");
        if (!$result){ return false; }
        $result = pg_execute($db->db, null, array($id) );
        
        
        $competition = new Competition();
        $competition->loadArray( pg_fetch_assoc( $result ) );
         
        return $competition;
    }
	
	function findAll() {
		$objs = array ();
		$db = $this->db;
		
		$result = $db->query ( "SELECT * from rbs_opta_competition order by id asc" );
		while ($row = pg_fetch_assoc($result)) {
			$obj= new Competition ();
			$obj->loadArray ( $row );
			$objs[] = $obj;
		}
		return $objs;
	}
	
     

}
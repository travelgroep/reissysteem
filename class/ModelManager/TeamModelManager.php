<?php

class TeamModelManager {
	
	protected $db;
	
	// rsdb 
	function __construct($name='rsdb') {
		$this->db = & $GLOBALS [$name];
	}
	function store($array = null) {
		$array ['id'] = $this->getId ();
		$array ['name'] = $this->getName ();
		$array ['optaId'] = $this->getoptaId ();
	}
	
	function post($array) {
		$this->id = $array ['id'];
		$this->name = $array ['name'];
		$this->optaId = $array ['opta_id'];
	}
	
	function findAll() {
		$teams = array ();
		$db = $this->db;
		
		$result = $db->query ( "SELECT * from rbs_opta_team order by name asc" );
		while ($row = pg_fetch_assoc($result)) {
			$team = new Team ();
			$team->loadArray ( $row );
			$teams[] = $team;
		}
		return $teams;
	}
	
    function find($id) {
    	
        $db = $this->db;
        
        $result = pg_prepare($db->db, null ,"SELECT * from rbs_opta_team where id = $1");
        if (!$result){ return false; }
        $result = pg_execute($db->db, null, array($id) );
        
        
        $team = new Team();
        $team->loadArray( pg_fetch_assoc( $result ) );
         
        return $team;
    }
    
    
    

}
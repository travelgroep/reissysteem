<?php

class MatchModelManager {
	
	protected $db;
	
	// rsdb 
	function __construct($name='rsdb') {
		$this->db = & $GLOBALS[$name];
	}
	function store($array = null) {
		$array ['id'] = $this->getId ();
		$array ['name'] = $this->getName ();
		$array ['optaId'] = $this->getoptaId ();
	}
	
	function post($array) {
		$this->id = $array ['id'];
		$this->name = $array ['name'];
		$this->optaId = $array ['opta_id'];
	}
	
    function find($id) {
        
        $db = $this->db;
        
        $result = pg_prepare($db->db, null ,"SELECT * from rbs_opta_match where id = $1");
        if (!$result){ return false; }
        $result = pg_execute($db->db, null, array($id) );
        
        
        $match = new Match();
        $match->loadArray( pg_fetch_assoc( $result ) );
         
        return $match;
    }
	
	function findAll() {
		$matches = array ();
		$db = $this->db;
		
		$result = $db->query ( "SELECT * from rbs_opta_match order by id asc" );
		while ($row = pg_fetch_assoc($result)) {
			$match= new Match ();
			$match->loadArray ( $row );
			$matches[] = $match;
		}
		return $matches;
	}
	 
    function findByCompetitionId($competitionId) {
        $matches = array ();
        $db = $this->db;
        
        $result = pg_prepare($db->db, null ,"SELECT * from rbs_opta_match where competition_id = $1 order by kickoff_date, id asc");
        if (!$result){ return false; }
        $result = pg_execute($db->db, null, array($competitionId) );
        
        while ($row = pg_fetch_assoc($result)) {
            $match= new Match ();
            $match->loadArray ( $row );
            $matches[] = $match;
        }
        return $matches;
    }

    function findByPeriod($competitionId, $fromDate, $toDate) {
        $matches = array ();
        $db = $this->db;
        
        $query  = "SELECT * FROM rbs_opta_match WHERE competition_id = $1 AND kickoff_date >= $2 AND kickoff_date <= $3 ORDER BY kickoff_date, id ASC";
        $parameters = array($competitionId, $fromDate->format('Y-m-d'), $toDate->format('Y-m-d'));
        
        $result = pg_prepare($db->db, null , $query);
        if (!$result){ return false; }
        $result = pg_execute($db->db, null, $parameters);
        
        while ($row = pg_fetch_assoc($result)) {
            $match= new Match ();
            $match->loadArray ( $row );
            $matches[] = $match;
        }
        
        return $matches;
    }
    
    function setConfirmedByPeriod($competitionId, $fromDate, $toDate, $confirmed)
    {
        $db = $this->db;
        
        $query  = "UPDATE rbs_opta_match SET is_date_confirmed = $1 WHERE competition_id = $2 AND kickoff_date >= $3 AND kickoff_date <= $4";
        $parameters = array($confirmed ? 'true' : 'false', $competitionId, $fromDate->format('Y-m-d'), $toDate->format('Y-m-d'));
        
        $result = pg_prepare($db->db, null , $query);
        if (!$result){ return false; }
        $result = pg_execute($db->db, null, $parameters);
    }
    
    function findByHomeTeamId($homeTeamId) {
        $matches = array ();
        $db = $this->db;
        
        $result = pg_prepare($db->db, null ,"SELECT * from rbs_opta_match where hometeam_id = $1 order by id asc");
        if (!$result){ return false; }
        $result = pg_execute($db->db, null, array($homeTeamId) );
        
        while ($row = pg_fetch_assoc($result)) {
            $match= new Match ();
            $match->loadArray ( $row );
            $matches[] = $match;
        }
        return $matches;
    }
    
    function save($matches) {
        foreach($matches as $match) {
            if ($match->getId()) {
                $this->update($match);
                
            } else {
                $this->insert($match);
            }
        }
    }
    
    public function insert($match) {
        $db = $this->db;
        $matchArray = $match->toSqlArray(true);
        
        $prepareValues = array();
        for($i = 1; $i < count($matchArray)+1; $i++) {
          $prepareValues[] = '$'.$i;
        }
        
        $prepareValuesString =  '('.implode(",", $prepareValues).')';
        $fieldsString = '('.implode(",", array_keys($matchArray)).')'; 
        
        $query= sprintf('INSERT INTO rbs_opta_match %s %s ',
            $fieldsString,
            $prepareValuesString
        );
    }

    public function update($match) {
        $db = $this->db;
        $matchArray = $match->toSqlArray(false);
    
        $fieldValuePairs = array();
        $values = array();
        $i = 1;
        foreach($matchArray as $field => $value) {
            $fieldValuePairs[] = sprintf('%s = %s', $field, '$'.$i);               
            $values[] = $value;
            $i++;
        }

        $values[] =  $match->getId();
        $fieldValuePairsString =  implode(",", $fieldValuePairs);        
        
        
        $query= sprintf('UPDATE rbs_opta_match SET %s WHERE id=%s',
            $fieldValuePairsString,
            '$'.($i)
        );
        
        $result = pg_prepare($db->db, null , $query);
        $result = pg_execute($db->db, null, $values);
    }
}
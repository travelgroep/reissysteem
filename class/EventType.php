<?
/*

  EventType.php

*/

function getAantalEventTypes() {
	$q = "SELECT COUNT(id) AS num FROM event_type";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	
	return 0;

}

function getEventTypes($size = 0, $start = 0, $sort = "", $ascdesc = "", $crit = null) {
	// Criterea
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	}
	
	// Sorteren
	if ($sort) {
		$order = "ORDER BY $sort";
		if ($ascdesc == "asc"){
			$order .= " ASC";
		}else{
			$order .= " DESC";
		}
	} else {
		$order = "ORDER BY id ASC";
	}	
	// Resultaat opdelen in pagina's
	if ($size > 0) {
		$limit = "LIMIT $size";
		if ($start)
			$limit .= " OFFSET $start";
	}
	
	$q 	= "SELECT * FROM event_type $where $order $limit ";
	$res = $GLOBALS ['rsdb']->query ( $q );
	 
	$arr		= array();
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row 		= pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$item  		= new EventType ( $row );
		$arr [$i]   = $item;
	}
	
	return $arr;
}

class EventType extends Webentiteit {
	
	function EventType($array = null) {
		//print "EventType constructor!!";
		Webentiteit::Webentiteit ();
		$this->tabelNaam = "event_type";
		
		if ($array) {
			$this->loadArray ( $array );
		}
	
	}
	
	function getArrangementen($sort = "", $ascdesc = "", $crit = null, $size = null) {
		// Criterea
		if (is_array ( $crit )) {
			$where = '';
			foreach ( $crit as $item ) {
				if ($where)
					$where .= ' AND ';
				$where .= $item;
			}
			$where = 'WHERE ' . $where;
		}
		
		// Sorteren
		if ($sort) {
			$order = " ORDER BY $sort";
			if ($ascdesc == "asc")
				$order .= " ASC";
			else
				$order .= " DESC";
		} else
			$order = "ORDER BY naam DESC";
		
		$limit = '';
		if ($size) {
			$limit = " LIMIT " . $size;
		}
		//print "SELECT * FROM arrangement $where $order $limit";
		$q = $this->db->query ( "SELECT * FROM arrangement $where $order $limit" );
		
		for($i = 0; $i < pg_num_rows ( $q ); $i ++) {
			$r = pg_fetch_array ( $q, $i, PGSQL_ASSOC );
			$array [] = new Arrangement ( NULL, $r );
		}
		return $array;
	}
}
?>

<?

class DB
{

  var $db;
  
  var $admingroup = "Admin";
  var $visitorgroup = "Visitors";

  var $adminuser = "Admin";
  var $visitoruser = "Visitor";


  function DB( $dbname=DBNAME, $dbuser=DBUSER, $dbpass=DBPASS)
  {
 		//$this->db = pg_connect("dbname=testcms user=testcms password=testcms");
    $this->db = pg_connect("dbname=$dbname user=$dbuser password=$dbpass");
    
    $GLOBALS[ 'rsdb' ] = $this;
    
//    checkDatabaseStructure();
    
  }

  function query( $query )
  {
    print "<PRE>DB->query: $query</PRE>\n";
    return pg_query( $this->db, $query );
  }
  
  function insert( $table, $data )
  {
    //print "<PRE>DB->insert: $table | ";print_r($data); print "</PRE>\n";
    
    if (! pg_insert( $this->db, $table, $data ) )
    {
    	//print "DB->insert: ".pg_last_notice( $this->db );
    	//print "table: $table "; print_r( $data );
    	return "DB->insert Error! ".pg_last_error( $this->db );
    }
    //print "insert: table: $table\n";
    
  }
  
  function update( $table, $data, $criterea )
  {
    //print "<PRE>DB->update: $data <br />criteria: $criterea</PRE>\n";
    if (! pg_update( $this->db, $table, $data, $criterea ) )
    	return "Error! ".pg_last_error();
  }
  
  function delete( $table, $criterea )
  {
    //print "<pre>deleting from table $table:\n";
    //print_r( $criterea );
    //print "</pre>\n";
    
    if( !pg_delete( $this->db, $table, $criterea ) )
   	 return "Error! ".pg_last_error( $this->db );
  }
  
  // Checks the username/password combination in the database,
  // and on succes returns the corresponding user object.
  function authenticate( $username, $pass )
  {
    $result = pg_query( $this->db, "SELECT id FROM users WHERE LOWER(name)= LOWER('$username') AND pass='$pass'" );
    if( pg_num_rows( $result ) > 0 )
    {
    	$user = new User( $this );
    	$user->load( pg_fetch_result( $result, 0, 'id' ) );
    	$user->loadUsersGroups();
    	return $user;
    }
  }
  
  function getArticles()
  {
		$authorization = $GLOBALS[ 'authorization' ];
  	$user = $authorization->getUser();

		//print "<BR>getArticles: SELECT article.* FROM article, plane, auth  WHERE groupsusers.group_id = auth.group_id AND auth.page_id=plane.page_id AND article.plane_id=plane.id AND auth.read='t' AND groupsusers.user_id=".$user->getId()." AND attributes.article_id=article.id";
		//print "SELECT DISTINCT ON(article.id) article.* FROM article, plane, auth WHERE groupsusers.group_id = auth.group_id AND auth.page_id=plane.page_id AND article.plane_id=plane.id AND auth.read='t' AND groupsusers.user_id=".$user->getId()." ";
  	$res = pg_query( $this->db, "SELECT DISTINCT ON(article.id) article.* FROM article, plane, auth, groupsusers WHERE groupsusers.group_id = auth.group_id AND auth.page_id=plane.page_id AND article.plane_id=plane.id AND auth.read='t' " );// AND groupsusers.user_id=".$user->getId()." "  );
		
		//print "numrows: ".pg_num_rows( $res );
		
  	for( $i=0; $i<pg_num_rows( $res ); $i++ )
  	{
			// Check if user has read access!!
			//
			
  		$article_array = pg_fetch_array( $res, $i, PGSQL_ASSOC );
  		
			$result[ $i ]= articlefactory( $article_array );
			//$result[ $i ]->load_extra_attributes();

  		//print "<BR>article_id: $article_id  Result: ".$result[$i]->getTitle()."\n";
  	}
  	
  	return $result;
  	
  }

  function getOrderedArticlesByType( $type, $order )
  {
		$authorization = $GLOBALS[ 'authorization' ];
  	$user = $authorization->getUser();
		
		$query = "SELECT * FROM ( SELECT DISTINCT ON(article.id) article.*, attributes.value FROM article, plane, auth, attributes, groupsusers WHERE attributes.attribute='$order' AND attributes.article_id=article.id AND article.type='$type' AND groupsusers.group_id = auth.group_id AND auth.page_id=plane.page_id AND article.plane_id=plane.id AND auth.read='t' AND groupsusers.user_id=".$user->getId()." ) AS foo ORDER by value ASC";
		
		//print "query =>".$query."<=";
  	
  	$res = pg_query( $this->db, $query  );
		
  	for( $i=0; $i<pg_num_rows( $res ); $i++ )
  	{
		
  		$article_array = pg_fetch_array( $res, $i, PGSQL_ASSOC );
  		
			$result[ $i ]= articlefactory( $article_array );
			//$result[ $i ]->load_extra_attributes();

  		//print "<BR>article_id: $article_id  Result: ".$result[$i]->getTitle()."\n";
  	}
  	
  	return $result;
  	
  }


  function getArticlesByType( $type )
  {
		$authorization = $GLOBALS[ 'authorization' ];
  	$user = $authorization->getUser();

		$query = "SELECT DISTINCT ON(article.id) article.* FROM article, plane, auth, groupsusers WHERE article.type='$type' AND groupsusers.group_id = auth.group_id AND auth.page_id=plane.page_id AND article.plane_id=plane.id AND auth.read='t' AND groupsusers.user_id=".$user->getId()."";
		
		//print "query =>".$query."<=";
		
  	$res = pg_query( $this->db, $query  );

  	for( $i=0; $i<pg_num_rows( $res ); $i++ )
  	{
		
  		$article_array = pg_fetch_array( $res, $i, PGSQL_ASSOC );
  		
			$result[ $i ]= articlefactory( $article_array );
			//$result[ $i ]->load_extra_attributes();

  		//print "<BR>article_id: $article_id  Result: ".$result[$i]->getTitle()."\n";
  	}
  	
  	return $result;
  	
  }

  function getVisitorUser()
  {
    $id = $this->getVisitorUserId();
		
		//print "id = ".$id;
		
    //print "SELECT id FROM users WHERE name='".$this->visitoruser."'";

    if( $id  )
    {
    	$user = new User( $this );
    	$user->load( $id  );
    	$user->loadUsersGroups();
    	return $user;
    }
  }

  function getVisitorGroupId()
  {
    $result = pg_query( $this->db, "SELECT id FROM groups WHERE lower(name) = '".strtolower($this->visitorgroup)."'" );

    if( pg_num_rows( $result ) > 0 )
    	return pg_fetch_result( $result, 0, 'id' );
  }

	function getAdminGroupId()
  {
    $result = pg_query( $this->db, "SELECT id FROM groups WHERE lower(name) = '".strtolower($this->admingroup)."'" );

    if( pg_num_rows( $result ) > 0 )
    	return pg_fetch_result( $result, 0, 'id' );
  }

  function getVisitorUserId()
  {
    $result = pg_query( $this->db, "SELECT id FROM users WHERE lower(name) ='".strtolower($this->visitoruser)."'" );

    if( pg_num_rows( $result ) > 0 )
    	return pg_fetch_result( $result, 0, 'id' );
  }

  function getPicture( $id )
  {
    $result = pg_query( $this->db, "SELECT * FROM pictures WHERE id = '$id'" );
    if( pg_num_rows( $result) )
    {
	    $pictureArray = pg_fetch_array( $result, 0, PGSQL_ASSOC );
    	return $pictureArray[name];
    }
  }
	
  function pictureSelectBox( $curid )
  {
    $result = pg_query( $this->db, "SELECT * FROM pictures" );
  
    if( pg_num_rows( $result ) > 0 )
    {
      
      print "  <select name=picture_id>\n";
      print "    <option value=\"0\">Geen plaatje..</option>\n";
      
      for( $i=0; $i<pg_num_rows( $result ); $i++ )
      {
        $pictureArray = pg_fetch_array( $result, $i, PGSQL_ASSOC );
        print "    <option value=\"$pictureArray[id]\"";
        if( $curid==$pictureArray[id] )
        print " selected =\"slected\" ";
        print ">$pictureArray[descr]</option>\n";
      }
      print "  </select>\n";
    }
      
  }

  function planeSelectBox( $curid )
  {
		$authorization = $GLOBALS[ 'authorization' ];
		if( $authorization->isAdmin() )
		{
			    $result = pg_query( $this->db, "SELECT * FROM plane" );
		}
		else
		{
			$user = $authorization->getUser();
	    $result = pg_query( $this->db, "SELECT plane.* FROM plane, auth, groupsusers WHERE auth.page_id=plane.page_id AND auth.write='t' AND groupsusers.group_id=auth.group_id AND groupsusers.user_id=".$user->getId() );
	  }
    
    if( pg_num_rows( $result ) > 0 )
    {
    
      print "  <select name=plane_id>\n";
    
      for( $i=0; $i<pg_num_rows( $result ); $i++ )
      {
        $planeArray = pg_fetch_array( $result, $i, PGSQL_ASSOC );
        $page = $this->getPageById( $planeArray[page_id] );
        print "    <option value=\"$planeArray[id]\"";
        if( $curid==$planeArray[id] )
          print " selected=\"selected\" ";
        print ">".$page->getTitle();
        if( $planeArray[name] )
        	print" | $planeArray[name]";
        
        print "</option>\n";
      }
      print "  </select>\n";
    }          
  }

  function linkSelectBox( $curid )
  {
    $result = pg_query( $this->db, "SELECT * FROM article" );
  
    if( pg_num_rows( $result ) > 0 )
    {  
      print "  <select length=\"40\" name=\"link\">\n";
      print "  <option value=\"0\">Geen link...</option>\n";
  
      for( $i=0; $i<pg_num_rows( $result ); $i++ )
      {
        $articleArray = pg_fetch_array( $result, $i, PGSQL_ASSOC );
  
        $page = $this->getPageByArticle( $articleArray );
        print "    <option value=\"$articleArray[id]\"";
        if( $curid == $articleArray[id] )
          print " selected=\"selected\" ";
        print ">".$page->getTitle()." | $articleArray[title]</option>\n";
      }
      print "  </select>\n";
    }          
  }




  function deleteArticle( $id )
  {
  
    $oldarray[id]=$id;
    return pg_delete($this->db, 'article', $oldarray);
  
  }

  function deletePlane( $id )
  {
    $articlearray[plane_id]=$id;
    pg_delete($this->db, 'article', $articlearray);

    $planearray[id]=$id;
    pg_delete($this->db, 'plane', $planearray);
  }

  function deletePage( $id )
  {
    /* delete all asociated planes first. */
    
    $res = pg_query( $this->db, "SELECT * FROM plane WHERE page_id = $id" );
    for( $i=0; $i<pg_num_rows( $res ); $i++ )
      $this->deletePlane( pg_fetch_result( $res, $i, 'id' ) );
  
    $oldarray[id]=$id;
    return pg_delete($this->db, 'page', $oldarray);
  }

	  
  function getPageByArticleId( $article_id )
  {
  	$res = pg_query( $this->db, "SELECT * FROM article WHERE id = $article_id" );
  	
  	if( pg_num_rows( $res ) == 0 )
  		return "";
  		
  	return $this->getPageByArticle( pg_fetch_array( $res, 0 ) );
  }
  
  function getPageByArticle( $article )
  {
  	$page_id = $this->getPageIdByPlaneId( $article[plane_id] );
  	
  	//print "<BR>getPageByArticle(): page_id: $page_id\n";
  	
  	return $this->getPageById( $page_id );
  }
  
  function getPageIdByPlaneId( $plane_id )
  {
  	$res = pg_query( $this->db, "SELECT page_id FROM plane WHERE id = $plane_id" );
  	
  	if( pg_num_rows( $res ) == 0 )
  		return "";
  	
  	$page = $this->getPageById( pg_fetch_result( $res, 0, 'page_id' ) );	
  	
  	return $page->getId();
  	
  }
  
  function getPageById( $id )
  {
  	//print "<BR>getPageById(): id: $id\n"; 	

  	$page = new Page( array() );
  	if( $page->load( $id ) )
	  	return $page;
  }
  
  function getIndexPage()
  {
  	$res = pg_query( $this->db, "SELECT * FROM page WHERE index='t' LIMIT 1" );
  	
  	if( pg_num_rows( $res ) == 0 )
    	$res = pg_query( $this->db, "SELECT * FROM page LIMIT 1" );
  	
  	if( pg_num_rows( $res ) == 0 )
  		return "no pages created!";	
  	
  	else
  	{
  	 	$page = new Page( pg_fetch_array( $res, 0, PGSQL_ASSOC ) );
  		$page->loadPlanes();
  	}
  	
  	return $page;
  }
  
  
  function getPlaneByName( $name )
  {
  	$res = pg_query( $this->db, "SELECT id FROM plane WHERE name = '$name'" );
  	
  	if( pg_num_rows( $res ) > 0 )
  	{
  		$plane_array = pg_fetch_array( $res, 0 );
  		
  		return new Plane( $plane_array[id] );
  	}		
  	else
  	{
  	  print "Error! No plane could be found with name: '$name'\n";
  	}
  }

  function getPlaneArrayById( $id )
  {
    $res = pg_query( $this->db, "SELECT * FROM plane WHERE id=$id LIMIT 1" );
    return pg_fetch_array( $res, 0, PGSQL_ASSOC );
  }

  function retrieveNavPages( $crit  )
  {
		$authorization = $GLOBALS[ 'authorization' ];
		$pages = Null;
		
		if( $authorization->isAdmin() )
		{
			$q = "SELECT * FROM page WHERE nav='$crit' ORDER BY descr";
		}
		else
		{
	  	$user = $authorization->getUser();
			$q = "SELECT DISTINCT ON(page.descr, page.id) page.* FROM page, groupsusers, auth WHERE groupsusers.group_id = auth.group_id AND auth.page_id=page.id AND auth.read='t' AND nav='$crit' AND groupsusers.user_id='".$user->getId()."' ORDER BY descr";
		}

		$res = pg_query( $this->db, $q );
		
		for( $i = 0; $i<pg_num_rows( $res ); $i++ )
		{
		  $pages[$i] = new Page( pg_fetch_array( $res, $i ) );
		}
		
		return $pages;
		
  }
  
  function retrieveAllPages()
  {
		$authorization = $GLOBALS[ 'authorization' ];
		if( $authorization->isAdmin() )
		{
			$q = "SELECT * FROM page ORDER BY descr";
		}
		else
		{
	  	$user = $authorization->getUser();
			$q = "SELECT DISTINCT ON(page.descr, page.id) page.* FROM page, groupsusers, auth WHERE groupsusers.group_id = auth.group_id AND auth.page_id=page.id AND auth.read='t' AND groupsusers.user_id='".$user->getId()."' ORDER BY descr";
		}

		$res = pg_query( $this->db, $q );
		
		
		for( $i = 0; $i<pg_num_rows( $res ); $i++ )
		{
		  $pages[$i] = new Page( pg_fetch_array( $res, $i ) );
		}
		
		return $pages;
		
  }

	function retrieveAllPagesAsc()
  {
		$authorization = $GLOBALS[ 'authorization' ];
		if( $authorization->isAdmin() )
		{
			$q = "SELECT * FROM page ORDER BY title";
		}
		else
		{
	  	$user = $authorization->getUser();
			$q = "SELECT DISTINCT ON(page.descr, page.id) page.* FROM page, groupsusers, auth WHERE groupsusers.group_id = auth.group_id AND auth.page_id=page.id AND auth.read='t' AND groupsusers.user_id='".$user->getId()."' ORDER BY page.title";
		}

		$res = pg_query( $this->db, $q );
		
		
		for( $i = 0; $i<pg_num_rows( $res ); $i++ )
		{
		  $pages[$i] = new Page( pg_fetch_array( $res, $i ) );
		}
		
		return $pages;
		
  }


  function getNextPageId()
  {
	  $result = $this->query( "SELECT nextval( 'page_id_seq' ) AS newid" );

	  return pg_fetch_result( $result, 0,  "newid" );
	}

  function getNextReisoptieId()
  {
	  $result = $this->query( "SELECT nextval( 'reisopties_id_seq' ) AS newid" );

	  return pg_fetch_result( $result, 0,  "newid" );
	}


  function getNextArticleId()
  {
		$result = pg_query( $this->db, "SELECT nextval( 'content_id_seq' ) AS newid" );
		
		return pg_fetch_result( $result, 0,  "newid" );
  }


  function getNextUsersId()
  {
		$result = pg_query( $this->db, "SELECT nextval( 'users_id_seq' ) AS newid" );
		
		return pg_fetch_result( $result, 0,  "newid" );
  }

  function getNextGroupsId()
  {
		$result = pg_query( $this->db, "SELECT nextval( 'groups_id_seq' ) AS newid" );
		
		return pg_fetch_result( $result, 0,  "newid" );
  }

	function getNextOrderId()
  {
		$result = pg_query( $this->db, "SELECT nextval( 'orders_id_seq' ) AS newid" );
		
		return pg_fetch_result( $result, 0,  "newid" );
  }
  
	function getOrders( $order, $start, $size, $status_crit )
	{
		if( !$order )
			$order = "id DESC";
			
		if( !$start )
			$start = 0;
			
		
		foreach( $status_crit as $crit=>$val )
		{
			if( $where )
				$where .= " OR ";
			$where .= "\"status_code\"=$crit";
		}
	
		if( $where )
		$where = "WHERE $where";
		
		//print "<PRE>\n";
		//print_r( $status_crit );
		//print $where;
		//print "</PRE>\n";
		
			
		$res = pg_query( $this->db, "SELECT * FROM orders $where ORDER BY $order LIMIT $size OFFSET $start" );
		for( $i = 0; $i<pg_num_rows( $res ); $i++ )
		{	  
		  $orders[ $i ] = new Order();
		  $orderarray = pg_fetch_array( $res, $i, PGSQL_ASSOC );
		  $orders[ $i ]->loadArray( $orderarray );
		}

		return $orders;		
	}
	
  function getUsers()
  {
	$res = pg_query( $this->db, "SELECT * FROM users ORDER BY id" );

	for( $i = 0; $i<pg_num_rows( $res ); $i++ )
	{
	  $users[$i] = new User( );

	  $user = pg_fetch_array( $res, $i );
	  
	  $users[$i]->id = $user['id'];
	  $users[$i]->name = $user['name'];
	  $users[$i]->descr = $user['descr'];
	  $users[$i]->fullname = $user['fullname'];
	  $users[$i]->loadUsersGroups();
	}
	
	return $users;
  }

  function getUserById( $id )
  {
	
	  $user = new User( );
	  if( $user->load( $id ) )
	  	return;
	  $user->loadUsersGroups();

		return $user;

  }

  
  function getGroups()
  {
	$res = pg_query( $this->db, "SELECT * FROM groups ORDER BY id" );

	for( $i = 0; $i<pg_num_rows( $res ); $i++ )
	{
	  $groups[$i] = new Group( );

	  $group = pg_fetch_array( $res, $i );
	  
	  $groups[$i]->id = $group['id'];
	  $groups[$i]->name = $group['name'];
	  $groups[$i]->descr = $group['descr'];
	  $groups[$i]->loadUsersGroups();
	}
	
	return $groups;
  }

	function getDefaultGroups()
	{
		$res = pg_query( $this->db, "SELECT * FROM groups WHERE name='Visitors'" );
		$group = pg_fetch_array( $res, 0 );

	  $groups[0]				= new Group( $this );
	  $groups[0]->id		= $group['id'];
	  $groups[0]->name	= $group['name'];
	  $groups[0]->descr	= $group['descr'];
	  
	  return $groups;

	}
  
  function linkUserToGroups( $user, $groups )
  {
	pg_query( $this->db, "DELETE FROM groupsusers WHERE user_id=".$user->getId() );

        foreach( $groups as $group )
		pg_query( $this->db, "INSERT INTO groupsusers (group_id, user_id) VALUES (".$group->getId().", ".$user->getId().")"  );

  }

  function linkGroupToUsers( $group, $users )
  {
		pg_query( $this->db, "DELETE FROM groupsusers WHERE group_id=".$group->getId() );
	
	        if( $users )
	        {
		        foreach( $users as $user )
				pg_query( $this->db, "INSERT INTO groupsusers (group_id, user_id) VALUES (".$group->getId().", ".$user->getId().")"  );
		}
	
  }

  function searchArticles( $terms )
  {
		$authorization = $GLOBALS[ 'authorization' ];
  	$user = $authorization->getUser();
  	$order = "article_id";
 	
  	foreach( $terms as $term )
  	{
  		if( $query )
  			$query .= " AND ";

  		$query .= "value ~* '.*$term.*'";
  	}
  	
		$q = "SELECT attributes.article_id FROM attributes, plane, auth, groupsusers, article WHERE $query AND groupsusers.group_id = auth.group_id AND auth.page_id=plane.page_id AND article.plane_id=plane.id AND auth.read='t' AND groupsusers.user_id=".$user->getId()." AND attributes.article_id=article.id GROUP BY article_id";
		//print "<BR>searchArticle: $q<BR><BR>\n";

  	$res = pg_query( $this->db, $q  );

  	for( $i=0; $i<pg_num_rows( $res ); $i++ )
  	{
			// Check if user has read access!!
			//
			
  		$article_id = pg_fetch_result( $res, $i, 'article_id' );
  		
  		$result[ $i ] =  article_load( $article_id );
  		//print "<BR>article_id: $article_id  Result: ".$result[$i]->getTitle()."\n";
  	}
  	
  	return $result;
  	
  }
  
  function searchArticlesByPictureID ($pic_id)
  {
  	$res = pg_query( $this->db, "SELECT * FROM article WHERE picture_id = $pic_id" );
  	
		for( $i=0; $i<pg_num_rows( $res ); $i++ )
		{
  		$arr = pg_fetch_array( $res, $i, PGSQL_ASSOC );
  		$articles[ $i ] = articlefactory( $arr );
  	}
  	
  	return $articles;
  		
	}

	function getNextPhotoId()
	{
		return pg_fetch_result (pg_exec($this->db, "SELECT nextval('\"foto_id_seq\"') "),0);				
	}

	function addPhoto( $foto_id, $name, $article_id )
	{		
		$data['id'] = $foto_id;
		$data['name'] = $name;
		$data['article_id'] = $article_id;
		
		if( !pg_insert( $this->db, 'foto', $data ) )
    	return "Error! ".pg_last_error( $this->db );

	}

	function deletePhoto( $foto_id )
	{		
		$data['id'] = $foto_id;
		
		if( !pg_delete( $this->db, 'foto', $data ) )
    	return "Error! ".pg_last_error( $this->db );

	}

	
	function getPhotoArray( $article_id, $start, $amount )
	{
		$res = pg_query( $this->db, "SELECT * FROM foto WHERE article_id=$article_id" );
  	for( $i=0; $i<pg_num_rows( $res ); $i++ )
  		$result[ $i ] =  pg_fetch_array( $res, $i );
  	
  	return $result;		
	}
}
?>
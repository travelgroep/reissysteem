<?
/*

  Hotel.php

*/

function getAantalHotels() {
	$q = "SELECT COUNT(id) AS num FROM hotel";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	
	return 0;

}

function isAdded($vendorId, $vendorKey) {
	$q = "SELECT id FROM hotel WHERE vendor_key='{$vendorKey}' AND vendor_id={$vendorId}";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	if (pg_num_rows ( $res ) >= 1)
		return pg_fetch_result ( $res, 0, 'id' );
	
	return false;
}
function countHotelsOnEventType($crit = null) {
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else {
		$where = NULL;
	}
	
	$q = "SELECT COUNT(DISTINCT h.id) AS num  FROM hotel h LEFT JOIN locatie l on h.stad_id = l.stad_id LEFT JOIN event e on e.locatie_id = l.id $where ";
	//echo $q;
	$res = $GLOBALS ['rsdb']->query ( $q );
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	return 0;
}

function joinHotelsByEvent($size = 0, $start = 0, $sort = "", $ascdesc = "", $crit = null) {
	
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	}
	// Sorteren
	if ($sort) {
		$order = "ORDER BY $sort";
		if ($ascdesc == "asc")
			$order .= " ASC";
		else
			$order .= " DESC";
	} else
		$order = "ORDER BY id ASC";
		
	// Resultaat opdelen in pagina's
	if ($size) {
		$limit = "LIMIT $size";
		if ($start)
			$limit .= " OFFSET $start";
	}
	
	$q = "SELECT DISTINCT h.*  FROM hotel h LEFT JOIN locatie l on h.stad_id = l.stad_id LEFT JOIN event e on e.locatie_id = l.id $where $order $limit ";
	//$q = "SELECT DISTINCT h.*  FROM hotel h LEFT JOIN locale l on h.stad_id = l.beschrijving_id LEFT JOIN event e on e.locatie_id = l.id $where $order $limit ";
//	echo $q;
	$res = $GLOBALS ['rsdb']->query ( $q );
	$array = array();
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$array [] = new Hotel ( null, $row );
	}
	return $array;
}

function getHotels($size = 0, $start = 0, $sort = "", $ascdesc = "", $crit = null) {
	// criteria
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else {
		$where = NULL;
	}
	
	// Sorteren
	if ($sort) {
		$order = "ORDER BY $sort";
		if ($ascdesc == "asc")
			$order .= " ASC";
		else
			$order .= " DESC";
	} else
		$order = "ORDER BY id ASC";
		
	// Resultaat opdelen in pagina's
	if ($size) {
		$limit = "LIMIT $size";
		if ($start)
			$limit .= " OFFSET $start";
	}
	
	$q = "SELECT * FROM hotel $where $order $limit ";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	$array = array();
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$array [$row ['id']] = new Hotel ( null, $row ); //&
	}
	return $array;
}

function getHotelIds($size = 0, $start = 0, $sort = "", $ascdesc = "", $crit = null) {
	// criteria
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else {
		$where = NULL;
	}
	
	// Sorteren
	if ($sort) {
		$order = "ORDER BY $sort";
		if ($ascdesc == "asc")
			$order .= " ASC";
		else
			$order .= " DESC";
	} else
		$order = "ORDER BY id ASC";
		
	// Resultaat opdelen in pagina's
	if ($size) {
		$limit = "LIMIT $size";
		if ($start)
			$limit .= " OFFSET $start";
	}
	
	$q = "SELECT * FROM hotel $where $order $limit ";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	$array = array();
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$id = pg_fetch_result ( $res, $i, 'id' );
		$array [$i] = $id;
	}
	return $array;
}

function getHotelsByKoppeling($koppeling, $type_id, $extra = null) {
	$array = array();
	if ($extra){
		$extra = 'AND ' . $extra;
	}
	$q = "SELECT * FROM hotel_$koppeling INNER JOIN hotel ON hotel_id = hotel.id WHERE {$koppeling}_id=$type_id $extra ORDER BY basisprijs_2pers_kamer";
	
	//echo "<br/> $q ";
	

	$res = $GLOBALS ['rsdb']->query ( $q );
	
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$id = pg_fetch_result ( $res, $i, 'hotel_id' );
		$array [$i] = new Hotel ( );
		$array [$i]->loadById ( $id );
	}
	return $array;
}

function getHotelIdsByKoppeling($koppeling, $type_id, $extra = null) {
	$array = array();
	if ($extra)
		$extra = 'AND ' . $extra;
	
	$q = "SELECT * FROM hotel_$koppeling INNER JOIN hotel ON hotel_id = hotel.id WHERE {$koppeling}_id=$type_id $extra ORDER BY basisprijs_2pers_kamer";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$id = pg_fetch_result ( $res, $i, 'hotel_id' );
		$array [$i] = $id;
	}
	return $array;
}

class Hotel extends Webentiteit {
	var $stad_id; // eventueel
	

	var $tabelNaam = 'hotel';
	
	var $basisprijs_1pers_kamer;
	var $basisprijs_2pers_kamer;
	var $basisprijs_3pers_kamer;
	var $type_1pers_id;
	var $type_2pers_id;
	var $type_3pers_id;
	
	var $addr1;
	
	var $vendor_key;
	var $vendor_id;
	
	var $rating;
	var $eenpersoonskamer_toeslag;
	var $longitude;
	var $latitude;
	
	var $f_pool;
	var $f_children;
	var $f_airco;
	var $f_fitness;
	var $f_internet;
	var $f_disabled;
	
	var $type; // 0 = hotel, 1 = apartment
	

	var $transfer_resort_id;

    var $budget_rating;
    var $luxury_rating;
    var $distance_to_stadium_rating;
    var $distance_to_city_center_rating;
	
	function Hotel($Stad = null, $array = null) {
		Webentiteit::Webentiteit ();
		$this->Stad = $Stad;
		$this->tabelNaam = 'hotel';
		
		if ($array) {
			$this->loadArray ( $array );
		}
		
	
	}
	
	function loadArray($array = null) {
		$this->stad_id = $array ['stad_id'];
		
		$this->basisprijs_2pers_kamer = $array ['basisprijs_2pers_kamer'];
		$this->eenpersoonskamer_toeslag = $array ['eenpersoonskamer_toeslag'];
		$this->rating = $array ['rating'];
		$this->vendor_id = $array ['vendor_id'];
		$this->vendor_key = $array ['vendor_key'];
		$this->addr1 = $array ['addr1'];
		$this->longitude = $array ['longitude'];
		$this->latitude = $array ['latitude'];
		
		// facilities
		$this->f_pool = $array ['f_pool'];
		$this->f_children = $array ['f_children'];
		$this->f_airco = $array ['f_airco'];
		$this->f_fitness = $array ['f_fitness'];
		$this->f_internet = $array ['f_internet'];
		$this->f_disabled = $array ['f_disabled'];
		$this->type = $array ['type'];
		
		$this->transfer_resort_id = $array ['transfer_resort_id'];

        $this->budget_rating = $array ['budget_rating'];
        $this->luxury_rating = $array ['luxury_rating'];
        $this->distance_to_stadium_rating = $array ['distance_to_stadium_rating'];
        $this->distance_to_city_center_rating = $array ['distance_to_city_center_rating'];

		parent::loadArray ( $array );
	}
	
	function store($array = null) {
		$array ['stad_id'] = $this->getStadId ();
		$array ['basisprijs_2pers_kamer'] = $this->getBasisprijs2persKamer ();
		$array ['rating'] = $this->getRating ();
		$array ['vendor_id'] = ( int ) $this->getVendorId ();
		$array ['vendor_key'] = $this->getVendorKey ();
		$array ['addr1'] = $this->getAdres ();
		$array ['longitude'] = $this->getLongitude ();
		$array ['latitude'] = $this->getLatitude ();
		
		$array ['f_pool'] = $this->getFpool ();
		$array ['f_children'] = $this->getFchildren ();
		$array ['f_airco'] = $this->getFairco ();
		$array ['f_fitness'] = $this->getFfitness ();
		$array ['f_disabled'] = $this->getFdisabled ();
		$array ['f_internet'] = $this->getFinternet ();
		
		$array ['type'] = $this->getType ();
		
		$array ['eenpersoonskamer_toeslag'] = $this->getEenpersoonskamerToeslag ();
		$array ['transfer_resort_id'] = $this->getTransferResortId ();

        $array ['budget_rating'] = $this->getBudgetRating();
        $array ['luxury_rating'] = $this->getLuxuryRating();
        $array ['distance_to_stadium_rating'] = $this->getDistanceToStadiumRating() ;
        $array ['distance_to_city_center_rating'] = $this->getDistanceToCityCenterRating();

		return parent::store ( $array );
	}
	
	function post($array) {
		$resultArray = array ();
		$this->stad_id = $array ['stad_id'];
		$this->basisprijs_2pers_kamer = $array ['basisprijs_2pers_kamer'];
		$this->eenpersoonskamer_toeslag = $array ['eenpersoonskamer_toeslag'];
		$this->rating = $array ['rating'];
		$this->vendor_id = $array ['vendor_id'];
		$this->vendor_key = $array ['vendor_key'];
		$this->addr1 = $array ['addr1'];
		$this->longitude = $array ['longitude'];
		$this->latitude = $array ['latitude'];

        $this->budget_rating = $array ['budget_rating'];
        $this->luxury_rating = $array ['luxury_rating'];
        $this->distance_to_stadium_rating = $array ['distance_to_stadium_rating'];
        $this->distance_to_city_center_rating = $array ['distance_to_city_center_rating'];


		$this->f_pool = $array ['f_pool'] == 'on' ? 't' : 'f';
		$this->f_children = $array ['f_children'] == 'on' ? 't' : 'f';
		$this->f_airco = $array ['f_airco'] == 'on' ? 't' : 'f';
		$this->f_fitness = $array ['f_fitness'] == 'on' ? 't' : 'f';
		$this->f_internet = $array ['f_internet'] == 'on' ? 't' : 'f';
		$this->f_disabled = $array ['f_disabled'] == 'on' ? 't' : 'f';
		
		$this->type = $array ['type'];
		
		$this->transfer_resort_id = $array ['transfer_resort_id'];
		if (! preg_match ( '/^[0-9.]*$/', $this->basisprijs_2pers_kamer ))
			$resultArray ['basisprijs_2pers_kamer'] = 'NOINT';
			
		// oude tariefwijzigingen?
		

		if (is_array ( $array ['tariefwijziging_id'] )) {
			foreach ( $array ['tariefwijziging_id'] as $tariefwijzigingId ) {
				$array ['tariefwijziging_event_id_' . $tariefwijzigingId];
				
				if (! preg_match ( '/^[0-9.\-]*$/', $array ['tariefwijziging_percentage_' . $tariefwijzigingId] ))
					$resultArray ['tariefwijziging_percentage_' . $tariefwijzigingId] = 'NOINT';
				
				$tariefwijziging = new Tariefwijziging ( );
				$tariefwijziging->id = $tariefwijzigingId;
				$tariefwijziging->post ( array ('hotel_id' => $this->getId (), 'naam' => $array ['tariefwijziging_naam_' . $tariefwijzigingId], 'datum_start' => $array ['tariefwijziging_datum_start_' . $tariefwijzigingId], 'datum_eind' => $array ['tariefwijziging_datum_eind_' . $tariefwijzigingId], 'percentage' => $array ['tariefwijziging_percentage_' . $tariefwijzigingId], 'event_id' => $array ['tariefwijziging_event_id_' . $tariefwijzigingId], 'actief' => $array ['tariefwijziging_actief_' . $tariefwijzigingId] ) );
				$tariefwijziging->store ();
			}
		}
		
		// nieuwe tariefwijziging?
		if ($array ['tariefwijziging_datum_start_new'] && $array ['tariefwijziging_datum_eind_new'] && $array ['tariefwijziging_percentage_new']) {
			if (! preg_match ( '/^[0-9.]*$/', $array ['tariefwijziging_percentage_new'] ))
				$resultArray ['tariefwijziging_percentage'] = 'NOINT';
			
			$tariefwijziging = new Tariefwijziging ( );
			$tariefwijziging->post ( array ('hotel_id' => $this->getId (), 'naam' => $array ['tariefwijziging_naam_new'], 'datum_start' => $array ['tariefwijziging_datum_start_new'], 'datum_eind' => $array ['tariefwijziging_datum_eind_new'], 'percentage' => $array ['tariefwijziging_percentage_new'], 'event_id' => $array ['tariefwijziging_event_id_new'], 'actief' => $array ['tariefwijziging_actief_new'] ) );
			
			$tariefwijziging->store ();
		
		}
		
		$resultArray += parent::post ( $array );
		return $resultArray;
	}
	
	function getTariefwijzigingen() {
		$array = array ();
		
		$q = @$GLOBALS ['rsdb']->query ( "SELECT * FROM tariefwijziging WHERE hotel_id = {$this->id}" ); // ORDER BY waarde");
		

		if (@pg_num_rows ( $q )) {
			for($i = 0; $i < @pg_num_rows ( $q ); $i ++) {
				$r = @pg_fetch_array ( $q, $i, PGSQL_ASSOC );
				$array [] = new Tariefwijziging ( $r );
			}
		}
		return $array ? $array : null;
	}
	
	function getBasisprijs2persKamer($arrangement = null) {
		$returnPrice = $this->basisprijs_2pers_kamer;
		
		if (is_object ( $arrangement ) && $this->getId ()) // als arrangementId mee wordt gegeven, uit hotelcache prijs krijgen
{
			$arrangementId = $arrangement->getId ();
			
			//      echo "SELECT p2 FROM hotel_cache WHERE hotel_id = " . $this->getId() . " AND arrangement_id = {$arrangementId}\n";
			

			$q = $GLOBALS ['rsdb']->query ( "SELECT p2 FROM hotel_cache WHERE hotel_id = " . $this->getId () . " AND arrangement_id = {$arrangementId}" );
			
			$r = pg_fetch_result ( $q, 0 );
			//      $r = floor ( $r/2 ); 
			//      echo "uit cache: $r uit gewoon: " . $this->basisprijs_2pers_kamer . "|";
			if ($r) {
				if ($this->getVendorKey () == 'pronto') {
					$r *= PRONTO_MARGE;
				}
				return $r;
			} else {
				$tariefwijzigingen = $this->getTariefwijzigingen ();
				
				if (is_array ( $tariefwijzigingen )) {
					foreach ( $tariefwijzigingen as $tariefwijziging ) {
						// is tariefwijziging geldig voor dit event?
						if ($tariefwijziging->getActief () == 't') {
							
							$arrVertrekdatum = new DateTime ( $arrangement->getHeen () );
							$arrTerugkomstdatum = new DateTime ( $arrangement->getTerug () );
							$start = new DateTime ( $tariefwijziging->getDatumStart () );
							$eind = new DateTime ( $tariefwijziging->getDatumEind () );
							
							if ($tariefwijziging->getEventId () == $arrangement->getEventId () 
							 || $tariefwijziging->getEventId () == 0 && ($start->format ( "U" ) <= $arrVertrekdatum->format ( "U" ) && $eind->format ( "U" ) >= $arrTerugkomstdatum->format ( "U" ))) {
								$percentage = $tariefwijziging->getPercentage ();
								
								if ($percentage >= 0) {
									$returnPrice *= 1 + $percentage / 100;
								} else {
									$returnPrice *= 1 - abs ( $percentage ) / 100;
								}
							
							}
						
						}
					}
				}
				
				return $returnPrice;
			}
		} else
			return $returnPrice;
	}
	
	function hasCache($arrangementId) {
		$q = $GLOBALS ['rsdb']->query ( "SELECT p2 FROM hotel_cache WHERE hotel_id = " . $this->getId () . " AND arrangement_id = {$arrangementId}" );
		$r = pg_fetch_result ( $q, 0 );
		
		if ($r) {
			return ($r);
		} else {
			return (false);
		}
	}
	
	function setBasisprijs2persKamer($prijs) {
		$this->basisprijs_2pers_kamer = $prijs;
	}
	
	function getAdres() {
		return $this->addr1;
	}
	
	function getVendorId() {
		return $this->vendor_id;
	}
	
	function getVendorKey() {
		return $this->vendor_key;
	}
	
	function setVendorId($vendorId) {
		$this->vendor_id = $vendorId;
	}
	
	function setVendorKey($vendorKey) {
		$this->vendor_key = $vendorKey;
	}
	
	function getStadId() {
		if (is_object ( $this->Stad ))
			$this->stad_id = $this->Stad->getID ();
		
		return $this->stad_id;
	}
	
	function getStad() {
		if (! is_object ( $this->Stad ) && $this->stad_id) {
			$this->Stad = new Stad ( );
			$this->Stad->loadById ( $this->stad_id );
		}
		
		return $this->Stad;
	}
	
	function setRating($rating) {
		$this->rating = $rating;
	}
	
	function setNaam($item) {
		$this->naam = $item;
	}
	/**
	function getNaam() {
		return $this->naam;
	}
	*/
	function getRating() {
		return $this->rating;
	}
	function getLongitude() {
		return $this->longitude;
	}
	function setLongitude($longitude) {
		$this->longitude = $longitude;
	}
	
	function getLatitude() {
		return $this->latitude;
	}
	
	function getFpool() {
		return $this->f_pool;
	}
	
	function getFchildren() {
		return $this->f_children;
	}
	function getFairco() {
		return $this->f_airco;
	}
	
	function getFfitness() {
		return $this->f_fitness;
	}
	
	function getFinternet() {
		return $this->f_internet;
	}
	
	function getFdisabled() {
		return $this->f_disabled;
	}
	
	function getType() {
		return $this->type;
	}
	
	function setType($type) {
		$this->type = $type;
	}
	
	function setLatitude($latitude) {
		$this->latitude = $latitude;
	}
	
	function getEenpersoonskamerToeslag($arrangementId = null) {
		if ($arrangementId) // als arrangementId mee wordt gegeven, uit hotelcache prijs krijgen
{
			$q = $GLOBALS ['rsdb']->query ( "SELECT p1,p2 FROM hotel_cache WHERE hotel_id = " . $this->getId () . " AND arrangement_id = {$arrangementId}" );
			$r = pg_fetch_array ( $q );
			
			$p1 = $r ['p1'];
			$p2 = floor ( $r ['p2'] / 2 );
			
			$v = $p1 - $p2;
			
			if ($v > 0)
				return $v;
			else
				return $this->eenpersoonskamer_toeslag;
		} else
			return $this->eenpersoonskamer_toeslag;
	}
	
	function setEenpersoonskamerToeslag($eenpersoonskamer_toeslag) {
		$this->eenpersoonskamer_toeslag = $eenpersoonskamer_toeslag;
	}
	
	function setAdres($adres) {
		$this->addr1 = $adres;
	}
	
	function getTransferResortId() {
		return $this->transfer_resort_id;
	}

    /**
     * @return mixed
     */
    public function getBudgetRating()
    {
        return $this->budget_rating;
    }

    /**
     * @param mixed $budget_rating
     */
    public function setBudgetRating($budget_rating)
    {
        $this->budget_rating = $budget_rating;
    }

    /**
     * @return mixed
     */
    public function getLuxuryRating()
    {
        return $this->luxury_rating;
    }

    /**
     * @param mixed $luxury_rating
     */
    public function setLuxuryRating($luxury_rating)
    {
        $this->luxury_rating = $luxury_rating;
    }

    /**
     * @return mixed
     */
    public function getDistanceToStadiumRating()
    {
        return $this->distance_to_stadium_rating;
    }

    /**
     * @param mixed $distance_to_stadium_rating
     */
    public function setDistanceToStadiumRating($distance_to_stadium_rating)
    {
        $this->distance_to_stadium_rating = $distance_to_stadium_rating;
    }

    /**
     * @return mixed
     */
    public function getDistanceToCityCenterRating()
    {
        return $this->distance_to_city_center_rating;
    }

    /**
     * @param mixed $distance_to_city_center_rating
     */
    public function setDistanceToCityCenterRating($distance_to_city_center_rating)
    {
        $this->distance_to_city_center_rating = $distance_to_city_center_rating;
    }
}
?>
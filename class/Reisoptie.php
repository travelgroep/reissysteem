<?
/*

  Reisoptie.php

*/

function getReisoptieByReisoptieWaardeId( $id )
{
  $array = array();

  $q = $GLOBALS['rsdb']->query("SELECT reisoptie_id FROM reisoptiewaarde WHERE id = {$id}");

  $r = pg_fetch_result( $q, 0, 0 );
  $reisoptie = new Reisoptie( );
  $reisoptie->loadById( $r );
  return $reisoptie;
}

class Reisoptie extends Beschrijving
{
  var $webentiteit_id;
  var $pp;
  var $pn;
    
  function Reisoptie( $array = null )
  {
    parent::Beschrijving();
    $this->tabelNaam = "reisoptie";
    if ($array)
    {
      $this->loadArray($array);
    }
  }

  function getReisoptieWaardes() {
    $array = array();

    // echo "SELECT * FROM reisoptiewaarde WHERE reisoptie_id = {$this->id}";
    $q = $GLOBALS['rsdb']->query("SELECT * FROM reisoptiewaarde WHERE reisoptie_id = {$this->id}"); // ORDER BY waarde");
  	for( $i=0; $i<pg_num_rows( $q ); $i++ )
	  {
		  $r = pg_fetch_array( $q, $i, PGSQL_ASSOC );
      $array[] = new ReisoptieWaarde( $r );
	  }
    return $array;
  }

  function loadArray($array)
  {
    $this->pp = $array['pp'];
    $this->pn = $array['pn'];
    $this->webentiteit_id = $array['webentiteit_id'];
    parent::loadArray($array);    
  }

  function post($array)
  {
    $resultArray = array();
    ($array['pp']) ? $this->pp = 1 : $this->pp = 0;
    if ( $this->pp )
    {
      ($array['pn']) ? $this->pn = 1 : $this->pn = 0;
    }
    else
    {
      $this->pn = 0;
    }
    $array['naam'] = $array['reisoptie_naam'];
    $array['waarde'] = $array['reisoptie_waarde'];
    $array['beschrijving'] = $array['reisoptie_beschrijving'];
    $resultArray += parent::post($array);    
    return $resultArray;
  }

  function store($array = null)
  {
    $resultArray = array();
    $array['pp'] = $this->pp;
    $array['pn'] = $this->pn;
    $array['webentiteit_id'] = $this->webentiteit_id;
    $resultArray = parent::store( $array );
    return $resultArray;
  }

  function getPp()
  {
    return $this->pp;
  }

  function getPn()
  {
    return $this->pn;
  }
  
}
?>

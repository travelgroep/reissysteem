<?
/*

  Event.php

*/
function countEventOnEventType($crit = null) {
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else {
		$where = NULL;
	}
	
	$q = "SELECT  COUNT(id) AS num FROM event $where";
	$res = $GLOBALS ['rsdb']->query ( $q );
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	return 0;
}

function getAantalEvents() {
	$q = "SELECT COUNT(id) AS num FROM event";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	
	return 0;

}

function getEvents($size = 0, $start = 0, $sort = "", $ascdesc = "", $crit = null ) {
	// Criterea
	if (is_array ( $crit )) {
		$where = '';
		
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	}
	
	// Sorteren
	if ($sort) {
		$order = "ORDER BY $sort";
		if ($ascdesc == "asc")
			$order .= " ASC";
		else
			$order .= " DESC";
	} else
		$order = "ORDER BY id ASC";
		
	// Resultaat opdelen in pagina's
	$limit = "";
	if ($size) {
		$limit = "LIMIT $size";
		if ($start)
			$limit .= " OFFSET $start";
	}
	
	$q = "SELECT * FROM event $where $order $limit ";
	//echo $q;
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$array [] = new Event ( null, $row );
	}
	return $array;
}

function getEventsByStadId($stad_id) {
	$events = getEvents ();
	foreach ( $events as $event ) {
		$locatie = new Locatie ( );
		$locatie->loadById ( $event->getLocatieId () );
		if ($locatie->getStadId () == $stad_id) {
			$useEvents [] = $event;
		}
	}
	return $useEvents;
}

class Event extends Webentiteit {
	var $Locatie;
	var $EventType;
	
	var $locatie_id;
	var $event_type_id;
	var $marge_prijs;
	var $geen_vluchten;
	var $extra_info;
	var $isTopClub;
	var $optaTeamId;
	
	function Event($Locatie = null, $array = null) {
		Webentiteit::Webentiteit ();
		$this->tabelNaam = "event";
		$this->Locatie = $Locatie;
		
		if ($array) {
			$this->loadArray ( $array );
		}
	
	}
	
	// explicit destructor for in-script memory clearing
	function __destruct() {
		unset ( $this );
	}
	
	function loadArray($array) {
		$this->event_type_id = $array ['event_type_id'];
		$this->locatie_id = $array ['locatie_id'];
		$this->marge_prijs = $array ['marge_prijs'];
		$this->geen_vluchten = $array ['geen_vluchten'];
		$this->extra_info = $array ['extra_info'];
		$this->optaTeamId = $array ['opta_team_id'];
		//echo "EVENT:: => ".  $this->marge_prijs ;
		parent::loadArray ( $array );
	}
	
	function post($array) {
		$resultArray = array ();
		$this->event_type_id = $array ['event_type_id'];
		$this->locatie_id = $array ['locatie_id'];
		$this->marge_prijs = $array ['marge_prijs'];
		$this->geen_vluchten = $array ['geen_vluchten'];
		$this->extra_info = $array ['extra_info'];
		$this->optaTeamId = $array ['opta_team_id'];
		
		($array ['geen_vluchten']) ? $this->geen_vluchten = 1 : $this->geen_vluchten = 0;
		if (! $array ['land_id'])
			$resultArray ['land_id'] = 'EMPTY';
		if (! $array ['stad_id'])
			$resultArray ['stad_id'] = 'EMPTY';
		if (! $array ['locatie_id'])
			$resultArray ['locatie_id'] = 'EMPTY';
		if (! $array ['event_type_id'])
			$resultArray ['event_type_id'] = 'EMPTY';
		
		if (! $this->marge_prijs)
			$resultArray ['marge_prijs'] = 'EMPTY';
		else if (! preg_match ( '/^[0-9.]*$/', $this->marge_prijs ))
			$resultArray ['marge_prijs'] = 'NOINT';
		
		$resultArray += parent::post ( $array );
		
		$results = print_r ( $resultArray, true ); // $results now contains output from print_r
		return $resultArray;
	}
	
	function store($array = null) {
		
		
		$array ['locatie_id'] = $this->getLocatieId ();
		$array ['event_type_id'] = $this->event_type_id;
		$array ['marge_prijs'] = $this->marge_prijs;
		$array ['geen_vluchten'] = $this->geen_vluchten;
		$array ['extra_info'] = $this->extra_info;
		$array ['opta_team_id'] = $this->optaTeamId;
		 
		// handle locale for this value
		if (isset ( $_SESSION ['taalcode'] )) {
			$lv = getLocaleValueByBeschrijvingId ( $this->id, $_SESSION ['taalcode'] );
			
			if (! $lv) {
				$lv = new LocaleValue ( array ('naam' => 'geen_vluchten', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['geen_vluchten'] ) );
			} else {
				$lv->setWaarde ( $array ['geen_vluchten'] );
			}
			$lv->store ();
			$array ['geen_vluchten'] = null; // skip this!
			

			// marge_prijs
			//mail( "alert@travelgroep.nl", "LOG marge_prijs " ,  "  marge_prijs =>    ". $this->marge_prijs );
			

			$lv = getLocaleValueByNaam ( $this->id, 'marge_prijs', $_SESSION ['taalcode'] );
			if (! $lv) {
				$lv = new LocaleValue ( array ('naam' => 'marge_prijs', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['marge_prijs'] ) );
			} else {
				$lv->setWaarde ( $array ['marge_prijs'] );
			}
			//$array['marge_prijs'] =  $this->marge_prijs; // skip this!
			$lv->store ();
		
		} else {
			
			// als lv bestaat ook opslaan als er geen session code is!
			$lv = getLocaleValueByNaam ( $this->id, 'marge_prijs', $_SESSION ['taalcode'] );
			if (! $lv) {
				$lv = new LocaleValue ( array ('naam' => 'marge_prijs', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['marge_prijs'] ) );
			} else {
				$lv->setWaarde ( $array ['marge_prijs'] );
			}
			$lv->store ();
		}
 
		return parent::store ( $array );
	}
	
	function getMargePrijs() {
		if ( isset($this->id) ){
			$lv = getLocaleValueByNaam ( $this->id, "marge_prijs", $_SESSION ['taalcode'] );
			if ($lv) {
				//echo "<pre>"; print_r( $lv ); echo "</pre>"; 
				return ($lv->getWaarde ());
			} else {
				return $this->marge_prijs;
			}
		} else {
			return $this->marge_prijs;	
		}
	}
	
	function getLocatie() {
		if (! is_object ( $this->Locatie ) && $this->locatie_id) {
			$this->Locatie = new Locatie ( );
			$this->Locatie->loadById ( $this->locatie_id );
		}
		
		return $this->Locatie;
	}
	
	function getEventType() {
		if (! is_object ( $this->EventType ) && $this->event_type_id) {
			$this->EventType = new EventType ( );
			$this->EventType->loadById ( $this->event_type_id );
		}
		
		return $this->EventType;
	
	}
	
	function getEventTypeId() {
		return $this->event_type_id;
	}
	
	function getLocatieId() {
		if ($this->Locatie)
			$this->locatie_id = $this->Locatie->getID ();
		
		return $this->locatie_id;
	}
	
	function getGeenVluchten() {
		if ( $this->id ) {
			$lv = getLocaleValueByBeschrijvingId ( $this->id, $_SESSION ['taalcode'] );
			if ($lv) {
				return $lv->getWaarde () == 1 ? 't' : 'f';
			}
		}
		return $this->geen_vluchten;
	}
	
	function setGeenVluchten() {
		$this->geen_vluchten = 'f';
	}
	
	function getExtraInfo() {
		return $this->extra_info;
	}
	function getIsTopClub() {
		return $this->isTopClub;
	}
	function setIsTopClub($isTopClub) {
		$this->isTopClub = $isTopClub;
	}
	
	function setExtraInfo($extra_info) {
		$this->extra_info = $extra_info;
	}
	
	function getArrangementen($sort = "", $ascdesc = "", $crit = null) {
		// Criterea
		if (is_array ( $crit )) {
			$where = 'event_id = ' . $this->id;
			foreach ( $crit as $item ) {
				if ($where)
					$where .= ' AND ';
				$where .= $item;
			}
			$where = 'WHERE ' . $where;
		}
		
		// Sorteren
		if ($sort) {
			$order = "ORDER BY $sort";
			if ($ascdesc == "asc")
				$order .= " ASC";
			else
				$order .= " DESC";
		} else {
			$order = "ORDER BY naam DESC";
		}
		// echo "SELECT * FROM arrangement $where $order $limit";
		

		$q = $this->db->query ( "SELECT * FROM arrangement $where $order $limit" );
		
		for($i = 0; $i < pg_num_rows ( $q ); $i ++) {
			$r = pg_fetch_array ( $q, $i, PGSQL_ASSOC );
			$array [] = new Arrangement ( $this, $r );
		}
		return $array;
	}
	/**
	 * @return the $optaTeamId
	 */
	public function getOptaTeamId() {
		return $this->optaTeamId;
	}

	/**
	 * @param $optaTeamId the $optaTeamId to set
	 */
	public function setOptaTeamId($optaTeamId) {
		$this->optaTeamId = $optaTeamId;
	}


}
?>

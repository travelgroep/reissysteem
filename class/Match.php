<?php 

class Match {
    
  var $id;
  var $name;
  var $optaId;
  var $competitionId;     
  var $kickoffDate;
  var $isDateConfirmed;
  var $updateEnable;
  var $period;     
  var $matchDay ;     
  var $venueId;     
  var $homeTeamId;
  var $awayTeamId;
  
  function loadArray($array = null)
  {
      
      $this->id = $array['id']; 
      
      $this->optaId = $array['opta_id'];
      
      $this->matchDay = $array['match_day'];
      $this->competitionId = $array['competition_id'];
      $this->kickoffDate = $array['kickoff_date'];
      $this->isDateConfirmed = $array['is_date_confirmed']  == 't' ? true: false;
      $this->updateEnable = $array['update_enable'] == 't' ? true: false;
      $this->period = $array['period'];
      $this->venueId = $array['venue_id'];

      $this->homeTeamId = $array['hometeam_id'];
      $this->awayTeamId = $array['awayteam_id'];
  }
  
  function toSqlArray($withPrimarykey)
  {
      $array = array();
      if ($withPrimarykey) {
         $array['id'] = $this->id; 
      }
      
      $array['opta_id'] = $this->id;
      
      $array['match_day'] = $this->matchDay;
      $array['competition_id'] = $this->competitionId;
      $array['kickoff_date'] = $this->kickoffDate;
      $array['is_date_confirmed'] = $this->isDateConfirmed ? 'true' : 'false';
      $array['update_enable'] = $this->updateEnable ? 'true' : 'false';
      $array['period'] = $this->period;
      $array['venue_id'] = $this->venueId;

      $array['hometeam_id'] = $this->homeTeamId;
      $array['awayteam_id'] = $this->awayTeamId;
      
      return $array;
  }
  
 
/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

/**
	 * @return the $optaId
	 */
	public function getOptaId() {
		return $this->optaId;
	}

/**
	 * @param $id the $id to set
	 */
	public function setId($id) {
		$this->id = $id;
	}

/**
	 * @param $name the $name to set
	 */
	public function setName($name) {
		$this->name = $name;
	}

/**
	 * @param $optaId the $optaId to set
	 */
	public function setOptaId($optaId) {
		$this->optaId = $optaId;
	}
	 

	/**
	 * @return the $homeTeamId
	 */
	public function getHomeTeamId() {
		return $this->homeTeamId;
	}

	/**
	 * @return the $awayTeamId
	 */
	public function getAwayTeamId() {
		return $this->awayTeamId;
	}

	/**
	 * @param $homeTeamId the $homeTeamId to set
	 */
	public function setHomeTeamId($homeTeamId) {
		$this->homeTeamId = $homeTeamId;
	}

	/**
	 * @param $awayTeamId the $awayTeamId to set
	 */
	public function setAwayTeamId($awayTeamId) {
		$this->awayTeamId = $awayTeamId;
	}
	
	/**
	 * @return the $competitionId
	 */
	public function getCompetitionId() {
		return $this->competitionId;
	}

	/**
	 * @return the $kickoffDate
	 */
	public function getKickoffDate() {
		return $this->kickoffDate;
	}

	/**
	 * @return the $period
	 */
	public function getPeriod() {
		return $this->period;
	}

	/**
	 * @return the $venueId
	 */
	public function getVenueId() {
		return $this->venueId;
	}

	/**
	 * @param $competitionId the $competitionId to set
	 */
	public function setCompetitionId($competitionId) {
		$this->competitionId = $competitionId;
	}

	/**
	 * @param $kickoffDate the $kickoffDate to set
	 */
	public function setKickoffDate($kickoffDate) {
		$this->kickoffDate = $kickoffDate;
	}

	/**
	 * @param $period the $period to set
	 */
	public function setPeriod($period) {
		$this->period = $period;
	}

	/**
	 * @param $venueId the $venueId to set
	 */
	public function setVenueId($venueId) {
		$this->venueId = $venueId;
	}
	/**
	 * @return the $matchDay
	 */
	public function getMatchDay() {
		return $this->matchDay;
	}

	public function setMatchDay($matchDay) {
		$this->matchDay = $matchDay;
	}

    public function isDateConfirmed()
    {
        return $this->isDateConfirmed;
    }

    public function setDateConfirmed($isDateConfirmed)
    {
        $this->isDateConfirmed = $isDateConfirmed;
    }
	
    public function getUpdateEnable()
    {
        return $this->updateEnable;
    }

    public function setUpdateEnable($updateEnable)
    {
        $this->updateEnable = $updateEnable;
    }
}

<?
/*

  LocaleReisoptieWaarde.php

*/

function getReisoptieWaardeLocales()
{
  $q = "SELECT DISTINCT taalcode FROM locale_reisoptiewaarde";
  $res = $GLOBALS['rsdb']->query( $q );
	for( $i=0; $i<pg_num_rows( $res ); $i++ )
 	{
		$row = pg_fetch_array( $res, $i, PGSQL_ASSOC );
    $array[] = new LocaleReisoptieWaarde( $row );
	}
	return $array;
}

// :)
function getLocaleReisoptieWaardeByReisoptieWaardeId( $reisoptiewaarde_id, $taalcode )
{
  $q = "SELECT * FROM locale_reisoptiewaarde WHERE reisoptiewaarde_id={$reisoptiewaarde_id} AND taalcode='{$taalcode}'";
  $res = $GLOBALS['rsdb']->query( $q );
	$row = @pg_fetch_array( $res, 0, PGSQL_ASSOC );
	if ( $row )
	{
    $locale = new LocaleReisoptieWaarde( $row );
    return $locale;  
  }
}

function getLocaleReisoptieWaardesByReisoptieWaardeId( $reisoptiewaarde_id )
{
  $q = "SELECT * FROM locale_reisoptiewaarde WHERE reisoptiewaarde_id={$reisoptiewaarde_id}";
  $res = $GLOBALS['rsdb']->query( $q );
	$row = @pg_fetch_array( $res, 0, PGSQL_ASSOC );
	if ( $row )
	{
    $locale[] = new LocaleReisoptieWaarde( $row );
    return $locale;  
  }
}


class LocaleReisoptieWaarde extends DBRecord
{
  var $reisoptiewaarde_id;
  var $naam;
  var $waarde;
  var $purchase_price;
  var $taalcode;
  
  function LocaleReisoptieWaarde( $array = null)
  {
    DBRecord::DBRecord( 'rsdb' );
    $this->tabelNaam = 'locale_reisoptiewaarde'; 
    $this->loadArray( $array );
  }
  
  function loadArray( $array )
  {
    $this->reisoptiewaarde_id = $array['reisoptiewaarde_id'];
    $this->naam = $array['naam'];
    $this->waarde = $array['waarde'];
    $this->purchase_price = $array['purchase_price'];
    $this->id = $array['id'];
    $this->taalcode = $array['taalcode'];
  }
  
  function setReisoptieWaardeId( $reisoptiewaarde_id )
  {
    $this->reisoptiewaarde_id = $reisoptiewaarde_id;
  }

  function setNaam( $naam )
  {
    $this->naam = $naam;
  }

  function setWaarde( $waarde )
  {
    $this->waarde = $waarde;
  }

  function setPurchasePrice( $purchase_price )
  {
    $this->purchase_price = $purchase_price;
  }

  function getNaam()
  {
    return $this->naam;
  }

  function getTaalcode()
  {
    return $this->taalcode;
  }

  function getWaarde()
  {
    return $this->waarde;
  }

  function getPurchasePrice()
  {
    return $this->purchase_price;
  }

  function getReisoptieId()
  {
    return $this->reisoptie_id;
  }

  function store($array = null)
  {
    $resultArray = array();
    $array['id'] = $this->id;
    $array['reisoptiewaarde_id'] = $this->reisoptiewaarde_id;
    $array['naam'] = $this->naam;
    $array['waarde'] = $this->waarde;
    $array['purchase_price'] = $this->purchase_price;
    $array['taalcode'] = $this->taalcode;
    $resultArray = parent::store($array);
    return $resultArray;
  }

  function post($array)
  {
    $resultArray = array();
    
    $this->reisoptiewaarde_id = $array['reisoptiewaarde_id'];
    $this->naam = $array['naam'];
    $this->waarde = $array['waarde'];
    $this->purchase_price = $array['purchase_price'];
/*
    if (!$array['naam']) $resultArray['naam']='EMPTY';
    if (!$array['waarde']) $resultArray['waarde']='EMPTY';
*/

    return $resultArray;
  }
}
?>
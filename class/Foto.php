<?
/*

  Foto.php

*/

function getNextPhotoId() {
	return pg_fetch_result ( pg_exec ( "SELECT nextval('foto_id_seq')" ), 0 );
}

function getFotoById($id) {
	$result = NULL;  
	if (! empty ( $id )) {
		$q = "SELECT * FROM foto WHERE id = {$id}";
		$res = $GLOBALS ['rsdb']->query ( $q );
		if (pg_num_rows ( $res ) > 0) {
			if ($row = pg_fetch_array ( $res, 0, PGSQL_ASSOC )) {
				$result = new Foto ( $row );
			}
			pg_free_result ( $res );
		}
	}
	return $result;
}

function getFotosByBeschrijvingId($beschrijving_id, $taalcode = null) {
	$q = "SELECT * FROM foto WHERE beschrijving_id = {$beschrijving_id} AND (taalcode='alle'";
	
	if (isset ( $taalcode )) {
		$q .= " OR taalcode = '{$taalcode}'";
	}
	
	$q .= ")";
	
	if (@$r = $GLOBALS ['rsdb']->query ( $q )) {
		//echo "teller" . pg_num_rows ( $r ) ;
		for($i = 0; $i < pg_num_rows ( $r ); $i ++) {
			$row = pg_fetch_array ( $r, $i, PGSQL_ASSOC );
			$fotoArray [] = new Foto ( $row );
		}
		return $fotoArray;
	}
}

class Foto extends DBRecord {
	var $url;
	var $beschrijving;
	var $naam;
	var $beschrijving_id;
	var $taalcode;
	
	function Foto($array = null) {
		$this->tabelNaam = 'foto';
		parent::DBRecord ();
		
		if ($array) {
			$this->loadArray ( $array );
		}
	
	}
	
	function getNaam() {
		return $this->naam;
	}
	
	function getBeschrijving() {
		return $this->beschrijving;
	}
	
	function getUrl() {
		return $this->url;
	}
	
	function getId() {
		return $this->id;
	}
	
	function getSrc($resolution = 300) {
		$path = $this->getPath () . $this->id . '_' . $resolution . 'px.jpg';
		
		//echo "<br/> path=>" . $path;
		//echo "<br/> PHOTOSTORE_path=>" .  PHOTOSTOREPATH  ;
		
 
		if (file_exists ( PHOTOSTOREPATH . $path )) {
			$dimensions = getimagesize ( PHOTOSTOREPATH . $this->getPath () . $this->id . '_full.jpg' );
			//echo "<br/>dimensions =" . PHOTOSTOREPATH . $this->getPath () . $this->id . '_full.jpg';
			//echo "<pre>";echo print_r($dimensions,true);echo "</pre>";
			
			if ($resolution == 'square') {
				return PHOTOPATH . $this->getPath () . $this->id . '_squarepx.jpg';
			} else if ($resolution > $dimensions [0]) {
				//echo "2" . PHOTOPATH . $this->getPath () . $this->id . '_full.jpg';
				return PHOTOPATH . $this->getPath () . $this->id . '_full.jpg';
			} else {
				return PHOTOPATH . $path;
			}
		} else {
			return false;
		}
	}
	
	function getDimensions($resolution = 300) {
		if (file_exists ( PHOTOSTOREPATH . $this->getPath () . $this->id . '_' . $resolution . 'px.jpg' )) {
			return getimagesize ( PHOTOSTOREPATH . $this->getPath () . $this->id . '_' . $resolution . 'px.jpg' );
		} else {
			return false;
		}
	}
	
	function getSize() {
		if (file_exists ( PHOTOSTOREPATH . $this->getPath () . $this->id . '_full.jpg' )) {
			return getimagesize ( PHOTOSTOREPATH . $this->getPath () . $this->id . '_full.jpg' );
		}
	}
	
	function getPath() {
		// make sure its a string
		$id_str = "" . $this->id;
		$path_str = "";
		
		// walk through the string _until_ the last character.
		for($i = 1; $i < strlen ( $id_str ); $i ++) {
			$path_str .= substr ( $id_str, 0, $i ) . "x/";
		}
		//echo $path_str;
		return $path_str;
	
	}
	
	function checkPath($prefix) {
		// make sure its a string
		$id_str = "" . $this->id;
		$path_str = "";
		
		//print 'prefix: '.$prefix.'  id: '.$this->id."\n";
		

		// walk through the string _until_ the last character.
		for($i = 1; $i < strlen ( $id_str ); $i ++) {
			$path_str .= substr ( $id_str, 0, $i ) . "x/";
			if (! is_dir ( $prefix . $path_str )) {
				//print $prefix.$path_str."\n";
				if (! mkdir ( $prefix . $path_str ))
					return FALSE;
			}
		}
		return TRUE;
	}
	
	function loadArray($array) {
		$this->id = $array ['id'];
		$this->naam = $array ['naam'];
		$this->beschrijving = $array ['beschrijving'];
		$this->beschrijving_id = $array ['beschrijving_id'];
		$this->taalcode = $array ['taalcode'];
	}
	
	function post($array) {
		if (! $this->naam = $array ['naam'])
			$this->naam = $_FILES ['bestand'] ['name'];
			
		//echo '<br/>' . $this->naam;
		//echo '<br/>' . $_FILES['bestand']['name'];
		$this->beschrijving = $array ['beschrijving'];
		$this->beschrijving_id = $array ['beschrijving_id'];
		$this->taalcode = $array ['taalcode'];
		
		if (exif_imagetype ( $_FILES ['bestand'] ['tmp_name'] )) {
			$this->id = $nextId = getNextPhotoId ();
			
			$original = $_FILES ['bestand'] ['tmp_name'];
			
			if ($this->checkPath ( PHOTOSTOREPATH )) {
				$toPath = PHOTOSTOREPATH . $this->getPath ();
				$destination = $toPath . $nextId . '_full.jpg';
				
				if (copy ( $original, $destination )) {
					exec ( "/usr/local/bin/convert -geometry 40x30 $destination {$toPath}{$nextId}_40px.jpg > /dev/null" );
					exec ( "/usr/local/bin/convert -geometry 90x75 $destination {$toPath}{$nextId}_90px.jpg > /dev/null" );
					exec ( "/usr/local/bin/convert -geometry 120x $destination {$toPath}{$nextId}_120px.jpg > /dev/null" );
					exec ( "/usr/local/bin/convert -geometry 300x $destination {$toPath}{$nextId}_300px.jpg > /dev/null" );
					exec ( "/usr/local/bin/convert -geometry 600x $destination {$toPath}{$nextId}_600px.jpg > /dev/null" );
					
					exec ( "/usr/local/bin/convert -geometry 150x150 $destination /tmp/square.jpg > /dev/null" );
					exec ( "/usr/local/bin/convert -crop 70x70+0+0 -gravity center /tmp/square.jpg {$toPath}{$nextId}_squarepx.jpg > /dev/null" );
					
					//echo "<br/> => " . $destination . " <br/> ";
					//echo "<br/> => " . $toPath . " <br/> ";
					//echo "<br/> => " . $nextId . " <br/> ";
					// /data1/www/reissysteem/rbs_photo/2x/22x/227x/2275x/22753_full.jpg 
					

					//unlink("/tmp/square.jpg");
					//echo "<br/> 1=> " . $this->getPath() . " <br/> " ;
					//echo "<br/> 2=> " . PHOTOSTOREPATH . " <br/> " ;
					// $this->getPath() => 2x/22x/227x/2277x/
					// PHOTOSTOREPATH => /data1/www/reissysteem/rbs_photo/ 
					

					unlink ( $original );
				} else
					$resultArray ['upload'] = "FAIL_MOVE";
			} else
				$resultArray ['upload'] = "FAIL_MKDIR";
		} else
			$resultArray ['upload'] = 'NO_JPG';
		//return $resultArray['upload'];
	}
	
	function getTaalcode() {
		return $this->taalcode;
	}
	
	function delete() {
		@unlink ( PHOTOSTOREPATH . $this->getPath () . $this->getId () . '_40px.jpg' );
		@unlink ( PHOTOSTOREPATH . $this->getPath () . $this->getId () . '_90px.jpg' );
		@unlink ( PHOTOSTOREPATH . $this->getPath () . $this->getId () . '_120px.jpg' );
		@unlink ( PHOTOSTOREPATH . $this->getPath () . $this->getId () . '_300px.jpg' );
		@unlink ( PHOTOSTOREPATH . $this->getPath () . $this->getId () . '_600px.jpg' );
		parent::delete ();
	}
	
	function storeWithId($array = null) {
		$array ['beschrijving_id'] = $this->beschrijving_id;
		$array ['id'] = $this->id;
		$array ['url'] = $this->url;
		$array ['beschrijving'] = $this->beschrijving;
		$array ['naam'] = $this->naam;
		$array ['taalcode'] = $this->taalcode;
		/*
    echo  "<br/>1 =>" . $this->beschrijving_id;
    echo  "<br/>2 =>"  . $this->id;
    echo  "<br/>3 =>" . $this->url;
    echo  "<br/>4 =>" . $this->beschrijving;
    echo  "<br/>6 =>" . $this->taalcode;
    echo  "<br/>5 =>" . $this->naam;
    */
		//$out = print_r ($this, true);
		//mail( "alert@travelgroep.nl", "RW_ XXXX - ",  " " . $out   );
		
		return parent::storeWithId ( $array );
	}
}
?>
<?
/*

  Hotel.php

*/
/*
 * SELECT cast(tsh.id as int), tsh.name, h.vendor_id, h.naam
 * FROM total_stay_hotel tsh left
 * JOIN hotel h ON (h.vendor_id= cast(tsh.id as int))
 * where h.vendor_id is null and h.vendor_key is null*/
function getTotalStayHotels($crit = null) {
	$crit[] = ' ((h.vendor_id is null AND h.vendor_key is null) OR (h.naam != tsh.name)) ';
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else {
		$where = NULL;
	}

	$q = "SELECT tsh.*, h.id as hotel_id, s.id as stad_id FROM total_stay_hotel tsh INNER JOIN locale l ON (l.taalcode='en' and l.naam=tsh.town_city) INNER JOIN stad s ON l.beschrijving_id=s.id LEFT JOIN hotel h ON (h.vendor_id= cast(tsh.id as int)) $where ORDER BY hotel_id DESC";
//	$q = "SELECT tsh.*, h.naam as hotel_name, h.id as hotel_id, h2.naam as hotel2_name, h2.id as hotel2_id  FROM total_stay_hotel tsh LEFT JOIN hotel h ON (h.vendor_id= cast(tsh.id as int)) LEFT JOIN hotel h2 ON (h2.naam = tsh.name and (h2.vendor_id IS Null or h2.vendor_id=0)) $where";

	$res = $GLOBALS ['rsdb']->query ( $q );
	$array = array();
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$hotel = new Hotel();
		$hotel->loadById($row['hotel_id']);
		$row['hotel'] = $hotel;
		$array [$row ['id']] = new TotalStayHotel ( null, $row ); //&
	}
	return $array;
}

function countTotalStayHotels($crit = null) {
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else {
		$where = NULL;
	}

	$q = "SELECT COUNT(DISTINCT s.id) AS num  FROM total_stay_hotel tsh  INNER JOIN locale l ON (l.taalcode='en' and l.naam=tsh.town_city) INNER JOIN stad s ON l.beschrijving_id=s.id $where ";
	//echo $q;
	$res = $GLOBALS ['rsdb']->query ( $q );
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	return 0;
}

class TotalStayHotel extends Webentiteit {

	var $tabelNaam = 'total_stay_hotel';
	var $id;
	var $locale;
	var $name;
	var $geography_level_3id;
	var $geography_level_2id;
	var $address1;
	var $address2;
	var $town_city;
	var $county;
	var $postcode_zip;
	var $country;
	var $telephone;
	var $fax;
	var $latitude;
	var $longitude;
	var $resort;
	var $region;
	var $country1;
	var $property_type;
	var $property_group;
	var $rating;
	var $airport;
	var $thumbnail_url;
	var $image1_url;
	var $image2_url;
	var $image3_url;
	var $image4_url;
	var $image5_url;
	var $image6_url;
	var $image7_url;
	var $image8_url;
	var $image9_url;
	var $image10_url;
	var $product_attributes;
	var $facilities;
	var $description;
	var $vendor_key = 'pronto';
	var $hotel;
	var $stad_id;

	/**
	 * @return mixed
	 */
	public function getStadId()
	{
		return $this->stad_id;
	}

	/**
	 * @param mixed $stad_id
	 */
	public function setStadId($stad_id)
	{
		$this->stad_id = $stad_id;
	}

	/**
	 * @return mixed
	 */
	public function getHotel()
	{
		return $this->hotel;
	}

	/**
	 * @param mixed $hotel
	 */
	public function setHotel(Hotel $hotel)
	{
		$this->hotel = $hotel;
	}

	
	function TotalStayHotel($Stad = null, $array = null) {
		Webentiteit::Webentiteit ();
		$this->Stad = $Stad;
		$this->tabelNaam = 'total_stay_hotel';
		
		if ($array) {
			$this->loadArray ( $array );
		}
		
	
	}

	/**
	 * @return string
	 */
	public function getVendorKey()
	{
		return $this->vendor_key;
	}

	/**
	 * @param string $vendor_key
	 */
	public function setVendorKey($vendor_key)
	{
		$this->vendor_key = $vendor_key;
	}


	
	function loadArray($array = null) {
		$this->id = $array ['id'];
		$this->locale = $array ['locale'];
		$this->name = $array ['name'];
		$this->geography_level_3id = $array ['geography_level_3id'];
		$this->geography_level_2id = $array ['geography_level_2id'];
		$this->address1 = $array ['address1'];
		$this->address2 = $array ['address2'];
		$this->town_city = $array ['town_city'];
		$this->county = $array ['county'];
		$this->postcode_zip = $array ['postcode_zip'];
		$this->country = $array ['country'];
		$this->telephone = $array ['telephone'];
		$this->fax = $array ['fax'];
		$this->latitude = $array ['latitude'];
		$this->longitude = $array ['longitude'];
		$this->resort = $array ['resort'];
		$this->region = $array ['region'];
		$this->country1 = $array ['country1'];
		$this->property_type = $array ['property_type'];
		$this->property_group = $array ['property_group'];
		$this->rating = $array ['rating'];
		$this->airport = $array ['airport'];
		$this->thumbnail_url = $array ['thumbnail_url'];
		$this->image1_url = $array ['image1_url'];
		$this->image2_url = $array ['image2_url'];
		$this->image3_url = $array ['image3_url'];
		$this->image4_url = $array ['image4_url'];
		$this->image5_url = $array ['image5_url'];
		$this->image6_url = $array ['image6_url'];
		$this->image7_url = $array ['image7_url'];
		$this->image8_url = $array ['image8_url'];
		$this->image9_url = $array ['image9_url'];
		$this->image10_url = $array ['image10_url'];
		$this->product_attributes = $array ['product_attributes'];
		$this->facilities = $array ['facilities'];
		$this->description = $array ['description'];
		$this->hotel = $array ['hotel'];
		$this->stad_id = $array ['stad_id'];

		parent::loadArray ( $array );
	}

	/**
	 * @return string
	 */
	public function getTabelNaam()
	{
		return $this->tabelNaam;
	}

	/**
	 * @param string $tabelNaam
	 */
	public function setTabelNaam($tabelNaam)
	{
		$this->tabelNaam = $tabelNaam;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getLocale()
	{
		return $this->locale;
	}

	/**
	 * @param mixed $locale
	 */
	public function setLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getGeographyLevel3id()
	{
		return $this->geography_level_3id;
	}

	/**
	 * @param mixed $geography_level_3id
	 */
	public function setGeographyLevel3id($geography_level_3id)
	{
		$this->geography_level_3id = $geography_level_3id;
	}

	/**
	 * @return mixed
	 */
	public function getGeographyLevel2id()
	{
		return $this->geography_level_2id;
	}

	/**
	 * @param mixed $geography_level_2id
	 */
	public function setGeographyLevel2id($geography_level_2id)
	{
		$this->geography_level_2id = $geography_level_2id;
	}

	/**
	 * @return mixed
	 */
	public function getAddress1()
	{
		return $this->address1;
	}

	/**
	 * @param mixed $address1
	 */
	public function setAddress1($address1)
	{
		$this->address1 = $address1;
	}

	/**
	 * @return mixed
	 */
	public function getAddress2()
	{
		return $this->address2;
	}

	/**
	 * @param mixed $address2
	 */
	public function setAddress2($address2)
	{
		$this->address2 = $address2;
	}

	/**
	 * @return mixed
	 */
	public function getTownCity()
	{
		return $this->town_city;
	}

	/**
	 * @param mixed $town_city
	 */
	public function setTownCity($town_city)
	{
		$this->town_city = $town_city;
	}

	/**
	 * @return mixed
	 */
	public function getCounty()
	{
		return $this->county;
	}

	/**
	 * @param mixed $county
	 */
	public function setCounty($county)
	{
		$this->county = $county;
	}

	/**
	 * @return mixed
	 */
	public function getPostcodeZip()
	{
		return $this->postcode_zip;
	}

	/**
	 * @param mixed $postcode_zip
	 */
	public function setPostcodeZip($postcode_zip)
	{
		$this->postcode_zip = $postcode_zip;
	}

	/**
	 * @return mixed
	 */
	public function getCountry()
	{
		return $this->country;
	}

	/**
	 * @param mixed $country
	 */
	public function setCountry($country)
	{
		$this->country = $country;
	}

	/**
	 * @return mixed
	 */
	public function getTelephone()
	{
		return $this->telephone;
	}

	/**
	 * @param mixed $telephone
	 */
	public function setTelephone($telephone)
	{
		$this->telephone = $telephone;
	}

	/**
	 * @return mixed
	 */
	public function getFax()
	{
		return $this->fax;
	}

	/**
	 * @param mixed $fax
	 */
	public function setFax($fax)
	{
		$this->fax = $fax;
	}

	/**
	 * @return mixed
	 */
	public function getLatitude()
	{
		return $this->latitude;
	}

	/**
	 * @param mixed $latitude
	 */
	public function setLatitude($latitude)
	{
		$this->latitude = $latitude;
	}

	/**
	 * @return mixed
	 */
	public function getLongitude()
	{
		return $this->longitude;
	}

	/**
	 * @param mixed $longitude
	 */
	public function setLongitude($longitude)
	{
		$this->longitude = $longitude;
	}

	/**
	 * @return mixed
	 */
	public function getResort()
	{
		return $this->resort;
	}

	/**
	 * @param mixed $resort
	 */
	public function setResort($resort)
	{
		$this->resort = $resort;
	}

	/**
	 * @return mixed
	 */
	public function getRegion()
	{
		return $this->region;
	}

	/**
	 * @param mixed $region
	 */
	public function setRegion($region)
	{
		$this->region = $region;
	}

	/**
	 * @return mixed
	 */
	public function getCountry1()
	{
		return $this->country1;
	}

	/**
	 * @param mixed $country1
	 */
	public function setCountry1($country1)
	{
		$this->country1 = $country1;
	}

	/**
	 * @return mixed
	 */
	public function getPropertyType()
	{
		return $this->property_type;
	}

	/**
	 * @param mixed $property_type
	 */
	public function setPropertyType($property_type)
	{
		$this->property_type = $property_type;
	}

	/**
	 * @return mixed
	 */
	public function getPropertyGroup()
	{
		return $this->property_group;
	}

	/**
	 * @param mixed $property_group
	 */
	public function setPropertyGroup($property_group)
	{
		$this->property_group = $property_group;
	}

	/**
	 * @return mixed
	 */
	public function getRating()
	{
		return $this->rating;
	}

	/**
	 * @param mixed $rating
	 */
	public function setRating($rating)
	{
		$this->rating = $rating;
	}

	/**
	 * @return mixed
	 */
	public function getAirport()
	{
		return $this->airport;
	}

	/**
	 * @param mixed $airport
	 */
	public function setAirport($airport)
	{
		$this->airport = $airport;
	}

	/**
	 * @return mixed
	 */
	public function getThumbnailUrl()
	{
		return $this->thumbnail_url;
	}

	/**
	 * @param mixed $thumbnail_url
	 */
	public function setThumbnailUrl($thumbnail_url)
	{
		$this->thumbnail_url = $thumbnail_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage1Url()
	{
		return $this->image1_url;
	}

	/**
	 * @param mixed $image1_url
	 */
	public function setImage1Url($image1_url)
	{
		$this->image1_url = $image1_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage2Url()
	{
		return $this->image2_url;
	}

	/**
	 * @param mixed $image2_url
	 */
	public function setImage2Url($image2_url)
	{
		$this->image2_url = $image2_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage3Url()
	{
		return $this->image3_url;
	}

	/**
	 * @param mixed $image3_url
	 */
	public function setImage3Url($image3_url)
	{
		$this->image3_url = $image3_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage4Url()
	{
		return $this->image4_url;
	}

	/**
	 * @param mixed $image4_url
	 */
	public function setImage4Url($image4_url)
	{
		$this->image4_url = $image4_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage5Url()
	{
		return $this->image5_url;
	}

	/**
	 * @param mixed $image5_url
	 */
	public function setImage5Url($image5_url)
	{
		$this->image5_url = $image5_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage6Url()
	{
		return $this->image6_url;
	}

	/**
	 * @param mixed $image6_url
	 */
	public function setImage6Url($image6_url)
	{
		$this->image6_url = $image6_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage7Url()
	{
		return $this->image7_url;
	}

	/**
	 * @param mixed $image7_url
	 */
	public function setImage7Url($image7_url)
	{
		$this->image7_url = $image7_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage8Url()
	{
		return $this->image8_url;
	}

	/**
	 * @param mixed $image8_url
	 */
	public function setImage8Url($image8_url)
	{
		$this->image8_url = $image8_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage9Url()
	{
		return $this->image9_url;
	}

	/**
	 * @param mixed $image9_url
	 */
	public function setImage9Url($image9_url)
	{
		$this->image9_url = $image9_url;
	}

	/**
	 * @return mixed
	 */
	public function getImage10Url()
	{
		return $this->image10_url;
	}

	/**
	 * @param mixed $image10_url
	 */
	public function setImage10Url($image10_url)
	{
		$this->image10_url = $image10_url;
	}

	/**
	 * @return mixed
	 */
	public function getProductAttributes()
	{
		return $this->product_attributes;
	}

	/**
	 * @param mixed $product_attributes
	 */
	public function setProductAttributes($product_attributes)
	{
		$this->product_attributes = $product_attributes;
	}

	/**
	 * @return mixed
	 */
	public function getFacilities()
	{
		return $this->facilities;
	}

	/**
	 * @param mixed $facilities
	 */
	public function setFacilities($facilities)
	{
		$this->facilities = $facilities;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function updateHotel ()
	{
		$array = array();
		$hotel = $this->getHotel();
		$array ['stad_id'] = $this->getStadId ();
		$array ['naam'] = $this->getName();
//		$array ['zitplaatsen'] = $this->getPostcodeZip();
		$array ['rating'] = $this->getRating ();
		$array ['vendor_id'] = ( int ) $this->getId ();
		$array ['vendor_key'] = $this->getVendorKey();
		$array ['addr1'] = '<p>'.$this->getAddress1 ().', '. $this->getPostcodeZip().' ' . $this->getTownCity(). ' '. $this->getCountry(). ' Tel. ' . $this->getTelephone(). ($this->getFax()?' Fax. '.$this->getFax():'') .'</p>';
		$array ['longitude'] = $this->getLongitude ();
		$array ['latitude'] = $this->getLatitude ();

//		$array ['type'] = $this->getType ();

//		$array ['eenpersoonskamer_toeslag'] = $this->getEenpersoonskamerToeslag ();
//		$array ['transfer_resort_id'] = $this->getTransferResortId ();
//
//		$array ['budget_rating'] = $this->getBudgetRating();
//		$array ['luxury_rating'] = $this->getLuxuryRating();
//		$array ['distance_to_stadium_rating'] = $this->getDistanceToStadiumRating() ;
//		$array ['distance_to_city_center_rating'] = $this->getDistanceToCityCenterRating();

		$hotel = new Hotel();
		$hotel_id = isAdded($this->getId(), $this->getVendorKey());
		if($hotel_id) {
			$array['id'] = $hotel_id;
			$hotel->loadById($hotel_id);
			$hotel->loadArray($array);
			$hotel->storeNoLocale();
			$locales = getLocalesByBeschrijvingId($hotel_id);
			foreach($locales as $locale)
			{
				$locale->setNaam($this->getName());
				$locale->setSeolabel(seo($this->getName()));
				$locale->store();
			}
		}
		else
		{
			$result = $hotel->post ( $array );
			if (! $result)
				$hotel->store ();
		}
	}

	function loadById($id) {
		if ($id)
		{
//      print_r( $GLOBALS['rsdb'] );

//debug_print_backtrace();
			$q = $this->db->query("SELECT tsh.*, s.id as stad_id FROM {$this->tabelNaam} tsh INNER JOIN locale l ON (l.taalcode='en' and l.naam=tsh.town_city) INNER JOIN stad s ON l.beschrijving_id=s.id WHERE tsh.id='{$id}'");
			$r = pg_fetch_array($q, NULL, PGSQL_ASSOC);
			$hotel = new Hotel();
			$hotel->loadById($r['hotel_id']);
			$r['hotel'] = $hotel;
			$this->loadArray($r);
		}
	}
}
?>
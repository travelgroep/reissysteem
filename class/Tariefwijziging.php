<?
/*

  Tariefwijziging.php

*/

class Tariefwijziging extends DBRecord
{
  var $hotel_id;
  var $naam;
  var $percentage;
  var $datum_start;
  var $datum_eind;
  var $event_id;
  var $arrangement_id;
  var $actief;
  
  function Tariefwijziging( $array = null)
  {
    DBRecord::DBRecord( 'rsdb' );
    $this->tabelNaam = 'tariefwijziging'; 

    $this->hotel_id = $array['hotel_id'];
    $this->percentage = $array['percentage'];
    $this->naam = $array['naam'];
    $this->id = $array['id'];
    $this->datum_start = $array['datum_start'];
    $this->datum_eind = $array['datum_eind'];
    $this->event_id = $array['event_id'];
    $this->arrangement_id = $array['arrangement_id'];
    $this->actief = $array['actief'];
  }

  function getNaam()
  {
    return $this->naam;
  }

  function getDatumStart()
  {
    return $this->datum_start;
  }

  function getDatumEind()
  {
    return $this->datum_eind;
  }

  function getPercentage()
  {
    return $this->percentage;
  }

  function getEventId()
  {
    return $this->event_id;
  }

  function getArrangementId()
  {
    return $this->arrangement_id;
  }

  function getHotelId()
  {
    return $this->hotel_id;
  }

  function getActief()
  {
    return $this->actief;
  }

  function loadArray($array = null)
  {
    $this->hotel_id = $array['hotel_id'];
    $this->percentage = $array['percentage'];
    $this->naam = $array['naam'];
    $this->id = $array['id'];
    $this->datum_start = $array['datum_start'];
    $this->datum_eind = $array['datum_eind'];
    $this->event_id = $array['event_id'];
    $this->actief = $array['actief'];
  }

  function store($array = null)
  {
    $resultArray = array();
    $array['id'] = $this->id;
    $array['hotel_id'] = $this->hotel_id;
    $array['naam'] = $this->naam;
    $array['percentage'] = $this->percentage;
    $array['datum_start'] = $this->datum_start;
    $array['datum_eind'] = $this->datum_eind;
    $array['event_id'] = $this->event_id;
    $array['arrangement_id'] = $this->arrangement_id;
    $array['actief'] = $this->actief;
        
    $resultArray = parent::store($array);
    return $resultArray;
  }

  function post($array)
  {
    $resultArray = array();
    
    $this->hotel_id = $array['hotel_id'];
    $this->percentage = $array['percentage'];
    $this->naam = $array['naam'];
    $this->datum_start = $array['datum_start'];
    $this->datum_eind = $array['datum_eind'];
    $this->event_id = $array['event_id'];

    $this->actief = $array['actief']=='on'?'t':'f';

/*
    if (!$array['naam']) $resultArray['naam']='EMPTY';
    if (!$array['waarde']) $resultArray['waarde']='EMPTY';
*/

    return $resultArray;
  }

  function render()
  {
    echo '<tr>';
    echo '<td><input name="tariefwijziging_naam_' . $this->getId() . '" style="width:80px" type="text" value="' . $this->getNaam() . '"></input></td><td><input onclick="insertDate(event, this)" value="' . $this->getDatumStart(). '" name="tariefwijziging_datum_start_' . $this->getId() . '" style="width:60px" type="text"></input></td><td><input onclick="insertDate(event, this)" value="' . $this->getDatumEind(). '" name="tariefwijziging_datum_eind_' . $this->getId() . '" style="width:60px" type="text"></input><td><input value="' . $this->getPercentage(). '" name="tariefwijziging_percentage_' . $this->getId() . '" type="text" style="width:40px"></input></td>';
    echo '<td>';
    echo '<select style="width:80px" name="tariefwijziging_event_id_' . $this->getId() . '">';
    echo '<option value="0"';
    if ( $this->getEventId() == 0 ) echo ' selected';
    echo '>alle</option>';

    $hotel = new Hotel( );
    $hotel->loadById( $this->getHotelId() );
    
    $events = getEventsByStadId( $hotel->getStadId() );

    foreach ( $events as $event )
    {
      $selected = $event->getId() == $this->getEventId() ? 'selected': '';
      echo '<option ' . $selected . ' value="' . $event->getId() . '">' . $event->getNaam() . '</option>';
    }

    echo '</select>';
    echo '</td>';
    echo '<td>';
    
    if ( $this->getActief() == 't' )
    {
      $checked = 'checked="checked"';
    }
    echo '<input name="tariefwijziging_actief_' . $this->getId() . '" style="width:20px" type="checkbox" ' . $checked . '></input>';
    echo '<a style="cursor:pointer" onclick="deleteTariefwijziging(' . $this->getId() . ', ' . $this->getHotelId() . ')"><img src="../i/del.gif" /></a>';
    echo '</td>';
    echo '<input type="hidden" name="tariefwijziging_id[]" value="' . $this->getId() . '"></input>';
    echo '</tr>';
  }
}
?>
<?php

class OptaMatchController
{
    const URL_CONFIRM_ACTION = 'confirm';
    const URL_UNCONFIRM_ACTION = 'unconfirm';

    const URL_UPDATE_ENABLE_ACTION = 'enable_update';
    const URL_UPDATE_DISABLE_ACTION = 'disable_update';

    const URL_PREVIEW_CONFIRM_PERIOD = 'preview_confirm_period';
    const URL_CONFIRM_PERIOD = 'confirm_period';

    const URL_UPDATE_ARRANGEMENT = 'update_arrangement';
    
    public function __construct()
    {
        $this->db = new DB();
    }
    
    public function confirmAction()
    {
        $id = $_GET['matchId'];
        $this->setConfirmed($id, true);
        
        $redirectUrl =  $_SERVER['HTTP_REFERER'];
        header("Location: $redirectUrl");        
    }

    public function unconfirmAction()
    {
        $id = $_GET['matchId'];
        $this->setConfirmed($id, false);
        
        $redirectUrl =  $_SERVER['HTTP_REFERER'];
        header("Location: $redirectUrl");        
    }
    
    public function enableUpdateAction()
    {
        $id = $_GET['matchId'];
        $this->setUpdateEnable($id, true);
        
        $redirectUrl =  $_SERVER['HTTP_REFERER'];
        header("Location: $redirectUrl");        
    }

    public function disableUpdateAction()
    {
        $id = $_GET['matchId'];
        $this->setUpdateEnable($id, false);
        
        $redirectUrl =  $_SERVER['HTTP_REFERER'];
        header("Location: $redirectUrl");        
        
    }
    
   
    public function validateConfirmForm()
    {
        if (!isset($_POST['confirm'])) {
            print "Confirm value is not set";
            exit;
        } else {
            if ($_POST['confirm'] == 'true') {
                $confirm = true;
            } elseif ($_POST['confirm'] == 'false') {
                $confirm = false;
            } else {
                print "Confirm value is not valid";
                exit;
            }
        }
        
        if (!isset($_POST['competitionId'])) {
            print "CompetitionId value is not set";
            exit;
        } else 
        {
            $competitionId = $_POST['competitionId'];
        }

        if (!isset($_POST['fromDate'])) {
            print "From value is not set";
            exit;
        } else {
            $fromDateString = $_POST['fromDate'];
        }

        if (!isset($_POST['toDate'])) {
            print "To value is not set";
            exit;
        } else {
            $toDateString = $_POST['toDate'];
        }

        $fromDate = new \DateTime($fromDateString);
        $fromDate->setTime(0, 0, 0);
        $toDate = new \DateTime($toDateString);
        $toDate->setTime(0, 0, 0);

        return array(
            'confirm' => $confirm,
            'competitionId' => $competitionId,
            'fromDate' => $fromDate,
            'toDate' => $toDate
        );
    }

    public function previewConfirmPeriodAction()
    {
        $parameters = $this->validateConfirmForm();

        $confirm = $parameters['confirm'];
        $competitionId = $parameters['competitionId'];
        $fromDate = $parameters['fromDate'];
        $toDate = $parameters['toDate'];
        
        $matchModelManager = new MatchModelManager();

        $matches = $matchModelManager->findByPeriod($competitionId, $fromDate, $toDate);
        
        $competitionModelManager= new CompetitionModelManager();
        $competition = $competitionModelManager->find($competitionId);
        
        return array(
            'matches' => $matches,
            'competition' => $competition,
            'confirm' => $confirm,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'referer' => $_SERVER['HTTP_REFERER']
        );
    }
    
    public function confirmPeriodAction()
    {
        $referer = $_POST['referer'];        
        $parameters = $this->validateConfirmForm();

        $confirm = $parameters['confirm'];
        $competitionId = $parameters['competitionId'];
        $fromDate = $parameters['fromDate'];
        $toDate = $parameters['toDate'];
        
        $matchModelManager = new MatchModelManager();

        $matchModelManager->setConfirmedByPeriod($competitionId, $fromDate, $toDate, $confirm);
        
        $redirectUrl =  $referer;
        header("Location: $redirectUrl");        
    }

    public function updateArrangementAction()
    {
        $competitionId = $_GET['competitionId'];
        $commandTemplate = OPTA_UPDATE_COMPETITION_COMMAND_TEMPLATE;
        $command = sprintf($commandTemplate, $competitionId);
        
        exec($command, $output);
        
        return array(
            'commandOutput' => $output,
            'referer' => $_SERVER['HTTP_REFERER']
        );
    }
    
    public static function generateUrl($name, $urlPath = '')
    {
        switch($name) {
            case self::URL_CONFIRM_ACTION:
                $fileName = 'confirm.php';
                break;
                
            case self::URL_UNCONFIRM_ACTION:
                $fileName = 'unconfirm.php';
                break;

            case self::URL_UPDATE_ENABLE_ACTION:
                $fileName = 'enable-update.php';
                break;
                
            case self::URL_UPDATE_DISABLE_ACTION:
                $fileName = 'disable-update.php';
                break;
                
            case self::URL_PREVIEW_CONFIRM_PERIOD:
                $fileName = 'preview-confirm-period.php';
                break;
                
            case self::URL_CONFIRM_PERIOD:
                $fileName = 'confirm-period.php';
                break;
            
            case self::URL_UPDATE_ARRANGEMENT:
                $fileName = 'update-arrangement.php';
                break;
            
        }
        
        return $urlPath.$fileName;
    }
  
    protected function setConfirmed($id, $confirmed) {
        $matchModelManager = new MatchModelManager();
        $match = $matchModelManager->find($id);
        
        $match->setDateConfirmed($confirmed);
        $matchModelManager->save(array($match));
    }
    
    protected function setUpdateEnable($id, $enable) {
        $matchModelManager = new MatchModelManager();
        $match = $matchModelManager->find($id);
        
        $match->setUpdateEnable($enable);
        $matchModelManager->save(array($match));
    }
    
}

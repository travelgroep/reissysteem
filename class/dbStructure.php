<?

// Dit is de minimale Database Structuur die nodig is voor het CMS zodat het werkt.
// Orders en Reisopties bijvoorbeeld staan er niet in!!! 


function relExists( $rel )
{
	
	$db = $GLOBALS['db'];
	$result = pg_query( $db, "SELECT relname FROM pg_class WHERE relname = '$rel'" );
	return (pg_num_rows( $result ) > 0);		
}

function colExists( $col, $table )
{
	
	$db = $GLOBALS['db'];
	$result = pg_query( $db, "SELECT $col FROM $table" );
	return (pg_num_rows( $result ) > 0);		
}


function setPrimaryKey( $table, $col )
{
		$db = $GLOBALS['db'];
		pg_query( $db, "
				ALTER TABLE ONLY $table
    			ADD CONSTRAINT ".$table."_pkey PRIMARY KEY ($col)
    ");
}

function createUser( $user )
{
		// Maak $user aan met standaard wachtwoord 'blaat'
		$db = $GLOBALS['db'];
		pg_query( $db, "
				INSERT INTO users VALUES(
				    nextval('users_id_seq'),
				    'startup',
				    '".$user."',
				    'initiele data',
				    'blaat'
				)
		" );
		print "created user =>$user<br />\n";
}

function createGroup( $group )
{
		// Maak $group aan
		$db = $GLOBALS['db'];
		pg_query( $db, "
				INSERT INTO groups VALUES (
				    nextval('groups_id_seq'),
				    '".$group."' ,
				    'initial group'
				)
		" );
		print "created Group => ".$group."<br />\n";
}

function setUserGroup( $user, $group )
{
		// Voeg $user toe aan $group
		$db = $GLOBALS['db'];
		pg_query( $db, "
				INSERT INTO groupsusers VALUES(
				    ( SELECT id FROM groups WHERE name = '".$group."' ) ,
				    ( SELECT id FROM users WHERE name = '".$user."' )
				)
		" );
		print $user." -> ".$group." relation inserted<br />\n";	
}

function createHomePage( $page )
{
		// Maak eerste 'Home' pagina met standaard plane
		$db = $GLOBALS['db'];
		pg_query( $db, "
				INSERT INTO page VALUES (
				    nextval('page_id_seq'),
				    '',
				    '1',
				    'Home',
				    FALSE,
				    FALSE,
				    'nav2',
				    TRUE,
				    ''
				)
		" );
		print "created page => ".$page."<br />\n";
		
		pg_query( $db, "
				INSERT INTO plane VALUES(
				    nextval('plane_id_seq'),
				    'standard',
				    (SELECT id FROM page WHERE title = 'Home' ),
				    'position',
				    ''
				)
		" );	
		print "page plane type set for => ".$page."<br />\n";
}

function checkDatabaseStructure(  )
{
	$db = $GLOBALS[ 'db' ];
	
	$GLOBALS[ 'db' ] = $db;
	
	// TABLE
	$table = "article";
	if ( !relExists( $table ) )
	{
		// SEQUENCE
		pg_query( $db, "
				CREATE SEQUENCE content_id_seq
				    INCREMENT BY 1
				    NO MAXVALUE
				    NO MINVALUE
				    CACHE 1
		" );
		print "created sequence => content_id_seq<br />\n";
		
		pg_query( $db, "
				CREATE TABLE article (
			    id integer DEFAULT nextval('content_id_seq') NOT NULL,
			    prev integer,
			    \"next\" integer,
			    title text,
			    content text,
			    picture_id integer,
			    plane_id integer,
			    \"position\" integer,
			    link integer,
			    link_descr character varying(20),
			    \"type\" character varying(12)
				)
		" );
		setPrimaryKey( $table , "id" );
		print "created table => ".$table."<br />\n";
	}
	
	// TABLE
	$table = "attributes";
	if ( !relExists( $table ) )
	{
		pg_query( $db, "
				CREATE TABLE attributes (
			    article_id integer NOT NULL,
			    attribute text,
			    value text
				)	
		" );
		print "created table => ".$table."<br />\n";
	}

	// TABLE
	$table = "auth";
	if ( !relExists( $table ) )
	{
		pg_query( $db, "
				CREATE TABLE auth (
				    group_id integer NOT NULL,
				    page_id integer NOT NULL,
				    \"read\" boolean,
				    \"write\" boolean
				)
		" );
		setPrimaryKey( $table, "group_id, page_id"  );
		print "created table => ".$table."<br />\n";
	}
	
	// TABLE
	$table = "foto";
	if ( !relExists( $table ) )
	{
		pg_query( $db, "
				CREATE SEQUENCE foto_id_seq
				    INCREMENT BY 1
				    NO MAXVALUE
				    NO MINVALUE
				    CACHE 1
		" );
		print "created sequence => ".$table."<br />\n";
		pg_query( $db, "
				CREATE TABLE foto (
				    id integer DEFAULT nextval('foto_id_seq') NOT NULL,
				    name text,
				    datum date,
				    article_id integer NOT NULL
				)
		" );
		print "created table => ".$table."<br />\n";
	}
	
	// TABLE
	$table = "groups";
	if ( !relExists( $table ) )
	{
		
		pg_query( $db, "
				CREATE SEQUENCE groups_id_seq
				    INCREMENT BY 1
				    NO MAXVALUE
				    NO MINVALUE
				    CACHE 1
		" );
		print "created sequence => ".$table."<br />\n";
		pg_query( $db, "
				CREATE TABLE groups (
				    id integer DEFAULT nextval('groups_id_seq') NOT NULL,
				    name text,
				    descr text
				)
		" );
		setPrimaryKey( $table , "id" );		
		print "created table => ".$table."<br />\n";
	}
	
	// TABLE
	$table = "groupsusers";
	if ( !relExists( $table ) )
	{
		
		pg_query( $db, "
				CREATE TABLE groupsusers (
				    group_id integer NOT NULL,
				    user_id integer NOT NULL
				)
		" );
		print "created table => ".$table."<br />\n";	
		
	}

	// TABLE
	$table = "page";
	if ( !relExists( $table ) )
	{
		
		pg_query( $db, "
				CREATE SEQUENCE page_id_seq
				    INCREMENT BY 1
				    NO MAXVALUE
				    NO MINVALUE
				    CACHE 1
		" );
		print "created sequence => ".$table."<br />\n";		
		pg_query( $db, "
				CREATE TABLE page (
				    id integer DEFAULT nextval('page_id_seq') NOT NULL,
				    name character varying(12),
				    descr text,
				    title text,
				    nav1 boolean,
				    nav2 boolean,
				    nav character varying(4),
				    \"index\" boolean,
				    layout text,
				    picture_id integer
				)
		" );
		setPrimaryKey( $table , "id" );
		print "created table => ".$table."<br />\n";
					
	}
	$col = "picture_id";
	if ( !colExists( $col , $table ) )
	{
		pg_query( $db, "
				ALTER TABLE $table ADD COLUMN $col integer" );
		print "added column $col <br />\n";
	}
	
	// TABLE
	$table = "pictures";
	if ( !relExists( $table ) )
	{
		
		pg_query( $db, "
				CREATE SEQUENCE pictures_id_seq
				    INCREMENT BY 1
				    NO MAXVALUE
				    NO MINVALUE
				    CACHE 1

		" );
		print "created sequence => ".$table."<br />\n";		
		pg_query( $db, "
				CREATE TABLE pictures (
				    id integer DEFAULT nextval('pictures_id_seq') NOT NULL,
				    name character varying(30),
				    descr text
				)
		" );
		setPrimaryKey( $table , "id" );
		print "created table => ".$table."<br />\n";
	}
		
	// TABLE
	$table = "plane";
	if ( !relExists( $table ) )
	{
		
		pg_query( $db, "
				CREATE SEQUENCE plane_id_seq
				    INCREMENT BY 1
				    NO MAXVALUE
				    NO MINVALUE
				    CACHE 1
		" );
		print "created sequence => ".$table."<br />\n";		
		pg_query( $db, "
				CREATE TABLE plane (
				    id integer DEFAULT nextval('plane_id_seq') NOT NULL,
				    name character varying(15),
				    page_id integer,
				    \"type\" text,
				    crit text
				)
		" );
		setPrimaryKey( $table , "id" );
		print "created table => ".$table."<br />\n";
	}
	
	// TABLE
	$table = "users";
	if ( !relExists( $table ) )
	{
		
		pg_query( $db, "
				CREATE SEQUENCE users_id_seq
				    INCREMENT BY 1
				    NO MAXVALUE
				    NO MINVALUE
				    CACHE 1
		" );
		print "created sequence => ".$table."<br />\n";		
		pg_query( $db, "
				CREATE TABLE users (
				    id integer DEFAULT nextval('users_id_seq') NOT NULL,
				    descr text,
				    name text,
				    fullname text,
				    pass text
				)
		" );
		setPrimaryKey( $table , "id" );
		print "created table => ".$table."<br />\n";

		createUser( "Visitor" );
		createUser( "Carlos" );
		
		createGroup( "Visitors" );
		createGroup( "Admin" );
		
		setUserGroup( "Carlos", "Admin" );
		setUserGroup( "Visitor", "Visitors" );
		
		createHomePage( "Home" );
		
		print "<br /><br /><b>Database is ready!</b>\n";
				
	}	
}

?>

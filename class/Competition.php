<?php 

class Competition {
    
  var $id;
  var $name;
  var $seasonId;
  var $optaId;
  var $code;
  
  function loadArray($array = null){
  	
    $this->id = $array ['id']; 
    $this->name = $array ['name']; 
    $this->optaId = $array ['opta_id']; 
    $this->seasonId = $array ['season_id']; 
    $this->code = $array ['code']; 
    
  }
/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

/**
	 * @return the $seasonId
	 */
	public function getSeasonId() {
		return $this->seasonId;
	}

/**
	 * @return the $optaId
	 */
	public function getOptaId() {
		return $this->optaId;
	}

/**
	 * @return the $code
	 */
	public function getCode() {
		return $this->code;
	}

/**
	 * @param $id the $id to set
	 */
	public function setId($id) {
		$this->id = $id;
	}

/**
	 * @param $name the $name to set
	 */
	public function setName($name) {
		$this->name = $name;
	}

/**
	 * @param $seasonId the $seasonId to set
	 */
	public function setSeasonId($seasonId) {
		$this->seasonId = $seasonId;
	}

/**
	 * @param $optaId the $optaId to set
	 */
	public function setOptaId($optaId) {
		$this->optaId = $optaId;
	}

/**
	 * @param $code the $code to set
	 */
	public function setCode($code) {
		$this->code = $code;
	}

 


  
}

<?php 

class Venue {
    
  var $id;
  var $name;
  var $optaId;
  var $city;     
  
  function loadArray($array = null){
    $this->id = $array ['id']; 
    $this->name = $array ['name']; 
    
    $this->optaId = $array ['opta_id']; 
    $this->city= $array ['city']; 
    
    
  }
/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

/**
	 * @return the $optaId
	 */
	public function getOptaId() {
		return $this->optaId;
	}

/**
	 * @return the $city
	 */
	public function getCity() {
		return $this->city;
	}

/**
	 * @param $id the $id to set
	 */
	public function setId($id) {
		$this->id = $id;
	}

/**
	 * @param $name the $name to set
	 */
	public function setName($name) {
		$this->name = $name;
	}

/**
	 * @param $optaId the $optaId to set
	 */
	public function setOptaId($optaId) {
		$this->optaId = $optaId;
	}

/**
	 * @param $city the $city to set
	 */
	public function setCity($city) {
		$this->city = $city;
	}

 



  
}

<?
/*

  implementatie Twinfield

*/

define( 'TYPE_VLIEGTICKETS', 50100 );	
define( 'TYPE_HOTELS', 50110 ); 	
define( 'TYPE_VOETBALKAARTEN', 50120 );	
define( 'TYPE_MUSICALKAARTEN', 50121 );	
define( 'TYPE_OPERAKAARTEN', 50122 );	
define( 'TYPE_CONCERTKAARTEN', 50123 );	
define( 'TYPE_TENNISKAARTEN', 50124 );	

define( 'CODE_VOETBALKAARTEN', 7527 );	
define( 'CODE_MUSICALKAARTEN', 6207 );	
define( 'CODE_OPERAKAARTEN', 6402 );
define( 'CODE_CONCERTKAARTEN', 5204 );
define( 'CODE_TENNISKAARTEN', 6908  );

define( 'TYPE_HOTELVLUCHT', 50130 );	
define( 'TYPE_EXTRAS', 50140 );	
define( 'TYPE_STADSKAARTEN',  50210 );	
define( 'TYPE_VERZEKERINGEN', 50500 );
define( 'BTW_MULTI', 1.19 );
	
define( 'METHOD_IDEAL', 1301 );
define( 'METHOD_CREDITCARD', 2501 );

$item2twinfieldid = array( 
	'Stadium.it'=>											 '0000',
	'Stadium'=>   											 '0000',
  'Air Berlin'=>												5005,
  'Aer Lingus'=>												5010,
  'Brussels Airlines'=>									5106,
  'Fly VLM'=>														7101,
  'Flybe'=>															5501,
  'German Wings'=>											5607,
  'Jet 2'=>															5901,
  'LOT'=>																6101,
  'Malev Hungarian Airlines'=>					6205,
  'Meridiana'=>													6206,
  'Spanair'=>														6802,
  'Test Carrier'=>											5001,
  'Virgin Express'=>										7105,
  'Volare'=>														7106,
  'Volareweb'=>													7106,
  'BMI'=>																5101,
  'BMIbaby'=>														5101,
  'British Airways'=>										5103,
  'British Airways (BA)'=>							5103,
  'British Midland (BMI)'=>							5101,
  'ClickAir'=>													5203,
  'Clickair'=>													5203,
  'EasyJet'=>														5401,
  'HSV'=>																5702,
  'Iberia'=>														5801,
  'KLM'=>																6002,
  'RyanAir'=>														6702,
  'Ryanair'=>														6702,
  'Transavia'=>													6901,
  'TuiFly'=>														6907,
  'VLM'=>																7101,
  'Vueling'=>														7102,
  'De Europeesche'=>										5408,  
  'pronto'=> 														5402,
	'Aemstelhorst'=>                      5001, 
	'ASP'=>                               5002,
	'ANWB'=>                              5003,
	'BMI'=>                               5101,
	'Booking.com'=>                       5102,
	'British airways'=>                   5103,
	'Bol.com'=>                           5104,
	'Encore Tickets Limited'=>						5109,
	'Encore'=>														5109,
	'Bohemia ticket'=>										5109,
	'CostaVision'=>                       5201,
	'CJ2'=>                               5202,
	'Clickair Airlines'=>                 5203,
	'COSI S.R.L. - TICKET SERVICE'=>			5205,
	'Cosi tickets'=>											5205,
	'culturall.com'=>											5206,
	'Culturall'=>													5206,
	'DHL'=>                               5301,
	'Dell'=>                              5302,
	'Easyjet'=>                           5401,
	'Exclusively Hotels'=>                5402,
	'Exclusively hotels'=>                5402,
	'Expedia.nl'=>                        5403,
	'EDreams.com'=>                       5404,
	'Esatour'=>                           5405,
	'EasyPrint'=>                         5406,
	'Eventfactory'=>											5407,
	'EuroTTicket.com'=>										5407,
	'FIRST CLASS INCOMING'=>							5502,
	'First Class Events Ltd'=>				  	5502,
	'Hostal Santo Domingo'=>              5701,
	'Iberia'=>                            5801,
	'Interopa'=>													5804,
	'ITComplete'=>                        5802,
	'IDEA Centers B.V.'=>                 5803,
	'Jacob Online'=>											5902,
	'Jacob Online Ltd'=>									5902,
	'Jacobres'=>													5902,
	'Keith Prowse (Seatem)'=>             6001,
	'KLM'=>                               6002,
	'Live Dabei'=>                        6102,
	'Live Dabei tours'=>                  6102,
	'Group Line-Tix Lovetheatre'=>				6104,
	'Lovetheatre'=>												6104,	
	'Marktplaats.nl'=>                    6201,
	'Mainevent.es'=>                      6202,
	'Mainevent'=>		                      6202,
	'Mission Impossible Events Limited'=> 6203,
  'Mission Impossible Tickets'=>				6203,
	'MKB Administrateurs'=>               6204,
	'North West Events'=>                 6301,
	'Irish ticket brokers'=>              6301,
	'Nespresso'=>                         6302,
	'Ontwerpstra'=>                       6401,
	'Reprint.nl'=>                        6701,
	'Ryanair'=>                           6702,
	'ROC.nl'=>                            6703,
	'Sky Europe'=>                        6801,
	'Transavia.com'=>                     6901,
	'Thomas Cook'=>                       6902,
  'Thomas Cook Sports'=>								6902,
	'Tradetracker'=>                      6903,
	'TravelConnection'=>									6906,
	'Twinfield'=>                         6904,
	'TW productions'=>                    6905,
	'VLM Airlines'=>                      7101,
	'Vueling Airlines'=>                  7102,
	'VoetbalTravel.nl'=>									7114,
	'Voetbal OnTour'=>                    7103,
	'Voetbal Ontour'=>										7103,
	'Vodafone'=>                          7104,
	'Voss + Votava'=>											7107,
	'VIT Promotion'=>											7109,
	'Where2Guv.com'=>                     7201,
	'Wizz'=>															7203
);         
         

function dom2array($node){
    $result = array();
    if($node->nodeType == XML_TEXT_NODE) {
        $result = $node->nodeValue;
    }
    else {
        if($node->hasAttributes()) {
            $attributes = $node->attributes;
            if(!is_null($attributes)) 
                foreach ($attributes as $index=>$attr) 
                    $result[$attr->name] = $attr->value;
        }
        if ( $node->textContent )
        {
          $result['textContent'] = $node->textContent;
        }
        if($node->hasChildNodes()){
            $children = $node->childNodes;
            for($i=0;$i<$children->length;$i++) {
                $child = $children->item($i);
                if($child->nodeName != '#text')
                if(!isset($result[$child->nodeName]))
                    $result[$child->nodeName] = dom2array($child);
                else {
                    $aux = $result[$child->nodeName];
                    $result[$child->nodeName] = array( $aux );
                    $result[$child->nodeName][] = dom2array($child);
                }
            }
        }
    }
    return $result;
} 

class Twinfield extends SoapClient
{
  var $url = "https://login.twinfield.com/webservices/session.asmx?WSDL";

  var $params = array
  (
    'user' => 'Reinier',
    'password' => 'Thomas20',
    'organisation' => 'Aemstelhorst' 
  );
  
  var $logon;
  var $cluster;
  var $sessionID;
  var $header;

//  var $url = "https://login.twinfield.com/SOAP/Redirect.wsdl";

  var $options = array(									
    "trace"      => 1,
    //"exceptions" => 0,
    //"user_agent" =>"SOAP/php-5",
  );
  
  var $client;
  
  function __construct()
  {
    parent::__construct( $this->url, $this->options );
    $this->logon = $this->logon( $this->params );

    $this->cluster = $this->logon->cluster;

    $qq = new domDocument();
    $qq->loadXML( $this->__getLastResponse() );
    $this->sessionID = $qq->getElementsByTagName('SessionID')->item(0)->textContent;

    $newurl = $this->cluster . '/webservices/processxml.asmx?wsdl';
    try 
    {
      $this->client = new SoapClient( $newurl );
      $this->header = new SoapHeader('http://www.twinfield.com/', 'Header', array('SessionID'=> $this->sessionID));
    }
    catch( SoapFault $e )
    {
      echo $e->getMessage();
    }
  }

  private function processXMLString( $xml )
  {
    $return = array();
    //echo '<br />ProcessXmlString<br /><br />';
    //$xml = "<office><code>660</code></office>";

    $result = $this->client->__soapCall( 'ProcessXmlString', array( array( 'xmlRequest'=>$xml ) ), null, $this->header );

    $qq = new domDocument();
    $qq->loadXML( $result->ProcessXmlStringResult );

    $responseElement = $qq->documentElement;
    $xpath = new DOMXPath( $qq );
    $transactions = $xpath->query( "//transactions/transaction", $responseElement );

    //echo $result->ProcessXmlStringResult;

    // handle errors
    // errors in lines
    $msgtypes = $xpath->query("//*[@msgtype=\"error\"]");
    foreach ( $msgtypes as $msgtype )
    {
      $parent = $msgtype->parentNode;

      if ( $parent->nodeName == 'line' )
      {
        $id = $parent->getAttribute('id');
        $msg = $msgtype->getAttribute('msg');
        $return['transaction_errors'][] = "Fout in de lines: $id: $msg";
      }
      else
      {
        $msg = $msgtype->getAttribute('msg');
        $return['transaction_errors'][] = "Fout in de header: $msg";
      }
    }   
  
    foreach ( $transactions as $transaction )
    {
      $item = $xpath->query( "//lines", $transaction );
    }
    
    return $return;
  }
  
  // create a dimension for a new entry, does geen fuck if dimension already exists ;) 
  private function createDimension( $orderNo )
  {
    // create new dimension for booking
    $xml = '
    <dimension>
    <code>' . $orderNo . '</code>
    <name>' . $orderNo . '</name>
    <type>PRJ</type>
    </dimension>
    ';
    $r = $this->processXMLString( $xml );
    $this->handleErrors( $r );
  }
  
  // books an entry in twinfield for 'omzet'

  function bookOmzet( $type, $orderNo, $date, $total, $sum, $sumExtra, $sumVerzekeringen )
  {
    $this->createDimension( $orderNo );
    
    // type = docdate/iDEAL(1301, page/creditcard 2501

    $period = $this->calcPeriod( $date );

    $xml = '
    <transactions>
    <transaction destiny="temporary" raisewarning="true">
    <header>
    <code>VRK</code>
    <currency>EUR</currency>
    <date>' . $date . '</date>
    <period>' . $period . '</period>
    <invoicenumber>' . $orderNo . '</invoicenumber>
    </header>
    
    <lines>
    <line type="total" id="1">
    <dim1>14000</dim1>
    <dim2>' . $type . '</dim2>
    <value>' . $total . '</value>
    <debitcredit>debit</debitcredit>
    <description>totaalbedrag</description>
    </line>
    
    <line type="detail" id="2">
    <dim1>80100</dim1>
    <dim2>00000</dim2>
    <dim3>' . $orderNo . '</dim3>
    <value>' . $sum . '</value>
    <debitcredit>credit</debitcredit>
    <description>' . $orderNo . '</description>
    </line>
    
    <line type="detail" id="3">
    <dim1>80200</dim1>
    <dim2>00000</dim2>
    <dim3>' . $orderNo . '</dim3>
    <value>' . $sumExtra . '</value>
    <debitcredit>credit</debitcredit>
    <description>stadskaarten</description>
    </line>
    
    <line type="detail" id="4">
    <dim1>80500</dim1>
    <dim2>00000</dim2>
    <dim3>' . $orderNo . '</dim3>
    <value>' . $sumVerzekeringen . '</value>
    <debitcredit>credit</debitcredit>
    <description>verzekeringen</description>
    </line>
    
    </lines>
    </transaction>
    </transactions>
    ';
    
    $r = $this->processXMLString( $xml );
    $this->handleErrors( $r );
  }


/*



*/


  // books an entry in twinfield for 'inkoop'
  function bookInkoop( $orderNo, $cNummer, $cType, $date, $total, $invoiceNo, $referenceDetails )  
  {
    $this->createDimension( $orderNo );
    $period = $this->calcPeriod( $date );

    $sum = $total;
 
    if ( $cType == TYPE_STADSKAARTEN )
    {
      $total *= BTW_MULTI;
    }

    $xml = '
    <transactions>
    <transaction destiny="temporary" raisewarning="false">
    <header>
    <code>INK</code>
    <currency>EUR</currency>
    <date>' . $date . '</date>
    <period>' . $period . '</period>
    <invoicenumber>' . $invoiceNo . '</invoicenumber>
    <description />
    </header>
    
    <lines>
    <line type="total" id="1">
    <dim1>16000</dim1>
    <dim2>' . $cNummer . '</dim2>
    <value>' . $total . '</value>
    <debitcredit>credit</debitcredit>
    <description>' . substr( $referenceDetails, 0, 40) . '</description>
    </line>

    <line type="detail" id="2">
    <dim1>' . $cType . '</dim1>
    <dim2>00000</dim2>
    <dim3>' . $orderNo . '</dim3>
    <value>' . $sum . '</value>
    <debitcredit>debit</debitcredit>
    <description>' . substr( $invoiceNo, 0, 40) . '</description>
    ';
    
    if ( $cType == TYPE_STADSKAARTEN )
    {
      $xml .= "\n<vatcode>IH</vatcode>\n";
    }
    
    $xml .= 
    '</line>

    </lines>
    </transaction>
    </transactions>
    ';
    
    //echo '<pre>'. $xml . '</pre>';
    
    $r = $this->processXMLString( $xml );
    $this->handleErrors( $r );
  }

  private function handleErrors( $r )
  {
    if ( $r && is_array( $r['transaction_errors'] ) )
    {
      foreach ( $r['transaction_errors'] as $error )
      {
        echo '<p>Errors: <br/>' . $error . '</p>';
      }
      //die;
    }
  }
  
  // calculates the period, needed for Twinfield
  public function calcPeriod( $date )
  {
    $y = substr($date, 0, 4 );
    $m = substr($date, 4, 2 );
    
    $p = $m-6;
    
    if ( $p<= 0 )
    {
      $j = $y;
      $p += 12;
    }
    else
    {
      $j = $y+1;
    }
    
    $p = ($p<10) ? 0 . "$p" : $p;
    
    $r = "$j/$p";
    
    return $r;
  }
}

/*
try 
{
  $twinfield = new Twinfield();
}
catch (SoapFault $e)
{
  echo $e->getMessage();
}

$twinfield->bookInkoop( 'VBT000001', 5001, TYPE_STADSKAARTEN, '20090101', 30 );

//$twinfield->bookOmzet( METHOD_IDEAL, 'VBT000001', '20090101', 100, 50, 25, 25 );

die;
		
50100	vliegtickets	
50110	hotels 	
50120	voetbalkaarten	
50130	hotel-vlucht pakket (een pakket waarbij vlucht en hotel samen wordt ingekocht bv Thomas Cook, Esatour en Interopa)	
50140	Extra's (transfers, stadtour e.d. Extra reisonderdelen)	
50210	Stadskaarten	
50500	Verzekeringen	

*/

?>
<?
/*

  Land.php

*/

function getAantalLanden(  )
{
  $q = "SELECT COUNT(id) AS num FROM land";
  
  $res = $GLOBALS['rsdb']->query( $q );

	if( pg_num_rows( $res ) == 1 )
		return pg_fetch_result( $res, 0, 'num' );

	return 0;

}


function getLanden( $size=0, $start=0, $sort="", $ascdesc="" )
{
	
	// Sorteren
	if( $sort ) 
	{
		$order = "ORDER BY $sort";
		if( $ascdesc=="asc" )
			$order .= " ASC";
		else
			$order .= " DESC";
	}
	else
		$order = "ORDER BY id ASC";
	
	// Resultaat opdelen in pagina's
	if( $size )
	{
		$limit = "LIMIT $size";
		if( $start )
			$limit .= " OFFSET $start";
	}
	
  $q = "SELECT * FROM land $order $limit ";
  
  $res = $GLOBALS['rsdb']->query( $q );

	for( $i=0; $i<pg_num_rows( $res ); $i++ )
 	{
		$row = pg_fetch_array( $res, $i, PGSQL_ASSOC );
    $array[] = new Land($row);
	}
  return $array;
}

class Land extends Webentiteit
{
  var $Steden;
  
  var $ISO_code;
  var $timezone;
  
  function Land($array = null)
  {
    Webentiteit::Webentiteit();
    $this->tabelNaam = "land";
    
    if ($array)
    {
      $this->loadArray($array);
    }
  }
  
  function getSteden()
  {
    $q = $this->db->query("SELECT * FROM stad WHERE land_id={$this->id} ORDER BY naam DESC");

  	for( $i=0; $i<pg_num_rows( $q ); $i++ )
  	{
  		$r = pg_fetch_array( $q, $i, PGSQL_ASSOC );
      $array[] = new Stad($this, $r);
  	}
    return $array;
  }

  function loadArray($array)
  {
    $this->naam = $array['naam'];
    $this->land_id = $array['id'];
    $this->ISO_code = $array['iso_code'];
    $this->timezone = $array['timezone'];  
    parent::loadArray($array);    
  }

  function post($array)
  {
    if (!$array['iso_code']) $resultArray['iso_code']='EMPTY';
  
    $this->ISO_code = $array['iso_code'];
    $this->timezone = $array['timezone'];

    $resultArray = array();
    $resultArray += parent::post($array);    
    return $resultArray;
  }

  function store($array = null)
  {
    $array['iso_code'] = $this->getISOcode();
    $array['timezone'] = $this->getTimezone();
    return parent::store( $array );
  }


	function getISOcode()
	{
		return $this->ISO_code;
	}
	function getTimezone()
	{
		return $this->timezone;
	}



}
?>
<?
/*

  Arrangement.php

*/

/*
 * 
 * 
Select  COUNT(arrangement.id) AS num From arrangement
LEFT JOIN event on arrangement.event_id = event.id Where event.event_type_id = 1
 * */
function countArrangementenOnEventType($crit = null) {
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else {
		$where = NULL;
	}
	$q = "Select  COUNT(arrangement.id) AS num From arrangement LEFT JOIN event on arrangement.event_id = event.id $where";
		$res = $GLOBALS ['rsdb']->query ( $q );
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	return 0;
}


function getAantalArrangementen($crit = null) {
	$q = "SELECT COUNT(id) AS num FROM arrangement";
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	return 0;
}

function utime2() {
	$time = explode ( " ", microtime () );
	$usec = ( double ) $time [0];
	$sec = ( double ) $time [1];
	return $sec + $usec;
}

function getArrangementen($size = 0, $start = 0, $sort = "", $ascdesc = "", $crit = null) {
	// Criterea
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else {
		$where = NULL;
	}
	
	// Sorteren
	if ($sort) {
		$order = "ORDER BY $sort";
		if ($ascdesc == "asc")
			$order .= " ASC";
		else
			$order .= " DESC";
	} else {
		$order = "ORDER BY arrangement.id ASC";
	}
	
	// Resultaat opdelen in pagina's
	if ($size) {
		$limit = "LIMIT $size";
		if ($start)
			$limit .= " OFFSET $start";
	} else {
		$limit = NULL;
	}
	$q = "SELECT arrangement.*, event.naam as event FROM arrangement LEFT JOIN event on arrangement.event_id = event.id $where $order $limit ";
	 //echo "<br/>  =>".$q;
	$res = $GLOBALS ['rsdb']->query ( $q );
	$array = array();
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$array [] = new Arrangement ( null, $row );
	}
	return $array;
}

function getArrangementenIds($size = 0, $start = 0, $sort = "", $ascdesc = "", $crit = null) {
	// Criterea
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else {
		$where = NULL;
	}
	
	// Sorteren
	if ($sort) {
		$order = "ORDER BY $sort";
		if ($ascdesc == "asc")
			$order .= " ASC";
		else
			$order .= " DESC";
	} else
		$order = "ORDER BY arrangement.id ASC";
		
	// Resultaat opdelen in pagina's
	if ($size) {
		$limit = "LIMIT $size";
		if ($start)
			$limit .= " OFFSET $start";
	} else {
		$limit = NULL;
	}
	
	$q = "SELECT id FROM arrangement $where $order $limit ";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$array [] = $row ['id'];
	}
	return $array;
}

function getArrangementCountPerMonth(&$event_id, &$crit) {
	$event = new Event ( );
	$event->loadById ( $event_id );
	
	$returnArray = array ();
	
	for($i = 0; $i < 12; $i ++) {
		$c = array ();
		$c = $crit;
		$c [] = "date_part('month', heen) = " . ($i + 1);
		
		$arrangementen = $event->getArrangementen ( null, null, $c );
		
		if ($c = count ( $arrangementen )) {
			$returnArray [$i + 1] = $c;
		}
	}
	
	return $returnArray;
}

class Arrangement extends Webentiteit {
	var $Event;
	
	var $event_id;
	
	var $datum;
	
	var $subevent;
	var $aantal_geboekt;
	var $prijs;
	var $ticket_prijs;
	var $actief_overboeken;
	var $actief_boeken;
	
	var $heen;
	var $terug;
	
	var $extra_marge;
	
	// Standaard bepaald dmv. pricingagent.
	var $heen_beschr;
	var $terug_beschr;
	
	var $flight_id_heen;
	var $flight_id_terug;
	var $request_id;
	
	var $vlucht_prijs;
	var $vlucht_heen_tarief;
	var $vlucht_terug_tarief;
	
	var $vlucht_boeken;
	
	var $actie_code;
	var $actie_bedrag;
	var $actie_vervaldatum;
	var $actie_pp;
	var $actie_beschrijving;
	var $thuisclub;
	var $toparrangement;
	
	var $stock_to_amount;
	var $stock_return_amount;
	
	var $stock_from_iata;
	var $stock_to_iata;
	var $stock_heen_vertrek;
	var $stock_heen_aankomst;
	var $stock_terug_vertrek;
	var $stock_terug_aankomst;
	var $stock_heen_prijs;
	var $stock_terug_prijs;
	var $optaMatchId;
	
	function Arrangement($Event = null, $array = null) {
		Webentiteit::Webentiteit ();
		$this->Event = $Event;
		$this->tabelNaam = "arrangement";
		
		if ($array) {
			$this->loadArray ( $array );
		}
	
	}
	
	function loadArray($array) {
		$this->event_id = $array ['event_id'];
		
		$this->datum = $array ['datum'];
		$this->subevent = $array ['subevent'];
		$this->aantal_geboekt = $array ['aantal_geboekt'];
		$this->prijs = $array ['prijs'];
		$this->ticket_prijs = $array ['ticket_prijs'];
		$this->vlucht_prijs = $array ['vlucht_prijs'];
		$this->actief_overboeken = $array ['actief_overboeken'];
		$this->actief_boeken = $array ['actief_boeken'];
		$this->extra_marge = $array ['extra_marge'];
		
		$this->vlucht_heen_beschr = $array ['vlucht_heen_beschr'];
		$this->vlucht_terug_beschr = $array ['vlucht_terug_beschr'];
		
		$this->flight_id_heen = $array ['flight_id_heen'];
		$this->flight_id_terug = $array ['flight_id_terug'];
		$this->request_id = $array ['request_id'];
		$this->vlucht_heen_tarief = $array ['vlucht_heen_tarief'];
		$this->vlucht_terug_tarief = $array ['vlucht_terug_tarief'];
		
		$this->heen = $array ['heen'];
		$this->terug = $array ['terug'];
		
		$this->vlucht_boeken = $array ['vlucht_boeken'];
		
		$this->actie_code = $array ['actie_code'];
		$this->actie_bedrag = $array ['actie_bedrag'];
		$this->actie_vervaldatum = $array ['actie_vervaldatum'];
		$this->actie_pp = $array ['actie_pp'];
		$this->actie_beschrijving = $array ['actie_beschrijving'];
		$this->toparrangement = $array ['toparrangement'];
		
		if ( isset($array ['thuisclub'])) {
		  $this->thuisclub = $array ['thuisclub']; 
		}
		
		$this->stock_to_amount = $array ['stock_to_amount'];
		$this->stock_return_amount = $array ['stock_return_amount'];
		
		$this->stock_from_iata = $array ['stock_from_iata'];
		$this->stock_to_iata = $array ['stock_to_iata'];
		$this->stock_heen_vertrek = $array ['stock_heen_vertrek'];
		$this->stock_heen_aankomst = $array ['stock_heen_aankomst'];
		$this->stock_terug_vertrek = $array ['stock_terug_vertrek'];
		$this->stock_terug_aankomst = $array ['stock_terug_aankomst'];
		
		$this->stock_heen_prijs = $array ['stock_heen_prijs'];
		$this->stock_terug_prijs = $array ['stock_terug_prijs'];
		$this->optaMatchId = $array ['opta_match_id'];
		
		parent::loadArray ( $array );
	}
	
	function post($array) {
		$resultArray = array ();
		
		$this->event_id = $array ['event_id'];
		
		$this->datum = $array ['datum'];
		$this->subevent = $array ['subevent'];
		$this->prijs = str_replace ( ",", ".", $array ['prijs'] );
		$this->ticket_prijs = str_replace ( ",", ".", $array ['ticket_prijs'] );
		$this->vlucht_prijs = str_replace ( ",", ".", $array ['vlucht_prijs'] );
		$this->extra_marge = $array ['extra_marge'];
		$this->vlucht_boeken = ($array ['vlucht_boeken'] ? 't' : 'f');
		$this->actief_overboeken = ($array ['actief_overboeken'] ? 't' : 'f');
		$this->actief_boeken = ($array ['actief_boeken'] ? 't' : 'f');
		
		$this->heen = $array ['heen'];
		$this->terug = $array ['terug'];
		
		$this->vlucht_heen_tarief = $array ['vlucht_heen_tarief'];
		$this->vlucht_terug_tarief = $array ['vlucht_terug_tarief'];
		
		$this->actie_code = $array ['actie_code'];
		$this->actie_bedrag = $array ['actie_bedrag'];
		$this->actie_vervaldatum = $array ['actie_vervaldatum'];
		$this->actie_pp = ($array ['actie_pp'] ? 't' : 'f');
		$this->actie_beschrijving = $array ['actie_beschrijving'];
		$this->toparrangement = ($array ['toparrangement'] ? 't' : 'f');
		
		$this->stock_to_amount = $array ['stock_to_amount'];
		$this->stock_return_amount = $array ['stock_return_amount'];
		$this->stock_from_iata = $array ['stock_from_iata'];
		$this->stock_to_iata = $array ['stock_to_iata'];
		$this->stock_heen_vertrek = str_replace ( '.', ':', $array ['stock_heen_vertrek'] );
		$this->stock_heen_aankomst = str_replace ( '.', ':', $array ['stock_heen_aankomst'] );
		$this->stock_terug_vertrek = str_replace ( '.', ':', $array ['stock_terug_vertrek'] );
		$this->stock_terug_aankomst = str_replace ( '.', ':', $array ['stock_terug_aankomst'] );
		$this->optaMatchId = $array ['opta_match_id'];
		
		if ($this->stock_heen_vertrek) {
			if (! preg_match ( '/^[0-9.]*:[0-9.]*$/', $array ['stock_heen_vertrek'] ))
				$resultArray ['stock_heen_vertrek'] = 'MASK';
		}
		
		if ($this->stock_terug_vertrek) {
			if (! preg_match ( '/^[0-9.]*:[0-9.]*$/', $array ['stock_terug_vertrek'] ))
				$resultArray ['stock_terug_vertrek'] = 'MASK';
		}
		
		if ($this->stock_heen_aankomst) {
			if (! preg_match ( '/^[0-9.]*:[0-9.]*$/', $array ['stock_heen_aankomst'] ))
				$resultArray ['stock_heen_aankomst'] = 'MASK';
		}
		
		if ($this->stock_terug_aankomst) {
			if (! preg_match ( '/^[0-9.]*:[0-9.]*$/', $array ['stock_terug_aankomst'] ))
				$resultArray ['stock_terug_aankomst'] = 'MASK';
		}
		
		$this->stock_heen_prijs = $array ['stock_heen_prijs'];
		$this->stock_terug_prijs = $array ['stock_terug_prijs'];
		/*
    if ( !$array['vlucht_heen_tarief'] )
      $resultArray['vlucht_heen_tarief'] = 'EMPTY';
    else if (!preg_match('/^[0-9]*$/', $array['vlucht_heen_tarief'])) 
      $resultArray['vlucht_heen_tarief'] = 'NOINT';   

    if ( !$array['vlucht_terug_tarief'] )
      $resultArray['vlucht_terug_tarief'] = 'EMPTY';
    else if (!preg_match('/^[0-9]*$/', $array['vlucht_terug_tarief'])) 
      $resultArray['vlucht_terug_tarief'] = 'NOINT';   
*/
		
		//    if( !$this->marge_prijs ) $resultArray['marge_prijs'] = 'EMPTY';
		if (! preg_match ( '/^[0-9.]*$/', $this->prijs ))
			$resultArray ['prijs'] = 'NOINT';
		if (! preg_match ( '/^[0-9.]*$/', $this->ticket_prijs ))
			$resultArray ['ticket_prijs'] = 'NOINT';
		if (! preg_match ( '/^[0-9.]*$/', $this->vlucht_prijs ))
			$resultArray ['vlucht_prijs'] = 'NOINT';
		
		$resultArray += parent::post ( $array );
		return $resultArray;
	}
	
	function store($array = null) {
		
		if ($event_id = $this->getEventId ()) {
			$array ['event_id'] = $this->getEventId ();
			$array ['datum'] = $this->datum;
			$array ['subevent'] = $this->subevent;
			$array ['prijs'] = $this->prijs;
			$array ['ticket_prijs'] = $this->ticket_prijs;
			$array ['vlucht_prijs'] = $this->vlucht_prijs;
			$array ['actief_overboeken'] = $this->actief_overboeken;
			$array ['actief_boeken'] = $this->actief_boeken;
			$array ['extra_marge'] = $this->extra_marge;
			
			$array ['heen'] = $this->heen;
			$array ['terug'] = $this->terug;
			
			$array ['vlucht_heen_beschr'] = $this->vlucht_heen_beschr;
			$array ['vlucht_terug_beschr'] = $this->vlucht_terug_beschr;
			
			$array ['flight_id_heen'] = $this->flight_id_heen;
			$array ['flight_id_terug'] = $this->flight_id_terug;
			$array ['request_id'] = $this->request_id;
			
			$array ['vlucht_heen_tarief'] = $this->vlucht_heen_tarief;
			$array ['vlucht_terug_tarief'] = $this->vlucht_terug_tarief;
			
			$array ['vlucht_boeken'] = $this->vlucht_boeken;
			
			$array ['actie_code'] = $this->actie_code;
			$array ['actie_bedrag'] = $this->actie_bedrag;
			$array ['actie_vervaldatum'] = $this->actie_vervaldatum;
			$array ['actie_pp'] = $this->actie_pp;
			$array ['actie_beschrijving'] = $this->actie_beschrijving;
			$array ['toparrangement'] = $this->getToparrangement ();
			$array ['stock_to_amount'] = $this->getStockToAmount ();
			$array ['stock_return_amount'] = $this->getStockReturnAmount ();
			$array ['stock_from_iata'] = $this->getStockFromIATA ();
			$array ['stock_to_iata'] = $this->getStockToIATA ();
			$array ['stock_heen_vertrek'] = $this->getStockHeenVertrek ();
			$array ['stock_heen_aankomst'] = $this->getStockHeenAankomst ();
			$array ['stock_terug_vertrek'] = $this->getStockTerugVertrek ();
			$array ['stock_terug_aankomst'] = $this->getStockTerugAankomst ();
			$array ['stock_heen_prijs'] = $this->getStockHeenPrijs ();
			$array ['stock_terug_prijs'] = $this->getStockTerugPrijs ();
			
			$array ['vlucht_heen_tarief'] = $this->vlucht_heen_tarief; //$this->getVluchtHeenTarief();
			$array ['vlucht_terug_tarief'] = $this->vlucht_terug_tarief; //$this->getVluchtTerugTarief();
			$array ['opta_match_id'] = $this->optaMatchId;  

			// locales, waardes die in meerdere talen voor kunnen komen
			

			if (isset ( $_SESSION ['taalcode'] ) && $_SESSION ['taalcode']) {
				
				if (isset($this->id)){
					
					//$q = "SELECT MAX(id) FROM beschrijving";
        			//$res = $GLOBALS['rsdb']->query( $q );
    				//$this->id = pg_fetch_result( $res, 0 ) + 1;
    				
				
					// heenvluchttarief
					$lv = getLocaleValueByNaam ( $this->id, 'vlucht_heen_tarief', $_SESSION ['taalcode'] );
					
					if (! $lv) {
						$lv = new LocaleValue ( array ('naam' => 'vlucht_heen_tarief', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['vlucht_heen_tarief'] ) );
					} else {
						$lv->setWaarde ( $array ['vlucht_heen_tarief'] );
					}
					$lv->store ();
					unset ( $array ['vlucht_heen_tarief'] ); // skip this!
					
	
					// terugvluchttarief
					$lv = getLocaleValueByNaam ( $this->id, 'vlucht_terug_tarief', $_SESSION ['taalcode'] );
					if (! $lv) {
						$lv = new LocaleValue ( array ('naam' => 'vlucht_terug_tarief', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['vlucht_terug_tarief'] ) );
					} else {
						$lv->setWaarde ( $array ['vlucht_terug_tarief'] );
					}
					$lv->store ();
					unset ( $array ['vlucht_terug_tarief'] ); // skip this!
					
	
					// actiecode
					$lv = getLocaleValueByNaam ( $this->id, 'actie_code', $_SESSION ['taalcode'] );
					
					if (! $lv) {
						$lv = new LocaleValue ( array ('naam' => 'actie_code', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['actie_code'] ) );
					} else {
						$lv->setWaarde ( $array ['actie_code'] );
					}
					
					$lv->store ();
					
					unset ( $array ['actie_code'] ); // skip this!
					
	
					// actie_bedrag
					$lv = getLocaleValueByNaam ( $this->id, 'actie_bedrag', $_SESSION ['taalcode'] );
					
					if (! $lv) {
						$lv = new LocaleValue ( array ('naam' => 'actie_bedrag', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['actie_bedrag'] ) );
					} else {
						$lv->setWaarde ( $array ['actie_bedrag'] );
					}
					$lv->store ();
					unset ( $array ['actie_bedrag'] ); // skip this!
					
	
					// actie_vervaldatum
					$lv = getLocaleValueByNaam ( $this->id, 'actie_vervaldatum', $_SESSION ['taalcode'] );
					
					if (! $lv) {
						$lv = new LocaleValue ( array ('naam' => 'actie_vervaldatum', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['actie_vervaldatum'] ) );
					} else {
						$lv->setWaarde ( $array ['actie_vervaldatum'] );
					}
					$lv->store ();
					unset ( $array ['actie_vervaldatum'] ); // skip this!
					
	
					// actie_pp
					$lv = getLocaleValueByNaam ( $this->id, 'actie_pp', $_SESSION ['taalcode'] );
					
					if (! $lv) {
						$lv = new LocaleValue ( array ('naam' => 'actie_pp', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['actie_pp'] ) );
					} else {
						$lv->setWaarde ( $array ['actie_pp'] );
					}
					$lv->store ();
					unset ( $array ['actie_pp'] ); // skip this!
					
	
					// actie_beschrijving
					$lv = getLocaleValueByNaam ( $this->id, 'actie_beschrijving', $_SESSION ['taalcode'] );
					
					if (! $lv) {
						$lv = new LocaleValue ( array ('naam' => 'actie_beschrijving', 'beschrijving_id' => $this->id, 'taalcode' => $_SESSION ['taalcode'], 'waarde' => $array ['actie_beschrijving'] ) );
					} else {
						$lv->setWaarde ( $array ['actie_beschrijving'] );
					}
					$lv->store ();
					unset ( $array ['actie_beschrijving'] ); // skip this!
				}
			}
		}
		
		parent::store ( $array );
	}
	
	// returns true if all stock options are filled in
	public function hasStock() {
		if ($this->getStockToAmount () > 0 && $this->getStockReturnAmount () > 0 && $this->stock_from_iata && $this->stock_to_iata && $this->stock_heen_vertrek && $this->stock_heen_aankomst && $this->stock_terug_vertrek && $this->stock_terug_aankomst) {
			return (true);
		}
	}
	
	function getDatum($format = null) {
		if ($format)
			return strftime ( $format, strtotime ( $this->datum ) );
		else
			return $this->datum;
	}
	
	function getDatumStamp() {
		return strtotime ( $this->datum );
	}
	
	function getStockToAmount() {
		return $this->stock_to_amount;
	}
	
	function getStockReturnAmount() {
		return $this->stock_return_amount;
	}
	
	function decreaseStockToAmount($count) {
		if ($this->stock_to_amount >= $count) {
			$this->stock_to_amount -= $count;
			$q = "UPDATE arrangement SET stock_to_amount = {$this->stock_to_amount} WHERE id={$this->id}";
			$GLOBALS ['rsdb']->query ( $q );
			return (true);
		} else {
			return (false);
		}
	}
	
	function decreaseStockReturnAmount($count) {
		if ($this->stock_return_amount >= $count) {
			$this->stock_return_amount -= $count;
			$q = "UPDATE arrangement SET stock_return_amount = {$this->stock_return_amount} WHERE id={$this->id}";
			$GLOBALS ['rsdb']->query ( $q );
			return (true);
		} else {
			return (false);
		}
	}
	
	function getStockFromIATA() {
		return $this->stock_from_iata;
	}
	
	function getStockToIATA() {
		return $this->stock_to_iata;
	}
	
	function getStockHeenVertrek() {
		return $this->stock_heen_vertrek;
	}
	
	function getStockHeenAankomst() {
		return $this->stock_heen_aankomst;
	}
	
	function getStockTerugVertrek() {
		return $this->stock_terug_vertrek;
	}
	
	function getStockTerugAankomst() {
		return $this->stock_terug_aankomst;
	}
	
	function getStockTerugPrijs() {
		return $this->stock_terug_prijs;
	}
	
	function getStockHeenPrijs() {
		return $this->stock_heen_prijs;
	}
	
	function getToparrangement() {
		if ($this->toparrangement == 't') {
			return $this->toparrangement;
		} else {
			return ('f');
		}
	}
	
	function getGekoppeldeHotelsLocale() {
		
		$hotels 		= getHotelsByKoppeling ( 'arrangement', $this->getId () );
		
		$event 			= $this->getEvent ();
		if (!$hotels){
			$hotels 	= getHotelsByKoppeling ( 'event', $event->getId () );
		}
		
		if (!$hotels){
			$locatie 	= $event->getLocatie ();
			$stad 		= $locatie->getStad ();
			$hotels 	= getHotels ( 0, 0, "", "", array ("stad_id=" . $stad->getId () ) );
		}
		 
		if ($hotels) {
			return $hotels;
		}
	}
	
	function getGekoppeldeHotels() {
		$event = $this->getEvent ();
		$locatie = $event->getLocatie ();
		$stad = $locatie->getStad ();
		if (! $hotels = getHotelsByKoppeling ( 'arrangement', $this->getId () )){
			if (! $hotels = getHotelsByKoppeling ( 'event', $event->getId () )){
				$hotels = getHotels ( 0, 0, "", "", array ("stad_id=" . $stad->getId () ) );
			}
		}
		if ($hotels) {
			return $hotels;
		}
	}
	
	function getGekoppeldeHotelIds() {
		$event = $this->getEvent ();
		$locatie = $event->getLocatie ();
		$stad = @$locatie->getStad ();
		
		if (! $hotels = getHotelIdsByKoppeling ( 'arrangement', $this->getId () ))
			if (! $hotels = getHotelIdsByKoppeling ( 'event', $event->getId () ))
				$hotels = getHotelIds ( 0, 0, "", "", array ("stad_id=" . $stad->getId () ) );
		
		if ($hotels) {
			return $hotels;
		}
	}
	
	function getThuisclub() {
		return $this->thuisclub;
	}
	
	function getGoedkoopsteHotel() {
		$q = "SELECT id, hotel_id FROM hotel_cache WHERE p2 > 0 AND arrangement_id = " . $this->id . " AND hotel_id IN( SELECT id FROM hotel WHERE id=hotel_id ) ORDER BY p2 LIMIT 1";
		
		$res = $GLOBALS ['rsdb']->query ( $q );
		if (pg_num_rows ( $res ) == 1) {
			$row = pg_fetch_array ( $res, 0, PGSQL_ASSOC );
		}
		
		if (empty($row)) // get non-xml hotel
{
			if ($hotelIds = $this->getGekoppeldeHotelIds ()) {
				$q = "SELECT id as hotel_id,basisprijs_2pers_kamer FROM hotel WHERE id IN (";
				
				foreach ( $hotelIds as $id ) {
					$q .= $id . ',';
				}
				$q .= "0) ORDER BY basisprijs_2pers_kamer ASC LIMIT 1";
				
				$res = $GLOBALS ['rsdb']->query ( $q );
				
				if (pg_num_rows ( $res ) == 1) {
					$row = pg_fetch_array ( $res, 0, PGSQL_ASSOC );
				}
			}
		}
		
		if (is_array ( $row )) {
			$hotel = new Hotel ( );
			$hotel->loadById ( $row ['hotel_id'] );
			
			return $hotel;
		} else {
			//return null;
		}
	}
	
	function getHotelId() {
		$hotel = $this->getGoedkoopsteHotel ();
		if (is_object ( $hotel ))
			return $hotel->getId ();
	}
	
	function getHotelPrijs($currency = null) {
		$multi = ($currency) ? getMultiByCode ( $currency ) : 1;
		$hotel = $this->getGoedkoopsteHotel ();
		
		if (is_object ( $hotel ) && $prijs = $hotel->getBasisprijs2persKamer ( $this )) {
			return $prijs * $multi;
		} else {
			if ($event = @$this->getEvent ()) {
				return $event->getLocatie ()->getStad ()->getHotelPrijs () * $multi;
			} else {
				return (50);
			}
		}
	}
	
	function getLengte() {
		$datum1 = new DateTime ( $this->getHeen () );
		$datum2 = new DateTime ( $this->getTerug () );
		
		if ($datum1->format ( "U" ) > $datum2->format ( "U" )) {
			$d1 = $datum1;
			$datum1 = $datum2;
			$datum2 = $d1;
		}
		
		$dagNummer1 = $datum1->format ( "z" );
		$dagNummer2 = $datum2->format ( "z" );
		
		$verschilInJaren = ( int ) $datum2->format ( "Y" ) - ( int ) $datum1->format ( "Y" );
		
		if ($verschilInJaren) {
			$dagNummer2 = $dagNummer2 + ($verschilInJaren * 365);
		}
		
		return ($dagNummer2 - $dagNummer1);
	}
	
	function getHotelNaam() {
		$hotel = $this->getGoedkoopsteHotel ();
		if (is_object ( $hotel ))
			return $hotel->getNaam ();
	}
	
	function getPrijs($currency = null, $actie = null) {
		$multi = $currency ? getMultiByCode ( $currency ) : 1; // determine currency multiplier
		$cc_multi = is_int ( CC_MULTI ) ? CC_MULTI : 1; // determine possible credit card currency multiplier
		
		 
		//mail( "alert@travelgroep.nl", "RW_ XXXX - ",  " " . $cc_multi  ." - " . $multi );
		

		if (! $event = &$this->getEvent ()) {
			return false;
		}
		
		$ticket_prijs = $this->getTicketPrijs ();
		
		$vlucht_prijs = ($event->getGeenVluchten () == 't' || $this->getVluchtBoeken () == 'f') ? 0 : $this->getVluchtPrijs ();
		
		if ($ticket_prijs && $vlucht_prijs) {
			$r = (ceil ( ($ticket_prijs + $vlucht_prijs + $this->getExtraMarge () + ($this->getHotelPrijs () * $this->getLengte ()) + $event->getMargePrijs ()) * $multi * $cc_multi / 10 ) * 10) - 1;
		} else {
			if (! $r = $this->getPrijsNoFlight ( $currency )) {
				$r = $this->prijs * $multi;
			}
		}
		
		return $r;
	}
	
	function getPrijsNoFlight($currency = null) {
		$multi = $currency ? getMultiByCode ( $currency ) : 1; // determine currency multiplier
		$cc_multi = is_int ( CC_MULTI ) ? CC_MULTI : 1; // determine possible credit card currency multiplier
		

		if (! $event = &$this->getEvent ()) {
			return false;
		}
		
		$ticket_prijs = $this->getTicketPrijs ();
		$vlucht_prijs = 0;
		
		if ($ticket_prijs) {
			$r = (ceil ( ($ticket_prijs + $vlucht_prijs + $this->getExtraMarge () + ($this->getHotelPrijs () * $this->getLengte ()) 
					+ $event->getMargePrijs ()) * $multi * $cc_multi / 10 ) * 10) - 1;
		} else {
			$r = $this->prijs * $multi;
		}
		
		return $r;
	}
	
	function getActieBedrag() {
		$lv = getLocaleValueByNaam ( $this->id, "actie_bedrag", $_SESSION ['taalcode'] );
		
		if ($lv && $waarde = $lv->getWaarde ()) {
			return ($waarde);
		} else {
			return $this->actie_bedrag;
		}
	}
	
	function getActieCode() {
		$lv = getLocaleValueByNaam ( $this->id, "actie_code", $_SESSION ['taalcode'] );
		
		if ($lv && $waarde = $lv->getWaarde ()) {
			return ($waarde);
		} else {
			return strtolower ( $this->actie_code );
		}
	}
	
	function getActieVervaldatum() {
		$lv = getLocaleValueByNaam ( $this->id, "actie_vervaldatum", $_SESSION ['taalcode'] );
		
		if ($lv && $waarde = $lv->getWaarde ()) {
			return ($waarde);
		} else {
			return $this->actie_vervaldatum;
		}
	}
	
	function getActiePp() {
		$lv = getLocaleValueByNaam ( $this->id, "actie_pp", $_SESSION ['taalcode'] );
		
		if ($lv && $waarde = $lv->getWaarde () && $lv->taalcode) {
			return ($waarde);
		} else {
			return $this->actie_pp;
		}
	}
	
	function getActieBeschrijving() {
		$lv = getLocaleValueByNaam ( $this->id, "actie_beschrijving", $_SESSION ['taalcode'] );
		
		if ($lv && $waarde = $lv->getWaarde ()) {
			return ($waarde);
		} else {
			return $this->actie_beschrijving;
		}
	}
	
	function getTicketPrijs() {
		return $this->ticket_prijs;
	}
	
	function getExtraMarge() {
		return $this->extra_marge;
	}
	
	function getVluchtPrijs() {
		/*
    if ( $this->hasStock() )
    {
      $vlucht_heen_tarief = $this->getStockHeenPrijs();
      $vlucht_terug_tarief = $this->getStockTerugPrijs();
    }
    else
    */
		
		$vlucht_heen_tarief = $this->getVluchtHeenTarief ();
		$vlucht_terug_tarief = $this->getVluchtTerugTarief ();
		
		if ($vlucht_heen_tarief && $vlucht_terug_tarief && $this->getEnableVluchtBoeken ()) {
			return ($vlucht_heen_tarief + $vlucht_terug_tarief);
		} else {
			return $this->vlucht_prijs;
		}
	}
	
	function getCheckDateBeforeToday() {
		 $today = new DateTime();
		 $match = new DateTime ( $this->getDatum() );
		 $isToLate = $today->format("U") > $match->format("U"); 
		return $today->format("U") > $match->format("U"); 	 
	}
	
	function getEnableBoeken() {
		if ($this->actief_boeken == 't')
			return "enabled";
	}
	
	function getEnableOverBoeken() {
		if ($this->actief_overboeken == 't')
			return "enabled";
	}
	
	function getEnableVluchtBoeken() {
		if ($this->vlucht_boeken == 't')
			return "enabled";
	}
	
	function getFlightIDHeen() {
		return $this->flight_id_heen;
	}
	
	function getFlightIDTerug() {
		return $this->flight_id_terug;
	}
	
	function getRequestID() {
		return $this->request_id;
	}
	
	function getVluchtHeenTarief() {
		$lv = getLocaleValueByNaam ( $this->id, "vlucht_heen_tarief", $_SESSION ['taalcode'] );
		
		if ($lv && $_SESSION ['taalcode']) {
			return $lv->getWaarde ();
		} else {
			
			if ($this->vlucht_heen_tarief) {
				return ($this->vlucht_heen_tarief);
			} else {
				return $this->vlucht_prijs / 2;
			}
		}
		/*
    if( $this->vlucht_heen_tarief )
      return $this->vlucht_heen_tarief;
    else
      return $this->vlucht_prijs / 2;
*/
	}
	
	function getVluchtTerugTarief() {
		$lv = getLocaleValueByNaam ( $this->id, "vlucht_terug_tarief", $_SESSION ['taalcode'] );
		
		if ($lv && $_SESSION ['taalcode']) {
			return $lv->getWaarde ();
		} else {
			if ($this->vlucht_terug_tarief) {
				return ($this->vlucht_terug_tarief);
			} else {
				return $this->vlucht_prijs / 2;
			}
		}
	}
	
	function getVluchtBoeken() {
		return $this->vlucht_boeken;
	}
	
	function getHeen($format = null) {
		//debug_print_backtrace();
		if ($format)
			return strftime ( $format, strtotime ( $this->heen ) );
		else
			return $this->heen;
	}
	
	function getTerug($format = null) {
		if ($format)
			return strftime ( $format, strtotime ( $this->terug ) );
		else
			return $this->terug;
	}
	
	function setFlightIDHeen($flight_id_heen) {
		$this->flight_id_heen = $flight_id_heen;
	}
	
	function setFlightIDterug($flight_id_terug) {
		$this->flight_id_terug = $flight_id_terug;
	}
	
	function setVluchtHeenTarief($tarief) {
		$this->vlucht_heen_tarief = str_replace ( ',', '.', $tarief );
	}
	
	function setVluchtTerugTarief($tarief) {
		$this->vlucht_terug_tarief = str_replace ( ',', '.', $tarief );
	}
	
	function SetRequestID($request_id) {
		$this->request_id = $request_id;
	}
	
	function setHeen($heen) {
		$this->heen = $heen;
	}
	
	function setTerug($terug) {
		$this->terug = $terug;
	}
	
	function setVluchtPrijs($value) {
		$this->vlucht_prijs = $value;
	}
	
	function setVluchtBoeken($value) {
		$this->vlucht_boeken == $value;
	}
	
	function incrementAantalGeboekt() {
		$this->aantal_geboekt ? $this->aantal_geboekt : 0;
		$this->aantal_geboekt ++;
		$array ['id'] = $this->id;
		$array ['aantal_geboekt'] = intval($this->aantal_geboekt);
		parent::storeNoLocale ( $array );
	}
	
	function getEvent() {
		if (! is_object ( $this->Event ) && $this->event_id) {
			$this->Event = new Event ( );
			$this->Event->loadById ( $this->event_id );
		}
		
		return $this->Event;
	}
	
	function getEventId() {
		if (is_object ( $this->Event ))
			$this->event_id = $this->Event->getID ();
		
		return $this->event_id;
	}
	/**
	 * @return the $optaMatchId
	 */
	public function getOptaMatchId() {
		return $this->optaMatchId;
	}

	/**
	 * @param $optaMatchId the $optaMatchId to set
	 */
	public function setOptaMatchId($optaMatchId) {
		$this->optaMatchId = $optaMatchId;
	}

	public function isMatchConfirmed()
	{
	    $date = new \DateTime($this->getDatum());
	    $t = strftime('%T' , $date->format("U"));
	            
        if ( $t != '00:00:00' ){
            return true;
        }
        
        return false;
	}
		
}

?>

<?php 

class Team {
    
  var $id;
  var $name;
  var $optaId;
  
  function loadArray($array = null){
    $this->id = $array ['id']; 
    $this->name = $array ['name']; 
    $this->optaId = $array ['opta_id']; 
  }
  
/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

/**
	 * @return the $optaId
	 */
	public function getoptaId() {
		return $this->optaId;
	}

/**
	 * @param $id the $id to set
	 */
	public function setId($id) {
		$this->id = $id;
	}

/**
	 * @param $name the $name to set
	 */
	public function setName($name) {
		$this->name = $name;
	}

/**
	 * @param $optaId the $optaId to set
	 */
	public function setOptaId($optaId) {
		$this->optaId = $optaId;
	}

  
}

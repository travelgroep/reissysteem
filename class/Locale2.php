<?
/*

  Locale.php


  nieuwe taal inrichten:
  
  insert into locale (beschrijving_id, naam, beschrijving, beschrijving_kort, taalcode) select id, '[NAAM]', '[BESCHRIJVING]', '[BESCHRIJVING_KORT]', 'en' from beschrijving;

*/

function getLocales() {
	$q = "SELECT DISTINCT taalcode FROM locale";
	$res = $GLOBALS ['rsdb']->query ( $q );
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$array [] = new Locale2 ( $row );
	}
	return $array;
}

function getTalen($taalcode = null) {
	$q = "SELECT * FROM talen";
	if ($taalcode) {
		$q .= " WHERE taalcode={$taalcode}";
	}
	$res = $GLOBALS ['rsdb']->query ( $q );
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$array [] = $row;
	}
	return $array;
}

function getLocaleByBeschrijvingId($beschrijving_id, $taalcode) {
	$locale = null;
	
	$q = "SELECT * FROM locale WHERE beschrijving_id={$beschrijving_id} AND taalcode='{$taalcode}'";
	//mail( "alert@travelgroep.nl", "RW_getLocaleByBeschrijvingId- ",  " " . $q   );
	$res = $GLOBALS ['rsdb']->query ( $q );
	if (pg_num_rows ( $res ) > 0) {
		$row = pg_fetch_array ( $res, 0, PGSQL_ASSOC );
		$locale = new Locale2 ( $row );
	}
	return $locale;
}

function getLocalesByBeschrijvingId($beschrijving_id) {
	
	$q = "SELECT * FROM locale WHERE beschrijving_id={$beschrijving_id}";
	//echo "twee => " . $q;
	$res = $GLOBALS ['rsdb']->query ( $q );
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$locales [] = new Locale2 ( $row );
	}
	return $locales;
}

class Locale2 extends DBRecord {
	var $beschrijving_id;
	var $taalcode;
	var $beschrijving;
	var $beschrijving_kort;
	var $zitplaatsen;
	var $naam;
	var $seo_label;
	
	
	function Locale2($array = null) {
		DBRecord::DBRecord ( 'rsdb' );
		$this->tabelNaam = "locale";
		if ($array) {
			$this->loadArray ( $array );
		}
	}
	
	function loadArray($array) {
		$this->taalcode = $array ['taalcode'];
		$this->beschrijving = $array ['beschrijving'];
		$this->beschrijving_kort = $array ['beschrijving_kort'];
		$this->zitplaatsen = $array ['zitplaatsen'];
		$this->seo_label = $array ['seo_label'];
		
		$this->naam = $array ['naam'];
		$this->id = $array ['id'];
		$this->beschrijving_id = $array ['beschrijving_id'];
	}
	
	function post($array) {
		$resultArray = array ();
		$this->beschrijving = $array ['beschrijving'];
		$this->beschrijving_kort = $array ['beschrijving_kort'];
		$this->zitplaatsen = $array ['zitplaatsen'];
		$this->seo_label = $array ['seo_label'];
		
		$this->naam = $array ['naam'];
		$this->beschrijving_id = $array ['beschrijving_id'];
		if (! $array ['naam']){
			$resultArray ['naam'] = 'EMPTY';
		}
		return $resultArray;
	}
	
	function getId() {
		return $this->id;
	}
	
	function getTaalcode() {
		return $this->taalcode;
	}
	
	function getBeschrijvingId() {
		return $this->id;
	}
	
	function getNaam() {
		return $this->naam;
	}
	
	function getBeschrijving() {
		return $this->beschrijving;
	}
	
	function getBeschrijvingKort() {
		return $this->beschrijving_kort;
	}
	
	function getZitplaatsen() {
		return $this->zitplaatsen;
	}
	
	function getSeolabel() {
		return $this->seo_label;
	}
	
	function setSeolabel($seolabel) {
		$this->seo_label = $seolabel;
	}
	
	function setNaam($naam) {
		$this->naam = $naam;
	}
	
	function setBeschrijving($beschrijving) {
		$this->beschrijving = $beschrijving;
	}
	
	function setBeschrijvingKort($beschrijving_kort) {
		$this->beschrijving_kort = $beschrijving_kort;
	}
	
	function setZitplaatsen($zitplaatsen) {
		$this->zitplaatsen = $zitplaatsen;
	}
	
	function store($array = null) {
		$resultArray = array ();
		$storeArray ['id'] = $this->id;
		$storeArray ['beschrijving_id'] = $this->beschrijving_id;
		$storeArray ['naam'] = $this->getNaam ();
		$storeArray ['beschrijving'] = $this->getBeschrijving ();
		$storeArray ['beschrijving_kort'] = $this->getBeschrijvingKort ();
		$storeArray ['zitplaatsen'] = $this->getZitplaatsen ();
		$storeArray ['seo_label'] = $this->getSeolabel();
		$storeArray ['taalcode'] = $this->taalcode;
		 
		//$out = print_r ( $storeArray, true );
		//mail ( "alert@travelgroep.nl", "LOG storeArray  ", "\n ALGEMEEN: =>" . $out );
		$resultArray = parent::store ( $storeArray );
		
		return $resultArray;
	}

}
?>
<?
/*

  ReisoptieWaarde.php

*/

class ReisoptieWaarde extends DBRecord {
	var $reisoptie_id;
	var $naam;
	var $waarde;
	var $locale;
	
	function ReisoptieWaarde($array = null) {
		DBRecord::DBRecord ( 'rsdb' );
		$this->tabelNaam = 'reisoptiewaarde';
		
		if (is_array ( $array )) {
			$this->loadArray ( $array );
		}
	}
	
	function loadArray($array) {
		$this->reisoptie_id = $array ['reisoptie_id'];
		$this->naam = $array ['naam'];
		$this->waarde = $array ['waarde'];
		$this->purchase_price = $array ['purchase_price'];
		$this->id = $array ['id'];
		
		if ($_SESSION ['taalcode']) {
			$this->locale = getLocaleReisoptieWaardeByReisoptieWaardeId ( $this->id, $_SESSION ['taalcode'] );
		}
	}
	
	function getNaam() {
		if (is_object ( $this->locale )) {
			return $this->locale->getNaam ();
		} else {
			return $this->naam;
		}
	}
	
	function getWaarde($currency = null) {
		$multi = $currency ? getMultiByCode ( $currency ) : 1; // determine currency multiplier
		

		if (is_object ( $this->locale )) {
			return ceil ( $this->locale->getWaarde () * $multi );
		} else {
			if ($multi != 1) {
				return ceil ( $this->waarde * $multi );
			} else {
				return $this->waarde;
			}
		}
	}
	
	function getPurchasePrice($currency = null) {
		$multi = $currency ? getMultiByCode ( $currency ) : 1; // determine currency multiplier
		

		if (is_object ( $this->locale )) {
			return ceil ( $this->locale->getPurchasePrice () * $multi );
		} else {
			if ($multi != 1) {
				return ceil ( $this->purchase_price * $multi );
			} else {
				return $this->purchase_price;
			}
		}
	}
	
	function getReisoptieId() {
		return $this->reisoptie_id;
	}
	
	function store($array = null) {
		$resultArray = array ();
		
		$array ['id'] = $this->id;
		$array ['reisoptie_id'] = $this->reisoptie_id;
		$array ['naam'] = $this->naam;
		$array ['waarde'] = $this->waarde;
		$array ['purchase_price'] = $this->purchase_price;
		
		if ($this->locale) {
			$this->locale->setNaam ( $this->naam );
			$this->locale->setWaarde ( $this->waarde );
			$resultArray = $this->locale->store ();
		} else {
			
			$resultArray = parent::store ( $array );
			
			// fetch id and store new locales
			if (! $array ['id']) {
				$q = "SELECT MAX(id) FROM reisoptiewaarde";
				$res = $GLOBALS ['rsdb']->query ( $q );
				$reisoptiewaarde_id = pg_fetch_result ( $res, 0 );
				
				$talen = getTalen ();
				
				foreach ( $talen as $taal ) {
					$locale = new LocaleReisoptieWaarde ( );
					$array ['reisoptiewaarde_id'] = $reisoptiewaarde_id;
					$array ['naam'] = $this->getNaam () . '_' . $taal ['taalcode'];
					$array ['waarde'] = $this->getNaam () . '_' . $taal ['taalcode'];
					$array ['purchase_price'] = $this->getNaam () . '_' . $taal ['taalcode'];
					$array ['taalcode'] = $taal ['taalcode'];
					
					$locale->loadArray ( $array );
					$locale->store ();
				}
			}
		
		}
		return $resultArray;
	}
	
	function post($array) {
		$resultArray = array ();
		
		$this->reisoptie_id = $array ['reisoptie_id'];
		$this->naam = $array ['naam'];
		$this->waarde = $array ['waarde'];
		$this->purchase_price = $array ['purchase_price'];
		
		/*
    if (!$array['naam']) $resultArray['naam']='EMPTY';
    if (!$array['waarde']) $resultArray['waarde']='EMPTY';
*/
		
		return $resultArray;
	}
	
	function delete() {
		// delete all possible locales aswell!
		$locales = getLocaleReisoptieWaardesByReisoptieWaardeId ( $this->getId () );
		
		if (is_array ( $locales )) {
			foreach ( $locales as $locale ) {
				$locale->delete ();
			}
		}
		parent::delete ();
	}
	
	function render() {
		echo '<tr><td>naam(Optioneel!)</td><td><input class="optieRegel" type="text" name="reisoptiewaarde_naam[]" value="' . $this->getNaam () . '"></input></td></tr>';
		echo '<tr>';
		echo '<td>verkoopprijs</td><td><input class="optieRegel" type="text" name="reisoptiewaarde_waarde[]" value="' . $this->getWaarde () . '"></input></td>';
		//echo '<td>inkoopprijs</td><td><input class="optieRegel" type="text" name="reisoptiewaarde_purchase_price[]" value="' . $this->getPurchasePrice() . '"></input></td>';
		echo '<a style="cursor:pointer" onclick="deleteReisoptieWaarde(' . $this->getId () . ', ' . $this->getReisoptieId () . ')"><img src="../i/del.gif" /></a>';
		echo '</tr>';
		echo '<tr><td colspan="3" style="border-top:1px solid #8C560F"></td></tr>';
	}
}
?>
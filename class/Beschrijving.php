<?
/*

  Beschrijving.php

*/

function getNamenByType($type) {
	$array = array ();
	
	$res = $GLOBALS ['rsdb']->query ( "SELECT DISTINCT naam FROM $type ORDER BY naam" );
	
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$array [] = pg_fetch_result ( $res, $i, 0 );
	}
	return $array;
}

// filter out chars that don't comply with seo friendly standard
function seo($s) {
	$s = strtolower ( str_replace ( " ", "-", $s ) );
	
	$invalid = array ('é' => 'e', 'ë' => 'e', 'é' => 'e', 'ó' => 'o', 'ö' => 'o', 'ö' => 'o', 'á' => 'a', 'ã' => 'a', 'à' => 'a', 'ä' => 'a', 'í' => 'i', 'ï' => 'i', 'ú' => 'u', 'ü' => 'u', 'ñ' => 'n', '[' => '', ']' => '', '`' => '', "'" => '' );
	
	foreach ( $invalid as $i => $key ) {
		$s = str_replace ( $i, $key, $s );
	}
	
	return ($s);
}

class Beschrijving extends DBRecord {
	var $beschrijving;
	var $beschrijving_kort;
	var $zitplaatsen;
	var $naam;
	var $locale;
	var $seo_label;
	//var $counter;
	
	function Beschrijving($array = null) {
		//print "Beschrijving constructor!!";
		DBRecord::DBRecord ( 'rsdb' );
		$this->tabelNaam = "beschrijving";
		if ($array) {
			$this->loadArray ( $array );
		}
	}
	
	function loadArray($array) {
		
		$this->beschrijving = $array ['beschrijving'];
		$this->beschrijving_kort = $array ['beschrijving_kort'];
		$this->zitplaatsen = $array ['zitplaatsen'];
		$this->naam = $array ['naam'];
		$this->seo_label = $array ['seo_label'];
		$this->id = $array ['id'];
		
		if (isset($_SESSION ['taalcode']) && $this->getId ()) {
			//echo "<br/> Id => " . $this->getId();
			//echo "<br/> taalcode => " . $_SESSION['taalcode'] ;
			$this->locale = getLocaleByBeschrijvingId ( $this->id, $_SESSION ['taalcode'] );
		}
		
	}
	
	function post($array) {
		$resultArray = array ();
		$this->beschrijving = $array ['beschrijving'];
		$this->beschrijving_kort = $array ['beschrijving_kort'];
		$this->zitplaatsen = $array ['zitplaatsen'];
		$this->naam = $array ['naam'];
		$this->seo_label = $array ['seo_label'] ? $array ['seo_label'] : seo ( $array ['naam'] );
		
		//if (!$array['beschrijving']) $resultArray['beschrijving']='EMPTY';
		//if (!$array['beschrijving_kort']) $resultArray['beschrijving_kort']='EMPTY';
		// if (!$array['zitplaatsen']) $resultArray['zitplaatsen']='EMPTY';
		if (! $array ['naam']){
			$resultArray ['naam'] = 'EMPTY';
		}
		return $resultArray;
	}
	
	function getId() { return $this->id; }
	
	function getNaam() {
		
		//$out = print_r ($_SESSION['taalcode'], true);
		//
		
		if (is_object ( $this->locale )) {
			
			// return stripslashes( $this->locale->getNaam() );
			//echo $this->naam;
			//echo "<pre>";echo print_r($this->locale->naam,true);echo "</pre>";
			//$out2 = print_r ($this->locale, true);
			//mail( "alert@travelgroep.nl", "RW_ DEV _ XXXX - ",  " " . $out ." ". $out2   );
			
			if ( isset($this->locale->naam) ){
				return stripslashes ( $this->locale->naam );
			}else {
				return stripslashes ( $this->naam );
			}
			
		} else {
			//echo "<br/>2";
			
			return stripslashes ( $this->naam );
		}
	}
	
	function getSeoLabel() {
		if (is_object ( $this->locale )) {
			
			if (isset($this->locale->seo_label)){
				return stripslashes ( $this->locale->seo_label );
			}else {
				return stripslashes ( $this->seo_label );
			}
		} else {
			return stripslashes ( $this->seo_label );
		}
		
	}
	
	function getBeschrijving() {
		if (is_object ( $this->locale )) {
			return stripslashes ( $this->locale->getBeschrijving () );
		} else {
			return stripslashes ( $this->beschrijving );
		}
	}
	
	function getBeschrijvingKort() {
		if (is_object ( $this->locale )) {
			return stripslashes ( $this->locale->getBeschrijvingKort () );
		} else {
			return stripslashes ( $this->beschrijving_kort );
		}
	}
	
	function getZitplaatsen() {
		if (is_object ( $this->locale )) {
			return stripslashes ( $this->locale->getZitplaatsen () );
		} else {
			return stripslashes ( $this->zitplaatsen );
		}
	}
	
	function setNaam($naam) {
		$this->naam = $naam;
	}
	
	function setSeo_label($seo_label) {
		$this->seo_label = $seo_label;
	}
	
	function setBeschrijving($beschrijving) {
		$this->beschrijving = $beschrijving;
	}
	
	function setBeschrijvingKort($beschrijving_kort) {
		$this->beschrijving_kort = $beschrijving_kort;
	}
	
	function setZitplaatsen($zitplaatsen) {
		$this->zitplaatsen = $zitplaatsen;
	}
	
	function store($array = null) {
		
		$resultArray = array ();
		$array ['id'] = $this->id;
		$array ['naam'] = $this->naam;
		$array ['beschrijving'] = $this->beschrijving;
		$array ['beschrijving_kort'] = $this->beschrijving_kort;
		$array ['zitplaatsen'] = $this->zitplaatsen;
		$array ['seo_label'] = seo ( $this->seo_label );
		
		
					
		if ($this->locale) {
			// quick fix to ensure daemon doesn't fuck up localised var!
			if (! $_SESSION ['agent']) {
				 
				$this->locale->setNaam ( $this->naam );
				$this->locale->setBeschrijving ( $this->beschrijving );
				$this->locale->setBeschrijvingKort ( $this->beschrijving_kort );
				$this->locale->setZitplaatsen ( $this->zitplaatsen );
				$this->locale->setSeolabel ( $this->seo_label );
				$this->locale->store ();
			}
				unset ( $array ['naam'] ); // = null;
				unset ( $array ['beschrijving'] );
				unset ( $array ['beschrijving_kort'] );
				unset ( $array ['zitplaatsen'] );
				unset ( $array ['seo_label'] );
			
			// store non-locale extra attributes:      
			
			parent::store ( $array );
		
		} else {
			$resultArray = parent::store ( $array );
			
			// fetch id and store new locales
			if (! $array ['id']) {
				$q = "SELECT MAX(id) FROM beschrijving";
				$res = $GLOBALS ['rsdb']->query ( $q );
				$beschrijving_id = pg_fetch_result ( $res, 0 );
				
				
				/* mulite records*/
				/* 
				 * Het is niet nodig om meteen alle talen aan te maken! 
				 * onnodige records in de database! 
				 * */
				
				
				//$talen = getTalen ();
				//foreach ( $talen as $taal ) {
					
					//$myLocale = new Locale2 ( );
					//unset ( $array ['id'] );
					
					$array ['beschrijving_id'] = $beschrijving_id;
					$array ['naam'] = $this->getNaam ();
					$array ['seo_label'] = $this->getSeoLabel ();
					$array ['taalcode'] = $taal ['taalcode'];
					 
					//$myLocale->loadArray ( $arr );
					//$myLocale->store ();
					//$out = print_r ($array, true);
					//mail( "alert@travelgroep.nl", "RW_ save talen - ",  " " . $out   );
		
				//}
			
			} else {
				// SELECT * FROM locale WHERE beschrijving_id=13942
				/* Bestaand object alleen de taal nog niet. */
				
				if (isset ( $this->id )) {
					$taalcode = $_SESSION ['taalcode'];
					$q1 = "SELECT id FROM locale where beschrijving_id= $this->id and taalcode = '$taalcode'";
					$res = $GLOBALS ['rsdb']->query ( $q1 );
					$locale_id = pg_fetch_result ( $res, 0 );
					
					if (! $locale_id) {
						$myLocale = new Locale2 ( );
						unset ( $array ['id'] );
						$array ['beschrijving_id'] = $this->id;
						//$array['naam'] = $this->getNaam() . '_' . $taalcode;
						$array ['naam'] = $this->getNaam ();
						$array ['seo_label'] = $this->getSeoLabel ();
						$array ['taalcode'] = $taalcode;
						$myLocale->loadArray ( $array );
						$myLocale->store ();
					}
					//mail ( "alert@travelgroep.nl", "LOG beschrijving $locale_id  ", "\n ALGEMEEN: =>" . $q1 );
					
				/* Nieuwe taal toegevoegd! 
		       * Moet worden gekeken naar of het object taal bestaat maak een nieuwe record aan. */
				} else {
					//$out = print_r ( $this, true );
					//mail ( "alert@travelgroep.nl", "LOG beschrijving (Geen id)  ", "\n ALGEMEEN: =>" . $out );
				}
			
			}
			//echo "<pre> talen=>";print_r($talen,true);echo "</pre>";
		}
		return $resultArray;
	}
	
	// stores items without setting locale to default value
	function storeNoLocale($array = null) {
		$resultArray = array ();
		$array ['id'] = $this->id;
		$array ['naam'] = $this->naam;
		$array ['seo_label'] = seo( $this->seo_label );
		$array ['beschrijving'] = $this->beschrijving;
		$array ['beschrijving_kort'] = $this->beschrijving_kort;
		$array ['zitplaatsen'] = $this->zitplaatsen;
		
		$resultArray = parent::store ( $array );
		return $resultArray;
	}
	
	function delete() {
		// delete all possible locales aswell!
		$locales = getLocalesByBeschrijvingId ( $this->getId () );
		
		if (is_array ( $locales )) {
			foreach ( $locales as $locale ) {
				$locale->delete ();
			}
		}
		parent::delete ();
	}
	
	function getFotos($taalcode = null, $name = null, $withoutCheck = false) {
		$name_clause ='';
		if ($name) {
			$name_clause = "naam='{$name}' AND";
		}
		
		$q = "SELECT * FROM foto WHERE {$name_clause} beschrijving_id = {$this->getId()} AND (taalcode='alle'";
		
		if (isset ( $taalcode )) {
			$q .= " OR taalcode = '{$taalcode}'";
		}
		
		$q .= ")";
		
		//echo '<br/> q: ' . $q . ' '; 
		
        $fotoArray = array();
		if (@$r = $GLOBALS ['rsdb']->query ( $q )) {
			for($i = 0; $i < pg_num_rows ( $r ); $i ++) {
				$row = pg_fetch_array ( $r, $i, PGSQL_ASSOC );
				$foto = new Foto ( $row );
				
				//echo "<br/> getSrc =>  " .  $foto->getSrc();
				if ($withoutCheck) {
                    $fotoArray [] = $foto;
				} else {
                    if ($foto->getSrc ()) {
                        $fotoArray [] = $foto;
                    }
				}
			}
			
			return $fotoArray;
		}
	}
	
	function getLocale() {
		return $this->locale;
	}
	
	function renderEditblock() {
		
		echo '<div>';
		
		echo '<table border="0">';
		
		echo '<tr><td class="left">id</td><td><a href="?id=' . $this->getId () . '"><span id="id">' . $this->getId () . '</span></a></td></tr>';
		echo '<tr id="naam"><td class="left">naam</td><td><input name="naam" type="text" value="' . $this->getNaam () . '"><span class="knop" onclick="openMenuWrapper(event, \'names\', \'naam\')">...</span></td><td id="naam_err">';
		echo '</td></tr>';
		echo '<tr id="seo_label"><td class="left">seo-label</td><td><input name="seo_label" value="' . $this->getSeoLabel () . '" /><td id="seo_label_err"></td></tr>';
		echo '<tr id="beschrijving_kort"><td class="left">beschrijving (kort)</td><td><textarea name="beschrijving_kort">' . $this->getBeschrijvingKort () . '</textarea></td><td id="beschrijving_kort_err"></td></tr>';
		echo '<tr id="beschrijving"><td class="left">beschrijving</td><td><textarea name="beschrijving">' . $this->getBeschrijving () . '</textarea><td id="beschrijving_err"></td></tr>';
		echo '<tr id="zitplaatsen"><td class="left">extra info</td><td><textarea name="zitplaatsen">' . $this->getZitplaatsen () . '</textarea><td id="zitplaatsen_err"></td></tr>';
		
		echo '</table>';
		
		echo '<input name="id" type="hidden" value="' . $this->getId () . '"></input>';
		
		echo '</div>';
		
		echo '<div id="names" class="selector">';
		echo '...';
		echo '</div>';
	
	}
}
?>
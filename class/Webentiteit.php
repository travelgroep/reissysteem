<?
/*

  Webentiteit.php

*/

function renderRating($name, $value) {
	$maxStars = 5; // max aantal sterren voor beoordeling
	

	$code = '<span style="cursor:pointer">';
	for($x = 0; $x < $value; $x ++) {
		$code .= '<img class="rating" onclick="setRating(\'' . $name . '\', ' . ($x + 1) . ', this)" src="../i/ster.gif" border="0" />';
	}
	for($y = 0; $y < ($maxStars - $x); $y ++) {
		$code .= '<img class="rating" onclick="setRating(\'' . $name . '\', ' . ($x + $y + 1) . ', this)" src="../i/ster_leeg.gif" border="0" />';
	}
	$code .= '<input type="hidden" name="' . $name . '" id="' . $name . '_rating" value="' . $value . '"></input>';
	$code .= '</span>';
	
	return $code;
}

class Webentiteit extends Beschrijving {
	var $aantal_view;
	var $actief;
	var $actief_comments;
	var $beoordeling_intern;
	var $beoordeling_extern;
	
	function Webentiteit() {
		//print "Webentiteit constructor!!";
		Beschrijving::Beschrijving ();
	}
	
	function getReisopties($pp = false) {
		//    debug_print_backtrace();
		$array = array();
		($pp) ? $ppClause = 'AND pp = ' . $pp : $ppClause = '';
		$q = $GLOBALS ['rsdb']->query ( "SELECT * FROM reisoptie WHERE webentiteit_id = {$this->id} {$ppClause} ORDER BY naam DESC" );
		
		for($i = 0; $i < pg_num_rows ( $q ); $i ++) {
			$r = pg_fetch_array ( $q, $i, PGSQL_ASSOC );
			$array [] = new Reisoptie ( $r );
		}
		return $array;
	}
	
	function getActief() {
		return $this->actief == 't' ? 1 : 0;
	}
	
	function incrementAantalView() {
		$this->aantal_view ? $this->aantal_view : 0;
		$this->aantal_view ++;
		$array ['id'] = $this->id;
		$array ['aantal_view'] = $this->aantal_view;
		parent::storeNoLocale ( $array );
	}
	 
	function store($array = null) {
		$resultArray = array ();
		
		if ($this->aantal_view) {
			$array ['aantal_view'] = $this->aantal_view;
		}
		
		$array ['actief'] = $this->actief;
		$array ['actief_comments'] = $this->actief_comments;
		$array ['beoordeling_intern'] = $this->beoordeling_intern;
		$array ['beoordeling_extern'] = $this->beoordeling_extern;
		
		
		$resultArray = parent::store ( $array );
		return $resultArray;
	}
	
	
	
	function storeNoLocale($array = null) {
		$resultArray = array ();
		
		if ($this->aantal_view)
			$array ['aantal_view'] = $this->aantal_view;
		
		$array ['actief'] = $this->actief;
		$array ['actief_comments'] = $this->actief_comments;
		$array ['beoordeling_intern'] = $this->beoordeling_intern;
		$array ['beoordeling_extern'] = $this->beoordeling_extern;
		
		$resultArray = parent::storeNoLocale ( $array );
		return $resultArray;
	}
	
	function loadArray($array) {
		$this->aantal_view = $array ['aantal_view'];
		$this->actief = $array ['actief'];
		$this->actief_comments = $array ['actief_comments'];
		$this->beoordeling_intern = $array ['beoordeling_intern'];
		$this->beoordeling_extern = $array ['beoordeling_extern'];
		parent::loadArray ( $array );
	}
	
	function post($array) {
		$resultArray = array ();
		$this->aantal_view = $array ['aantal_view'];
		
		($array ['actief']) ? $this->actief = 1 : $this->actief = 0;
		($array ['actief_comments']) ? $this->actief_comments = 1 : $this->actief_comments = 0;
		
		$this->beoordeling_intern 	= $array ['beoordeling_intern'];
		$this->beoordeling_extern 	= $array ['beoordeling_extern'];
		
		$resultArray = parent::post ( $array );
		return $resultArray;
	}
	
	function renderLeft() {
		echo '<table>';
		
		echo '<tr><td class="left">beoordeling intern</td><td>' . renderRating ( 'beoordeling_intern', $this->beoordeling_intern ) . '</td></tr>';
		echo '<tr><td class="left">beoordeling extern</td><td>' . renderRating ( 'beoordeling_extern', $this->beoordeling_extern ) . '</td></tr>';
		
		($this->actief == "t") ? $sel = 'checked' : $sel = '';
		echo '<tr><td class="left">actief</td><td><input class="checkbox" type="checkbox" name="actief" ' . $sel . '></input></td></tr>';
		($this->actief_comments == "t") ? $sel = 'checked' : $sel = '';
		echo '<tr><td class="left">commentaar</td><td><input class="checkbox" type="checkbox" name="actief_comments" ' . $sel . '></input></td></tr>';
		echo '</table>';
	}
}
?>
<?
/*

  Vliegvelden.php

*/

function getAantalVliegvelden(  )
{
  $q = "SELECT COUNT(id) AS num FROM vliegveld";
  
  $res = $GLOBALS['rsdb']->query( $q );

	if( pg_num_rows( $res ) == 1 )
		return pg_fetch_result( $res, 0, 'num' );

	return 0;
}

function getVliegveldByIATA( $iata )
{
  $q = "SELECT id FROM vliegveld WHERE iata='{$iata}'";
  
  $res = $GLOBALS['rsdb']->query( $q );

	if( pg_num_rows( $res ) == 1 )
	{
    $id = pg_fetch_result( $res, 0 );
    $vliegveld = new Vliegveld();
    $vliegveld->loadById( $id );
    return ( $vliegveld );
	}
}

function getVliegvelden( $size=0, $start=0, $sort="", $ascdesc="", $crit=null )
{
	// Criterea
	if( is_array( $crit ) ) 
	{
		$where = '';
		foreach( $crit as $item )
		{
			if( $where )
				$where .= ' AND ';
			$where .= $item;			
		}
		$where = 'WHERE '.$where;
	}
		
	// Sorteren
	if( $sort ) 
	{
		$order = "ORDER BY $sort";
		if( $ascdesc=="asc" )
			$order .= " ASC";
		else
			$order .= " DESC";
	}
	else
		$order = "ORDER BY id ASC";
	
	// Resultaat opdelen in pagina's
	if( $size )
	{
		$limit = "LIMIT $size";
		if( $start )
			$limit .= " OFFSET $start";
	}
	
  $q = "SELECT * FROM vliegveld $where $order $limit ";
  
  $res = $GLOBALS['rsdb']->query( $q );

	for( $i=0; $i<pg_num_rows( $res ); $i++ )
 	{
		$row = pg_fetch_array( $res, $i, PGSQL_ASSOC );
    $array[ $row['id'] ] = new Stad(null, $row);
	}
  return $array;
}


class Vliegveld extends Beschrijving
{
  var $iata; 
  
  function Vliegveld($Land = null, $array = null)
  {
    Beschrijving::Beschrijving();
    $this->tabelNaam = "vliegveld";    
    $this->Land = $Land;
    
    if ($array)
    {
      $this->loadArray($array);
    }
  }
  
  function loadArray($array = null)
  {
    $this->iata = $array['iata'];    
    parent::loadArray($array);    
  }
  
  function store($array = null)
  {
    $array['iata'] = $this->getIATA();
    return parent::store( $array );
  }

  function post($array)
  {
    $resultArray = array();
    $this->iata = $array['iata'];

    if ( !$array['beschrijving_kort'] )
    {
      $array['beschrijving_kort'] = $array['beschrijving'];
    }
    
    if ( !$array['iata'] ) $resultArray['iata'] = 'EMPTY';

    $resultArray += parent::post($array);    
    return $resultArray;
  }

  function getIATA()
  {
  	return $this->iata;
  }
 
}
?>
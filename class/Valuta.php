<?
/*

  Valuta.php

*/

function getValutas()
{
  $q = "SELECT * FROM valuta";
  $res = $GLOBALS['rsdb']->query( $q );
	for( $i=0; $i<pg_num_rows( $res ); $i++ )
 	{
		$row = pg_fetch_array( $res, $i, PGSQL_ASSOC );
    $array[] = new Valuta( $row );
	}
	return $array;
}

function getMultiByCode( $code )
{
  $q = "SELECT multi FROM valuta WHERE code = '$code'";
  $res = $GLOBALS['rsdb']->query( $q );
	$r = pg_fetch_result( $res, 0 );

  if ( $r )
  {
  	return $r;
  }
  else
  {
    return 1;
  }
}


class Valuta extends DBRecord
{
  var $code;
  var $multi;

  function Valuta( $array=null )
  {
    DBRecord::DBRecord( 'rsdb' );
    $this->tabelNaam = "valuta"; 
    
    if ( $array )
    {
      $this->loadArray( $array );
    }
  }

  function loadArray($array)
  {
    $this->id = $array['id'];
    $this->code = $array['code'];
    $this->multi = $array['multi'];
  }

  function post($array)
  {
    $resultArray = array();
    $this->id = $array['id'];
    $this->code = $array['code'];
    $this->multi = $array['multi'];
    return $resultArray;
  }

  
  function store($array = null)
  {
    $resultArray = array();
    $storeArray['id'] = $this->id;
    $storeArray['code'] = $this->code;
    $storeArray['multi'] = $this->multi;
    $resultArray = parent::store($storeArray);
    return $resultArray;
  }
  
  function getCode()
  {
    return $this->code;
  }
  
  function getMulti()
  {
    return $this->multi;
  }
  
  function setMulti( $multi )
  {
    $this->multi = $multi;
  }

  function render()
  {
    echo '<tr>';
    echo '<td><input type="text" name="code[]" value="'.$this->getCode().'"></td>';
    echo '<td><input type="text" name="multi[]" value="'.$this->getMulti().'"></td>';
    echo '<td><input type="hidden" name="id[]" value="'.$this->getId().'"></td>';
    echo '</tr>';
  }
}
?>
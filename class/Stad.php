<?
/*

  Stad.php

*/

function getAantalSteden() {
	$q = "SELECT COUNT(id) AS num FROM stad";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	
	return 0;

}

function countStadOnEventType($eventTypeID) {
	$q = "SELECT COUNT(s.id) AS num FROM stad s , locatie l LEFT JOIN event on l.id = event.locatie_id" . 
		" WHERE l.stad_id = s.id and event.event_type_id = $eventTypeID ";
	$res 	= $GLOBALS ['rsdb']->query ( $q );
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	return 0;

}

function getStadOnEventType($size = 0, $start = 0, $sort = "", $ascdesc = "", $eventTypeId = null) {
	if ($size) {
		$limit = "LIMIT $size";
		if ($start)
			$limit .= " OFFSET $start";
	}
	
	// Sorteren
	if ($sort) {  
		$order = "ORDER BY s.$sort";
		if ($ascdesc == "asc")
			$order .= " ASC";
		else
			$order .= " DESC";
	} else{
		$order = "ORDER BY s.id ASC";
	}
	 
	if ($eventTypeId) {
		$q = "SELECT  l.*, s.* FROM  stad s , locatie l" . 
				" LEFT JOIN event on l.id = event.locatie_id" . 
				" WHERE  l.stad_id = s.id and event.event_type_id = $eventTypeId" . 
				" $order $limit";
		
		$res = $GLOBALS ['rsdb']->query ( $q );
		
		if( $res ){
			if (pg_num_rows ( $res ) > 0 ){
				for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
					$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
					$array [$row ['id']] = new Stad ( null, $row );
				}
			}
		}
		
	}
	return $array;

}

function getSteden($size = 0, $start = 0, $sort = "", $ascdesc = "", $crit = null) {
	// Criterea
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	}
	
	// Sorteren
	if ($sort) {
		$order = "ORDER BY $sort";
		if ($ascdesc == "asc")
			$order .= " ASC";
		else
			$order .= " DESC";
	} else
		$order = "ORDER BY id ASC";
		
	// Resultaat opdelen in pagina's
	if ($size) {
		$limit = "LIMIT $size";
		if ($start)
			$limit .= " OFFSET $start";
	}
	
	$q = "SELECT * FROM stad $where $order $limit ";
	
	$res = $GLOBALS ['rsdb']->query ( $q );
	
	for($i = 0; $i < pg_num_rows ( $res ); $i ++) {
		$row = pg_fetch_array ( $res, $i, PGSQL_ASSOC );
		$array [$row ['id']] = new Stad ( null, $row );
	}
	return $array;
}

class Stad extends Webentiteit {
	var $Land;
	var $land_id;
	
	var $iata;
	var $gta_code;
	var $hotelpronto_id;
	
	var $hotel_prijs;
	
	var $vendor_code;
	var $vendor_id;
	
	function Stad($Land = null, $array = null) {
		Webentiteit::Webentiteit ();
		$this->tabelNaam = "stad";
		$this->Land = $Land;
		
		if ($array) {
			$this->loadArray ( $array );
		}
	}
	
	function loadArray($array = null) {
		$this->land_id = $array ['land_id'];
		$this->iata = $array ['iata'];
		$this->gta_code = $array ['gta_code'];
		$this->hotelpronto_id = $array ['hotelpronto_id'];
		$this->hotel_prijs = $array ['hotel_prijs'];
		$this->hotel_eenpersoonskamer_toeslag = $array ['hotel_eenpersoonskamer_toeslag'];
		$this->vendor_id = $array ['vendor_id'];
		$this->vendor_key = $array ['vendor_key'];
		
		parent::loadArray ( $array );
	}
	
	function store($array = null) {
		$array ['land_id'] = $this->getLandId ();
		$array ['iata'] = $this->getIATA ();
		$array ['vendor_id'] = $this->getVendorId ();
		$array ['vendor_key'] = $this->getVendorKey ();
		$array ['gta_code'] = $this->getGTACode ();
		$array ['hotelpronto_id'] = $this->getHotelprontoId ();
		$array ['hotel_prijs'] = $this->getHotelPrijs ();
		$array ['hotel_eenpersoonskamer_toeslag'] = $this->getHotelEenpersoonskamerToeslag ();
		
		return parent::store ( $array );
	}
	
	function post($array) {
		$resultArray = array ();
		$this->land_id = $array ['land_id'];
		$this->iata = $array ['iata'];
		$this->gta_code = $array ['gta_code'];
		$this->hotelpronto_id = $array ['hotelpronto_id'];
		$this->hotel_prijs = $array ['hotel_prijs'];
		$this->hotel_eenpersoonskamer_toeslag = $array ['hotel_eenpersoonskamer_toeslag'];
		$this->vendor_id = $array ['vendor_id'];
		$this->vendor_key = $array ['vendor_key'];
		
		if (! $array ['land_id'])
			$resultArray ['land_id'] = 'EMPTY';
		if (! $array ['iata'])
			$resultArray ['iata'] = 'EMPTY';
		if (! $array ['hotel_prijs'])
			$resultArray ['hotel_prijs'] = 'EMPTY';
		if (! $array ['hotel_eenpersoonskamer_toeslag'])
			$resultArray ['hotel_eenpersoonskamer_toeslag'] = 'EMPTY';
		
		$resultArray += parent::post ( $array );
		return $resultArray;
	}
	
	function getLandId() {
		if (is_object ( $this->Land ))
			$this->land_id = $this->Land->getID ();
		
		return $this->land_id;
	}
	
	function getLand() {
		if (! is_object ( $this->Land ) && $this->land_id) {
			$this->Land = new Land ( );
			$this->Land->loadById ( $this->land_id );
		}
		
		return $this->Land;
	}
	
	// getNaam function which avoids locale. Quick hack for departure airports
	function getDutchNaam() {
		return $this->naam;
	}
	
	function getGTACode() {
		return $this->gta_code;
	}
	
	function getHotelprontoId() {
		return $this->hotelpronto_id;
	}
	
	function setGTACode($gta_code) {
		$this->gta_code = $gta_code;
	}
	
	function getIATA() {
		return $this->iata;
	}
	
	function getHotelPrijs() {
		return $this->hotel_prijs;
	}
	
	function getHotelEenpersoonskamerToeslag() {
		return $this->hotel_eenpersoonskamer_toeslag;
	}
	
	function getLocaties() {
		$q = $this->db->query ( "SELECT * FROM locatie WHERE stad_id={$this->id} ORDER BY naam DESC" );
		
		for($i = 0; $i < pg_num_rows ( $q ); $i ++) {
			$r = pg_fetch_array ( $q, $i, PGSQL_ASSOC );
			$array [] = new Locatie ( $this, $r );
		}
		return $array;
	}
	
	function getVendorId() {
		return $this->vendor_id;
	}
	
	function getVendorKey() {
		return $this->vendor_key;
	}
	
	function setVendorId($vendorId) {
		$this->vendor_id = $vendorId;
	}
	
	function setVendorKey($vendorKey) {
		$this->vendor_key = $vendorKey;
	}

}
?>
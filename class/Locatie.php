<?
/*

  Locatie.php

*/

function getAantalLocaties(  )
{
  $q = "SELECT COUNT(id) AS num FROM locatie";
  
  $res = $GLOBALS['rsdb']->query( $q );

	if( pg_num_rows( $res ) == 1 )
		return pg_fetch_result( $res, 0, 'num' );

	return 0;

}

 
function countLocactieOnEventType($crit = null) {
	if (is_array ( $crit )) {
		$where = '';
		foreach ( $crit as $item ) {
			if ($where)
				$where .= ' AND ';
			$where .= $item;
		}
		$where = 'WHERE ' . $where;
	} else { $where = NULL; }
	
	$q 		= "SELECT COUNT(locatie.id) AS num FROM locatie LEFT JOIN event on locatie.id = event.locatie_id $where ";
	$res 	= $GLOBALS ['rsdb']->query ( $q );
	if (pg_num_rows ( $res ) == 1)
		return pg_fetch_result ( $res, 0, 'num' );
	return 0;
}
function joinLocactieEvent( $size=0, $start=0, $sort="", $ascdesc="", $crit=null  ){


	if( is_array( $crit ) ) 	{
		$where = '';
		foreach( $crit as $item )
		{
			if( $where )
				$where .= ' AND ';
			$where .= $item;			
		}
		$where = 'WHERE '.$where;
	}
	// Sorteren
	if( $sort ) 
	{
		$order = "ORDER BY $sort";
		if( $ascdesc=="asc" )
			$order .= " ASC";
		else
			$order .= " DESC";
	}
	else
		$order = "ORDER BY id ASC";
		
	// Resultaat opdelen in pagina's
	if( $size )
	{
		$limit = "LIMIT $size";
		if( $start )
			$limit .= " OFFSET $start";
	}
		
	$q = "SELECT  locatie.* FROM locatie LEFT JOIN event on locatie.id = event.locatie_id $where $order $limit ";
   $res = $GLOBALS['rsdb']->query( $q );

	for( $i=0; $i<pg_num_rows( $res ); $i++ )
 	{
		$row = pg_fetch_array( $res, $i, PGSQL_ASSOC );
    $array[] = new Locatie(null, $row);
	}
  return $array;
}

function getLocaties( $size=0, $start=0, $sort="", $ascdesc="", $crit=null )
{
	// Criterea
	if( is_array( $crit ) ) 
	{
		$where = '';
		foreach( $crit as $item )
		{
			if( $where )
				$where .= ' AND ';
			$where .= $item;			
		}
		$where = 'WHERE '.$where;
	}
		
	// Sorteren
	if( $sort ) 
	{
		$order = "ORDER BY $sort";
		if( $ascdesc=="asc" )
			$order .= " ASC";
		else
			$order .= " DESC";
	}
	else
		$order = "ORDER BY id ASC";
	
	// Resultaat opdelen in pagina's
	if( $size )
	{
		$limit = "LIMIT $size";
		if( $start )
			$limit .= " OFFSET $start";
	}
	
  $q = "SELECT * FROM locatie $where $order $limit ";
  
  $res = $GLOBALS['rsdb']->query( $q );

	for( $i=0; $i<pg_num_rows( $res ); $i++ )
 	{
		$row = pg_fetch_array( $res, $i, PGSQL_ASSOC );
    $array[] = new Locatie(null, $row);
	}
  return $array;
}

class Locatie extends Webentiteit
{
  var $Stad;

  var $stad_id;
  var $marge_prijs;
  var $adres;
  var $zitplaats;

  function Locatie($Stad = null, $array = null)
  {
    Webentiteit::Webentiteit();
    $this->tabelNaam = "locatie";
    $this->Stad = $Stad;
    
    if ($array)
    {
      $this->loadArray($array);
    }
    
  }

  function loadArray($array = null)
  {
    $this->stad_id = $array['stad_id'];
    $this->adres = $array['adres'];
    $this->zitplaats = $array['zitplaats'];
    parent::loadArray($array);    
  }

  function store( $array = null )
  {
    $array['stad_id'] = $this->getStadId();
    $array['adres'] = $this->adres;
    $array['zitplaats'] = $this->zitplaats;
    parent::store( $array );
  }

  function post($array)
  {
    $resultArray = array();
    $this->stad_id = $array['stad_id'];
    $this->adres = $array['adres'];
    $this->zitplaats = pg_escape_string( stripslashes($array['zitplaats']) );

    if ( !$array['stad_id'] ) $resultArray['stad_id'] = 'EMPTY';
    if ( !$array['land_id'] ) $resultArray['land_id'] = 'EMPTY';  
    
    $resultArray += parent::post($array);    
    return $resultArray;
  }

  function getStadId()
  {
    if( $this->Stad )
    	$this->stad_id = $this->Stad->getID();

    return $this->stad_id;
  }
  
  function getStad()
  {
    if (!is_object($this->Stad) && $this->stad_id)
    {
    	$this->Stad = new Stad();
    	$this->Stad->loadById( $this->stad_id );
    }

    return $this->Stad;
  }

  function getAdres()
  {
    return $this->adres;
  }

  function getZitplaats()
  {
    return $this->zitplaats;
  }

  function getEvents()
  {
    $q = $this->db->query("SELECT * FROM event WHERE locatie_id={$this->id} ORDER BY naam DESC");

  	for( $i=0; $i<pg_num_rows( $q ); $i++ )
  	{
  		$r = pg_fetch_array( $q, $i, PGSQL_ASSOC );
      $array[] = new Event($this, $r);
  	}
    return $array;
  }

  
}
?>
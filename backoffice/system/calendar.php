<html>
<head>
<title>calendar</title>
<link rel="stylesheet" type="text/css" href="../../stylesheets/calendar.css">
<script type="text/javascript">
<!--

function geefDoor(j, m, d)
{
  f = top.window.dateToId;
  
  if (m<10) m = "0" + m;
  if (d<10) d = "0" + d;

//  f.value = d + "-" + m + "-" + j;
  f.value = j + "-" + m + "-" + d;
  top.window.killMenu('date');
}
-->
</script>
</head>
<body margin="0" leftmargin="0" topmargin="0" marginwidth="0">

<center>
<div id="container">
<?
/*
	kalender code / 2003
*/
$mj_array = split(" ",date("m Y d"));

if (!$m=@$_GET['m']) $m=$mj_array[0];
if (!$j=@$_GET['j']) $j=$mj_array[1];

if ($m==$mj_array[0] && $j==$mj_array[1]) $d = $mj_array[2];

$feed = mktime(0,0,0,$m,1,$j);
$kalender = split(" ",date("t j Y F n",$feed));

$offset = date("w",strtotime ("1 $kalender[3] $kalender[2]"));
if (!$offset) $offset=7;	// maandag eerste dag :)
$dag=1;
$jaar = $kalender[2];
$maand = $kalender[4];

echo "<table align=center valign=center width=\"160\" height=\"140\">";

echo '<tr><td class="title" colspan="6">Selecteer de datum</td><td><a style="float:right" href="#" onclick="top.window.killMenu(\'date\')"><img src="../i/del.gif" border="0" /></a></td></tr>';

echo "<tr class=\"dag\"><td>M</td><td>D</td><td>W</td><td>D</td><td>V</td><td>Z</td><td>Z</td></tr>";

for ($x=0; $x<6; $x++)
{
 echo "<tr>";
 for ($y=0; $y<7; $y++)
 {
  if ($dag>=$offset && $dag-$offset<$kalender[0])
  {
   $dagnummer = $dag-($offset)+1;
   if ($dagnummer == @$d) $kleur = "E6F6FF";
    else $kleur = "aaaaaaa";

  $res = 0;

   echo "<td onmouseover=\"this.bgcolor='#ffffff'\" onmouseout=\"this.bgcolor='#$kleur'\" align=\"center\" bgColor=\"#$kleur\"";

   //if ($m >= $mj_array[0] && $dagnummer >= $mj_array[2])
   {
     echo "onclick=\"geefDoor($jaar, $maand, $dagnummer)\"";
     echo 'class="day"';
   }
   
   echo '>';
   echo $dagnummer;

   echo "</td>";
  }
  else echo "<td></td>";

  $dag++;
 }
 echo "</tr>";
}
echo "</table>";

?>
<form style="margin-bottom:0px; margin-top:4px" name="datum" method="GET" action="<? echo $_SERVER['PHP_SELF'] ?>">

<select class="month" name="m" onChange="datum.submit()">
<option <? if ($m==1) echo "selected " ?>value="1">januari
<option <? if ($m==2) echo "selected " ?>value="2">februari
<option <? if ($m==3) echo "selected " ?>value="3">maart
<option <? if ($m==4) echo "selected " ?>value="4">april
<option <? if ($m==5) echo "selected " ?>value="5">mei
<option <? if ($m==6) echo "selected " ?>value="6">juni
<option <? if ($m==7) echo "selected " ?>value="7">juli
<option <? if ($m==8) echo "selected " ?>value="8">augustus
<option <? if ($m==9) echo "selected " ?>value="9">september
<option <? if ($m==10) echo "selected " ?>value="10">oktober
<option <? if ($m==11) echo "selected " ?>value="11">november
<option <? if ($m==12) echo "selected " ?>value="12">december
</select>

<select name="j" onChange="datum.submit()">

<?

$thisYear = date("Y");
$period = $thisYear + 2;

for ($x=$thisYear; $x <= $period; $x++)
{
  echo '<option ';
  if ($j == $x) echo 'selected ';
  echo 'value="'.$x.'">'.$x;
  echo '</option>';
}

?>
<!-- <?=$jaar ?>-->
</select>

</form>
</div>
</center>

</body>
</html>

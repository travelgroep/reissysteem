<?
  header("Content-Type:text/html;charset=utf-8");

  require_once("../../inc/config.php");
  $id = @$_POST['id'];
  $naam = @$_POST['naam'];
  $beschrijving = @$_POST['beschrijving'];
  $beschrijving_kort = @$_POST['beschrijving_kort'];

  $db = new DB();  

  if ($id)
  {
    $obj = new Beschrijving();
 
//    $obj->post($_POST);

    $obj->loadById( $id );
    $obj->setNaam($naam);
    $obj->setBeschrijving($beschrijving);
    $obj->setBeschrijvingKort($beschrijving_kort);
    $obj->store();   
  }  
?>
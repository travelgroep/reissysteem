<?

require_once ("../../inc/config.php");
if (! $beschrijving_id = @$_GET ['beschrijving_id'])
	die ();

$db = new DB ( );

?>
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="../../stylesheets/layout_foto.css" />
</head>
<body margin="0">
<?

$object = new Beschrijving ( );
$object->loadById ( $beschrijving_id );

$fotoArray = $object->getFotos ( $_SESSION ['taalcode'], null, true );
//echo "<br/>taal =>". $_SESSION ['taalcode'];
//echo "<pre>";
//echo print_r ( $fotoArray, true );
//echo "</pre>";

if (is_array ( $fotoArray )) {
	echo '<table border="0">';
	
	$width = 5;
	for($x = 0; $x < count ( $fotoArray ); $x ++) {
		if ($x % $width == 0)
			echo '<tr>';
		$fotoId = $fotoArray [$x]->getId ();
		if ($img = $fotoArray [$x]->getSrc ( 40 )) {
			echo '<td><a title="bekijk foto" href="showPic.php?id=' . $fotoArray [$x]->getId () . '"><img border="0" src="' . $img . '" /></a><br />' 
				. $fotoArray [$x]->getNaam () . '(' . $fotoArray [$x]->getTaalcode () 
				. ') <a title="foto verwijderen" href="deleteFoto.php?id=' . $fotoId . '&beschrijving_id=' . $beschrijving_id . '"><img src="../i/del.gif" border="0" />'
				. '</a></td>';
		} else {
            echo '<td><a title="bekijk foto" href="showPic.php?id=' . $fotoArray [$x]->getId () . '"><img border="0" src="' . $img . '" /></a><br />' 
                . $fotoArray [$x]->getNaam () . '(' . $fotoArray [$x]->getTaalcode () 
                . ') <a title="foto verwijderen" href="deleteFoto.php?id=' . $fotoId . '&beschrijving_id=' . $beschrijving_id . '"><img src="../i/del.gif" border="0" />'
                . '</a></td>';
		}
		
		if ($x % $width == 4)
			echo '</tr>';
	}
	echo '</table>';
}

?>

<h1>Foto toevoegen:</h1>

<form enctype="multipart/form-data" action="uploadFoto.php"
	method="post">
<table class="input_tabel">
	<input type="hidden" name="unique" value="<?=$unique?>" />
	<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
	<input type="hidden" name="beschrijving_id" value="<?=$beschrijving_id?>" />
	<tr>
		<td>bestand</td>
		<td><input class="input_veld" name="bestand" type="file" size="5" /></td>
	</tr>
	<tr>
		<td>naam</td>
		<td><input class="input_veld" name="naam" type="text" maxlength="20" /></td>
	</tr>
	<tr>
		<td>zichtbaarheid</td>
		<td><select name="taalcode">
<?
if (isset ( $_SESSION ['taalcode'] )) {
	//echo '<option value="' . $_SESSION ['taalcode'] . '">' . $_SESSION ['taalcode'] . '</option>';
}


  $talen = getTalen();
  $talen[] = '';

	foreach( $talen as $taal )
 	{
 	  $s = ( $_SESSION['taalcode'] == $taal['taalcode'] ) ? ' selected' : '';
    if ( $taal['taalcode'] ) echo '<option' . $s . '>' . $taal['taalcode'] . '</option>';
	}

?>
			<option value="alle">alle talen</option>
		</select></td>
	</tr>
	<tr>
		<td></td>
		<td><input class="input_veld" type="submit" name="submit" value="voeg toe" /></td>
	</tr>
</table>
</form>

</body>
</html>
<?
require_once ("../../../inc/config.php");

$db = new DB ( );
$matchModelManager = new MatchModelManager();
$start = 0;
$paginagrootte = PAGINAGROOTTE;

/*
     * Deze 3 velden bepalen hoe de lijst getoond wordt,
     * zoals sortering, en welke pagina. Deze moeten 
     * altijd worden doorgegeven als naar 'PHP_SELF' 
     * wordt verwezen.
     */
$start = $_POST ['start'];
if (! $sort = $_POST ['sort'])
	$sort = "id";
if (! $ascdesc = $_POST ['ascdesc'])
	$ascdesc = "desc";

$currentSort = "sort={$sort}&ascdesc={$ascdesc}&start={$start}";

$arrangementChangelogModelManager = new ArrangementChangelogModelManager ( );
$arrangementChangelogs = $arrangementChangelogModelManager->findAll ();

$aantal_objecten = count ( $arrangementChangelogs );

if ($arrangementChangelogs) {
	
	print "<table class=\"overzicht\">\n";
	print " <tr>\n";
	
	$fields = array ('id', 'Match', 'ChangeField' , 'Old value' , 'New value', 'Change date',  );
	
	print "     <th></th>\n";
	foreach ( $fields as $field ) {
		if ($field == $sort && $ascdesc == 'desc') {
			$fld_ascdesc = 'asc';
			$pic = "up.png";
		} else if ($field == $sort) {
			$fld_ascdesc = 'desc';
			$pic = "down.png";
		} else {
			$fld_ascdesc = 'desc';
			$pic = "leeg.gif";
		}
		
		//$p = "sort={$field}&ascdesc={$fld_ascdesc}&start={$start}";
		//$onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
		
		print "<th>$field </th>";
	}
	
	print " </tr>";
	
	$odd_even = "even";
	$teller = $start;
	
	foreach ( $arrangementChangelogs as $arrangementChangelog ) {
        $arrangement = new Arrangement();
		$arrangement->loadById( $arrangementChangelog->getArrangementId() );
		 
		
		
		if ($odd_even == 'odd')
			$rowColor = '#B1B1F9';
		else
			$rowColor = '#ffffff';
			
		// $start telt vanaf 0,
		// maar wij tellen vanaf 1.
		$teller ++;
		
		print " <tr class=\"" . $odd_even . "\" >\n";
		
		print "     <td>$teller</td>\n";
		print "     <td>" . $arrangementChangelog->getId () . "</td>\n";

		$isArr = !empty ($arrangement);
		echo "<td>";
		
        if ($isArr && $arrangement->getId()) {
            $matchName = sprintf('%s - %s',
              $arrangement->getEvent()->getNaam(),
              $arrangement->getNaam()
              );
            echo "<a href='".PATH. "backoffice/arrangement/edit.php?id=" . $arrangement->getId() ."'>".$matchName."</a>";
            $match = $matchModelManager->find($arrangement->getOptaMatchId());
            echo " (<a href='".PATH. "backoffice/opta/index.php?view=3&compid=".$match->getCompetitionId()."#".$match->getId()."'>".$match->getId()."</a>)";
        } else {
            echo "Het arrangement is leeg";
        }		
		
		$olddateTime = new \DateTime( $arrangementChangelog->getOldValue ());
		$newDateTime = new \DateTime( $arrangementChangelog->getNewValue ());

		//$olddateTime->setTimezone(new DateTimeZone($timezone));
		//$newDateTime->setTimezone(new DateTimeZone($timezone));
		
		echo "</td>";  
		print "     <td>" . $arrangementChangelog->getChangedField (). "</td>\n";
		print "     <td>" . $olddateTime->format("Y-m-d H:i:s") . "</td>\n";
		print "     <td>" . $newDateTime->format("Y-m-d H:i:s") . "</td>\n";
		print "     <td>" . $arrangementChangelog->getCreatedAt () . "</td>\n";
		
		print " </tr>\n";
		
		$odd_even = ($odd_even == "even") ? "odd" : "even";
	}
	
	// Navigatie
	print "<tr><td colspan=\"6\">\n";
	
	// Vorige pagina
	//$prev = $start - $paginagrootte;
	
//	if ($prev >= 0) {
//		$p = "sort={$sort}&ascdesc={$ascdesc}&start={$prev}";
//		$onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
//		print "     <a onclick=\"{$onclick}\" title=\"vorige\" href=\"javascript:X()\">&lt;</a>\n";
//	} 
//
//	else
//		print "     &lt;\n";
//		
//	// Stappen
//	for($i = 0; $i < $aantal_objecten; $i += $paginagrootte) {
//		if ($start != $i) {
//			$p = "sort={$sort}&ascdesc={$ascdesc}&start={$i}";
//			$onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
//			
//			print "     <a onclick=\"{$onclick}\" href=\"javascript:X()\">" . ($i + 1) . "</a>\n";
//		} else
//			print $i + 1;
//	}
//	
//	// Volgende pagina
//	$next = $start + $paginagrootte;
//	if ($next < $aantal_objecten) {
//		$p = "sort={$sort}&ascdesc={$ascdesc}&start={$next}";
//		$onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
//		print "     <a onclick=\"{$onclick}\" title=\"volgende\" href=\"javascript:X()\">&gt;</a>\n";
//	} 
//
//	else
//		print "     &gt;\n";
		
	
	print "</tr>\n";
	
	print " </table>\n";

} else {
	print "<i>Geen opta gevonden!</i>";
}


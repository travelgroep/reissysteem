<?
require_once ("../../../inc/config.php");

$db = new DB ( );
$start = 0;
$paginagrootte = PAGINAGROOTTE;

/*
     * Deze 3 velden bepalen hoe de lijst getoond wordt,
     * zoals sortering, en welke pagina. Deze moeten 
     * altijd worden doorgegeven als naar 'PHP_SELF' 
     * wordt verwezen.
     */
$start = $_POST ['start'];
if (! $sort = $_POST ['sort'])
	$sort = "id";
if (! $ascdesc = $_POST ['ascdesc'])
	$ascdesc = "desc";

$currentSort = "sort={$sort}&ascdesc={$ascdesc}&start={$start}";

$matchChangelogModelManager = new MatchChangelogModelManager ( );
$matchChangelogs = $matchChangelogModelManager->findAll ();

//$objecten = getVliegvelden( $paginagrootte, $start, $sort, $ascdesc );
$aantal_objecten = count ( $matchChangelogs );

if ($matchChangelogs) {
	
	print "<table class=\"overzicht\">\n";
	print " <tr>\n";
	
	$fields = array ('id', 'Match', 'ChangeField' , 'Old value' , 'New value', 'Change date',  );
	
	print "     <th></th>\n";
	foreach ( $fields as $field ) {
		if ($field == $sort && $ascdesc == 'desc') {
			$fld_ascdesc = 'asc';
			$pic = "up.png";
		} else if ($field == $sort) {
			$fld_ascdesc = 'desc';
			$pic = "down.png";
		} else {
			$fld_ascdesc = 'desc';
			$pic = "leeg.gif";
		}
		
		$p = "sort={$field}&ascdesc={$fld_ascdesc}&start={$start}";
		$onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
		
		print "<th><a href=\"javascript:X()\" onclick=\"{$onclick}\">$field <img src=\"../i/$pic\" border=\"0\" /></a></th>";
	}
	
	print " </tr>";
	
	$odd_even = "even";
	$teller = $start;
	
	$matchModelManager= new MatchModelManager ( );
	$teamModelManager= new TeamModelManager ( );
	
	
	foreach ( $matchChangelogs as $matchChangelog ) {
		
		$matchId = $matchChangelog->getMatchId();
		$crit []       = "opta_match_id = '$matchId' "; 
		$arrangementen = getArrangementen( $paginagrootte, $start, $sort, $ascdesc, $crit );
		$arrangement = $arrangementen[0];
		 
	    $match = $matchModelManager->find( $matchId );
		//$homeTeam = $teamModelManager->find( $match->getHomeTeamId() );
		//$awayTeam = $teamModelManager->find( $match->getAwayTeamId() );
		
		if ($odd_even == 'odd')
			$rowColor = '#B1B1F9';
		else
			$rowColor = '#ffffff';
			
		// $start telt vanaf 0,
		// maar wij tellen vanaf 1.
		$teller ++;
		
		print " <tr class=\"" . $odd_even . "\" >\n";
		
		print "     <td>$teller</td>\n";
		print "     <td>" . $matchChangelog->getId () . "</td>\n";
		
		$isArr = !empty ($arrangement);
		echo "<td>";
		if ($isArr) {
			echo "<a href='".PATH. "backoffice/arrangement/edit.php?id=" . $arrangement->getId() ."'>";
			  echo $arrangement->getNaam() ." - " . $arrangement->getEvent()->getNaam();
			echo "</a>";
		} else {
			echo "Opta id => " .$matchId;
		}
		
		$createDateTime = new \DateTime( $matchChangelog->getCreatedAt());
		
		switch($matchChangelog->getChangedField ())
		{
			case 'kickoffDate':
	            $olddateTime = new \DateTime( $matchChangelog->getOldValue ());
	            $newDateTime = new \DateTime( $matchChangelog->getNewValue ());
	            $oldValue = $olddateTime->format(\DateTime::W3C);
                $newValue = $newDateTime->format(\DateTime::W3C);
	            break;
			case 'isDateConfirmed':
                $oldValue = ($matchChangelog->getOldValue() ? 'true' : 'false');
                $newValue = ($matchChangelog->getNewValue() ? 'true' : 'false');
                break;                
            default:
                $oldValue = $matchChangelog->getOldValue();
                $newValue = $matchChangelog->getNewValue();
            	break;
		}
		
		echo "</td>";
		print "     <td>" . $matchChangelog->getChangedField (). "</td>\n";
		print "     <td>" . $oldValue . "</td>\n";
		print "     <td>" . $newValue . "</td>\n";
		print "     <td>" . $createDateTime->format("Y-m-d H:i:s") . "</td>\n";
		print " </tr>\n";
		
		$odd_even = ($odd_even == "even") ? "odd" : "even";
	}
	
	// Navigatie
	print "<tr><td colspan=\"6\">\n";
	
	// Vorige pagina
	$prev = $start - $paginagrootte;
	
	if ($prev >= 0) {
		$p = "sort={$sort}&ascdesc={$ascdesc}&start={$prev}";
		$onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
		print "     <a onclick=\"{$onclick}\" title=\"vorige\" href=\"javascript:X()\">&lt;</a>\n";
	} 

	else
		print "     &lt;\n";
		
	// Stappen
	for($i = 0; $i < $aantal_objecten; $i += $paginagrootte) {
		if ($start != $i) {
			$p = "sort={$sort}&ascdesc={$ascdesc}&start={$i}";
			$onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
			
			print "     <a onclick=\"{$onclick}\" href=\"javascript:X()\">" . ($i + 1) . "</a>\n";
		} else
			print $i + 1;
	}
	
	// Volgende pagina
	$next = $start + $paginagrootte;
	if ($next < $aantal_objecten) {
		$p = "sort={$sort}&ascdesc={$ascdesc}&start={$next}";
		$onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
		print "     <a onclick=\"{$onclick}\" title=\"volgende\" href=\"javascript:X()\">&gt;</a>\n";
	} 

	else
		print "     &gt;\n";
		
	
	print "</tr>\n";
	
	print " </table>\n";

} else {
	print "<i>Geen opta gevonden!</i>";
}


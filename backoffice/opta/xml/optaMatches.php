<?
require_once ("../../../inc/config.php");

$db = new DB ( );
$start = 0;
$paginagrootte = PAGINAGROOTTE;

/*
     * Deze 3 velden bepalen hoe de lijst getoond wordt,
     * zoals sortering, en welke pagina. Deze moeten 
     * altijd worden doorgegeven als naar 'PHP_SELF' 
     * wordt verwezen.
     */


$start = $_POST ['start'];
if (! $sort = $_POST ['sort'])
    $sort = "id";
if (! $ascdesc = $_POST ['ascdesc'])
    $ascdesc = "desc";

$currentSort = "sort={$sort}&ascdesc={$ascdesc}&start={$start}";

 
$compid = $_POST ['compid'];


$competitionModelManager= new CompetitionModelManager ();
$competition = $competitionModelManager->find ( $compid );

$matchModelManager = new MatchModelManager ( );
$matches = $matchModelManager->findByCompetitionId ($compid);

$venueModelManager = new VenueModelManager  ( );

$aantal_objecten = count ( $matches );

if ($matches) {
    
    print '<h3>'.$competition->getName().'</h3>'."\n";
    
    print '<form action="'.OptaMatchController::generateUrl(OptaMatchController::URL_PREVIEW_CONFIRM_PERIOD).'" method="POST">';
        print '<input type="hidden" name="competitionId" value="'.$competition->getId().'">';
        print '<input type="radio" name="confirm" value="true" style="width: auto";>Confirm ';
        print '<input type="radio" name="confirm" value="false" style="width: auto";>Unconfirm ';
        print ' From: <input type="text" name="fromDate" style="width: 80px;">';
        print ' To: <input type="text" name="toDate" style="width: 80px;">';
        print '<input type="submit" name="submit" value="Submit" style="width: 80px;">';
    print "</form>";

    print "<br>";
    $updateArrangementUrl = OptaMatchController::generateUrl(OptaMatchController::URL_UPDATE_ARRANGEMENT).'?competitionId='.$competition->getId();
    print '<a href="'.$updateArrangementUrl.'" onClick="return confirm(\'Are you sure?\');">Update arrangements</a>';
    print "<table class=\"overzicht\">\n";
    print " <tr>\n";
    
    $fields = array('id', 'MatchDay', 'Match', 'Match Date/Time', 'Confirmed', 'Update enable', 'Period', 'Location', 'City', 'Action(s)');
    
    foreach ( $fields as $field ) {
        if ($field == $sort && $ascdesc == 'desc') {
            $fld_ascdesc = 'asc';
            $pic = "up.png";
        } else if ($field == $sort) {
            $fld_ascdesc = 'desc';
            $pic = "down.png";
        } else {
            $fld_ascdesc = 'desc';
            $pic = "leeg.gif";
        }
        
        $p = "sort={$field}&ascdesc={$fld_ascdesc}&start={$start}";
        $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
        
        print "<th><a href=\"javascript:X()\" onclick=\"{$onclick}\">$field <img src=\"../i/$pic\" border=\"0\" /></a></th>";
    }
    
    print " </tr>";
    
    $odd_even = "even";
    $teller = $start;
    
    $teamModelManager= new TeamModelManager ( );
    foreach ( $matches as $match ) {
        
        $matchId = $match->getId();
//        $crit []       = "opta_match_id = '$matchId' "; 
//        $arrangementen = getArrangementen( $paginagrootte, $start, $sort, $ascdesc, $crit );
//        $arrangement = $arrangementen[0];
         
        $homeTeam = $teamModelManager->find( $match->getHomeTeamId() );
        $awayTeam = $teamModelManager->find( $match->getAwayTeamId() );
        $venue    = $venueModelManager->find( $match->getVenueId() );
        
        if ($odd_even == 'odd')
            $rowColor = '#B1B1F9';
        else
            $rowColor = '#ffffff';
            
        // $start telt vanaf 0,
        // maar wij tellen vanaf 1.
        $teller ++;
        
        print " <tr class=\"" . $odd_even . "\" id=\"".$match->getId()."\">\n";
        
        print "<td>"."(".$teller.") ".$match->getId()."</td>\n";
        print "<td>" . $match->getMatchDay      () . "</td>\n";
        print "<td>";
        echo $homeTeam->getName()." - ". $awayTeam->getName();
        echo "</td>";
        print "<td>" . $match->getKickoffDate (). "</td>\n";
        $isDateConfirmedString = $match->isDateConfirmed() ? 'yes' : 'no';
        print "<td>" . $isDateConfirmedString. "</td>\n";
        $updateEnable = $match->getUpdateEnable() ? 'yes' : 'no';
        print "<td>" . $updateEnable. "</td>\n";
        
        print "<td>" . $match->getPeriod () . "</td>\n";
        print "<td>" . $venue->getName () . "</td>\n";
        print "<td>" . $venue->getCity () . "</td>\n";
        $urlParams = '?matchId='.urlencode($match->getId());
        
        print '<td>';        
        if ($match->isDateConfirmed()) {
            $url = OptaMatchController::generateUrl(OptaMatchController::URL_UNCONFIRM_ACTION); 
            $url .= $urlParams;
            print '<a href="'.$url.'">Set unconfirmed</a>';
        } else {
            $url = OptaMatchController::generateUrl(OptaMatchController::URL_CONFIRM_ACTION); 
            $url .= $urlParams;
            print '<a href="'.$url.'">Set confirmed</a>';
        }

        print "<br>";
        if ($match->getUpdateEnable()) {
            $url = OptaMatchController::generateUrl(OptaMatchController::URL_UPDATE_DISABLE_ACTION); 
            $url .= $urlParams;
            print '<a href="'.$url.'">Disable update</a>';
        } else {
            $url = OptaMatchController::generateUrl(OptaMatchController::URL_UPDATE_ENABLE_ACTION); 
            $url .= $urlParams;
            print '<a href="'.$url.'">Enable update</a>';
        }
        print '</td>';        
        
        
        print " </tr>\n";
        
        $odd_even = ($odd_even == "even") ? "odd" : "even";
    }
    
    // Navigatie
    print "<tr><td colspan=\"6\">\n";
    
    // Vorige pagina
    $prev = $start - $paginagrootte;
    
    if ($prev >= 0) {
        $p = "sort={$sort}&ascdesc={$ascdesc}&start={$prev}";
        $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
        print "     <a onclick=\"{$onclick}\" title=\"vorige\" href=\"javascript:X()\">&lt;</a>\n";
    } 

    else
        print "     &lt;\n";
        
    // Stappen
    for($i = 0; $i < $aantal_objecten; $i += $paginagrootte) {
        if ($start != $i) {
            $p = "sort={$sort}&ascdesc={$ascdesc}&start={$i}";
            $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
            
            print "     <a onclick=\"{$onclick}\" href=\"javascript:X()\">" . ($i + 1) . "</a>\n";
        } else
            print $i + 1;
    }
    
    // Volgende pagina
    $next = $start + $paginagrootte;
    if ($next < $aantal_objecten) {
        $p = "sort={$sort}&ascdesc={$ascdesc}&start={$next}";
        $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
        print "     <a onclick=\"{$onclick}\" title=\"volgende\" href=\"javascript:X()\">&gt;</a>\n";
    } 

    else
        print "     &gt;\n";
        
    
    print "</tr>\n";
    
    print " </table>\n";

} else {
    print "<i>Geen opta gevonden!</i>";
}


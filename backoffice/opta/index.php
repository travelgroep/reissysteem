<?
  require_once("../../inc/config.php");

  $db = new DB();
  $layout = new Layout('Opta', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js') );

  $layout->header();
  $layout->topMenu("Opta");
  $layout->contentHeader();
  
  
  ?>
    <div id="submenu"> 
	    <ul>
	         <li> <a href="index.php?view=1">Arrangment changelog </a> </li>
	         <li> <a href="index.php?view=2">Optamatch changelog </a> </li>
	         <li> <a href="index.php?view=3">Opta matches</a>
                <?php 
                $competitionModelManager = new CompetitionModelManager ( );
                $competitions = $competitionModelManager->findAll ();
                
                if (!empty ($competitions)) { 
                ?>
	            <ul>
	               <?php foreach ($competitions as $competition){ ?>
	               <li><a href="index.php?view=3&compid=<?= $competition->getId() ?>"><?= $competition->getName() ?></a></li>
	               <? } ?>
	            </ul>
	            <?  
                }
	            ?>
	            
	         </li>
	    </ul>
    </div>
  <?php
  
    echo '<div id="overzicht">...</div>';
   
  $view = $_GET ['view'];
  $compid = $_GET ['compid'];
  switch ($view) {
  	
  	case 1:
  		echo '<script type="text/javascript">Xpost("./xml/arrangementChangelog.php", "x=1", "overzicht", function(){})</script>';
  		break;
  		
  	case 2:
  		echo '<script type="text/javascript">Xpost("./xml/matchChangeLog.php", "x=1", "overzicht", function(){})</script>';
        break;
        
    case 3:
 
  
    	//$postParm = " {'x': '1', 'compid': $compid}";
    	$postParm = "x=1&compid=$compid";
        echo '<script type="text/javascript">Xpost("./xml/optaMatches.php",  "'.$postParm.'", "overzicht", function(){})</script>';
        break;    
    
  	default:
  		echo '<script type="text/javascript">Xpost("./xml/arrangementChangelog.php", "x=1", "overzicht", function(){})</script>';
        break;
  }
 
    
	
  $layout->contentFooter();
  
  $layout->footer();
?>
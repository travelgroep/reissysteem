<?php
require_once ("../../inc/config.php");

$controller = new OptaMatchController();
$parameters = $controller->previewConfirmPeriodAction();
extract($parameters);


$layout = new Layout('Preview confirm period', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js') );

$layout->header();
$layout->topMenu("Opta");
$layout->contentHeader();

$teamModelManager= new TeamModelManager();
$venueModelManager = new VenueModelManager();

print "<h3>".$competition->getName()."</h3>\n";
print "<table class=\"overzicht\">\n";
    print "<tr>\n";
        print "<th>Id</th>\n";
        print "<th>MatchDay</th>\n";
        print "<th>Match</th>\n";
        print "<th>Match Date/Time</th>\n";
        print "<th>Update enable</th>\n";
        print "<th>Period</th>\n";
        print "<th>Location</th>\n";
        print "<th>City</th>\n";
        print "<th>Confirmed</th>\n";
    print "</tr>\n";
    
    $odd_even = "even";
    
    foreach($matches as $match) {
        $homeTeam = $teamModelManager->find( $match->getHomeTeamId());
        $awayTeam = $teamModelManager->find( $match->getAwayTeamId());
        $venue = $venueModelManager->find( $match->getVenueId());        
        print '<tr class="'.$odd_even.'">'."\n";
            print "<td>".$match->getId()."</td>\n";
            print "<td>".$match->getMatchDay()."</td>\n";
            print "<td>".$homeTeam->getName()." - ".$awayTeam->getName()."</td>\n";
            print "<td>" .$match->getKickoffDate()."</td>\n";
            $updateEnable = $match->getUpdateEnable() ? 'yes' : 'no';
            print "<td>".$updateEnable."</td>\n";
            print "<td>".$match->getPeriod()."</td>\n";
            print "<td>".$venue->getName()."</td>\n";
            print "<td>".$venue->getCity()."</td>\n";
            $isDateConfirmedString = $match->isDateConfirmed() ? 'yes' : 'no';
            $confirmString = $confirm ? 'yes' : 'no';
            print "<td>from: ".$isDateConfirmedString." to: ".$confirmString."</td>\n";
        print "</tr>\n";
        
        $odd_even = ($odd_even == "even") ? "odd" : "even";
    }
print "</table>\n";

print '<form action="'.OptaMatchController::generateUrl(OptaMatchController::URL_CONFIRM_PERIOD, '').'" method="POST">';
    print '<input type="hidden" name="competitionId" value="'.$competition->getId().'">';
    print '<input type="hidden" name="confirm" value="'.($confirm ? 'true' : 'false').'">';
    print '<input type="hidden" name="fromDate" value="'.$fromDate->format('Y-m-d').'">';
    print '<input type="hidden" name="toDate" value="'.$toDate->format('Y-m-d').'">';
    print '<input type="hidden" name="referer" value="'.$referer.'">';
    print '<center><input type="submit" name="submit" value="Change" style="width: 80px;"></center>';
print "</form>";

$layout->contentFooter();
 
$layout->footer();

<?php
require_once ("../../inc/config.php");

$controller = new OptaMatchController();
$parameters= $controller->updateArrangementAction();
extract($parameters);


$layout = new Layout('Update arrangement command output', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js') );

$layout->header();
$layout->topMenu("Update competition");
$layout->contentHeader();

print '<center><a href="'.$referer.'">Back</a></center>';
print '<br>';
foreach($commandOutput as $outputRow) {
    echo $outputRow;
    echo '<br>';
}

$layout->contentFooter();
$layout->footer();
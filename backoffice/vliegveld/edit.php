<?
  require_once("../../inc/config.php");
  $id = @$_GET['id'];

  $db = new DB(); 

  $obj = new Vliegveld();

  if ($id)
  {
    $obj->loadById( $id );
    $naam = $obj->getNaam();
    $onload = 'initMenu ( function(){ syncMenu( null,' . $obj->getId() . ',null,null) } );';
  }
  else
  { 
    $naam = 'nieuw';
    $onload = '_currentView=\'\'; initMenu ( function(){ syncMenu(null, null, null, null, null) } );';
  }

  $layout = new Layout('Vliegveld: ' . $naam, array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js'), $onload );

  $layout->header();
  $layout->topMenu( 'Vliegveld' );
  $layout->contentHeader();

  echo '<form action="update.php" onsubmit="return update(this)">';

  echo '<div id="beschrijvingContent" style="margin-left:0px">';
    include("../xml/renderBeschrijving.php");
  echo '</div>';

  echo '<div id="specificContent" style="margin-left:0px">';
    include("./xml/render.php");
  echo '</div>';

  echo '<div id="submit"><input type="submit" value="WIJZIGINGEN OPSLAAN"></input></div>';

  echo '</form>';

	$layout->contentFooter();
  $layout->footer();

?>
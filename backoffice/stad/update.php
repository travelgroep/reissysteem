<?
  require_once("../../inc/config.php");
  header("Content-Type:text/html;charset=utf-8");

  $db = new DB();  

  if ($id = @$_POST['id'])                                              // oude bewerken
  {
    $stad = new Stad();
    $stad->loadById( $id );
    $result = $stad->post($_POST);
    if (!$result) $stad->store();   
  }
  else                                                  // nieuwe aanmaken
  {
    $land_id = $_POST['land_id'];
    $land = new Land();
    $land->loadById( $land_id );

    $stad = new Stad( $land );
    $result = $stad->post($_POST);
    
    if (!$result) $result = $stad->store();
  } 

  if (!$result)
  {
    if ( $reisoptieId  = $_POST['reisoptie_id'] )           // oude bewerken
    {
      $reisoptie = new Reisoptie();
      $reisoptie->loadById( $reisoptieId );
      $reisoptie->post( $_POST );
      
      $reisoptie->store();

      $reisoptiewaardes = $reisoptie->getReisoptieWaardes();

      $x = 0;
      foreach ($reisoptiewaardes as $reisoptiewaarde)
      {
        $reisoptiewaarde->post( array ('reisoptie_id' => $reisoptieId, 'naam' => $_POST['reisoptiewaarde_naam'][$x], 'waarde' => $_POST['reisoptiewaarde_waarde'][$x] ) );
        $reisoptiewaarde->store();
        $x++;
      }
    }
  }

// XML genereren ahv resultaatset
if ( $result ) generateResultXML( $result );

?>
<?
  if (!@$id) 
  {
    $id = @$_POST['id'];

    if ($id)
      include('../../../inc/config.php');

    $db = new DB();  
  }
  
  if (!$event) {
    $stad = new Stad();        // als event nog niet is aangemaakt
    if ($id) $stad->loadById( $id );
  }

  echo '<table border="0">';
  echo '<tr id="iata"><td class="left">iata</td><td><input type="text" name="iata" value="'.$stad->getIATA().'"></td><td id="iata_err"></td></tr>';
  echo '<tr id="hotel_prijs"><td class="left">standaard hotelprijs</td><td><input type="text" name="hotel_prijs" value="'.$stad->getHotelPrijs().'"></td><td id="hotel_prijs_err"></td></tr>';
  echo '<tr id="hotel_eenpersoonskamer_toeslag"><td class="left">standaard eenpersoonskamer-<br>toeslag</td><td><input type="text" name="hotel_eenpersoonskamer_toeslag" value="'.$stad->getHotelEenpersoonskamerToeslag().'"></td><td id="hotel_eenpersoonskamer_toeslag_err"></td></tr>';
  echo '<tr id="hotelpronto_id"><td class="left">hotelpronto-id<br/>(region)</td><td><input type="text" name="hotelpronto_id" value="'.$stad->getHotelprontoId().'"></td><td id="hotelpronto_id_err"></td></tr>';
  echo '<tr id="vendor_key"><td class="left">vendor-key</td><td><input type="text" name="vendor_key" value="'.$stad->getVendorKey().'"></td><td id="vendor_key_err"></td></tr>';
  echo '<tr id="vendor_id"><td class="left">vendor-id</td><td><input type="text" name="vendor_id" value="'.$stad->getVendorId().'"></td><td id="vendor_id_err"></td></tr>';
  echo '</table>';

?>
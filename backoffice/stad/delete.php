<?
  require_once("../../inc/config.php");
  $id = @$_POST['id'];
  
  $idList = explode('|', $id);  

  $db = new DB();  

  foreach ($idList as $id)
  {
    if ($id)
    {
      $obj = new Stad();
      $obj->loadById( $id );

      $naam = $obj->getNaam();
      $locaties = getLocaties( null, null, null, null, array('stad_id='  . $obj->getId()) );
 
      if ( count( $locaties ) >0 ) echo 'Er hangen nog locaties aan de stad \'' . $naam . '\'! Verwijder deze eerst.' . "\n";
      else
      {
        $obj->delete();
        echo 'Stad \''.$naam.'\' verwijderd!' . "\n";
      }
    }
    else
    {
      echo 'Ongeldige id: ' . $id . "\n";
    }
  }
?>

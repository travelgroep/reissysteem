<?
  require_once("../../inc/config.php");
  $id = @$_GET['id'];
    
  $db = new DB();  

  $obj = new Locatie();

  if ($id)
  {
    $obj->loadById( $id );
    $stad = new Stad();
    $stad->loadById( $obj->getStadId() );
    $land = new Land();
    $land->loadById( $stad->getLandId() );
    $naam = $obj->getNaam();
    $onload = 'initMenu ( function(){ syncMenu(' . $land->getId() . ',' . $stad->getId() . ',' . $obj->getId() . ', null); } );';
  }
  else
  {
    $naam = "nieuw";
    $onload = '_currentView=\'locatie_id\'; initMenu ( function(){ syncMenu(null, null, null, null, null) } );';
  }

  $layout = new Layout('Locatie: ' . $naam, array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js'), $onload );

  $layout->header();
  $layout->topMenu( 'Locatie' );
  $layout->contentHeader();

  echo '<form action="update.php" onsubmit="return update(this)">';

  echo '<div id="parentContent">';
  echo '<table border="0">';

  echo '<tr>';
  echo '<td class="left">land</td>';
  echo '<td id="landContent">';
  echo '</td>';
  echo '<td id="land_id_err"><td>';
  echo '</tr>';

  echo '<tr>';
  echo '<td class="left">stad</td>';
  echo '<td id="stadContent">';
  echo '</td>';
  echo '<td id="stad_id_err"><td>';
  echo '</tr>';

  echo '</table>'; 
  echo '</div>';

  echo '<div id="webentiteitContent">';
    include("../xml/renderWebentiteit.php");
  echo '</div>';

  if ($id)
  {
    echo '<div id="fotoContent">';
    echo '<iframe id="uploadIframe" frameborder="0" src="../system/uploadFotoIframe.php?beschrijving_id=' . $id . '" width="100%"></iframe>';
    echo '</div>';
  }

  
  echo '<div id="beschrijvingContent">';
    include("../xml/renderBeschrijving.php");
  echo '</div>';

  echo '<div id="specificContent">';
    include("./xml/render.php");
  echo '</div>';

  if ($id)
  {
    echo '<div id="reisoptieContent">';
      include("../xml/renderReisoptie.php");
    echo '</div>';
  
    echo '<div id="reisoptieWaardeContent"></div>';
  } 

  
  echo '<div id="submit"><input type="submit" value="WIJZIGINGEN OPSLAAN"></input></div>';

  echo '</form>';

	$layout->contentFooter();
  $layout->footer();

?>
<?
  require_once("../../inc/config.php");
  $id = @$_POST['id'];
  
  $idList = explode('|', $id);  

  $db = new DB();  

  foreach ($idList as $id)
  {
    if ($id)
    {
      $obj = new Locatie();
      $obj->loadById( $id );

      $naam = $obj->getNaam();
      $events = getEvents( null, null, null, null, array('locatie_id='  . $obj->getId()) );
 
      if ( count( $events ) >0 ) echo 'Er hangen nog events aan de locatie \'' . $naam . '\'! Verwijder deze eerst.' . "\n";
      else
      {
        $obj->delete();
        echo 'Locatie \''.$naam.'\' verwijderd!' . "\n";
      }
    }
    else
    {
      echo 'Ongeldige id: ' . $id . "\n";
    }
  }
?>


<?
  require_once("../../inc/config.php");
  header("Content-Type:text/html;charset=utf-8");

  $db = new DB();  

  if ($id = @$_POST['id'])                                              // oude bewerken
  {
    $locatie = new Locatie();
    $locatie->loadById( $id );
    $result = $locatie->post($_POST);
    if (!$result) $locatie->store();   
  }
  else                                                  // nieuwe aanmaken
  {
    $stad_id = $_POST['stad_id'];
    $stad = new Stad();
    $stad->loadById( $stad_id );

    $locatie = new Locatie( $stad );
    $result = $locatie->post($_POST);
    
    if (!$result) $result = $locatie->store();
  } 

  if (!$result)
  {
    if ( $reisoptieId  = $_POST['reisoptie_id'] )           // oude bewerken
    {
      $reisoptie = new Reisoptie();
      $reisoptie->loadById( $reisoptieId );
      $reisoptie->post( $_POST );
      
      $reisoptie->store();

      $reisoptiewaardes = $reisoptie->getReisoptieWaardes();

      $x = 0;
      foreach ($reisoptiewaardes as $reisoptiewaarde)
      {
        $reisoptiewaarde->post( array ('reisoptie_id' => $reisoptieId, 'naam' => $_POST['reisoptiewaarde_naam'][$x], 'waarde' => $_POST['reisoptiewaarde_waarde'][$x] ) );
        $reisoptiewaarde->store();
        $x++;
      }
    }
  }

// XML genereren ahv resultaatset
if ( $result ) generateResultXML( $result );
?>
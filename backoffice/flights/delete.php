<?php
require_once("../../inc/config.php");
require_once("../../vendor/autoload.php");
  
use Travelgroep\Reissysteem\Controller\FlightController;
    
$controller = new FlightController();
$controller->deleteAction();

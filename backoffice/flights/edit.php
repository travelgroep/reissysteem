<?php
require_once("../../inc/config.php");
require_once("../../vendor/autoload.php");
  
use Travelgroep\Reissysteem\Controller\FlightController;
  
$db = new DB();
$layout = new Layout('Edit flight', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js') );

$layout->header();
$layout->topMenu("Edit flight");
$layout->contentHeader();
    
$controller = new FlightController();
$controller->editAction();

$layout->contentFooter();
$layout->footer();
<?php
require_once("../../inc/config.php");
require_once("../../vendor/autoload.php");
  
use Travelgroep\Reissysteem\Controller\FlightController;
ob_start();  
$db = new DB();
$layout = new Layout('New flight', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js') );

$layout->header();
$layout->topMenu("New flight");
$layout->contentHeader();
    
$controller = new FlightController();
$controller->newAction();

$layout->contentFooter();
$layout->footer();
ob_flush();
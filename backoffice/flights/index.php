<?php
require_once("../../inc/config.php");
require_once("../../vendor/autoload.php");
  
use Travelgroep\Reissysteem\Controller\FlightController;
  
$db = new DB();
$layout = new Layout('Flights', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js') );

$layout->header();
$layout->topMenu("Flights");
$layout->contentHeader();
    
$controller = new FlightController();
$controller->indexAction();

$layout->contentFooter();
$layout->footer();
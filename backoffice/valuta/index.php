<?
  require_once("../../inc/config.php");

  $db = new DB();
  $layout = new Layout('Valuta', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js') );

  $layout->header();
  $layout->topMenu( 'Valuta' );
  $layout->contentHeader();
  
  echo '<p>';
  echo 'De wisselkoersen worden \'s nachts automatisch bijgewerkt. Met dit scherm kunnen tijdelijke wijzigingen worden aangebracht.';

  echo '<form action="update.php" onsubmit="return update(this)">';
  echo '<div id="overzicht">...</div>';
  echo '<script type="text/javascript">Xpost("./xml/render.php", "x=1", "overzicht", function(){})</script>';  
  echo '<div id="submit"><input type="submit" value="WIJZIGINGEN OPSLAAN"></input></div>';
  echo '</form>';

  echo '<p>';
  echo '<a href="update_xml.php">Koersen automatisch bijwerken</a>';
  echo '</p>';

  $layout->contentFooter();
  $layout->footer();

?>
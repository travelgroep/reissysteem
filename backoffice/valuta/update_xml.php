<?
/*
  script updates current currency rates
*/



//require_once("/data1/www/reissysteem/inc/config.php");
$root  = realpath(__DIR__.'/../../');
require_once( $root . "/inc/config.php");

function getRate( $code )
{
  global $xml;
  $currencies = $xml->Cube->Cube->Cube;
    
  if (empty($currencies)){
  	return null;
  } else {
	  foreach ( $currencies as $currency )
	  {
	    $attributes = $currency->attributes();
	
	    if ( $attributes[0] == $code )
	    {
	      return (float)$attributes[1];
	    }
	  }
  }
	   
}

$xml = @simplexml_load_file( 'http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml' );

if ($xml === false) {
	die ("NO connection with www.ecb.int for the currencies!\n");
} else {
	$db = new DB();
	$valutas = getValutas();
	if ( !empty ($valutas) && is_array($valutas) ) {
		
		foreach ( $valutas as $valuta )
		{
		  $rate = getRate( $valuta->getCode() );
		  if (  !empty($rate) && is_float($rate)  ) {
			  $valuta->setMulti( $rate );
			  $valuta->store();
			  //echo $rate . "\n";
		  }
		}
	}
    header('location: index.php');
}


<?
  require_once("../../inc/config.php");

  header("Content-Type:text/html;charset=utf-8");

  $db = new DB();  

  $type = $_POST['type'];

  if ($id = @$_POST['id'])                                              // oude bewerken
  {
    $event = new Event();
    $event->loadById( $id );
    $result = $event->post( $_POST );
    if (!$result) $event->store();   
  }
  else                                                  // nieuwe aanmaken
  {
    $locatie_id = $_POST['locatie_id'];
    $locatie = new Locatie();
    $locatie->loadById( $locatie_id );

    $event = new Event( $locatie );
    $result = $event->post( $_POST );
    
    if (!$result) $result = $event->store();
  } 

  if (!$result)
  {
    if ( $reisoptieId  = $_POST['reisoptie_id'] )           // oude bewerken
    {
      $reisoptie = new Reisoptie();
      $reisoptie->loadById( $reisoptieId );
      $reisoptie->post( $_POST );
      
      $reisoptie->store();

      $reisoptiewaardes = $reisoptie->getReisoptieWaardes();
      $x = 0;
      foreach ($reisoptiewaardes as $reisoptiewaarde)
      {
        $reisoptiewaarde->post( array ('reisoptie_id' => $reisoptieId, 'naam' => $_POST['reisoptiewaarde_naam'][$x], 'waarde' => $_POST['reisoptiewaarde_waarde'][$x] ) );
        $reisoptiewaarde->store();
        $x++;
      }
    }
  }

// XML genereren ahv resultaatset
if ( $result ) generateResultXML( $result );

?>
<?

  require_once("../../inc/config.php");
  $id = @$_GET['id'];
  $db = new DB();  

  $obj = new Event();

  if ($id)
  {
    $obj->loadById( $id );
    
    $locatie = new Locatie();
    $locatie->loadById( $obj->getLocatieId() );
    $stad = new Stad();
    $stad->loadById( $locatie->getStadId() );

    $land = new Land();
    $land->loadById( $stad->getLandId() );
    $naam = $obj->getNaam();
    $onload = 'initMenu ( function(){ syncMenu(' . $land->getId() . ',' . $stad->getId() . ',' . $locatie->getId() . ', null); } );';
  }
  else
  {
    $naam = 'nieuw';
    $onload = '_currentView=\'event_id\'; initMenu ( function(){ syncMenu(null, null, null, null, null) } );';
  }

  $layout = new Layout('Event: ' . $naam, array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js'), $onload );

  $layout->header();
  $layout->topMenu( 'Event' );
  $layout->contentHeader();

  echo '<form action="update.php" onsubmit="return update(this)">';

  echo '<div id="parentContent">';


  echo '<table border="0">';

  echo '<tr>';
  echo '<td class="left">land</td>';
  echo '<td id="landContent">';
  echo '</td>';
  echo '<td id="land_id_err"><td>';
  echo '</tr>';

  echo '<tr>';
  echo '<td class="left">stad</td>';
  echo '<td id="stadContent">';
  echo '</td>';
  echo '<td id="stad_id_err"><td>';
  echo '</tr>';

  echo '<tr>';
  echo '<td class="left">locatie</td>';
  echo '<td id="locatieContent">';
  echo '</td>';
  echo '<td id="locatie_id_err"><td>';
  echo '</tr>';
  
  echo '<tr>';
  echo '<td class="left">eventtype</td>';
  echo '<td>';
  echo '<select name="event_type_id" id="event_type_id">';  
  echo '<option value="">maak een keuze</option>';

  $eventtypes = getEventTypes();

  foreach ($eventtypes as $eventtype)
  {
		if( $eventtype->getId() == $obj->getEventtypeId() )
			$selected = "selected=\"selected\"";
		else
			$selected = "";
						
    echo '<option '.$selected.' value="'.$eventtype->getId().'">'.$eventtype->getNaam().'</option>';
  }

  echo '</select>'; 
  echo ' <a href="../eventtype/edit.php"><img border="0" alt="nieuw eventtype" src="../i/add.gif" title="nieuw eventtype" /></a>';

  echo '</td>';
  echo '<td id="event_type_id_err"><td>';
  echo '</tr>';


  echo '</table>'; 

  echo '</div>';

  echo '<div id="webentiteitContent">';
    include("../xml/renderWebentiteit.php");
  echo '</div>';

  if ($id)
  {
    echo '<div id="fotoContent">';
    echo '<iframe id="uploadIframe" frameborder="0" src="../system/uploadFotoIframe.php?beschrijving_id=' . $id . '" width="100%"></iframe>';
    echo '</div>';
  }
  
  echo '<div id="optaContainer">';
  include("./xml/opta.php");
  echo '</div>';
  
  echo '<div id="beschrijvingContent">';
    include("../xml/renderBeschrijving.php");
  echo '</div>';

  echo '<div id="specificContent">';
    include("./xml/render.php");
  echo '</div>';

  if ($id)
  {
    echo '<div id="reisoptieContent">';
      include("../xml/renderReisoptie.php");
    echo '</div>';
  
    echo '<div id="reisoptieWaardeContent"></div>';
  }
  
  echo '<div id="hotelContent">';
    include ("./xml/renderHotel.php");
  echo '</div>';


  echo '<div id="submit"><input type="submit" value="WIJZIGINGEN OPSLAAN"></input></div>';

  echo '</form>';

	$layout->contentFooter();
  $layout->footer();

?>
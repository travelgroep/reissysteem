<?
  require_once("../../inc/config.php");

  $db = new DB();
  $layout = new Layout('Events', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js') );

  $layout->header();
  $layout->topMenu( 'Event' );
  $layout->contentHeader();

  echo '<div id="overzicht">...</div>';
  echo '<script type="text/javascript">Xpost("./xml/overzicht.php", "x=1", "overzicht", function(){})</script>';  
	
  $layout->contentFooter();
  $layout->footer();

  die;
?>


<?
  require_once("../../inc/config.php");

  $db = new DB();  
	$start = 0;
	$paginagrootte = PAGINAGROOTTE;
	
  $layout = new Layout('events', array() );

  $layout->header();
  $layout->topMenu( 'Event' );
  $layout->contentHeader();

	/*
	 * Deze 3 velden bepalen hoe de lijst getoond wordt,
	 * zoals sortering, en welke pagina. Deze moeten 
	 * altijd worden doorgegeven als naar 'PHP_SELF' 
	 * wordt verwezen.
	 */	
	$start 		= $_GET['start'];
	$sort 		= $_GET['sort'];
	$ascdesc 	= $_GET['ascdesc'];
	
	 $eventypeId = $_SESSION['eventypeId'];
	if (isset($eventypeId)){
	  	$crit [] 		= "event_type_id = $eventypeId ";
  	}
	$events = getEvents( $paginagrootte, $start, $sort, $ascdesc , $crit );
	$aantal_events =  count($events);
	if( $events )
	{
				
		print "<table class=\"overzicht\">\n";
		print "	<tr>\n";

		$fields = array( 'id', 'naam', 'beschrijving_kort', 'event_type', 'marge_prijs' );

		print "		<th></th>\n";
		foreach( $fields as $field )
		{
			if( $field == $sort && $ascdesc=='desc' )
				$fld_ascdesc='asc';
			else
				$fld_ascdesc='desc';		
			
			print "		<th><a href=\"".$_SERVER['PHP_SELF']."?sort=$field&ascdesc=$fld_ascdesc&start=$start\">$field</a></th>\n";			
		}
		
		print "		<th>Controls</th>\n";
		print "	</tr>";
		
		$odd_even = "even";
		$teller = $start;
		
		foreach( $events as $event )
		{
			if ( $odd_even == 'odd' )
				$rowColor = '#B1B1F9';
			else
				$rowColor = '#ffffff';

			// $start telt vanaf 0,
			// maar wij tellen vanaf 1.
			$teller++;

			$eventtypenaam = "ONBEKEND";
			
			$eventtype = $event->getEventType();
			if( $eventtype )
				$eventtypenaam = $eventtype->getNaam();
	
			print "	<tr class=\"".$odd_even."\" >\n";

			print "		<td>$teller</td>\n";
			print "		<td>".$event->getId()."</td>\n";
			print "		<td>".$event->getNaam()."</td>\n";
			print "		<td>".$event->getBeschrijvingKort()."</td>\n";
			print "		<td>".$eventtypenaam."</td>\n";
			print "		<td>".$event->getMargePrijs()." </td>\n";

			print "		<td><a href=\"edit.php?id=".$event->getId()."\">wijzig</a> <a onclick=\"return confirm('Weet u zeker dat u \'".$event->getNaam()."\' wilt verwijderen?');\" href=\"delete.php?id=".$event->getId()."\">verwijder</a></td>\n";


			print "	</tr>\n";

			$odd_even = ($odd_even == "even") ? "odd" : "even";
		}

		// Navigatie
		print "<tr><td colspan=\"6\">\n";
		
	
		// Vorige pagina
		$prev = $start - $paginagrootte;
		if( $prev >= 0 )
			print "		<a title=\"vorige\" href=\"".$_SERVER['PHP_SELF']."?sort=$sort&ascdesc=$ascdesc&start=$prev\">&lt;</a>\n";			
		else
			print "		&lt;\n";			
		
		// Stappen
		for( $i=0; $i<$aantal_events; $i+=$paginagrootte )
		{
			if( $start != $i )
				print "		<a href=\"".$_SERVER['PHP_SELF']."?sort=$sort&ascdesc=$ascdesc&start=$i\">".($i+1)."</a>\n";			
			else
				print $i+1;
		}
		
		// Volgende pagina
		$next = $start + $paginagrootte;
		if( $next < $aantal_events )
			print "		<a title=\"volgende\" href=\"".$_SERVER['PHP_SELF']."?sort=$sort&ascdesc=$ascdesc&start=$next\">&gt;</a>\n";			
		else
			print "		&gt;\n";			


		// Toevoegen
		print "</td><td><nobr><a href=\"edit.php\"><img alt=\"nieuw\" src=\"../i/add.gif\" title=\"toevoegen\" border=\"0\"> Toevoegen</a></nobr></td>";

		print "</tr>\n";

		print "	</table>\n";

	}
	else
	{
		print "<i>Geen events gevonden!</i>";		
	}

	
  $layout->contentFooter();
  $layout->footer();


?>
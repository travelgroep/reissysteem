function Xpost(url, p, id, onReady)
{
  var xmlhttp = false;
  var l;

  if (id) l = document.getElementById(id);
  if( l )
  {
    l.innerHTML = '<img src="../i/loading.gif" />';
  }
  if (window.ActiveXObject) xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    else xmlhttp = new XMLHttpRequest();

  try
  {
    xmlhttp.open("POST", url, true);
    xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
    xmlhttp.onreadystatechange=onReady;
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        if (xmlhttp.responseText && l) l.innerHTML = xmlhttp.responseText;
        if (onReady)
        {
          //(xmlhttp.responseXML) ? onReady(xmlhttp.responseXML) : 
          onReady(xmlhttp.responseText);
        }
      }
    }
    xmlhttp.send(p);
  }
  catch(e)
  {
    alert ('XMLHTTP-fout: ' + e.name + ': ' + e.message);
  }
}

function XpostXML(url, p, id, onReady)
{
  var xmlhttp = false;
  var l;

  if (id) l = document.getElementById(id);

  if (window.ActiveXObject) xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    else xmlhttp = new XMLHttpRequest();

  try
  {
    xmlhttp.open("POST", url, true);
    xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
    xmlhttp.onreadystatechange=onReady;
    xmlhttp.onreadystatechange=function()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        if (onReady)
        {
          onReady(xmlhttp.responseXML);
        }
      }
    }
    xmlhttp.send(p);
  }
  catch(e)
  {
    alert ('XMLHTTP-fout: ' + e.name + ': ' + e.message);
  }
}

function XFormSubmit(f, id, myReady)
{
  var p = "";
  var url = f.action;

  for (x=0; x<f.length; x++)
  {
    if (f[x].name)
    {
      if (f[x].type == "select-one")
      {
        p += f[x].name + '=' + f[x].options[f[x].selectedIndex].value + '&';
      }
      else
      if (f[x].type == "input" || f[x].type == "text" || f[x].type == "textarea" || f[x].type == "hidden")
//        p += f[x].name + '=' + (f[x].value) + '&';
        p += f[x].name + '=' + encodeURIComponent(f[x].value) + '&';

      if  (f[x].type == "checkbox" && f[x].checked)
        p += f[x].name + '=' + f[x].value + '&';
    }
  }
  //Xpost(url, p, id, myReady);
  XpostXML(url, p, id, myReady);
}
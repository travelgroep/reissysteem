/* 

  additional Javascripts for Pronto 

*/

function downloadExtraInfo( prontoId, el )
{
  row = document.getElementById(el);	
  row.childNodes[6].innerHTML = '<img src="../i/loading.gif"></img>';
  function myReady( r )
  {
    tree = r.documentElement.getElementsByTagName('info');

    address = tree[0].getElementsByTagName('address')[0].firstChild.data;
    rating = tree[0].getElementsByTagName('rating')[0].firstChild.data;

    photos = tree[0].getElementsByTagName('photo');
    thumbs = tree[0].getElementsByTagName('thumbnail');
    cell = row.childNodes[6];
    cell.innerHTML = '';
    for ( x=0; x<thumbs.length; x++ )
    {
      cell.innerHTML += '<a href="http://www.hotelpronto.com' + photos[x].firstChild.data + '" target="_blank"><img border="0" src="http://www.hotelpronto.com' + thumbs[x].firstChild.data + '"></img></a> ';
    }

    // build star view
    row.childNodes[4].innerHTML = '';
    for ( x=0; x<rating; x++ )
    {
      row.childNodes[4].innerHTML += '<img src="../i/ster.gif"></img>';
    }

    row.childNodes[5].innerHTML = address; 
  }

  XpostXML( './xml/pronto_hotelinfo.php', 'prontoId=' + prontoId, null, myReady );

}

function addFromOverzicht( prontoId, stadId, el )
{
  row = document.getElementById( 'row' + prontoId );	
  editImg = document.getElementById('edit' + prontoId);
        
  function myReady(r)
  {
    if ( r == 'del' )
    {
      row.style.backgroundColor='#ffffff';
      el.src='../i/add.gif';
      editImg.src='../i/edit_off.png';
      editImg.onclick=function(){return false};
      editImg.style.cursor='default';
      editImg.parentNode.style.cursor='pointer';
    }
    else 
    {
      row.style.backgroundColor='#d3ffaa';
      el.src='../i/del.png';
      editImg.parentNode.href = "edit.php?id=" + r;
      editImg.src='../i/edit.png';
      editImg.style.cursor='pointer';
      editImg.onclick=function(){window.location.href='edit.php?id=' + r };
    }
  }
  
  
  Xpost( './addPronto.php', 'vendorId=' + prontoId + '&vendorKey=pronto&stadId=' + stadId, null, myReady );
}

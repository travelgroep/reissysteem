var _currentView = 'land_id';       // GLOBAL met huidige scherm

// bouwt het landenmenu op, en vanaf daar wordt de rest uitgerold
function initMenu(myFunction)
{
  function myReady()
  {
    myFunction();
  }
  
  Xpost('../xml/getLanden.php', 'x=1', 'landContent', myReady);
}

function getStedenFromSelect(s)
{
  function myReady(r)
  {
  }

  if (! document.getElementById('stadContent') )
  {
    // dit is land-edit!
    //fillFromSelect ( s );
  }
  else
  {
    clearContents( { 'stad' : 1, 'locatie' : 1, 'event' : 1 } );
  
    land_id = s[s.selectedIndex].value;
    if (land_id) Xpost('../xml/getSteden.php', 'land_id=' + land_id, 'stadContent', myReady);
  }
}

function getLocatiesFromSelect(s)
{
  function myReady(r)
  {
  }

  if (! document.getElementById('locatieContent') )
  {
    // dit is stad-edit!
    //fillFromSelect ( s );
  }
  else
  {
    clearContents( { 'locatie' : 1, 'event' : 1 } );

    stad_id = s[s.selectedIndex].value;
    if (stad_id) Xpost('../xml/getLocaties.php', 'stad_id=' + stad_id, 'locatieContent', myReady)
  }
}

function getEventsFromSelect(s)
{
  function myReady(r)
  {
    if (document.location.href.indexOf('toevoegen_multi.php'))
    {
      if ( document.location.href.indexOf('toevoegen_multi.php') != -1 )
        document.getElementById('event_id').onchange=function(){document.forms[0].submit()};
    }
  }


  if (! document.getElementById('eventContent') )
  {
    // dit is locatie-edit!
    
    //fillFromSelect ( s );
  }
  else
  {
    clearContents( { 'event' : 1 } );
    locatie_id = s[s.selectedIndex].value;
    
    if (locatie_id) Xpost('../xml/getEvents.php', 'locatie_id=' + locatie_id, 'eventContent', myReady)
  }
}

function clearContents( clearArray )
{  
  if ( clearArray['stad'] )
    if ( stadContent = document.getElementById('stadContent') ) stadContent.innerHTML = "";
  if ( clearArray['locatie'] )
    if ( locatieContent = document.getElementById('locatieContent') ) locatieContent.innerHTML = "";
  if ( clearArray['event'])
    if ( eventContent = document.getElementById('eventContent') ) eventContent.innerHTML = "";

/*
  if ( specificContent = document.getElementById('specificContent') ) specificContent.innerHTML = "";
  if ( webentiteitContent = document.getElementById('webentiteitContent') ) webentiteitContent.innerHTML = "";
  if ( beschrijvingContent = document.getElementById('beschrijvingContent') ) beschrijvingContent.innerHTML = "";
  if ( specificContent = document.getElementById('specificContent') ) specificContent.innerHTML = "";
*/

}


function syncMenu(land_id, stad_id, locatie_id, event_id)
{
  setSelect('land_id', land_id);
  if (stad_id) Xpost('../xml/getSteden.php', 'land_id=' + land_id, 'stadContent', function (){ setSelect('stad_id', stad_id); _currentView = "stad_id"});  
  if (locatie_id) Xpost('../xml/getLocaties.php', 'stad_id=' + stad_id, 'locatieContent', function (){ setSelect('locatie_id', locatie_id); _currentView = "locatie_id"});  
  if (event_id) Xpost('../xml/getEvents.php', 'locatie_id=' + locatie_id, 'eventContent', function (){ setSelect('event_id', event_id); _currentView = "event_id" });  
}

function setSelect(id, v)
{
  s = document.getElementById (id).options;

  for (x=0; x<s.length; x++)
  {
    if (s[x].value == v)
    {
      s.selectedIndex = x;
      break;
    }
  }
}

function fillFromSelect(s)
{
  if (id = s[s.selectedIndex].value)
  {
    renderWebentiteit(id);
    renderBeschrijving(id);
    render(id);
  }
  else
  {
//    query = '' + this.location;
//    alert ('Ongeldige keuze! Klik op het plusje om een nieuw object aan te maken.');
    clearContents ( new Array() );
    setSelect (s.id, "");         // RESETTEN

    return;
  }
}

function fillReisoptieFromSelect(s)
{
  if (id = s[s.selectedIndex].value)
  {
//    document.forms[0].reisoptie_naam.value = s[s.selectedIndex].innerHTML;
    renderReisoptieWaarde(id);
  }
  else
  {
    document.getElementById('reisoptieWaardeContent').innerHTML = '';
  }
}

function fillTariefwijziging( hotel_id )
{
  function myReady(r)
  {
  }

  document.getElementById('tariefwijzigingContent') . innerHTML = '';
  Xpost('../xml/renderTariefwijziging.php?x=1', 'hotel_id=' + hotel_id, 'tariefwijzigingContent', myReady )  
}

function renderBeschrijving(id)
{
  function myReady(r)
  {
  }

  if (!id) id = "";
  Xpost('../xml/renderBeschrijving.php?x=1', 'id=' + id, 'beschrijvingContent', myReady)
}

function render(id)
{
  function myReady(r)
  {
  }

  if (!id) id = "";
  Xpost('./xml/render.php?x=1', 'id=' + id, 'specificContent', myReady)
}

function renderWebentiteit(id)
{
  function myReady(r)
  {
  }

  if (!id) id = "";
  Xpost('../xml/renderWebentiteit.php?x=1', 'id=' + id, 'webentiteitContent', myReady);
}

function renderReisoptieWaarde(id)
{
  function myReady(r)
  {
  }

  if (!id) id = "";
  Xpost('../xml/renderReisoptieWaarde.php?x=1', 'id=' + id, 'reisoptieWaardeContent', myReady);
}

function update(f, externalMyReady)
{
  function myReady(r)
  {
    //alert ( r ) ;

    // zorg ervoor dat alle gemarkeerde velden worden hersteld
    tdArray = document.getElementsByTagName('TD');
    for (x=0; x<tdArray.length; x++)
    {
      if (tdArray[x].id.indexOf('_err') != -1)
      {
        tdArray[x].innerHTML = '';
        tdArray[x].style.backgroundColor = 'transparent';  // parentNode
      }
    }

    if (r)
    {
      errorTree = r.documentElement.getElementsByTagName('error');
      
      // zijn er fouten aangetroffen?
      for (x=0; x<errorTree.length; x++)
      {
        field = errorTree[x].getElementsByTagName('field')[0].firstChild.data;
        msg = errorTree[x].getElementsByTagName('msg')[0].firstChild.data;
        highLightErrors(field, msg);
      }
    }
    else
    { 
      if ( externalMyReady ) 
      {
        externalMyReady();
      }
      {         
        alert ('De wijzigingen zijn opgeslagen.');
      }
      
      myId = document.getElementById('id');           // als id nog niet is gevuld, dan naar index
      if ( !myId.innerHTML )
        window.location.href = 'index.php?sort=id&ascdesc=desc';           
    }
  }
  
  XFormSubmit (f, null, myReady);
  return false;
}

// highlight verkeerde velden
function highLightErrors(field, msg)
{
  fieldContent = document.getElementById(field);
  if ( errorContent = document.getElementById(field + '_err') )
  {
    if (fieldContent)
    {
      errorContent.style.backgroundColor = '#e44';
      errorContent.innerHTML = msg;
    }
  }
  else
    {
      alert ( 'Fout bij het opslaan: ' + msg );
    }
}

function newObject(selfId, parentId)
{
/*
  if (selfId == _currentView)                       // alleen nieuwe kunnen toevoegen als view overeenkomt
  {
    s = document.getElementById(selfId);
    s.selectedIndex = s.options.length - 1;

    function myReady(r)
    {
    }

    s = document.getElementById(parentId);
    if (id = s[s.selectedIndex].value || selfId == parentId)    // uitzondering voor Land
    {
   
      renderWebentiteit(null);                                  // velden leeg renderen
      renderBeschrijving(null);
      render(null);

    }
  }
  else
*/

  {
    newLocation = selfId.replace(/_id/, '');
    window.location.href="../" + newLocation + "/edit.php";
  }
}

function renderNew()
{
  renderWebentiteit(null);                                  // velden leeg renderen
  renderBeschrijving(null);
  render(null);
}

function setRating(field, rating, me)
{
  for (x=0; x<rating; x++)
    me.parentNode.childNodes[x].src = '../i/ster.gif';
    
  for (y=0; y<5-rating; y++)
    me.parentNode.childNodes[y + rating].src = '../i/ster_leeg.gif';

  document.getElementById(field + "_rating").value = rating;
}

function addReisoptie( id )
{
  var naam;
  function myReady(r)
  {
    if ( r ) alert ( r )
    else
    {
      function externalMyReady()
      {
        Xpost( '../xml/renderReisoptie.php?x=1', 'id=' + id, 'reisoptieContent', function(){} );
      }
      update(document.forms[0], externalMyReady);
    }
  }

  if (id) 
  {
    if ( (naam = prompt('Geef de naam van de reisoptie.')) ) 
    {
      Xpost( '../xml/addReisoptie.php?x=1', 'id=' + id + '&naam=' + naam, null, myReady );
    }
  }
}

function deleteReisoptie( id, webentiteitId )
{
  function myReady(r)
  {
    if ( r ) alert ( r )
    else
    {
      Xpost( '../xml/renderReisoptie.php?x=1', 'id=' + webentiteitId, 'reisoptieContent', function(){} );
      document.getElementById('reisoptieWaardeContent').innerHTML = "";
    }
  }

  if (id) 
  {
    if ( confirm ('Wil je deze reisopties en alle onderliggende waardes verwijderen?') )
      Xpost( '../xml/deleteReisoptie.php?x=1', 'id=' + id + '&webentiteitId=' + webentiteitId, null, myReady );
  }
}

function deleteTariefwijziging( id, hotel_id )
{
  function myReady(r)
  {
    if ( r ) alert ( r )
    else
    {
      Xpost( '../xml/renderTariefwijziging.php?x=1', 'hotel_id=' + hotel_id, 'tariefwijzigingContent', function(){} );
    }
  }

  if (id) 
  {
    if ( confirm ('Wil je deze tariefwijziging verwijderen?') )
      Xpost( '../xml/deleteTariefwijziging.php?x=1', 'id=' + id, null, myReady );
  }
}

function addReisoptieWaarde( reisoptieId )
{

  if (reisoptieId) 
  {
    function externalMyReady( r )
    {
      function myReady( r )
      {
        if ( r ) 
          alert ( r );
          
        Xpost( '../xml/renderReisoptieWaarde.php?x=1', 'id=' + reisoptieId, 'reisoptieWaardeContent', function(){} );
      }
      Xpost( '../xml/addReisoptieWaarde.php?x=1', 'reisoptieId=' + reisoptieId, null, myReady );
    }
    update(document.forms[0], externalMyReady);
  }
}

function deleteReisoptieWaarde( id, reisoptieId )
{
  function myReady(r)
  {
    if ( r ) alert ( r )
    else
    {
      function externalMyReady()
      {
        Xpost( '../xml/renderReisoptieWaarde.php?x=1', 'id=' + reisoptieId, 'reisoptieWaardeContent', function(){} );
      }
      update(document.forms[0], externalMyReady);
    }
  }

  if (id) 
  {
    Xpost( '../xml/deleteReisoptieWaarde.php?x=1', 'id=' + id, null, myReady );
  }
}

/*
function deleteReisoptieWaarde( id, reisoptieId )
{
  function myReady(r)
  {
    if ( r ) alert ( r )
    else
    {
      Xpost( '../xml/renderReisoptieWaarde.php?x=1', 'id=' + reisoptieId, 'reisoptieWaardeContent', function(){} );
    }
  }
    if (id) 
    Xpost( '../xml/deleteReisoptieWaarde.php?x=1', 'id=' + id, null, myReady );
}
*/

function deleteFromOverzicht( id, p )
{
  if ( confirm('Verwijderen?') )
  {
    function myReady(r)
    {
      if ( r.length>2 ) alert ( r );
      Xpost("./xml/overzicht.php", p, "overzicht", function(){});
    }
    Xpost( './delete.php', 'id=' + id, null, myReady );
  }
}

function createHotel( id, p )
{
  if ( confirm('Verwijderen?') )
  {
    function myReady(r)
    {
      if ( r.length>2 ) alert ( r );
      Xpost("./xml/overzicht.php", p, "overzicht", function(){});
    }
    Xpost( './delete.php', 'id=' + id, null, myReady );
  }
}

function addHotelToKoppel( koppeltabel, type_id, hotel_id )
{
  function myReady(r)
  {
    render( type_id ) ;
    if ( r.length>2 ) alert ( r );
    Xpost("./xml/renderHotel.php", 'id=' + type_id, 'hotelContent', function(){});
  }
 
  Xpost( '../xml/addHotelToKoppel.php', 'koppeltabel=' + koppeltabel + '&type_id=' +type_id + '&hotel_id=' + hotel_id, null, myReady );
}

function addHotelsToKoppel( type_id, koppeltabel, stad_id )
{
  function myReady(r)
  {
    render( type_id ) ;
    if ( r.length>2 ) alert ( r );
    Xpost("./xml/renderHotel.php", 'id=' + type_id, 'hotelContent', function(){});
  }
 
  Xpost( '../xml/addHotelsToKoppel.php', 'koppeltabel=' + koppeltabel + '&type_id=' + type_id + '&stad_id=' + stad_id, null, myReady );
}

function deleteHotelFromKoppel( id, koppeltabel, hotel_id )
{
  if ( confirm('Verwijderen?') )
  {
    function myReady(r)
    {
      render( id ) ;
      if ( r.length>2 ) alert ( r );
      Xpost("./xml/renderHotel.php", 'id=' + id, 'hotelContent', function(){});
    }
    Xpost( '../xml/deleteHotelFromKoppel.php', 'koppeltabel=' + koppeltabel + '&hotel_id=' + hotel_id, null, myReady );
  }
}

function deleteHotelsFromKoppel( id, koppeltabel )
{
  if ( confirm('Alle hotels ontkoppelen?') )
  {
    function myReady(r)
    {
      render( id ) ;
      if ( r.length>2 ) alert ( r );
      Xpost("./xml/renderHotel.php", 'id=' + id, 'hotelContent', function(){});
    }
    Xpost( '../xml/deleteHotelsFromKoppel.php', 'id=' + id + '&koppeltabel=' + koppeltabel, null, myReady );
  }
}


function deleteAllFromOverzicht( p )
{
  cb = getElementsByClassName(document, 'input', 'checkbox');
  var delList = "";

  if ( confirm('Alle geselecteerde items verwijderen?') )
  {
    for ( x=0; x<cb.length; x++)
    {
      if ( cb[x].checked )
      {
        delList += cb[x].value + "|";
      }
    }
    if (delList)
    {
      function myReady(r)
      {
        if ( r.length>2 ) alert ( r );
        Xpost("./xml/overzicht.php", p, "overzicht", function(){});
      }
      delList = delList.substr(0, delList.length-1);
      Xpost( './delete.php', 'id=' + delList, null, myReady );
    }
    else
      alert ('Niks geselecteerd.');
  }
}


function updateHotelFromTotalStay( id, p )
{
  if ( confirm('Add item??') )
  {
    function myReady(r)
    {
      if ( r.length>2 ) alert ( r );
      Xpost('./xml/totalstay_hotels.php', 'x=1&stad_id=' + p, "totalstay_hotels", function(){});
    }
    Xpost( './add_hotel_by_totalstay.php', 'id=' + id, null, myReady );
  }
}

function updateAllHotelFromTotalStay( p )
{
  cb = getElementsByClassName(document, 'input', 'checkbox');
  var hotelList = "";

  if ( confirm('Add all item?') )
  {
    for ( x=0; x<cb.length; x++)
    {
      if ( cb[x].checked )
      {
        hotelList += cb[x].value + "|";
      }
    }
    if (hotelList)
    {
      function myReady(r)
      {
        if ( r.length>2 ) alert ( r );
        Xpost('./xml/totalstay_hotels.php', 'x=1&stad_id=' + p, "totalstay_hotels", function(){});
      }
      hotelList = hotelList.substr(0, hotelList.length-1);
      Xpost( './add_hotel_by_totalstay.php', 'id=' + hotelList, null, myReady );
    }
    else
      alert ('Niks geselecteerd.');
  }
}

function toggleAll()
{
  cb = getElementsByClassName(document, 'input', 'checkbox');
  for ( x=0; x<cb.length; x++)
    cb[x].checked=cb[x].checked?'' : 'checked';
}

function getElementsByClassName(oElm, strTagName, strClassName)
{
    var arrElements = (strTagName == "*" && document.all) ? document.all : oElm.getElementsByTagName(strTagName);
    var arrReturnElements = new Array();
    strClassName = strClassName.replace(/\-/g, "\\-");
    var oRegExp = new RegExp("(^|\\s)" + strClassName + "(\\s|$)");
    var oElement;
    for(var i=0; i<arrElements.length; i++)
    {
        oElement = arrElements[i];      
        if(oRegExp.test(oElement.className))
        {
            arrReturnElements.push(oElement);
        }   
    }
    return (arrReturnElements)
}

function applyFilter( f, filterField, params )
{
  params += "&filter=" + f.value;
  params += "&filterField=" + filterField;
  Xpost( './xml/overzicht.php', params, 'overzicht', function(){} );
}

function X(){}

function enableCB( cb )
{
  ( cb.checked ) ? document.getElementById('cb_pn').disabled = false : document.getElementById('cb_pn').disabled = true;
}

function insertDate(event, id)
{
  dateToId = id;
  openMenu(event, 'date');
}

function checkAddress( lat, long )
{
	$.get( 'xml/get_address.php?lat=' + lat + '&long=' + long, function(t){

	if ( t.status == 'OK' )
	{
		alert( t.results[0].formatted_address );
	}

	}, 'json' );

	//alert( 'De code kon niet worden bepaald.' );
}

function getCoords( hotel_pronto_id )
{
	$.post( 'xml/get_coords.php', {hotel_pronto_id:hotel_pronto_id}, function(t)
	{
		if ( t['latitude'] && t['longitude'] )
		{
			$('input[name="latitude"]').val( t['latitude'] );		
			$('input[name="longitude"]').val( t['longitude'] );		
		}
		else
			{
				alert( 'Van dit hotel zijn de coordinaten niet bekend.' );
			}
	}, 'json');
}
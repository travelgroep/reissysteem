function openMenuWrapper(event, id, toId)
{
  openMenu(event, id);
  Xpost( '../xml/namen.php?x=1', 'toId=' + toId, id, null );
}

function openMenuWrapperHotels(event, id, koppeltabel, typeId, stadId)
{
  openMenu(event, id);
  Xpost( '../xml/hotels.php?x=1', 'koppeltabel=' + koppeltabel + '&typeId=' + typeId + '&stadId=' + stadId, id, null );
}

function openMenu(event, id, noX, noY) {
  var el, x, y;
  
  el = document.getElementById(id);

  bW = document.body.clientWidth;
  bH = document.body.clientHeight;

  if (window.event) {
    sizeY = document.documentElement.clientHeight;

    mouseY = window.event.clientY;
    corrY = document.documentElement.scrollTop + document.body.scrollTop;
    x = window.event.clientX + document.documentElement.scrollLeft
                             + document.body.scrollLeft;
    y = window.event.clientY + corrY; 
  }
  else 
  {
    sizeY = window.innerHeight;
    mouseY = event.clientY;
    corrY = window.scrollY;
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  el.style.visibility = "visible";
  el.style.display = "block";

  w = document.getElementById(id).offsetWidth;
  h = document.getElementById(id).offsetHeight;

//  alert ("y=" + (event.clientY + h) + "sizeY=" + sizeY);
  
  if (w + x > bW) x -= (w + x) - bW;

  (window.event) ? overY = (window.event.clientY + h) - sizeY : overY = (event.clientY + h) - sizeY;
  if (overY > 0) y -= overY;

  if (y<0) y = 10;
  if (x<0) x = 10;
    
  x -= 20; y -= 20;
  
  if (noX) x = 0;
  if (noY) y = 0;
  
  el.style.left = x + "px";
  el.style.top  = y + "px";
  //el.style.display = "";
}

function closeMenu(event) {
  var current, related;

  if (window.event) {          // IE
    current = this;
    related = window.event.toElement;
  }
  else {
    current = event.currentTarget;
    related = event.relatedTarget;
  }

  if (current != related && !contains(current, related))
  {

		// window.setTimeout(function(){current.style.visibility = "hidden"; current.style.display = "none";}, 2000);

    current.style.visibility = "hidden";
    current.style.display = "none";
  }
}

function contains(a, b) 
{
  // als a node b bevat -> true
    while (b.parentNode)
      if ((b = b.parentNode) == a)
        return true;
    return false;
}


function killMenu(id)
{
  l = document.getElementById(id);
  //l.style.visibility = "hidden";
  l.style.display = "none";
  propertiesOpen = false;
}
<?
  require_once("../../inc/config.php");

  $db = new DB();  
  $landen = getLanden();

  echo '<select id="land_id" name="land_id" onchange="getStedenFromSelect(this)">';
  echo '<option value="">maak een keuze</option>';  

  foreach ($landen as $land)
  {
    echo '<option value="'.$land->getId().'">'.$land->getNaam().'</option>';
  }
  echo '</select>';
  echo ' <a href="javascript:newObject(\'land_id\', \'land_id\')"><img id="land_id_img" border="0" alt="nieuw land" src="../i/add.gif" title="nieuw land" /></a>';
 
?>
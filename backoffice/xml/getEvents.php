<?
  require_once("../../inc/config.php");
  $locatie_id = @$_POST['locatie_id'];
  $event_id = @$_POST['event_id'];

  $db = new DB();  

  $locatie = new Locatie();
  $locatie->loadById( $locatie_id );

  $events = $locatie->getEvents();

  echo '<select name="event_id" id="event_id" nchange="submit();">';
  echo '<option value="">maak een keuze</option>';
  
  foreach ($events as $event)
  {
    if( $event_id == $event->getId() )
    	$selected = 'selected="selected"';
    else
    	unset( $selected );
    	
    echo '<option $selected value="'.$event->getId().'">'.$event->getNaam().'</option>';
  }
  echo '</select>';
  echo ' <a href="javascript:newObject(\'event_id\', \'locatie_id\')"><img id="event_id_img" border="0" alt="nieuw event" src="../i/add.gif" title="nieuw event" /></a>';

?>
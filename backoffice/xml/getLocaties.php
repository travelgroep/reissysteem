<?
  require_once("../../inc/config.php");
  $stad_id = @$_POST['stad_id'];

  //$stad_id = 4; // GRONINGEN

  $db = new DB();  

  $stad = new Stad();
  $stad->loadById( $stad_id );

  $locaties = $stad->getLocaties();

  echo '<select name="locatie_id" id="locatie_id" onchange="getEventsFromSelect(this)">';
  echo '<option value="">maak een keuze</option>';
  
  foreach ($locaties as $locatie)
  {
    echo '<option value="'.$locatie->getId().'">'.$locatie->getNaam().'</option>';
  }
  echo '</select>';
  echo ' <a href="javascript:newObject(\'locatie_id\', \'stad_id\')"><img id="locatie_id_img" border="0" alt="nieuwe locatie" src="../i/add.gif" title="nieuwe locatie" /></a>';
?>
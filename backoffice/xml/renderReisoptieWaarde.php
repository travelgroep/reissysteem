<?
  if (!@$id) 
  {
    require_once('../../inc/config.php');
    $id = $_POST['id'];
    $db = new DB();
  }
  
  $reisoptie = new Reisoptie();
  $reisoptie->loadById( $id );

  $reisoptiewaardes = $reisoptie->getReisoptieWaardes();

  echo '<table border="0">';

  ( $reisoptie->getPp()=='t' ) ? $checked = 'checked="checked"' : $checked = '';
  ( $reisoptie->getPn()=='t' ) ? $checked2 = 'checked="checked"' : $checked2 = '';

  if ( $reisoptie->getPp() == 'f' )
  {
    $disabled = 'disabled';
  }
  else
  {
    $disabled = '';
  }
  
  echo '<tr><td class="reisoptieWaardeNaam">naam</td><td class="reisoptieWaardeWaarde"><input class="checkbox" name="reisoptie_naam" type="text" value="' . $reisoptie->getNaam() . '" style="width:300px!important;"/></td></tr>';
  echo '<tr><td class="reisoptieWaardeNaam">beschrijving</td><td class="reisoptieWaardeWaarde"><textarea name="reisoptie_beschrijving">' . $reisoptie->getBeschrijving() . '</textarea></td></tr>';
  echo '<tr><td class="reisoptieWaardeNaam">per persoon</td><td class="reisoptieWaardeWaarde"><input onchange="enableCB(this)" class="checkbox" name="pp" type="checkbox" ' . $checked . '></input> <!-- per nacht <input ' . $disabled . ' id="cb_pn" class="checkbox" name="pn" type="checkbox" ' . $checked2 . ' --></input></td></tr>'; 

  foreach ( $reisoptiewaardes as $reisoptiewaarde )
  {
    $reisoptiewaarde->render();
  }

  echo '<tr><td><a style="cursor:pointer" onclick="addReisoptieWaarde( ' . $id . ')"><img src="../i/add.gif"> waarde toevoegen</a></td></tr>';

  echo '</table>';
  
  echo '<div id="fotoReisoptieContent">';
  echo '<iframe class="uloadIframe" frameborder="0" src="../system/uploadFotoIframe.php?beschrijving_id=' . $id . '&type=Reisoptie" width="100%"></iframe>';
  echo '</div>';

?>
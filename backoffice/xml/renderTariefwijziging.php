<?
  if (!@$id) 
  {
    require_once('../../inc/config.php');
    $hotel_id = $_POST['hotel_id'];
    $db = new DB();
  }
  
  $hotel = new Hotel();
  $hotel->loadById( $hotel_id );

  $tariefwijzigingen = $hotel->getTariefwijzigingen();

  if ( is_array ( $tariefwijzigingen ) )
  {
    echo '<table border="0">';

    echo '<tr><td width="20%">naam</td><td width="20%">van</td><td width="20%">tot</td><td width="20%" id="tariefwijziging_percentage_err">percentage</td><td width="20%">event</td><td>actief</td></tr>';
  
    foreach ( $tariefwijzigingen as $tariefwijziging )
    {
      $tariefwijziging->render();
    }
    
    echo '</table>';
  }
  else
  {
    echo '&nbsp;';
  }
?>
<?
  require_once("../../inc/config.php");
  $stad_id = @$_POST['stad_id'];
  $type_id = @$_POST['type_id'];
  $koppeltabel = @$_POST['koppeltabel'];

  $db = new DB();

  if ( $stad_id )
  {
    $hotels = getHotels( null, null, null, null, array( 'stad_id=' . $stad_id ) );

    if ( is_array( $hotels ) )
    {
      foreach ( $hotels as $hotel )
      {
        $q = "SELECT hotel_id FROM hotel_{$koppeltabel} WHERE hotel_id = {$hotel->getId()} AND {$koppeltabel}_id = {$type_id}";
        $res = $GLOBALS['rsdb']->query( $q );
        if ( !$r = @pg_fetch_result( $res, 0, 0 ) )
        {        
          $q = "INSERT INTO hotel_{$koppeltabel} (hotel_id, {$koppeltabel}_id) VALUES ({$hotel->getId()}, {$type_id})";
          $res = $GLOBALS['rsdb']->query( $q );
        }  
      }
    }
  }
?>
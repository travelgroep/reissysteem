<?
  if (!@$id) 
  {
    require_once('../../inc/config.php');
    $id = $_POST['id'];
    $db = new DB();  
  }

  $obj = new Webentiteit();
  if ($id) $obj->loadById( $id );
  
  $reisopties = $obj->getReisopties();

  echo '<table border="0">';

  echo '<tr><td class="left">reisopties</td>';
  echo '<td>';
  echo '<select name="reisoptie_id" onchange="fillReisoptieFromSelect(this)">';
  echo '<option value="">maak een keuze...</option>';
  foreach ($reisopties as $reisoptie)
  {
    echo '<option value="' . $reisoptie->getId() . '">' . $reisoptie->getNaam() . '(' . $reisoptie->getId(). ')</option>';
  }
  echo '</select>';
  echo ' <a onclick="addReisoptie(' . $id. ')" style="cursor:pointer"><img alt="nieuwe reisoptie" src="../i/add.gif" title="nieuwe reisoptie" border="0"></a>';  
  echo ' <a onclick="deleteReisoptie( document.forms[0][\'reisoptie_id\'][document.forms[0][\'reisoptie_id\'].selectedIndex].value, ' . $id . ')" style="cursor:pointer"><img alt="reisoptie verwijderen" src="../i/del.gif" title="reisoptie verwijderen" border="0"></a>';  

  echo '</td>';
  echo '</tr>';
  
  echo '</table>';
?>
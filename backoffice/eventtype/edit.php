<?
  require_once("../../inc/config.php");
  $id = @$_GET['id'];

  $db = new DB();  

  $obj = new EventType();

  if ($id)
  {
    $obj->loadById( $id );
    $naam = $obj->getNaam();
  }
  else
  { 
    $naam = 'nieuw';
  }

  $layout = new Layout('EventType: ' . $naam, array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js'), $onload );

  $layout->header();
  $layout->topMenu( 'EventType' );
  $layout->contentHeader();

  echo '<form action="update.php" onsubmit="return update(this)">';
  
  echo '<div id="webentiteitContent">';
    include("../xml/renderWebentiteit.php");
  echo '</div>';

  if ($id)
  {
    echo '<div id="fotoContent">';
    echo '<iframe id="uploadIframe" frameborder="0" src="../system/uploadFotoIframe.php?id=' . $id . '" width="100%"></iframe>';
    echo '</div>';
  }
  
  echo '<div id="beschrijvingContent">';
    include("../xml/renderBeschrijving.php");
  echo '</div>';

  echo '<div id="specificContent"></div>';

  if ($id)
  {
    echo '<div id="reisoptieContent">';
      include("../xml/renderReisoptie.php");
    echo '</div>';
  
    echo '<div id="reisoptieWaardeContent"></div>';
  }

  echo '<div id="submit"><input type="submit" value="WIJZIGINGEN OPSLAAN"></input></div>';

  echo '</form>';

	$layout->contentFooter();
  $layout->footer();

?>
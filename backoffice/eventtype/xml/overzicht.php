<?
  require_once("../../../inc/config.php");

  $db = new DB();  
	$start = 0;
	$paginagrootte = PAGINAGROOTTE;
	
	/*
	 * Deze 3 velden bepalen hoe de lijst getoond wordt,
	 * zoals sortering, en welke pagina. Deze moeten 
	 * altijd worden doorgegeven als naar 'PHP_SELF' 
	 * wordt verwezen.
	 */	
	$start = $_POST['start'];
	if ( !$sort = $_POST['sort'] ) $sort = "id";
	if ( !$ascdesc = $_POST['ascdesc'] ) $ascdesc = "desc";
	
  $currentSort = "sort={$sort}&ascdesc={$ascdesc}&start={$start}";

	$objects = getEventTypes( $paginagrootte, $start, $sort == 'event_type'?'event_type_id':$sort, $ascdesc );
	$aantal_objects = getAantalEventTypes();
	
	if( $objects )
	{
				
		print "<table class=\"overzicht\">\n";
		print "	<tr>\n";

		$fields = array( 'id', 'naam', 'beschrijving_kort' );

		print "		<th></th>\n";
		foreach( $fields as $field )
		{
			if( $field == $sort && $ascdesc=='desc' )
			{
				$fld_ascdesc='asc';
				$pic = "up.png";
			}
			else if ( $field == $sort )
			{
				$fld_ascdesc='desc';
				$pic = "down.png";
			}
			else 
			{
			  $fld_ascdesc = 'desc';
			  $pic = "leeg.gif";
		  }
		  		  
			$p = "sort={$field}&ascdesc={$fld_ascdesc}&start={$start}";
      $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
			
			print "<th><a href=\"javascript:X()\" onclick=\"{$onclick}\">$field <img src=\"../i/$pic\" border=\"0\" /></a></th>";			
		}
		
		print "		<th><img src=\"../i/leeg.gif\" width=\"16\" /> <img src=\"../i/leeg.gif\" width=\"16\" /> <input style=\"width:20px;\" type=\"checkbox\" onclick=\"toggleAll()\"></input></th>\n";
		print "	</tr>";
		
		$odd_even = "even";
		$teller = $start;
		
		foreach( $objects as $eventtype )
		{
			if ( $odd_even == 'odd' )
				$rowColor = '#B1B1F9';
			else
				$rowColor = '#ffffff';

			// $start telt vanaf 0,
			// maar wij tellen vanaf 1.
			$teller++;

			$eventtypenaam = "ONBEKEND";
			
			print "	<tr class=\"".$odd_even."\" >\n";

			print "		<td>$teller</td>\n";
			print "		<td>".$eventtype->getId()."</td>\n";
			print "		<td>".$eventtype->getNaam()."</td>\n";
			print "		<td>".$eventtype->getBeschrijvingKort()."</td>\n";

			print "		<td><nobr><a href=\"edit.php?id=".$eventtype->getId()."\"><img src=\"../i/edit.png\" border=\"0\"/></a> <a onclick=\"deleteFromOverzicht(" . $eventtype->getId() . ", '{$currentSort}')\" href=\"javascript:X()\"><img src=\"../i/del.png\" border=\"0\"/></a> <input class=\"checkbox\" type=\"checkbox\" value=\"" . $eventtype->getId() . "\"></input></nobr></td>\n";

			print "	</tr>\n";

			$odd_even = ($odd_even == "even") ? "odd" : "even";
		}

		// Navigatie
		print "<tr><td colspan=\"4\">\n";
		
		// Vorige pagina
		$prev = $start - $paginagrootte;
      
		if( $prev >= 0 )
		{
  		$p = "sort={$sort}&ascdesc={$ascdesc}&start={$prev}";
      $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
			print "		<a onclick=\"{$onclick}\" title=\"vorige\" href=\"javascript:X()\">&lt;</a>\n";			
    }
    
		else
			print "		&lt;\n";			
		
		// Stappen
		for( $i=0; $i<$aantal_objects; $i+=$paginagrootte )
		{
			if( $start != $i )
			{
  		  $p = "sort={$sort}&ascdesc={$ascdesc}&start={$i}";
        $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
      			  
				print "		<a onclick=\"{$onclick}\" href=\"javascript:X()\">".($i+1)."</a>\n";			
      }
			else
				print $i+1;
		}
		
		// Volgende pagina
		$next = $start + $paginagrootte;
		if( $next < $aantal_objects )
		{
		  $p = "sort={$sort}&ascdesc={$ascdesc}&start={$next}";
      $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
			print "		<a onclick=\"{$onclick}\" title=\"volgende\" href=\"javascript:X()\">&gt;</a>\n";			
    }
    
		else
			print "		&gt;\n";			

		// Toevoegen
		print "</td><td><nobr><a href=\"edit.php\"><img alt=\"nieuw\" src=\"../i/new.png\" title=\"toevoegen\" border=\"0\"></a> <a onclick=\"deleteAllFromOverzicht( '{$currentSort}' )\" href=\"javascript:X()\"><img alt=\"nieuw\" src=\"../i/del.png\" title=\"verwijderen\" border=\"0\"></a></nobr></td>";

		print "</tr>\n";

		print "	</table>\n";

	}
	else
	{
		print "<i>Geen eventtypes gevonden!</i>";		
	}

?>
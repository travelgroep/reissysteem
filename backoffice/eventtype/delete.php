<?
  require_once("../../inc/config.php");
  $id = @$_POST['id'];
  
  $idList = explode('|', $id);  

  $db = new DB();  

  foreach ($idList as $id)
  {
    if ($id)
    {
      $obj = new EventType();
      $obj->loadById( $id );

      $naam = $obj->getNaam();
 
      $events = getEvents( null, null, null, null, array('event_type_id='  . $obj->getId()) );
 
      if ( count( $events ) >0 ) echo 'Er hangen nog events aan het eventtype \'' . $naam . '\'! Verwijder deze eerst.' . "\n";
      else
      {
        $obj->delete();
        echo 'Eventtype \''.$naam.'\' verwijderd!' . "\n";
      }
    }
    else
    {
      echo 'Ongeldige id: ' . $id . "\n";
    }
  }
?>
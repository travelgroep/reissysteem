<?
  require_once("../../inc/config.php");
  $id = @$_POST['id'];
  
  $idList = explode('|', $id);  

  $db = new DB();  

  foreach ($idList as $id)
  {
    if ($id)
    {
      $obj = new Land();
      $obj->loadById( $id );

      $naam = $obj->getNaam();
      $steden = getSteden( null, null, null, null, array('land_id='  . $obj->getId()) );
 
      if ( count( $steden ) >0 ) echo 'Er hangen nog steden aan het land \'' . $naam . '\'! Verwijder deze eerst.' . "\n";
      else
      {
        $obj->delete();
        echo 'Land \''.$naam.'\' verwijderd!' . "\n";
      }
    }
    else
    {
      echo 'Ongeldige id: ' . $id . "\n";
    }
  }
?>


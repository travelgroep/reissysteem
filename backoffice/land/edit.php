<?
  require_once("../../inc/config.php");
  $id = @$_GET['id'];

  $db = new DB();  

  $obj = new Land();

  if ($id)  {
    $obj->loadById( $id );
    $naam = $obj->getNaam();
    $onload = 'initMenu ( function(){ syncMenu(' . $obj->getId() . ', null, null, null); } );';
  } else {
    $naam = 'nieuw';
    $onload = '_currentView=\'land_id\'; initMenu ( function(){ syncMenu(null, null, null, null, null) } );';
  }

  $layout = new Layout('Land: ' . $naam, array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js'), $onload );

  $layout->header();
  $layout->topMenu( 'Land' );
  $layout->contentHeader();

  echo '<form action="update.php" onsubmit="return update(this)">';

  echo '<div id="webentiteitContent">';
    include("../xml/renderWebentiteit.php");
  echo '</div>';

  if ($id)
  {
    echo '<div id="fotoContent">';
    echo '<iframe id="uploadIframe" frameborder="0" src="../system/uploadFotoIframe.php?beschrijving_id=' . $id . '" width="100%"></iframe>';
    echo '</div>';
  }
  
  echo '<div id="beschrijvingContent">';
    include("../xml/renderBeschrijving.php");
  echo '</div>';

  echo '<div id="specificContent">';
    include("./xml/render.php");
  echo '</div>';

  if ($id)
  {
    echo '<div id="reisoptieContent">';
      include("../xml/renderReisoptie.php");
    echo '</div>';
  
    echo '<div id="reisoptieWaardeContent"></div>';
  }
  
  echo '<div id="submit"><input type="submit" value="WIJZIGINGEN OPSLAAN"></input></div>';

  echo '</form>';

	$layout->contentFooter();
  $layout->footer();

?>
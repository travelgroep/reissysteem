<?
  require_once("../../inc/config.php");
  header("Content-Type:text/html;charset=utf-8");

  $db = new DB();  

  if ($id = @$_POST['id'])                                              // oude bewerken
  {
    $land = new Land();
    $land->loadById( $id );
    $result = $land->post($_POST);
    if (!$result) $land->store();   
  }
  else                                                  // nieuwe aanmaken
  {
    $land = new Land();
    $result = $land->post($_POST);
    if (!$result) $result = $land->store();
  } 

  if (!$result)
  {
    if ( $reisoptieId  = $_POST['reisoptie_id'] )           // oude bewerken
    {
      $reisoptie = new Reisoptie();
      $reisoptie->loadById( $reisoptieId );
      $reisoptie->post( $_POST );
      
      $reisoptie->store();

      $reisoptiewaardes = $reisoptie->getReisoptieWaardes();

      $x = 0;
      foreach ($reisoptiewaardes as $reisoptiewaarde)
      {
        $reisoptiewaarde->post( array ('reisoptie_id' => $reisoptieId, 'naam' => $_POST['reisoptiewaarde_naam'][$x], 'waarde' => $_POST['reisoptiewaarde_waarde'][$x] ) );
        $reisoptiewaarde->store();
        $x++;
      }
    }
  }

// XML genereren ahv resultaatset
if ( $result ) generateResultXML( $result );

?>
<?

  require_once("../../inc/config.php");
	include('../../class/gta/GTA.php');

  $db = new DB(); 

  $arrangement = new Arrangement();
  $arrangement->loadById( 476 );
//  $arrangement = $_SESSION['arrangement'];
  $event = $arrangement->getEvent();
  $locatie = $event->getLocatie();
  $stad = $locatie->getStad();
  $land = $stad->getLand();

  $stadId = $stad->getId();

  // als er hotels aan het arrangement hangen, die gebruiken, anders kijken naar event en als laatste naar de stad  
  if ( !$hotels = getHotelsByKoppeling( 'arrangement', $arrangement->getId() ) )
    if ( !$hotels = getHotelsByKoppeling( 'event', $event->getId() ) )
      $hotels = getHotels( 0, 0, "", "", "stad_id=$stadId" );

  $stad = new Stad();
  $stad->loadById( $stadId );

  $gta = new GTA();

  $x=0;
  $availableHotels = array();
  
  foreach ( $hotels as $hotel )
  {
    // alleen als het een GTA-hotel is
    if ( $hotel->getGTAcode() )
    { 
      $availableArray = $gta->hotelPriceAvailable($stad->getGTAcode(), $hotel->getGTAcode(), '2008-01-03', '2008-01-07', 2, 2);

      if ( $availableArray['error'] )
      {
        switch ( $availableArray['error'] )
        {
          case 'ERR0006':
            echo 'FOUT: de check-in-datum is ongeldig.';
            break;
          default:
            echo 'FOUT: er is een onbekend probleem opgetreden (' . $availableArray['error'] . ').';
        }
        break;
      }
    }
    
    print ( $availableArray['price'] ) ;
    echo $hotel->getNaam();

    if ( $availableArray['confirmationCode'] == 'IM' ) //|| $availableArray['confirmationCode'] == 'OR'  )
    {
      $availableHotels[$x]['price'] = $availableArray['price'];
      $availableHotels[$x]['hotelObj'] = $hotel;
      $x++;
    }
  }

  if ( $availableHotels )
  {
    asort ( $availableHotels );
    echo '<table>';
  
    foreach ( $availableHotels as $availableHotel )
      echo '<tr><td>' . $availableHotel['hotelObj']->getGTAcode() . '</td><td>' . $availableHotel['price'] . '</td></tr>';
  
    echo '</table>';
  }
  else
    echo 'Er zijn geen beschikbare hotels gevonden.';
    
  die;
?>


<?
require_once("../../inc/config.php");
include_once('../../class/gta/Hotelpronto.php');

$db = new DB();
$layout = new Layout('Hotels', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js'));

$layout->header();
$layout->topMenu('Hotel');
$layout->contentHeader();

$steden = getSteden(null, null, null, null, array("hotelpronto_id != 0"));

if ( $_POST ) {
    $stad_id = $_POST['stad_id'];
    $hotel_name = $_POST['hotel_name'];
}

print "<form name=\"chk\" method=\"POST\" action=\"" . $_SERVER['PHP_SELF'] . "\">\n";

echo '<select name="stad_id" id="stad_id" onchange="submit()">';
echo '<option value="">maak een keuze</option>';
foreach ($steden as $stad) {
    unset($selected);
    if ($stad->getId() == $stad_id) {
        $selected = 'selected="selected"';
        $stadIdDb = $stad->getId();
    }
    echo '<option value="' . $stad->getId() . '" ' . $selected . '>' . $stad->getNaam() . '</option>';
}
echo '</select>';

echo '<input type="text" name="hotel_name" value="'.$hotel_name.'">';


echo '<div id="overzicht">...</div>';
echo '<span style="color:#aaa;cursor:help" title="Module is uitgeschakeld">Zoeken naar GTA-hotels</span> | ';
echo '<a href="pronto-hotels.php">Zoeken naar Hotelpronto-hotels</a> | ';
echo '<a href="daemon/hotelDaemonForce.php">Prijzen actualiseren</a>';

$filter_request = '';
if(!empty($stad_id))
{
    $filter_request .= '&stad_id=' . $stad_id;
}
if(!empty($hotel_name))
{
    $filter_request .= '&hotel_name=' . $hotel_name;
}
echo '<script type="text/javascript">Xpost("./xml/overzicht.php", "x=1'. $filter_request.'", "overzicht", function(){})</script>';

$layout->contentFooter();
$layout->footer();

?>
<?
  require_once("../../inc/config.php");
  require_once( ROOT . 'class/TotalStayHotel.php');
  $id = @$_POST['id'];
  
  $idList = explode('|', $id);  

  $db = new DB();
  foreach ($idList as $id)
  {
    if ($id)
    {
      $obj = new TotalStayHotel();
      $obj->loadById($id);

      $obj->updateHotel();

      echo 'Hotel \''.$obj->getName().'\' added/updated!' . "\n";
    }
    else
    {
      echo 'Ongeldige id: ' . $id . "\n";
    }
  }
?>

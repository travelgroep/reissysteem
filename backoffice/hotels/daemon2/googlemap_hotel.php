<?php
  function getDateDifference( $datum1, $datum2 )  {
        if( $datum1->format("U") > $datum2->format("U") )
        {
          $d1 = $datum1;
          $datum1 = $datum2;
          $datum2 = $d1;
        }
        
        $dagNummer1 = $datum1->format( "z" );
        $dagNummer2 = $datum2->format( "z" );
        
        $verschilInJaren = (int) $datum2->format( "Y" ) - (int)$datum1->format( "Y" );
        
        if( $verschilInJaren )
        {
          $dagNummer2 = $dagNummer2 + ( $verschilInJaren * 365 );   
        }
            
        return ( $dagNummer2 - $dagNummer1 );
  }  
/* Beschrijving: 2011-07-19 update door R.C Wesselink
 * ModulePronto: reistype, search_days , ID
 * 
 * reistype : event_type tabel naam => Voetbalreis , Zonreis, Operareis , Musicalreis, Tennisreis , Concertreis ;
 * search_days : hangt af van het arrangement. de tijd hoe lang de reis/trip duurt in aantal dagen.
 * ID: arrangementID =>
 * 
 */

define( "PRONTO_PATH", realpath(__DIR__));
define('HOTEL_FEED_PROVIDER', 'total_stay_new');
define('HOTEL_FEED_URL', 'http://totalstayxml.ivector.co.uk/xml/book.aspx');
define('HOTEL_FEED_USER', 'voetbaltravel');
define('HOTEL_FEED_PASSWORD', 'xml');

define( "HOTEL_VENDOR_PATH", "/usr/home/ronald/public_html/dev/share/vendor" );
define( "HOTEL_LOG_PATH", "/usr/home/ronald/public_html/dev/share/log" );


require_once( PRONTO_PATH . '/../../../inc/config.php');
require_once( PRONTO_PATH . '/class/Daemon.php');
echo " feed provider => " . HOTEL_FEED_PROVIDER;
if (defined('HOTEL_FEED_PROVIDER') && HOTEL_FEED_PROVIDER == 'total_stay_new') {
    require_once(PRONTO_PATH . '/module/ModuleProntoNew.php');
    require_once(HOTEL_VENDOR_PATH. '/autoload.php');
} else {
    require_once( PRONTO_PATH . '/module/ModulePronto.php');
}

define ('HTML', false );
define( VENDOR_PREFIX, $argv[1] ); // pass this on to Pronto logger

$logDirPath = "/usr/home/ronald/public_html/dev/share/log";
$logFilePath = $logDirPath . '/googlemaps.log';
touch($logFilePath);
//$logger = new \Monolog\Logger('filelog');
//$logger->pushHandler(new \Monolog\Handler\StreamHandler($logFilePath, \Monolog\Logger::DEBUG));
            
$_SESSION['agent']  = TRUE;

    $db            = new DB();
    echo "\n  STRAAT GOOGLE "; 
 
    $_SESSION['taalcode'] = "en";
    
     
    /*
     * PropertyID => 1171958
     * PropertyReferenceID => $hotel->vendor_id
     * */
    $hotel = new Hotel();
     $hotel->loadById( 34106 );
     $propertyId = 1172056;
     //$propertyId = $hotel->vendor_id;
     //$stad = $hotel->getStad();
     // $regionId = $stad->getHotelProntoId();
     
        echo "\n  hotel id => " . $hotel->getId();; 
        echo "\n  propertyId => " . $propertyId; 
        echo "\n ";
     $supplier = new Pronto(); 
    $responseData = $supplier->hotelDetails( $propertyId  );
    
    
    
    
    echo "\n";
    
 
<?
/* Beschrijving: 2011-07-19 update door R.C Wesselink
 * ModulePronto: reistype, search_days , ID
 * 
 * reistype : event_type tabel naam => Voetbalreis , Zonreis, Operareis , Musicalreis, Tennisreis , Concertreis ;
 * search_days : hangt af van het arrangement. de tijd hoe lang de reis/trip duurt in aantal dagen.
 * ID: arrangementID =>
 * 
 */

define( "PRONTO_PATH", realpath(__DIR__));

require_once( PRONTO_PATH . '/../../../inc/config.php');
require_once( PRONTO_PATH . '/class/Daemon.php');
if (defined('HOTEL_FEED_PROVIDER') && HOTEL_FEED_PROVIDER == 'total_stay_new') {
	require_once(PRONTO_PATH . '/module/ModuleProntoNew.php');
	require_once(HOTEL_VENDOR_PATH. '/autoload.php');
} else {
	require_once( PRONTO_PATH . '/module/ModulePronto.php');
}

define ('HTML', false );
define( VENDOR_PREFIX, $argv[1] ); // pass this on to Pronto logger
$_SESSION['agent'] 	= TRUE;

 $db 			= new DB();
 $reis_type 	= $_GET['reis_type'];
 
 
if ( !isset( $argv[1] ) && !HTML  ) { echo "Gebruik argv: $argv[0] [reistype] [(id)]\n"; die;  
} else {
		$_SESSION['taalcode'] = "nl";
		$myDaemon	= new ModulePronto( $argv[1]   ); // "Voetbalreis"
		
        if (defined('HOTEL_FEED_PROVIDER') && HOTEL_FEED_PROVIDER == 'total_stay_new') {
    		$logDirPath = HOTEL_LOG_PATH;
    		$logFilePath = $logDirPath . '/hotel_base_price.log';
    
    		touch($logFilePath);
    		
    		$logger = new \Monolog\Logger('filelog');
            $logger->pushHandler(new \Monolog\Handler\StreamHandler($logFilePath, \Monolog\Logger::DEBUG));
    		
    		$myDaemon->setLogger($logger);
        }
		$myDaemon->processArrangementenByReisTypeOrId( $argv[2] ); // 28738
}


if ( HTML && !isset( $reis_type )  ) { 
	echo "Gebruik html: $reis_type [reistype] [(id)]\n"; die; 
} else {
	  //echo '<pre>';
		//$myDaemon	= new ModulePronto( $reis_type   ); // "Voetbalreis"
		//$myDaemon->processArrangementenByReisTypeOrId( ); // 28738
		//echo '</pre>';
}


<?

/**
 *  Haal alle arrangement waarvan de heen reis later is dan vandaag en alleen de landen spanje en italy! ()
 */
function getArrangementenSpanjeOrItalyByReisType( $type ){
	
	// Alleen Spanje en Italy 
	$q 	= "SELECT * FROM arrangement WHERE heen > NOW() AND actief = True AND event_id " . 
			"IN (SELECT id FROM event WHERE event_type_id = (SELECT id FROM event_type WHERE naam = '{$type}') " .
			"AND locatie_id in ( SELECT id FROM locatie WHERE stad_id IN (SELECT id FROM stad " .
			"WHERE land_id = 2 OR land_id = 15 OR land_id = 17 OR land_id = 14 )))";
  
	echo date('Y-m-d H:m:s') . ' +++  getArrangementenSpanjeOrItalyByReisType query =>' . $q . "<br/>";
  
  $res 	= $GLOBALS['rsdb']->query( $q );
	for( $i=0; $i<pg_num_rows( $res ); $i++ ) 	{
		$row 				= pg_fetch_array( $res, $i, PGSQL_ASSOC );
    $array[] 		= new Arrangement(null, $row);
	}
	
	echo date('Y-m-d H:m:s') . " ... aantal arr.= " . count($array)  . "<br/>";
  return $array;
}

/**
 *  Haal alle arrangement waarvan de heen reis later is dan vandaag! ()
 */
function getArrangementenByReisType( $type ){

	$q 	= "SELECT * FROM arrangement WHERE heen >= NOW() and actief = True and event_id IN " 
					."(SELECT id FROM event WHERE event_type_id = (SELECT id FROM event_type WHERE naam = '{$type}')) order by prijs";
  
	echo date('Y-m-d H:m:s') . '+++  getArrangementenByReisType query =>' . $q . "<br/>";
  
  $res 	= $GLOBALS['rsdb']->query( $q );
	for( $i=0; $i<pg_num_rows( $res ); $i++ ) 	{
		$row 		= pg_fetch_array( $res, $i, PGSQL_ASSOC );
    	$array[] 	= new Arrangement(null, $row);
	}
	
	echo date('Y-m-d H:m:s') . "... aantal arr.= " . count($array)  . "<br/>";
  return $array;
}

class Daemon {
  var $supplier;
  var $reis_type;
  var $id;
  
  function __construct() {
    $this->log( '+++ Daemon geladen' );
  }

  function __destruct() {
    $this->log( '+++ Daemon beeindigd' );
  }

  function update()  {
  
     
  }

  private function getVendorKey() {
    if ( is_object ( $this->supplier ) )  {
      if ( $vendorKey = $this->supplier->vendorKey ) {
        return ( $vendorKey );
      } else {
      $this->log( "!!! vendorKey is niet gevuld, check de XML-feed-class!" );
      }
    } else {
      $this->log( "!!! supplier is niet bekend!!!" );
    }
  }

  function getHotels() { return $this->hotels; }
  
  function getCurrentDbTimestamp()  {
    $q = $GLOBALS['rsdb']->query("SELECT EXTRACT('epoch' FROM NOW()) AS unixtime");
    return pg_fetch_result ( $q, 0 );
  }
  
  function getUpdateInterval( ) { return $this->updateInterval; }
  function setUpdateInterval( $minutes ) { $this->updateInterval = $minutes * 60; }
  
  function setId( $id ) { $this->id = $id; }
  
  function getReisType( ) { return $this->reis_type; }
  function setReisType( $reis_type )  { $this->reis_type = $reis_type; }
  
	function removeHotelsByArrangementInCache ( $arrangementId ) {
		if ($arrangementId > 0) {
	    $q 							= $GLOBALS['rsdb']->query("DELETE FROM hotel_cache WHERE arrangement_id = {$arrangementId}");  
	    $this->log( "--- Daemon::removeHotelsByArrangementIdInCache van Arrangement ='" . $arrangementId . "' ( is geldig )  " );    
		} else {
			$this->log( "!!! Daemon::removeHotelsByArrangementIdInCache van Arrangement ='" . $arrangementId . "' ( is niet geldig )  " );
		}
  }
  
  function removeHotelInCache( $hotel, $arrangement ) {
    $hotelId = $hotel->getId();
    $arrangementId = $arrangement->getId();
    $q = $GLOBALS['rsdb']->query("DELETE FROM hotel_cache WHERE hotel_id = {$hotelId} AND arrangement_id = {$arrangementId}");      
  }

  function clearCache() {
    $q = $GLOBALS['rsdb']->query("DELETE FROM hotel_cache");      
  }


  function log ( $msg ) { echo date('Y-m-d G:i:s') . ' '. $msg . "\n"; }
  function getDateDifference( $datum1, $datum2 )  {
		if( $datum1->format("U") > $datum2->format("U") )
		{
		  $d1 = $datum1;
		  $datum1 = $datum2;
		  $datum2 = $d1;
		}
		
		$dagNummer1 = $datum1->format( "z" );
		$dagNummer2 = $datum2->format( "z" );
	 	
		$verschilInJaren = (int) $datum2->format( "Y" ) - (int)$datum1->format( "Y" );
		
		if( $verschilInJaren )
		{
		  $dagNummer2 = $dagNummer2 + ( $verschilInJaren * 365 );	
		}
			
		return ( $dagNummer2 - $dagNummer1 );
  }  

}

?>
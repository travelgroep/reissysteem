<?
// cache module Pronto

require_once( PRONTO_PATH . '/../../../class/gta/HotelprontoNew.php');

class ModulePronto extends Daemon {
  private $pronto;
  protected $logger;
  
  function __construct( $reis_type ) {
    parent::__construct();
    
    $this->setUpdateInterval( 60*24 );
    $this->setReisType( $reis_type );
    $this->supplier = new Pronto();       // specific class
  }
  
  
  function  processArrangementenByReisTypeOrId ( $arrangementId=null ){
  	
  	$this->log( '--- processArrangementenByReisTypeOrId: ' .$arrangementId );
  	
  	$arrangementen = null;
  	if ( isset($arrangementId)  ){
			$arrangementen 		= getArrangementen( $size=0, $start=0, $sort="", $ascdesc="", array('arrangement.id=' . $arrangementId ) );
    } else {
    	$arrangementen 		= getArrangementenSpanjeOrItalyByReisType( $this->getReisType());
    }
    
    $this->processHotelCacheProntoByProntoStadId ($arrangementen);
  	
  } 
  
 public function setLogger($logger)
 {
	$this->logger = $logger;
 }
  
 function processHotelCacheProntoByProntoStadId( $arrangementen )
 {
	$logger = $this->logger;
	
 	if (is_array($arrangementen) && count($arrangementen) > 0) {
 		$sumUpdatedHotels = 0;
		
		foreach ($arrangementen as $arrangement)  {
			//alle hotels van pronto by stand
			$hotels = $arrangement->getGekoppeldeHotels();

			$countHotels = count($hotels);
			
			$event = $arrangement->getEvent();
			$locatie = $event->getLocatie();
			$stad = $locatie->getStad();
			$regionId = $stad->getHotelProntoId();
			
			if ($countHotels > 1) {
				$dateDifference	= $this->getDateDifference(new DateTime($arrangement->getHeen()), new DateTime($arrangement->getTerug()));
				$duration = (is_int($dateDifference)) ? $dateDifference : 1;
				$adults = 2;
				$arrivalDate = $arrangement->getHeen();
				
				//$this->log( "...(" . $a++ . ") UPDATE hotel_cache het arrangement {$arrangement->id} , Heen = {$arrangement->getHeen()}, dateDifference = {$dateDifference}, Stadnaam = {$stad->naam} \n" );
				//$sProntoStadId = '293';
				//$arrivalDate = '2013-03-26';
				
				$logger->addInfo(sprintf('arrangement: %s, arrival date: %s, duration: %s, number of adults: %s, region ID: %s', $arrangement->id, $arrivalDate, $duration, $adults, $regionId));
				
				$responseData = $this->supplier->hotelAvailableNw($arrivalDate, $duration , $regionId, array('twin'));
				
				$properties = $responseData->getData();
				
				if (count($properties) > 0) {
					$this->removeHotelsByArrangementInCache($arrangement->id);
				} else {
					$logger->addInfo(sprintf('There is no hotel to the arrangement %s in region %s', $arrangement->id, $regionId));
				}
				
				$indexedProperties = array();
				
				foreach($properties as $property) {
					$indexedProperties[$property->getReferenceId()] = $property;
				}
				
				$updatedHotels = 0;
				
				foreach ( $hotels as $hotel ) {
					$propertyReferenceId = $hotel->vendor_id;
					
					if (array_key_exists($propertyReferenceId, $indexedProperties) && $propertyReferenceId > 0) {
						$rooms = $indexedProperties[$propertyReferenceId]->getRooms();
						
						if (array_key_exists(1, $rooms)) {
							$roomTypes = $rooms[1]->getRoomTypes();
							$pricePerPerson = ceil($roomTypes[0]->getTotal() / $duration / $adults);
							
							$hotelPrice = $hotel->getBasisprijs2persKamer();
							
							$hotel->setBasisprijs2persKamer($pricePerPerson);
							$hotel->setEenpersoonskamerToeslag($pricePerPerson);
							
							$logger->addInfo(sprintf('hotel ID: %s, property reference ID: %s, hotel name: %s, old price: %s, new price: %s',
								$hotel->getId(),
								$propertyReferenceId,
								$hotel->naam,
								$hotelPrice,
								$pricePerPerson));

							$hotel->store();
							
							$q = $GLOBALS['rsdb']->query("INSERT INTO hotel_cache (hotel_id, arrangement_id, timestamp, p1,p2) VALUES ({$hotel->id}, {$arrangement->id}, NOW(), {$pricePerPerson}, {$pricePerPerson})");
							
							$updatedHotels++;
						} else {
							$logger->addError(sprintf('TotalStay returned a hotel (%s) without any rooms. (property reference ID: %s)', $hotel->getId(), $propertyReferenceId), $rooms);
						}
					} else {
						$logger->addInfo(sprintf('Hotel ID/ref ID (%s/%s) not found in the TotalStay DB.', $hotel->getId(), $propertyReferenceId));
					}
				}
				
				$logger->addInfo(sprintf('There were %s/%s hotels updated for arrangement %s.', $updatedHotels, $countHotels, $arrangement->getId()));
				$sumUpdatedHotels += $updatedHotels;
			}
			// effe een momentje rust
			sleep(1);
		} // loep Arrangementen
		$countArrangements = count($arrangementen);
		$logger->addInfo(sprintf('There were %s/%s hotels updated for %s arrangements.', $sumUpdatedHotels, ($countArrangements * $countHotels), $countArrangements));
	} else { // check array
		//$this->log( '!!! processHotelCacheProntoByProntoStadId-foutcode: ' . count($arrangementen) );
	}// check array
 } // end class
  
}

?>
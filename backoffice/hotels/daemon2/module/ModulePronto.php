<?
// cache module Pronto

require_once( PRONTO_PATH . '/../../../class/gta/Hotelpronto.php');

class ModulePronto extends Daemon {
  private $pronto;
  
  function __construct( $reis_type ) {
    parent::__construct();
    
    $this->setUpdateInterval( 60*24 );
    $this->setReisType( $reis_type );
    $this->supplier = new Pronto();       // specific class
  }
  
  
  function  processArrangementenByReisTypeOrId ( $arrangementId=null ){
  	
  	$this->log( '--- processArrangementenByReisTypeOrId: ' .$arrangementId );
  	
  	$arrangementen = null;
  	if ( isset($arrangementId)  ){
			$arrangementen 		= getArrangementen( $size=0, $start=0, $sort="", $ascdesc="", array('arrangement.id=' . $arrangementId ) );
    } else {
    	$arrangementen 		= getArrangementenSpanjeOrItalyByReisType( $this->getReisType());
    }
    
    $this->processHotelCacheProntoByProntoStadId ($arrangementen);
  	
  } 
  
  
 function processHotelCacheProntoByProntoStadId( $arrangementen ){
 		
 	// Zorg dat dit gedaan wordt alle benodige Arrangementen
 	$a = 1;
 	if(is_array($arrangementen) && count($arrangementen)>0) {
 		
 	foreach ( $arrangementen as $arrangement )  {
 		
    
    //alle hotels van pronto by stand
    $hotels 					= $arrangement->getGekoppeldeHotels();
    $countHotels 			= count ( $hotels );
    
    $this->log( '--- processHotelCacheProntoByProntoStadId-countHotels: ' .$countHotels );
    
 		$event 						= $arrangement->getEvent();
    $locatie 					= $event->getLocatie();
    $stad 						= $locatie->getStad();
    $sProntoStadId 		= $stad->getHotelProntoId();
    
    if ( $countHotels > 1 ){
    	
    
    $price 						= false;
    $dateDifference	 	= $this->getDateDifference( new DateTime( $arrangement->getHeen() ), new DateTime( $arrangement->getTerug() ) );
    
    $this->log( "...(" . $a++ . ") UPDATE hotel_cache het arrangement {$arrangement->id} , Heen = {$arrangement->getHeen()}, dateDifference = {$dateDifference}, Stadnaam = {$stad->naam} \n" );
    $responseData 		= $this->supplier->hotelAvailableNw( $arrangement->getHeen(), $dateDifference , array( 'twin' ), $hotelLeegId , $sProntoStadId );
    
    //RBS
    	
    	//verwijder oude hotel_cache data!
    	
    	$i 										= 1;
    	$removeHotelsUitCach 	= true;
    	
			foreach ( $hotels as $hotel ) {
				
				$roomsavaileble = $responseData[ $hotel->vendor_id ]['roomsPakket'];
				if ( is_array ( $roomsavaileble ) ) {
					
					// 1 keer uitvoeren! als er een kamer beschikbaar is!
					if ($removeHotelsUitCach) {
						$this->removeHotelsByArrangementInCache($arrangement->id);
						$removeHotelsUitCach 	= false;
					}
					
				  $pHotel 									= $responseData[ $hotel->vendor_id ]; 
				  
				  $roomsPakket								= $pHotel['roomsPakket'];
				  $totalPrice 								= $roomsPakket[0]['totalPrice'];
				  $pricePp									= $roomsPakket[0]['price_p_p'] / (is_int($dateDifference)?$dateDifference : 1);
				  
				  	// update basis prijs RBS hotel
						$hotel->setBasisprijs2persKamer( $pricePp  );
						$hotel->setEenpersoonskamerToeslag( $pricePp  );
						$hotel->store();
						
						$q = $GLOBALS['rsdb']->query("INSERT INTO hotel_cache (hotel_id, arrangement_id, timestamp, p1,p2) VALUES ({$hotel->id}, {$arrangement->id} , NOW(),{$pricePp},{$pricePp})");
						$this->log( "...(" . $i++ . ") uit Arr. {$arrangement->id}, hotel {$hotel->id} - {$pHotel['name']} is aan de daemon toegevoegd , prijs = {$pricePp} EURO" );
				} else {
					// niks update
				} 
			}
    } else {
    	 $this->log( "!!! (" . $a++ . ") arrangement {$arrangement->id} Geen hotels gekoppeld: " . $countHotels );
    }
    
    // effe een momentje rust
    sleep(1);
     
  } // loep Arrangementen
  
 } else { // check array
 	 $this->log( '!!! processHotelCacheProntoByProntoStadId-foutcode: ' . count($arrangementen) );
 }// check array
 } // end class
  
}

?>
<?php
  function getDateDifference( $datum1, $datum2 )  {
        if( $datum1->format("U") > $datum2->format("U") )
        {
          $d1 = $datum1;
          $datum1 = $datum2;
          $datum2 = $d1;
        }
        
        $dagNummer1 = $datum1->format( "z" );
        $dagNummer2 = $datum2->format( "z" );
        
        $verschilInJaren = (int) $datum2->format( "Y" ) - (int)$datum1->format( "Y" );
        
        if( $verschilInJaren )
        {
          $dagNummer2 = $dagNummer2 + ( $verschilInJaren * 365 );   
        }
            
        return ( $dagNummer2 - $dagNummer1 );
  }  
/* Beschrijving: 2011-07-19 update door R.C Wesselink
 * ModulePronto: reistype, search_days , ID
 * 
 * reistype : event_type tabel naam => Voetbalreis , Zonreis, Operareis , Musicalreis, Tennisreis , Concertreis ;
 * search_days : hangt af van het arrangement. de tijd hoe lang de reis/trip duurt in aantal dagen.
 * ID: arrangementID =>
 * 
 */

define( "PRONTO_PATH", realpath(__DIR__));
define('HOTEL_FEED_PROVIDER', 'total_stay_new');
define('HOTEL_FEED_URL', 'http://totalstayxml.ivector.co.uk/xml/book.aspx');
define('HOTEL_FEED_USER', 'voetbaltravel');
define('HOTEL_FEED_PASSWORD', 'xml');

define( "HOTEL_VENDOR_PATH", "/usr/home/ronald/public_html/dev/share/vendor" );
define( "HOTEL_LOG_PATH", "/usr/home/ronald/public_html/dev/share/log" );


require_once( PRONTO_PATH . '/../../../inc/config.php');
require_once( PRONTO_PATH . '/class/Daemon.php');
echo " feed provider => " . HOTEL_FEED_PROVIDER;
if (defined('HOTEL_FEED_PROVIDER') && HOTEL_FEED_PROVIDER == 'total_stay_new') {
    require_once(PRONTO_PATH . '/module/ModuleProntoNew.php');
    require_once(HOTEL_VENDOR_PATH. '/autoload.php');
} else {
    require_once( PRONTO_PATH . '/module/ModulePronto.php');
}

define ('HTML', false );
define( VENDOR_PREFIX, $argv[1] ); // pass this on to Pronto logger

$logDirPath = "/usr/home/ronald/public_html/dev/share/log";
$logFilePath = $logDirPath . '/googlemaps.log';
touch($logFilePath);
//$logger = new \Monolog\Logger('filelog');
//$logger->pushHandler(new \Monolog\Handler\StreamHandler($logFilePath, \Monolog\Logger::DEBUG));
            
$_SESSION['agent']  = TRUE;

    $db            = new DB();
    echo "\n  STRAAT GOOGLE "; 
 
    $_SESSION['taalcode'] = "en";
    
    $supplier = new Pronto(); 
    $arrangement = new Arrangement ( );
    $arrangement->loadById( 33642 );
    
    //var_dump ( $arrangement  );
    
    $hotels = $arrangement->getGekoppeldeHotels();
    $countHotels = count($hotels);
    $event = $arrangement->getEvent();
    $locatie = $event->getLocatie();
    $stad = $locatie->getStad();
    $regionId = $stad->getHotelProntoId();
    
    echo "\n  region => " . $regionId; 
    echo "\n  stad => " . $stad->getId(); 
    echo "\n  arrangement => " . $arrangement->getId(); 
    echo "\n ";
    $dateDifference = getDateDifference(new DateTime($arrangement->getHeen()), new DateTime($arrangement->getTerug()));
    
    $duration = (is_int($dateDifference)) ? $dateDifference : 1;
    $adults = 2;
    $arrivalDate = $arrangement->getHeen();
    
     $responseData = $supplier->hotelAvailableNw( $arrivalDate, $duration , $regionId, array('twin') );
     $properties = $responseData->getData();
     //var_dump($properties);
     $indexedProperties = array();
     foreach($properties as $property) {
        $indexedProperties[$property->getReferenceId()] = $property;
        //echo "\n  Ref=> " .$property->getReferenceId() . " ID=> " .$property->getID() . ", name =>". $property->getName();
     }
     //var_dump($indexedProperties);
     foreach ( $hotels as $hotel ) {
     	$propertyReferenceId = $hotel->vendor_id;
        if (array_key_exists($propertyReferenceId, $indexedProperties)) {
                //echo "\n Match => " . $hotel->vendor_id . ", name =>". $hotel->getNaam() . ", id =>". $hotel->getId();
        } 
     }
   
    
	//if (count($properties) > 0) {
	    //$this->removeHotelsByArrangementInCache($arrangement->id);
	//} else {
	    //$logger->addInfo(sprintf('There is no hotel to the arrangement %s in region %s', $arrangement->id, $regionId));
	//}
    echo "\n" . $countHotels;
    echo "\n";
    
 
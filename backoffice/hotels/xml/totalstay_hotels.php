<?
require_once("../../../inc/config.php");
require_once(ROOT . 'class/TotalStayHotel.php');

$db = new DB();
$eventypeId = $_SESSION['eventypeId'];
//  	if (isset($eventypeId)){ 	$crit []  		= "e.event_type_id = $eventypeId "; }

$stad_id = $_REQUEST['stad_id'];
$crit [] = " s.id = '" . $stad_id . "' ";
$objecten = getTotalStayHotels($crit);
$aantal_objecten = countTotalStayHotels($crit);

if ($objecten) {

    print "<table class=\"overzicht\">\n";
    print "	<tr>\n";

    $fields = array('id', 'name', 'address', 'rating'/*, 'city/town'*/);

    print "		<th></th>\n";
    foreach ($fields as $field)
        print "<th>$field</th>";

    print "		<th><img src=\"../i/leeg.gif\" width=\"16\" /> <input style=\"width:20px;\" type=\"checkbox\" onclick=\"toggleAll()\"></input></th>\n";
    print "	</tr>";

    $odd_even = "even";
    $teller = $start;

    foreach ($objecten as $total_stay_hotel) {
        $hotel = $total_stay_hotel->getHotel();
        if ($odd_even == 'odd')
            $rowColor = '#B1B1F9';
        else
            $rowColor = '#ffffff';

        // $start telt vanaf 0,
        // maar wij tellen vanaf 1.
        $teller++;

        print "	<tr class=\"" . $odd_even . "\" >\n";

        print "		<td>$teller</td>\n";
        if ($hotel->getId()) {
            print "		<td>" . $total_stay_hotel->getId() ."<br/>" . $hotel->getId()  . "</td>\n";
            print "		<td>" . $total_stay_hotel->getName() . "<br/>" . $hotel->getNaam() . "</td>\n";
            print "		<td>".$total_stay_hotel->getAddress1 ().', '. $total_stay_hotel->getPostcodeZip().' ' . $total_stay_hotel->getTownCity(). ' '. $total_stay_hotel->getCountry(). ' Tel. ' . $total_stay_hotel->getTelephone(). ($total_stay_hotel->getFax()?' Fax. '.$total_stay_hotel->getFax():'') ."<br/>" . (strlen($hotel->getAdres())>50?substr($hotel->getAdres(),0,50).'...':$hotel->getAdres()) . "</td>\n";
            print "		<td>" . (int)$total_stay_hotel->getRating() . "<br/>" . $hotel->getRating() . "</td>\n";
//            print "		<td>" . $total_stay_hotel->getTownCity() . "</td>\n";

            print "		<td><nobr><a onclick=\"updateHotelFromTotalStay(" . $total_stay_hotel->getId() . ", '" . $stad_id . "')\" href=\"javascript:X()\"><img src=\"../i/add.gif\" border=\"0\"/ alt=\"Update hotel\" title=\"Update hotel\"></a> <input class=\"checkbox\" type=\"checkbox\" value=\"" . $total_stay_hotel->getId() . "\"></input></nobr></td>\n";
        } else {
            print "		<td>" . $total_stay_hotel->getId() . "</td>\n";
            print "		<td>" . $total_stay_hotel->getName() . "</td>\n";
            print "		<td>".$total_stay_hotel->getAddress1 ().', '. $total_stay_hotel->getPostcodeZip().' ' . $total_stay_hotel->getTownCity(). ' '. $total_stay_hotel->getCountry(). ' Tel. ' . $total_stay_hotel->getTelephone(). ($total_stay_hotel->getFax()?' Fax. '.$total_stay_hotel->getFax():'') ."</td>\n";
            print "		<td>" . (int)$total_stay_hotel->getRating() . "</td>\n";
//            print "		<td>" . $total_stay_hotel->getTownCity() . "</td>\n";

            print "		<td><nobr><a onclick=\"updateHotelFromTotalStay(" . $total_stay_hotel->getId() . ", '" . $stad_id . "')\" href=\"javascript:X()\"><img src=\"../i/new.png\" border=\"0\"/  alt=\"Add new hotel\" title=\"Add new hotel\"></a> <input class=\"checkbox\" type=\"checkbox\" value=\"" . $total_stay_hotel->getId() . "\"></input></nobr></td>\n";
        }

        print "	</tr>\n";

        $odd_even = ($odd_even == "even") ? "odd" : "even";
    }

    // Navigatie
    print "<tr><td colspan=\"4\">\n";


    // Toevoegen
    print "</td><td></td><td><nobr><a onclick=\"updateAllHotelFromTotalStay( '{$stad_id}' )\" href=\"javascript:X()\"><img alt=\"Add/update all\" src=\"../i/add.gif\" title=\"Add/update all\" border=\"0\"></a></nobr></td>";

    print "</tr>\n";

    print "	</table>\n";
}
?>
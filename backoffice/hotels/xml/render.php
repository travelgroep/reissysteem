<?
  if (!@$id) 
  {
    $id = @$_POST['id'];

    if ($id) 
      include('../../../inc/config.php');

    $db = new DB(); 
  }
  
  if (!$hotel) 
  {
    $hotel = new Hotel();        // als event nog niet is aangemaakt
    if ($id) $hotel->loadById( $id );
  }

  echo '<table border="0">';

  echo '<tr id="adres"><td class="left">adres</td><td><textarea name="addr1">' . $hotel->getAdres() . '</textarea></td><td id="adres_err"></td></tr>';

	if ( $hotel->getLatitude() && $hotel->getLongitude() )
	{ 
		echo '<tr><td colspan="2"><a style="text-decoration:underline; cursor:pointer" onclick="checkAddress(' . $hotel->getLatitude() . ',' . $hotel->getLongitude() . ')">Adres controleren ahv Google Maps-co&ouml;rdinaten</a></td></tr>';
	}
  echo '<tr id="basisprijs_2pers_kamer"><td class="left">basisprijs</td><td><input type="text" name="basisprijs_2pers_kamer" value="'.$hotel->getBasisprijs2persKamer().'"></td><td id="basisprijs_2pers_kamer_err"></td></tr>';
  echo '<tr id="eenpersoonskamer_toeslag"><td class="left">eenpersoonskamertoeslag</td><td><input type="text" name="eenpersoonskamer_toeslag" value="'.$hotel->getEenpersoonskamerToeslag().'"></td><td id="eenpersoonskamer_toeslag_err"></td></tr>';

  if ( $hotel->getVendorId() )
  { 
    echo '<tr id="vendor_id"><td class="left">vendor-id</td><td>'.$hotel->getVendorId().'</td><td></td></tr>';
    echo '<tr id="vendor_key"><td class="left">vendor-key</td><td>'.$hotel->getVendorKey().'</td><td></td></tr>';
    echo '<input type="hidden" name="vendor_id" value="'.$hotel->getVendorId().'"></input>';
    echo '<input type="hidden" name="vendor_key" value="'.$hotel->getVendorKey().'"></input>';
  }

  echo '<tr id="rating"><td class="left">rating</td><td>';

  echo renderRating( 'rating', $hotel->getRating() ); 
  
  $events = getEventsByStadId( $hotel->getStadId() );
 
  echo '</td></tr>';
  echo '<tr id="longitude"><td class="left">longitude</td><td><input type="text" name="longitude" value="' . $hotel->getLongitude() . '" /></td><td id="map_longitude_err"></td></tr>';
  echo '<tr id="latitude"><td class="left">latitude</td><td><input type="text" name="latitude" value="' . $hotel->getLatitude() . '" /></td><td id="map_latitude_err"></td></tr>';
  echo '<tr id="budget_rating"><td class="left">budget_rating</td><td><input type="text" name="budget_rating" value="' . $hotel->getBudgetRating() . '" /></td><td id="map_budget_rating_err"></td></tr>';
  echo '<tr id="luxury_rating"><td class="left">luxury_rating</td><td><input type="text" name="luxury_rating" value="' . $hotel->getLuxuryRating() . '" /></td><td id="map_luxury_rating_err"></td></tr>';
  echo '<tr id="distance_to_stadium_rating"><td class="left">distance_to_stadium_rating</td><td><input type="text" name="distance_to_stadium_rating" value="' . $hotel->getDistanceToStadiumRating() . '" /></td><td id="map_distance_to_stadium_rating_err"></td></tr>';
  echo '<tr id="distance_to_city_center_rating"><td class="left">distance_to_city_center_rating</td><td><input type="text" name="distance_to_city_center_rating" value="' . $hotel->getDistanceToCityCenterRating() . '" /></td><td id="map_distance_to_city_center_rating_err"></td></tr>';

  echo '<tr><td colspan="2"><a style="text-decoration:underline; cursor:pointer" onclick="getCoords(' . $hotel->getVendorId() . ')"> Co&ouml;rdinaten bijwerken</a></td></tr>';
  
	echo '<tr id="transfer_resort_id"><td class="left">transfer resort-id</td><td><input type="text" name="transfer_resort_id" value="' . $hotel->getTransferResortId() . '" /></td><td id="transfer_resort_id_err"></td></tr>';

  echo '<tr id="type"><td class="left">type</td><td>';
  echo '<select name="type">';
  
  echo '<option value="0"';
  echo $hotel->getType() == '0' ? ' selected="selected"' : '';
  echo '>hotel</option>';
  
  echo '<option value="1"';
  echo $hotel->getType() == '1' ? ' selected="selected"' : '';
  echo '>appartement</option>';

  echo '<option value="2"';
  echo $hotel->getType() == '2' ? ' selected="selected"' : '';
  echo '>hotel+appartement</option>';

  echo '</select>';
  echo '</td><td id="map_latitude_err"></td></tr>';

	echo '<tr><td colspan="6">';


?>
<table>
	<tr>
		<td>
			<?
				$checked = $hotel->getFpool() == 't' ? 'checked="checked"' : '';
				echo '<input style="width:20px" ' . $checked . ' name="f_pool" type="checkbox" /> zwembad';
				$checked = $hotel->getFchildren() == 't' ? 'checked="checked"' : '';
				echo '<br /><input style="width:20px" ' . $checked . ' name="f_children" type="checkbox" /> voorz. voor kinderen';
				$checked = $hotel->getFairco() == 't' ? 'checked="checked"' : '';
				echo '<br /><input style="width:20px" ' . $checked . ' name="f_airco" type="checkbox" /> airco';
			?>
		</td>
		<td>
			<?
				$checked = $hotel->getFfitness() == 't' ? 'checked="checked"' : '';
				echo '<input style="width:20px" ' . $checked . ' name="f_fitness" type="checkbox" /> fitnessruimte';
				$checked = $hotel->getFinternet() == 't' ? 'checked="checked"' : '';
				echo '<br /><input style="width:20px" ' . $checked . ' name="f_internet" type="checkbox" /> internet';
				$checked = $hotel->getFdisabled() == 't' ? 'checked="checked"' : '';
				echo '<br /><input style="width:20px" ' . $checked . ' name="f_disabled" type="checkbox" /> voorz. voor gehandicapten';
			?>
		</td>
		<td>
		</td>
	</tr>
</table>
<?


	echo '</td></tr>';
  
  echo '<tr><td colspan="6">';
  echo '<p></p>';
  echo 'Tariefwijzigingen';
  echo '<div id="tariefwijzigingContent">';
  echo '<img src="../i/loading.gif" onload="fillTariefwijziging(' . $hotel->getId() . ')" />';
  echo '</div>';
  
  echo '<table>';
  echo '<tr><td width="20%">naam</td><td width="20%">van</td><td width="20%">tot</td><td width="20%">percentage</td><td width="20%">event</td><td>actief</td></tr>';
  echo '<tr>';
  echo '<td><input name="tariefwijziging_naam_new" style="width:80px" type="text"></input></td><td><input onclick="insertDate(event, this)" name="tariefwijziging_datum_start_new" style="width:60px" type="text"></input></td><td><input onclick="insertDate(event, this)" name="tariefwijziging_datum_eind_new" style="width:60px" type="text"></input><td><input name="tariefwijziging_percentage_new" type="text" style="width:40px"></input></td>';
  echo '<td>';
  echo '<select style="width:80px" name="tariefwijziging_event_id_new">';

  echo '<option value="0">alle</option>';

  foreach ( $events as $event )
  {
    echo '<option value="' . $event->getId() . '">' . $event->getNaam() . '</option>';
  }

  echo '</select>';
  echo '</td>';
  echo '<td>';
  echo '<input name="tariefwijziging_actief_new" style="width:20px" type="checkbox" ' . $selected . '></input>';
  echo '</td>';
  echo '</tr>';
  echo '</table>';
  
  echo '</td></tr>';
  
  
  echo '</table>';

  echo '<div id="date"><iframe name="calender" src="../system/calendar.php" margin="0" marginheight="0" marginwidth="0" frameborder="0" scrolling="auto" width="180" height="180"></iframe></div>';

?>

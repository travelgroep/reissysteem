<?
  require_once("../../../inc/config.php");

  $db = new DB();  
	$start = 0;
	$paginagrootte = PAGINAGROOTTE;
	
	/*
	 * Deze 3 velden bepalen hoe de lijst getoond wordt,
	 * zoals sortering, en welke pagina. Deze moeten 
	 * altijd worden doorgegeven als naar 'PHP_SELF' 
	 * wordt verwezen.
	 */	
	$start = $_POST['start'];
	$stad_id = $_POST['stad_id'];
	$hotel_name = $_POST['hotel_name'];
	if ( !$sort = $_POST['sort'] ) $sort = "id";
	if ( !$ascdesc = $_POST['ascdesc'] ) $ascdesc = "desc";
	if ( $sort == 'stad' ) $sort = 'stad_id';
    $currentSort = "sort={$sort}&ascdesc={$ascdesc}&start={$start}";

    //WHERE e.event_type_id = 1
    $eventypeId = $_SESSION['eventypeId'];
  	if (isset($eventypeId)){ 	$crit []  		= " (e.event_type_id = $eventypeId OR e.event_type_id IS NULL )"; }

	$filter_request = '';
	if(!empty($stad_id)){
		$crit [] = " h.stad_id = $stad_id ";
		$filter_request .= '&stad_id=' . $stad_id;
	}
	if(!empty($hotel_name)){
		$crit [] = " LOWER(h.naam) LIKE  LOWER('%$hotel_name%') ";
		$filter_request .= '&hotel_name=' . $hotel_name;
	}
  
	$objecten 			= joinHotelsByEvent( $paginagrootte, $start, $sort, $ascdesc, $crit );
	$aantal_objecten 	= countHotelsOnEventType($crit);

  if ( $sort == 'stad_id' ) $sort = 'stad';

	if( $objecten )
	{
				
		print "<table class=\"overzicht\">\n";
		print "	<tr>\n";

		$fields = array( 'id', 'naam', 'stad', 'transfer_resort_id', 'beschrijving_kort' );

		print "		<th></th>\n";
		foreach( $fields as $field )
		{
			if( $field == $sort && $ascdesc=='desc' )
			{
				$fld_ascdesc='asc';
				$pic = "up.png";
			}
			else if ( $field == $sort )
			{
				$fld_ascdesc='desc';
				$pic = "down.png";
			}
			else 
			{
			  $fld_ascdesc = 'desc';
			  $pic = "leeg.gif";
		  }
		  		  
			$p = "sort={$field}&ascdesc={$fld_ascdesc}&start={$start}".$filter_request;
     	 	$onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
			
			print "<th><a href=\"javascript:X()\" onclick=\"{$onclick}\">$field <img src=\"../i/$pic\" border=\"0\" /></a></th>";			
		}
		
		print "		<th><img src=\"../i/leeg.gif\" width=\"16\" /> <img src=\"../i/leeg.gif\" width=\"16\" /> <input style=\"width:20px;\" type=\"checkbox\" onclick=\"toggleAll()\"></input></th>\n";
		print "	</tr>";
		
		$odd_even = "even";
		$teller = $start;
		
		foreach( $objecten as $hotel ){
			if ( $odd_even == 'odd' )
				$rowColor = '#B1B1F9';
			else
				$rowColor = '#ffffff';

			// $start telt vanaf 0,
			// maar wij tellen vanaf 1.
			$teller++;
	
			print "	<tr class=\"".$odd_even."\" >\n";

			print "		<td>$teller</td>\n";
			print "		<td>".$hotel->getId()."</td>\n";
			print "		<td>".$hotel->getNaam()."</td>\n";
			
			if ( is_object( $hotel->getStad() ) ){
  			print "		<td>" . @$hotel->getStad()->getNaam() . "</td>\n";
			} else {
			  echo '<td style="color:#f00;font-weight:bold">stad bestaat niet meer</td>';
			}
			
			echo "<td>";
			echo  $hotel->getTransferResortId();
			
			$lat = $hotel->getLatitude();
			$long = $hotel->getLongitude();
			
			echo "</td>\n";
			
			
			print "		<td>".$hotel->getBeschrijvingKort()."</td>\n";

			print "		<td><nobr><a href=\"edit.php?id=".$hotel->getId()."\"><img src=\"../i/edit.png\" border=\"0\"/></a> <a onclick=\"deleteFromOverzicht(" . $hotel->getId() . ", '{$currentSort}')\" href=\"javascript:X()\"><img src=\"../i/del.png\" border=\"0\"/></a> <input class=\"checkbox\" type=\"checkbox\" value=\"" . $hotel->getId() . "\"></input></nobr></td>\n";

			print "	</tr>\n";

			$odd_even = ($odd_even == "even") ? "odd" : "even";
		}

		// Navigatie
		print "<tr><td colspan=\"4\">\n";
		
		// Vorige pagina
		$prev = $start - $paginagrootte;
      
		if( $prev >= 0 )
		{
  		$p = "sort={$sort}&ascdesc={$ascdesc}&start={$prev}".$filter_request;
      $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
			print "		<a onclick=\"{$onclick}\" title=\"vorige\" href=\"javascript:X()\">&lt;</a>\n";			
    }
    
		else
			print "		&lt;\n";			
		
		// Stappen
		for( $i=0; $i<$aantal_objecten; $i+=$paginagrootte )
		{
			if( $start != $i )
			{
  		  $p = "sort={$sort}&ascdesc={$ascdesc}&start={$i}".$filter_request;
        $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
      			  
				print "		<a onclick=\"{$onclick}\" href=\"javascript:X()\">".($i+1)."</a>\n";			
      }
			else
				print $i+1;
		}
		
		// Volgende pagina
		$next = $start + $paginagrootte;
		if( $next < $aantal_objecten )
		{
		  $p = "sort={$sort}&ascdesc={$ascdesc}&start={$next}".$filter_request;
      $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
			print "		<a onclick=\"{$onclick}\" title=\"volgende\" href=\"javascript:X()\">&gt;</a>\n";			
    }
    
		else
			print "		&gt;\n";			

		// Toevoegen
		print "</td><td></td><td><nobr><a href=\"edit.php\"><img alt=\"nieuw\" src=\"../i/new.png\" title=\"toevoegen\" border=\"0\"></a> <a onclick=\"deleteAllFromOverzicht( '{$currentSort}' )\" href=\"javascript:X()\"><img alt=\"nieuw\" src=\"../i/del.png\" title=\"verwijderen\" border=\"0\"></a></nobr></td>";

		print "</tr>\n";

		print "	</table>\n";

	}
	else
	{
		print "<i>Geen hotels gevonden!</i>";		
		print "<p><a href=\"edit.php\"><img alt=\"nieuw\" src=\"../i/new.png\" title=\"toevoegen\" border=\"0\"></a>";

	}

?>
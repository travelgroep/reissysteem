<?

$prontoId = $_POST['prontoId'];
//$prontoId = $_GET['prontoId'];
header('Content-Type:text/xml');

/**  http://dev.travelgroep.nl/~ronald/reissysteem/backoffice/hotels/xml/pronto_hotelinfo.php?prontoId=51594221 */
//echo "test= > " . $prontoId;
/*

Update local hotel cache

*/

include_once( "../../../inc/config.php" );
include_once( "../../../class/gta/Hotelpronto.php" );
$db = new DB();

//$prontoId = 42334296;

if ( $prontoId )
{
  $myPronto = new Pronto();
  $hotelData = $myPronto->getHotelData( $prontoId );

/*
// testdata for testing
  $hotelData = array( 
    'rating'=>4, 
    'address'=>'horkstraat 22', 
    'photo'=>array('p1', 'p2', 'p3'), 
    'thumbnail'=>array('t1', 't2', 't3'), 
  );
*/
  echo '<xml>';
  echo '<info>';
  echo '<rating>' . $hotelData['rating'] . '</rating>';
  echo '<address>' . $hotelData['address'] . '</address>';
    
  foreach ( $hotelData['thumbnail'] as $tn )
  {
    $thumbnail .= $tn . '|';
    echo '<thumbnail>' . $tn . '</thumbnail>';
  }
  
  foreach ( $hotelData['photo'] as $pic )
  {
    $photo .= $pic . '|';
    echo '<photo>' . $pic . '</photo>';
  }
  
  $update = array ( 'rating'=>$hotelData['rating'],
                    'address'=>(string)$hotelData['address'],
                    'thumbnail'=>(string)$thumbnail,
                    'photo'=>(string)$photo );

  // update table with extra info                  
  $db->update( 'pronto_hotel', $update, array( 'id'=>$hotelData['id'] ) );
  echo '</info>';
  echo '</xml>';
}

?>
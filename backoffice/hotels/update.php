<?
require_once ("../../inc/config.php");

header ( "Content-Type:text/html;charset=utf-8" );

$db = new DB ( );

$type = $_POST ['type'];

if ($id = @$_POST ['id']) {
	$hotel = new Hotel ( );
	$hotel->loadById ( $id );
	$result = $hotel->post ( $_POST );
	if (! $result)
		$hotel->store ();
} else { // nieuwe aanmaken
	$stad_id = $_POST ['stad_id'];
	$stad = new Stad ( );
	$stad->loadById ( $stad_id );
	
	$hotel = new Hotel ( $stad );
	$result = $hotel->post ( $_POST );
	
	if (! $result)
		$result = $hotel->store ();
}

if (! $result) {
	// reisopties
	if ($reisoptieId = $_POST ['reisoptie_id']) {
		$reisoptie = new Reisoptie ( );
		$reisoptie->loadById ( $reisoptieId );
		$reisoptie->post ( $_POST );
		
		$reisoptie->store ();
		
		$reisoptiewaardes = $reisoptie->getReisoptieWaardes ();
		$x = 0;
		foreach ( $reisoptiewaardes as $reisoptiewaarde ) {
			$reisoptiewaarde->post ( array ('reisoptie_id' => $reisoptieId, 'naam' => $_POST ['reisoptiewaarde_naam'] [$x], 'waarde' => $_POST ['reisoptiewaarde_waarde'] [$x] ) );
			$reisoptiewaarde->store ();
			$x ++;
		}
	}
}

// XML genereren ahv resultaatset
if ($result)
	generateResultXML ( $result );

?>
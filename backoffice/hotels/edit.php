<?
  require_once("../../inc/config.php");

  $gtaCode = @$_GET['gta_code'];
  $id = @$_GET['id'];

  $db = new DB();  

  if ( $gtaCode )
  {
    if ( !$id = isAdded( $gtaCode ) )
    {
      die('Dit hotel zit niet in de database.');
    }
  }
  
  $obj = new Hotel();

  if ($id) {
    $obj->loadById( $id );
    
    $stad = new Stad();
    $stad->loadById( $obj->getStadId() );

    $land = new Land();
    $land->loadById( $stad->getLandId() );
    $naam = $obj->getNaam();
    
    $onload = 'initMenu ( function(){ syncMenu(' . $land->getId() . ',' . $stad->getId() . ', null, null); } );';
  } else {
    $naam = 'nieuw';
    $onload = '_currentView=\'event_id\'; initMenu ( function(){ syncMenu(null, null, null, null, null) } );';
  }


  $layout = new Layout('Hotel: ' . $naam, array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js'), $onload );

  $layout->header();
  $layout->topMenu( 'Hotel' );
  $layout->contentHeader();

  echo '<form action="update.php" onsubmit="return update(this, function(){fillTariefwijziging(' . $id . '); document.forms[0][\'tariefwijziging_datum_start_new\'].value=\'\'; document.forms[0][\'tariefwijziging_datum_eind_new\'].value=\'\'; document.forms[0][\'tariefwijziging_naam_new\'].value=\'\'})">';

  echo '<div id="parentContent">';


  echo '<table border="0">';

  echo '<tr>';
  echo '<td class="left">land</td>';
  echo '<td id="landContent">';
  echo '</td>';
  echo '<td id="land_id_err"><td>';
  echo '</tr>';

  echo '<tr>';
  echo '<td class="left">stad</td>';
  echo '<td id="stadContent">';
  echo '</td>';
  echo '<td id="stad_id_err"><td>';
  echo '</tr>';
  
  echo '</table>'; 

  echo '</div>';

  echo '<div id="webentiteitContent">';
    include("../xml/renderWebentiteit.php");
  echo '</div>';

  if ($id) {
    echo '<div id="fotoContent">';
    echo '<iframe id="uploadIframe" frameborder="0" src="../system/uploadFotoIframe.php?beschrijving_id=' . $id . '" width="100%"></iframe>';
    echo '</div>';
  }
  
  echo '<div id="beschrijvingContent">';
    include("../xml/renderBeschrijving.php");
  echo '</div>';

  echo '<div id="specificContent">';
    include("./xml/render.php");
  echo '</div>';

  if ($id)
  {
    echo '<div id="reisoptieContent">';
      include("../xml/renderReisoptie.php");
    echo '</div>';
  
    echo '<div id="reisoptieWaardeContent"></div>';
  }


  echo '<div id="submit"><input type="submit" value="WIJZIGINGEN OPSLAAN"></input></div>';

  echo '</form>';

	$layout->contentFooter();
  $layout->footer();

?>
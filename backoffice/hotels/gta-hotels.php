<?
  require_once("../../inc/config.php");
	include('../../class/gta/GTA.php');

  $db = new DB();  
	
  $layout = new Layout('GTA hotels', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js', '/backoffice/js/hotel.js') );

  $layout->header();
  $layout->topMenu( 'Hotel' );
  $layout->contentHeader();


	$steden = getsteden( );


$gta = new GTA();

// Build Search Request Data
$stad_id = $_POST['stad_id'];
$stad_id_cur = $_POST['stad_id_current'];

// Is er een citycode gekozen van deze stad?
if( $stad_id == $stad_id_cur )
	$cityCode = $_POST['citycode'];

if( $stad_id && $steden[ $stad_id ] )
{

	$cityName = $steden[ $stad_id ]->getNaam();
	$countryCode = $steden[ $stad_id ]->getLand()->getISOcode();
	
	if( !$cityCode )
		$cityCode = $steden[ $stad_id ]->getGTACode();
}



	print "<form name=\"chk\" method=\"POST\" action=\"".$_SERVER['PHP_SELF']."\">\n";

  echo '<select name="stad_id" id="stad_id" onchange="submit()">';  
  echo '<option value="">maak een keuze</option>';
  foreach ($steden as $stad)
  {
    unset( $selected );
    if( $stad->getID() == $stad_id )
    	$selected = 'selected="selected"';
    echo '<option value="'.$stad->getId().'" '.$selected.'>'.$stad->getNaam().'</option>';
  }
  echo '</select>'; 

	echo '<input type="hidden" name="stad_id_current" value="'.$stad_id.'" />';

	$cityCodes = $gta->findGTACityCodes( $cityName, $countryCode );

  echo '<select name="citycode" id="citycode" onchange="submit()">';  

	// Zijn er meerdere mogelijkheden bij een enkele stad?
	if( count( $cityCodes ) > 1 )
	  echo '<option value="">maak een keuze</option>';
	else
		// Zonee, dan stellen we de huidige cityCode op deze enige optie
		$cityCode = array_shift(array_values($cityCodes));
		
  foreach ($cityCodes as $stad=>$cCode)
  {
    unset( $selected );
    if( $cCode == $cityCode )
    	$selected = 'selected="selected"';
    	
    echo '<option value="'.$cCode.'" '.$selected.'>'.$stad.' ('.$cCode.')</option>';
  }
  echo '</select>'; 

  $hotels = $gta->findGTAHotels( $cityCode );

	if( count( $hotels ) > 0 )
	{
				
		print "<table class=\"overzicht\">\n";
		print "	<tr>\n";

		print "		<th></th>\n";
		//print "		<th></th>\n";
		print "   <th>Hotel</th>\n";			
		print "   <th>Rating</th>\n";			
		print "   <th>Adres</th>\n";			
		print "   <th>Foto's (via GTA)</th>\n";
    echo '<th><img src="../i/leeg.gif" width="16"> <img src="../i/leeg.gif" width="16"> <input style="width: 20px;" onclick="toggleAll()" type="checkbox"></th>';			
		print "	</tr>";
		
		$odd_even = "even";
		$teller = $start;

	
		foreach( $hotels as $hotel_arr )
		{

/*
			print '<pre>';
    print_r ( $hotel_arr ) ;
    print "</pre>";
*/

/*
			if( $_POST[ 'add_hotel'.$hotel_arr['gta_code'] ] == 'yes' )
			{
				$hotel = new Hotel( $steden[ $stad_id ], $hotel_arr );
//				print_r( $hotel_arr );
//				print_r( $hotel );
					$hotel->store();
//				print "> ".pg_last_error()." <";
			}
//			print '</pre>';
*/


			// $start telt vanaf 0,
			// maar wij tellen vanaf 1.
			$teller++;


      if ( $hotelDbId = isAdded( $hotel_arr['gta_code'] ) )
      {
         $odd_even = "even_sel";
	    }
			print "	<tr id=\"row" . $hotel_arr['gta_code'] . "\" class=\"".$odd_even."\" >\n";

			print "		<td>$teller</td>\n";
//			echo '   <td><input type="checkbox" name="add_hotel'.$hotel_arr['gta_code'].'" value="yes" \>';
      echo '<td>'.$hotel_arr['naam'].' ('.$hotel_arr['gta_code'].') </td>';
			echo '<td>';
      for( $i=0; $i<$hotel_arr['rating']; $i++ )
      	print ' * ';
			echo '</td>';


      echo '<td>'.$hotel_arr['addr1'].', '.$hotel_arr['addr2'].', '.$hotel_arr['addr3'].'</td>';
      
			echo '<td>';
      foreach( $hotel_arr['images'] as $image )
      {
        echo '<img src="'.$image['url'].'" title="'.$image['descr'].'"/>';
      }
			echo '</td>';

      $img = ($hotelDbId)?'del.png':'add.gif';
      echo '<td><nobr><a href="javascript:X()"><img onclick="addFromOverzicht(' . $stad_id . ',\'' . $hotel_arr['naam'] . '\',' . $hotel_arr['rating'] . ',\'' . $hotel_arr['gta_code'] . '\',\'' . $hotel_arr['gta_city_code'] . '\',\'' . $hotel_arr['addr1'] . '\',\'' . $hotel_arr['addr2'] . '\',\'' . $hotel_arr['addr3'] . '\', this)" src="../i/' . $img . '" border="0" /></a> ';
      if ( $hotelDbId)
        echo '<a href="edit.php?gta_code=' . $hotel_arr['gta_code']. '"><img title="bewerken" id="edit' . $hotel_arr['gta_code']. '" src="../i/edit.png" border="0" /></a>';
      else echo '<img title="bewerken" id="edit' . $hotel_arr['gta_code']. '" src="../i/edit_off.png" border="0" />';
		    
		  echo ' <input class="checkbox" type="checkbox" name="add_hotel'.$hotel_arr['gta_code'].'" value="yes"></input>';
		  echo ' </nobr></td>';
		

			print "	</tr>\n";

			$odd_even = ($odd_even == "even") ? "odd" : "even";
		}

    echo '<tr><td colspan="5"></td><td><nobr><a href="javascript:X()" onclick="addAllHotelsFromOverzicht()"><img src="../i/add.gif" border="0" /></a> <a href="javascript:X()" onclick="removeAllHotelsFromOverzicht()"><img src="../i/del.png" border="0" /></a></nobr></td></tr>';
  
		print "	</table>\n";
		
		// GTA-code in stad opslaan
		if( $cityCode && $cityCode != $steden[ $stad_id ]->getGTACode() )
		{
			$steden[ $stad_id ]->setGTACode( $cityCode );
			$steden[ $stad_id ]->store();
		}
	}
	else
	{
		print "<i>Geen hotels gevonden!</i>";		
	}
	
  $layout->contentFooter();
  $layout->footer();


?>
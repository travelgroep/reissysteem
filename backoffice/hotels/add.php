﻿<?
  require_once("../../inc/config.php");

  function strProper($str) 
  {
    $noUp = array('a','an','of','the','are','at','in');
    $str = trim($str);
    $str = strtoupper($str[0]) . strtolower(substr($str, 1));
    for($i=1; $i<strlen($str)-1; ++$i) 
    {
      if($str[$i]==' ') 
      {
          for($j=$i+1; $j<strlen($str) && $str[$j]!=' '; ++$j);
          $size = $j-$i-1;
          $shortWord = false;
          if($size<=3) {
              $theWord = substr($str,$i+1,$size);
              for($j=0; $j<count($noUp) && !$shortWord; ++$j)
                  if($theWord==$noUp[$j])
                      $shortWord = true;
          }
          if( !$shortWord )
              $str = substr($str, 0, $i+1) . strtoupper($str[$i+1]) . substr($str, $i+2);
      }   
      $i+=$size;
    }
    return $str;
  }
  
  $db = new DB();  

  if ( !$id = isAdded( $_POST['gta_code'] ) )
  { 
    $obj = new Hotel();
    $_POST['naam'] = strtolower($_POST['naam']);
    $_POST['naam'] = strProper($_POST['naam']);
    $obj->post( $_POST );
    $obj->store();
    echo 'add';
  }
  else
  {
    $obj = new Hotel();
    $obj->loadById( $id );
      
    $naam = $obj->getNaam();
    $obj->delete(); 
    echo 'del';   
  }      
?>
<?

require_once('../../../class/gta/GTA.php');

class ModuleGTA extends Daemon implements DaemonTemplate
{
  private $clause = "gta_code != '' AND gta_code IS NOT NULL";
  private $gta;
  
  function __construct()
  {
    parent::__construct();
    $this->setUpdateInterval( 5 );
    $this->gta = new GTA( GTA_CLIENTID, GTA_EMAIL, GTA_PASSWORD );
  }
  
  function update()
  {
    // hier wordt de datum van het meest recente arrangement gebruikt

/*
    $arrangement = getArrangementen(0, 0, "id", "desc");
    $arrangement = $arrangement[0];

    $hotels = getHotels( 0, 0, null, null, array("gta_code != ''") );
    foreach ( $hotels as $hotel )
    {
      $this->updateHotelInCache( $hotel, $arrangement );
    }
    */

 
    foreach ( getArrangementen() as $arrangement )
    {

      $hotels = $arrangement->getGekoppeldeHotels();

      if ( $hotels )
      {
        foreach ( $hotels as $hotel )
        {
          $this->updateHotelInCache( $hotel, $arrangement );
        }
      }
    }

  }
  
  function cleanup()
  {
    foreach ( getArrangementen() as $arrangement )
    {
      $event = $arrangement->getEvent();
      
      if ( !$hotels = getHotelsByKoppeling( 'arrangement', $arrangement->getId(), $this->clause ) )
        $hotels = getHotelsByKoppeling( 'event', $event->getId(), $this->clause );

      if ( $hotels )
      {
        foreach ( $hotels as $hotel )
        {
          $this->removeHotelInCache( $hotel, $arrangement );
        }
      }
    }
  }
  
  function getNewPrice( $hotel, $arrangement, $roomType=2 )
  {
    $price = false;
  
    $responseData = $this->gta->hotelPriceAvailable( trim( $hotel->getGTAcityCode() ), trim( $hotel->getGTAcode() ), $arrangement->getHeen(), $arrangement->getTerug(), array( $roomType ) );
    if ( $price = $responseData['price'] )
    {
      $price = $responseData['price'];
      $price = floor( $price/$this->getDateDifference( new DateTime( $arrangement->getHeen() ), new DateTime( $arrangement->getTerug() ) ) );
    }
    if ( $responseData['error'] )
    {
      $this->log( '!!! GTA-foutcode: ' . $responseData['error'] );
    }
    return $price?$price:false;
  }
}

?>
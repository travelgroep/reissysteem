<?
// cache module Pronto

require_once( PRONTO_PATH . '/../../../class/gta/Hotelpronto.php');

class ModulePronto extends Daemon implements DaemonTemplate
{
  private $clause = "hotelpronto_id IS NOT NULL";
  private $pronto;
  
  function __construct( $reis_type, $search_days=null, $id=null )  {
    parent::__construct();
    $this->setUpdateInterval( 60*24 );
    $this->setReisType( $reis_type );
    if ( $search_days )    {
      $this->setSearchDays( $search_days );
    }

    if ( $id )
    {
      $this->setId( $id );
    }
    $this->supplier = new Pronto();       // specific class
  }
  
  function cleanup()
  {
    foreach ( getArrangementen() as $arrangement )
    {
      $event = $arrangement->getEvent();
      
      if ( !$hotels = getHotelsByKoppeling( 'arrangement', $arrangement->getId(), $this->clause ) )
        $hotels = getHotelsByKoppeling( 'event', $event->getId(), $this->clause );

      if ( $hotels )
      {
        foreach ( $hotels as $hotel )
        {
          $this->removeHotelInCache( $hotel, $arrangement );
        }
      }
    }
  }
  
  function getNewPrice( $hotel, $arrangement, $roomType=2 )
  {
    $price = false;
    $responseData = $this->supplier->hotelAvailable( $hotel->getVendorId(), $arrangement->getHeen(), $this->getDateDifference( new DateTime( $arrangement->getHeen() ), new DateTime( $arrangement->getTerug() ) ), array( 'twin' ) );

		$this->request_count++;

		//echo "[" . $this->request_count . "]";

    if ( $price = $responseData[0]['rooms'][0]['adultPrice'] )
    {
      $price = $responseData[0]['rooms'][0]['adultPrice'];
    }
    if ( $responseData['error'] )
    {
      $this->log( '!!! Supplier-foutcode: ' . $responseData['error'] );
    }
    return $price?$price:false;
  }
}

?>
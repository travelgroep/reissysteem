<?

function getArrangementenByReisType( $type, $searchDays=null )
{
  if ( $searchDays )
  {
    $crit = "AND heen < '".date('Y-m-d', strtotime( '+ '.$searchDays.' day' ))."'";
  }
  
  $q = "SELECT * FROM arrangement WHERE event_id IN (SELECT id FROM event WHERE event_type_id = (SELECT id FROM event_type WHERE naam = '{$type}')) $crit";
  $res = $GLOBALS['rsdb']->query( $q );

	for( $i=0; $i<pg_num_rows( $res ); $i++ )
 	{
		$row = pg_fetch_array( $res, $i, PGSQL_ASSOC );
    $array[] = new Arrangement(null, $row);
	}
  return $array;
}

class Daemon
{
  var $supplier;
  var $margin;
  var $reis_type;
  var $id;
  
  function __construct()
  {
    $this->log( '+++ Daemon geladen' );
  }

  function __destruct()
  {
    $this->log( '+++ Daemon be�indigd' );
  }

  function update()
  {
    if ( $this->id )
    {
      $arrangementen = getArrangementen( $size=0, $start=0, $sort="", $ascdesc="", array('arrangement.id=' . $this->id ) );
    }
    else
    {
      $arrangementen = getArrangementenByReisType( $this->getReisType(), $this->getSearchDays() );
    }

      foreach ( $arrangementen as $arrangement )
      {
      	// arrangement nog actueel?
      	$d = new DateTime( $arrangement->getHeen() );
      	$n = new DateTime( date("Y-m-d") );
      	if ( $d->format( "U" ) > $n->format( "U" ) )
      	{
  	    	$this->log( '-- ' . $arrangement->getNaam() . '(' . $arrangement->getId() . ')' );
     	
  	      $hotels = $arrangement->getGekoppeldeHotels();
  	
  	      if ( $hotels )
  	      {
  	        foreach ( $hotels as $hotel )
  	        {
  	          if ( $hotel->getVendorKey() == $this->getVendorKey() )
  	          {
  	            $x++; // tellen
        	    	$this->log( '(' . ($x+1) . ') ' . $hotel->getNaam() . '(' . $hotel->getId() . ')' );
  	            $this->updateHotelInCache( $hotel, $arrangement );
  	          }
  	        }
  	      }
  	    }
  	    else
  	    {
  	    	//$this->log( '. ' . $arrangement->getNaam() . '(' . $arrangement->getHeen() . ') is niet meer actueel' );
  	    }
  	    
      }
      $this->log( '-- Totaal aantal requests: ' . $x );
  }

  private function getVendorKey()
  {
    if ( is_object ( $this->supplier ) )
    {
      if ( $vendorKey = $this->supplier->vendorKey )
      {
        return ( $vendorKey );
      }
      else
      {
      $this->log( "!!! vendorKey is niet gevuld, check de XML-feed-class!" );
      }
    }
    else
    {
      $this->log( "!!! supplier is niet bekend!!!" );
    }
  }

  function getHotels()
  {
    return $this->hotels;
  }
  
  function getCurrentDbTimestamp()
  {
    $q = $GLOBALS['rsdb']->query("SELECT EXTRACT('epoch' FROM NOW()) AS unixtime");
    return pg_fetch_result ( $q, 0 );
  }
  
  function getUpdateInterval( )
  {
    return $this->updateInterval;
  }
  
  function setUpdateInterval( $minutes )
  {
    $this->updateInterval = $minutes * 60;
  }

  function getSearchDays( )
  {
    return $this->search_days;
  }
  
  function setSearchDays( $days )
  {
    $this->search_days = $days;
  }
  
  function setId( $id )
  {
    $this->id = $id;
  }

  function getReisType( )
  {
    return $this->reis_type;
  }

  function setReisType( $reis_type )
  {
    $this->reis_type = $reis_type;
  }
  
  function setMargin( $days )
  {
    $this->margin = $days;
  }
  
  function updateHotelInCache( $hotel, $arrangement )
  {
    $hotelId = $hotel->getId();
    $arrangementId = $arrangement->getId();
    $dbTimestamp = $this->getCurrentDbTimestamp();
    $pricePp = false;

    $q = $GLOBALS['rsdb']->query("SELECT hotel_id, EXTRACT('epoch' FROM timestamp) AS timestamp FROM hotel_cache WHERE hotel_id = {$hotelId} AND arrangement_id = {$arrangementId}");
   
	  if ( $r = @pg_fetch_array( $q, 0) )
	  {
      if ( floor( $r['timestamp'] ) < floor( $this->getCurrentDbTimestamp()-$this->getUpdateInterval() ) )
      {
        if ( $pricePp = $this->getNewPrice( $hotel, $arrangement, 2 ) )
        {
          $toeslag = $this->getNewPrice( $hotel, $arrangement, 1 );
           $this->updatePrice( $hotel, $arrangement, $pricePp, $toeslag );
        }
        else
          $this->log( "!!! van hotel '" . $hotel->getNaam() . "' ({$hotelId}) kon geen actuele prijs worden gevonden" );
 
        sleep(1);
        
        $q = $GLOBALS['rsdb']->query("UPDATE hotel_cache SET timestamp=NOW() WHERE hotel_id = {$hotelId} AND arrangement_id = {$arrangementId}");
        $this->log( "... hotel {$hotelId} is bijgewerkt" );
      }
      else
      {
        $this->log( "... hotel '" . $hotel->getNaam() . "' ({$hotelId}) is actueel" );
      }
    }
    else
    {
      $q = $GLOBALS['rsdb']->query("INSERT INTO hotel_cache (hotel_id, arrangement_id) VALUES ({$hotelId}, {$arrangementId})");      
      $this->log( "... hotel {$hotelId} uit arrangement {$arrangementId} is aan de daemon toegevoegd" );
      if ( $pricePp = $this->getNewPrice( $hotel, $arrangement, 2 ) )
      {
        $toeslag = $this->getNewPrice( $hotel, $arrangement, 1 );
        $this->updatePrice( $hotel, $arrangement, $pricePp, $toeslag );
      }
      else
        $this->log( "!!! van hotel '" . $hotel->getNaam() . "' ({$hotelId}) kon geen actuele prijs worden gevonden" );

      sleep(1);

    }
  }
  
  function removeHotelInCache( $hotel, $arrangement )
  {
    $hotelId = $hotel->getId();
    $arrangementId = $arrangement->getId();
    $q = $GLOBALS['rsdb']->query("DELETE FROM hotel_cache WHERE hotel_id = {$hotelId} AND arrangement_id = {$arrangementId}");      
  }

  function clearCache()
  {
    $q = $GLOBALS['rsdb']->query("DELETE FROM hotel_cache");      
  }

  function updatePrice( $hotel, $arrangement, $pricePp, $toeslag )
  {
    $hotelId = $hotel->getId();
    $arrangementId = $arrangement->getId();
    $q = $GLOBALS['rsdb']->query("UPDATE hotel_cache SET p1 = {$toeslag}, p2 = {$pricePp} WHERE hotel_id = {$hotelId} AND arrangement_id = {$arrangementId}");      

    $hotel->setBasisprijs2persKamer( $pricePp );
    $hotel->setEenpersoonskamerToeslag( $toeslag );
    $hotel->store();

    $this->log( "ooo prijs en toeslag zijn bijgewerkt (E{$pricePp} / nacht)" );
  }

  function log ( $msg )
  {
    echo date('Y-m-d H:m:s') . ' '. $msg . "\n";
  }
  
  function getDateDifference( $datum1, $datum2 )
  {
		if( $datum1->format("U") > $datum2->format("U") )
		{
		  $d1 = $datum1;
		  $datum1 = $datum2;
		  $datum2 = $d1;
		}
		
		$dagNummer1 = $datum1->format( "z" );
		$dagNummer2 = $datum2->format( "z" );
	 	
		$verschilInJaren = (int) $datum2->format( "Y" ) - (int)$datum1->format( "Y" );
		
		if( $verschilInJaren )
		{
		  $dagNummer2 = $dagNummer2 + ( $verschilInJaren * 365 );	
		}
			
		return ( $dagNummer2 - $dagNummer1 );
  }  

}

?>
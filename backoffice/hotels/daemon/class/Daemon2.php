<?

class Daemon
{
  function __construct()
  {
    $this->log( '+++ Daemon geladen' );
  }

  function __destruct()
  {
    $this->log( '+++ Daemon be�indigd' );
  }

  function getHotels()
  {
    return $this->hotels;
  }
  
  function getCurrentDbTimestamp()
  {
    $q = $GLOBALS['rsdb']->query("SELECT EXTRACT('epoch' FROM NOW()) AS unixtime");
    return pg_fetch_result ( $q, 0 );
  }
  
  function getUpdateInterval( )
  {
    return $this->updateInterval;
  }
  
  function setUpdateInterval( $minutes )
  {
    $this->updateInterval = $minutes * 60;
  }
  
  function updateHotelInCache( $hotel, $arrangement )
  {
    $hotelId = $hotel->getId();
    $arrangementId = $arrangement->getId();
    $dbTimestamp = $this->getCurrentDbTimestamp();
    $pricePp = false;

    $q = $GLOBALS['rsdb']->query("SELECT hotel_id, EXTRACT('epoch' FROM timestamp) AS timestamp FROM hotel_cache WHERE hotel_id = {$hotelId} AND arrangement_id = {$arrangementId}");
   
	  if ( $r = @pg_fetch_array( $q, 0) )
	  {
      if ( floor( $r['timestamp'] ) < floor( $this->getCurrentDbTimestamp()-$this->getUpdateInterval() ) )
      {
        if ( $pricePp = $this->getNewPrice( $hotel, $arrangement, 2 ) )
        {
          $toeslag = $this->getNewPrice( $hotel, $arrangement, 1 );
          $this->updatePrice( $hotel, $arrangement, $pricePp, $toeslag );
        }
        else
          $this->log( "!!! van hotel '" . $hotel->getNaam() . "' ({$hotelId}) kon geen actuele prijs worden gevonden" );
        
        $q = $GLOBALS['rsdb']->query("UPDATE hotel_cache SET timestamp=NOW() WHERE hotel_id = {$hotelId} AND arrangement_id = {$arrangementId}");
        $this->log( "... hotel {$hotelId} is bijgewerkt" );
      }
      else
      {
        $this->log( "... hotel '" . $hotel->getNaam() . "' ({$hotelId}) is actueel" );
      }
    }
    else
    {
      $q = $GLOBALS['rsdb']->query("INSERT INTO hotel_cache (hotel_id, arrangement_id) VALUES ({$hotelId}, {$arrangementId})");      
      $this->log( "... hotel {$hotelId} uit arrangement {$arrangementId} is aan de daemon toegevoegd" );
      if ( $pricePp = $this->getNewPrice( $hotel, $arrangement, 2 ) )
      {
        $toeslag = $this->getNewPrice( $hotel, $arrangement, 1 );
        $this->updatePrice( $hotel, $arrangement, $pricePp, $toeslag );
      }
      else
        $this->log( "!!! van hotel '" . $hotel->getNaam() . "' ({$hotelId}) kon geen actuele prijs worden gevonden" );
    }
  }
  
  function removeHotelInCache( $hotel, $arrangement )
  {
    $hotelId = $hotel->getId();
    $arrangementId = $arrangement->getId();
    $q = $GLOBALS['rsdb']->query("DELETE FROM hotel_cache WHERE hotel_id = {$hotelId} AND arrangement_id = {$arrangementId}");      
  }

  function clearCache()
  {
    $q = $GLOBALS['rsdb']->query("DELETE FROM hotel_cache");      
  }

  function updatePrice( $hotel, $arrangement, $pricePp, $toeslag )
  {
    $hotelId = $hotel->getId();
    $arrangementId = $arrangement->getId();
    $q = $GLOBALS['rsdb']->query("UPDATE hotel_cache SET p1 = {$toeslag}, p2 = {$pricePp} WHERE hotel_id = {$hotelId} AND arrangement_id = {$arrangementId}");      
/*
    $hotel->setBasisprijs2persKamer( $pricePp );
    $hotel->setEenpersoonskamerToeslag( $toeslag );
    $hotel->store();
*/
    $this->log( "ooo prijs en toeslag zijn bijgewerkt (E{$pricePp} / nacht)" );
  }

  function log ( $msg )
  {
    echo date('Y-m-d H:m:s') . ' '. $msg . "\n";
  }
  
  function getDateDifference( $datum1, $datum2 )
  {
		if( $datum1->format("U") > $datum2->format("U") )
		{
		  $d1 = $datum1;
		  $datum1 = $datum2;
		  $datum2 = $d1;
		}
		
		$dagNummer1 = $datum1->format( "z" );
		$dagNummer2 = $datum2->format( "z" );
	 	
		$verschilInJaren = (int) $datum2->format( "Y" ) - (int)$datum1->format( "Y" );
		
		if( $verschilInJaren )
		{
		  $dagNummer2 = $dagNummer2 + ( $verschilInJaren * 365 );	
		}
			
		return ( $dagNummer2 - $dagNummer1 );
  }  

}

?>
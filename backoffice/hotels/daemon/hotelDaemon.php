<?

define( "PRONTO_PATH", "/data1/www/reissysteem/backoffice/hotels/daemon" );

require_once( PRONTO_PATH . '/../../../inc/config.php');
require_once( PRONTO_PATH . '/class/Daemon.php');
require_once( PRONTO_PATH . '/class/DaemonTemplate.php');
require_once( PRONTO_PATH . '/module/ModulePronto.php');

define ('HTML', false );

if ( HTML )
  echo '<pre>';

$db = new DB();

if ( !isset( $argv[1] ) )
{
  echo "Gebruik: $argv[0] [reistype] [(dagen vooruit)] [(id)]\n";
  die;
}

$myDaemon = new ModulePronto( $argv[1], $argv[2], $argv[3] );
//$myDaemon->cleanup();
//$myDaemon->clearCache();
$myDaemon->update();

?>
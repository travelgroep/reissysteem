<?
  require_once("../../inc/config.php");
  $id = @$_POST['id'];
  
  $idList = explode('|', $id);  

  $db = new DB();  

  foreach ($idList as $id)
  {
    if ($id)
    {
      $obj = new Hotel();
      $obj->loadById( $id );

      $naam = $obj->getNaam();
      {
        $obj->delete();
        
        $q = $GLOBALS['rsdb']->query("DELETE FROM hotel_cache WHERE hotel_id = {$id}");
        $q = $GLOBALS['rsdb']->query("DELETE FROM hotel_event WHERE hotel_id = {$id}");
        $q = $GLOBALS['rsdb']->query("DELETE FROM hotel_arrangement WHERE hotel_id = {$id}");
        
        echo 'Hotel \''.$naam.'\' verwijderd!' . "\n";
      }
    }
    else
    {
      echo 'Ongeldige id: ' . $id . "\n";
    }
  }
?>

<?
  require_once("../../inc/config.php");
	include_once('../../class/gta/Hotelpronto.php');

  function strProper($str) 
  {
    $noUp = array('a','an','of','the','are','at','in');
    $str = trim($str);
    $str = strtoupper($str[0]) . strtolower(substr($str, 1));
    for($i=1; $i<strlen($str)-1; ++$i) 
    {
      if($str[$i]==' ') 
      {
          for($j=$i+1; $j<strlen($str) && $str[$j]!=' '; ++$j);
          $size = $j-$i-1;
          $shortWord = false;
          if($size<=3) {
              $theWord = substr($str,$i+1,$size);
              for($j=0; $j<count($noUp) && !$shortWord; ++$j)
                  if($theWord==$noUp[$j])
                      $shortWord = true;
          }
          if( !$shortWord )
              $str = substr($str, 0, $i+1) . strtoupper($str[$i+1]) . substr($str, $i+2);
      }   
      $i+=$size;
    }
    return $str;
  }
  
  $db = new DB();  

  $pronto = new Pronto();

  $vendorId = $_POST['vendorId'];
  $vendorKey = $_POST['vendorKey'];
  $stadId = $_POST['stadId'];
  
  if ( !$id = isAdded( $vendorId, $vendorKey ) )
  { 
    $obj = new Hotel();
    
 //   $hotelData = $pronto->getHotelData( $vendorId );

    $q = "SELECT * FROM pronto_hotel WHERE id={$vendorId}";
    $res = $GLOBALS['rsdb']->query( $q );
    $hotelData = pg_fetch_array( $res, 0, PGSQL_ASSOC );
     
    $hotelData['name'] = strtolower( $hotelData['name'] );
    $hotelData['name'] = strProper( $hotelData['name'] );
    
    $obj->post( 
      array (
        'naam'=>$hotelData['name'],
        'vendor_id'=>$hotelData['id'],
        'vendor_key'=>$vendorKey,
        'rating'=>$hotelData['rating'],
        'addr1'=>$hotelData['address'],
        'stad_id'=>$stadId
      )
    );

    $obj->store();

    $res = $GLOBALS['rsdb']->query( "SELECT id FROM hotel WHERE vendor_id=" . $hotelData['id'] );
    $id = pg_fetch_result( $res, 0 );    

		// download and add photos
		
		$photos = explode('|', $hotelData['photo'] );
		
		foreach ( $photos as $photo )
		{
			if ( $photo )
			{
				$pic = file_get_contents( IMAGES_URL . $photo );
		
				$tmpfname = tempnam("/tmp", "PHENO");
				$_FILES['bestand']['tmp_name'] = $tmpfname;
				$handle = fopen($tmpfname, "w");
				fwrite( $handle, $pic );
				fclose( $handle );
				
				$f = new Foto();
				$f->post( array( 'naam'=>'afbeelding', 'beschrijving'=>'', 'webentiteit_id'=>$id ) );
				$f->storeWithId();		
			}
		}
		
		echo $id;		
  }
  else
  {
    $obj = new Hotel();
    $obj->loadById( $id );
      
    $naam = $obj->getNaam();
    $obj->delete(); 
    echo 'del';   
  }      
?>
<?
define ('MAX_THUMBNAILS', 3 );
define ('MAX_PP', 25 );

require_once("../../inc/config.php");
include_once('../../class/gta/Hotelpronto.php');

$db = new DB();  

$layout = new Layout('Hotelpronto-hotels', array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js', '/backoffice/js/pronto.js') );

$layout->header();
$layout->topMenu( 'Hotel' );
$layout->contentHeader();

$pronto = new Pronto();
$steden = getSteden( null, null, null, null, array( "hotelpronto_id != 0" ) );

$p = $_GET['p'];
$order = $_GET['order'] ? $_GET['order']:'rating';
$ascdesc = $_GET['ascdesc'] ? $_GET['ascdesc'] : 'asc';
$stad_id = $_GET['stad_id'];
$stad_id_cur = $_GET['stad_id_current'];
$cityCode = $_GET['citycode'];

// Build Search Request Data
if ( $_POST )
{
	$stad_id = $_POST['stad_id'];
	$stad_id_cur = $_POST['stad_id_current'];
	
	// Is er een citycode gekozen van deze stad?
	if( $stad_id == $stad_id_cur )
		$cityCode = $_POST['citycode'];
}


if( $stad_id && $steden[ $stad_id ] )
{

	$cityName = $steden[ $stad_id ]->getNaam();
	$countryCode = $steden[ $stad_id ]->getLand()->getISOcode();

}

	print "<form name=\"chk\" method=\"POST\" action=\"".$_SERVER['PHP_SELF']."\">\n";

  echo '<select name="stad_id" id="stad_id" onchange="submit()">';  
  echo '<option value="">maak een keuze</option>';
  foreach ($steden as $stad)
  {
    unset( $selected );
    if( $stad->getHotelprontoId() == $stad_id )
    {
    	$selected = 'selected="selected"';
    	$stadIdDb = $stad->getId();
    }
    echo '<option value="'.$stad->getHotelprontoId().'" '.$selected.'>'.$stad->getNaam().'</option>';
  }
  echo '</select>'; 

	echo '<input type="hidden" name="stad_id_current" value="'.$stad_id.'" />';

/*
  echo '<select name="citycode" id="citycode" onchange="submit()">';  

   echo '<option value="">maak een keuze</option>';
		
  foreach ($cityCodes as $stad=>$cCode)
  {
    unset( $selected );
    if( $cCode == $cityCode )
    	$selected = 'selected="selected"';
    	
    echo '<option value="'.$cCode.'" '.$selected.'>'.$stad.' ('.$cCode.')</option>';
  }
  echo '</select>'; 
*/

  $hotels = @$pronto->getHotelDataByCityId( $stad_id, $order, $ascdesc, MAX_PP . " OFFSET " . (MAX_PP * $p) );

  $q = @$db->query( "SELECT count(1) FROM pronto_hotel WHERE city_id={$stad_id}" );
	@$count = pg_fetch_result( $q, 0 );
	
	if( count( $hotels ) > 0 )
	{
				
		print "<table class=\"overzicht\">\n";
		print "	<tr>\n";

		print "		<th></th>\n";
		//print "		<th></th>\n";
		
		// quickly hacked in: sorting of name and rating columns
		$ascdescFlip = $ascdesc=='desc'?'asc':'desc';
		$ascdescImg = $ascdesc=='desc'?'up':'down';
		
    $link .= '<a href="pronto-hotels.php?p=0&order=name&ascdesc=' . $ascdescFlip	. '&cityCode=' . $cityCode . '&stad_id=' . $stad_id . '&stad_id_current=' . $stad_id_cur . '">Hotel ';
    if ( $order == 'name' ) $link .= '<img border="0" src="../i/' . $ascdescImg . '.png">';
    $link .= '</a>';		
		print '   <th>' . $link . '</th>';
		
    $link = '<a href="pronto-hotels.php?p=0&order=rating&ascdesc=' . $ascdescFlip	. '&cityCode=' . $cityCode . '&stad_id=' . $stad_id . '&stad_id_current=' . $stad_id_cur . '">Rating ';
    if ( $order == 'rating' ) $link .= '<img border="0" src="../i/' . $ascdescImg . '.png">';
    $link .= '</a>';
		print '   <th>' . $link . '</th>';			

		print "   <th>Adres</th>\n";			
		print "   <th>Foto's</th>\n";
    echo '<th><img src="../i/leeg.gif" width="16"> <img src="../i/leeg.gif" width="16"> <input style="width: 20px;" onclick="toggleAll()" type="checkbox"></th>';			
		print "	</tr>";
		
		$odd_even = "even";
		$teller = $p * MAX_PP;

	
		foreach( $hotels as $hotel_arr )
		{
			// $start telt vanaf 0,
			// maar wij tellen vanaf 1.
			$teller++;


      if ( $hotelDbId = isAdded( $hotel_arr['id'], 'pronto' ) )
      {
         $odd_even = "even_sel";
	    }
			print "	<tr id=\"row" . $hotel_arr['id'] . "\" class=\"".$odd_even."\" >\n";

			print "		<td>$teller</td>\n";
//			echo '   <td><input type="checkbox" name="add_hotel'.$hotel_arr['gta_code'].'" value="yes" \>';
      echo '<td>'.$hotel_arr['name'].' ('.$hotel_arr['id'].') </td>';
			echo '<td>';
      for( $i=0; $i<$hotel_arr['rating']; $i++ )
      	echo '<img src="../i/ster.gif" />';
			echo '</td>';

      echo '<td>'.$hotel_arr['address'].'</td>';
      
			echo '<td>';
			
			if ( count( $hotel_arr['thumbnail'] ) > 1 )
			{
			  $x = 0;
  			foreach ( $hotel_arr['thumbnail'] as $tn )
  			{
  			  echo '<a href="' . IMAGES_URL . $hotel_arr['photo'][$x] . '" target="_blank"><img border="0" src="' . IMAGES_URL . $tn . '" /></a> ';
          $x++;
          
          if ( $x == MAX_THUMBNAILS ) 
          	break;
  			}
      }
      else
      {
        echo '<a href="javascript:X()" onclick="downloadExtraInfo(' . $hotel_arr['id']. ',\'row' . $hotel_arr['id'] . '\')">Haal extra informatie op</a>';
      }
      			
			echo '</td>';

      $img = ($hotelDbId)?'del.png':'add.gif';
      echo '<td><nobr><a href="javascript:X()"><img onclick="addFromOverzicht(' . $hotel_arr['id'] . ', ' . $stadIdDb . ', this)" src="../i/' . $img . '" border="0" /></a> ';

      if ( $hotelDbId)
        echo '<a href="edit.php?id=' . $hotelDbId. '"><img title="bewerken" id="edit' . $hotel_arr['id']. '" src="../i/edit.png" border="0" /></a>';
      else echo '<img title="bewerken" id="edit' . $hotel_arr['id']. '" src="../i/edit_off.png" border="0" />';
		    
		  echo ' <input class="checkbox" type="checkbox" name="add_hotel'.$hotel_arr['id'].'" value="yes"></input>';
		  echo ' </nobr></td>';
		

			print "	</tr>\n";

			$odd_even = ($odd_even == "even") ? "odd" : "even";
		}

		$pages = $count / MAX_PP;
		$bar = "";
		
		for ( $x=0; $x<$pages; $x++ )
		{
			if ( $x==$p )
			{
				$bar .= ($x+1) . ' ';
			}
			else
			{
				$bar .= '<a href="pronto-hotels.php?p=' . $x 
				. '&order=' . $order
				. '&ascdesc=' . $ascdesc
				. '&cityCode=' . $cityCode 
				. '&stad_id=' . $stad_id 
				. '&stad_id_current=' . $stad_id_current . '">' . ($x+1) . '</a> ';
			}
		}
		
    echo '<tr><td colspan="5">' . $bar . '</td><td><nobr><a href="javascript:X()" onclick="addAllHotelsFromOverzicht()"><img src="../i/add.gif" border="0" /></a> <a href="javascript:X()" onclick="removeAllHotelsFromOverzicht()"><img src="../i/del.png" border="0" /></a></nobr></td></tr>';
  
		print "	</table>\n";
		
		// GTA-code in stad opslaan
		if( $cityCode && $cityCode != $steden[ $stad_id ]->getGTACode() )
		{
			$steden[ $stad_id ]->setGTACode( $cityCode );
			$steden[ $stad_id ]->store();
		}
	}
	else
	{
		print "<p><i>Geen hotels gevonden!</i></p>";
		if ( $stad_id )
		{
			echo '<a target="_blank" href="../../class/gta/download_hoteldata.php?id=' . $stad_id . '">Download hotels</a> (alleen tussen 23.00 en 09.00)';
		}
	}
	echo '<div id="totalstay_hotels">...</div>';

	if($stad_id) {
		$city = getSteden( null, null, null, null, array( "hotelpronto_id = ".$stad_id ) );
		echo '<script type="text/javascript">Xpost("./xml/totalstay_hotels.php", "x=1&stad_id=' . current($city)->getId() . '", "totalstay_hotels", function(){})</script>';
		}
  $layout->contentFooter();
  $layout->footer();


?>
<?
  header("Content-Type:text/html;charset=utf-8");
    
  require_once("../../inc/config.php");

  $db = new DB();

  //echo "<pre>";echo print_r($_POST,true);echo "</pre>";
  
  if ($id = @$_POST['id'])                                             // oude bewerken
  {
    
  	
  	$arrangement = new Arrangement();
    $arrangement->loadById( $id );
    $result = $arrangement->post($_POST);

    if (!$result) $arrangement->store();   
  }
  else                                                  // nieuwe aanmaken
  {
    
  	$event_id = $_POST['event_id'];
    $event = new Event();
    $event->loadById( $event_id );

    $arrangement = new Arrangement( $event );
    $result = $arrangement->post($_POST);
    
    if (!$result) $result = $arrangement->store();
  } 

  if (!$result)
  {
    if ( $reisoptieId  = $_POST['reisoptie_id'] )           // oude bewerken
    {
      $reisoptie = new Reisoptie();
      $reisoptie->loadById( $reisoptieId );
      $reisoptie->post( $_POST );
      
      $reisoptie->store();

      $reisoptiewaardes = $reisoptie->getReisoptieWaardes();
      $x = 0;
      
      foreach ($reisoptiewaardes as $reisoptiewaarde)
      {
        $reisoptiewaarde->post( array ('reisoptie_id' => $reisoptieId, 'naam' => $_POST['reisoptiewaarde_naam'][$x], 'waarde' => $_POST['reisoptiewaarde_waarde'][$x], 'purchase_price' => $_POST['reisoptiewaarde_purchase_price'][$x] ) );
        $reisoptiewaarde->store();
        $x++;
      }
    }
  }

// XML genereren ahv resultaatset
if ( $result ) generateResultXML( $result );

?>
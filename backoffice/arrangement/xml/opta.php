<?
  if (!@$id) 
  {
    $id = @$_POST['id'];
    if ($id) include('../../../inc/config.php');
  }
  
  if (!$db) $db = new DB();  

  if (!$arrangement) 
  {
    $arrangement = new Arrangement();        // als arrangement nog niet is aangemaakt
    if ($id) $arrangement->loadById( $id );
    $event = $arrangement->getEvent();
  }
  

$OptaTeamId = $event->getOptaTeamId();  

if ( $OptaTeamId ) {
	
	$optaMatchId = $arrangement->getOptaMatchId();
	$matches = array ();
	
	$teamModelManager= new TeamModelManager ( );
	
	$matchModelManager= new MatchModelManager ( );
	$matches = $matchModelManager->findByHomeTeamId( $OptaTeamId );
	 
	//echo "<pre>"; echo print_r( $teams ,true); echo "</pre>";
	?>
	<table border="0">
	    <tr id="marge_prijs">
	        <td class="left"><label for='team_id'>Opta Match</label></td>
	        <td>
	            <?php 
	              $homeTeam = $teamModelManager->find( $OptaTeamId );
	              echo '<select name="opta_match_id" id="match_id">';
				  echo '<option value="">maak een keuze</option>';
				  foreach ($matches as $match)
				  {
				    
				  	$awayTeam = $teamModelManager->find( $match->getAwayTeamId() );
				  	 
				  	$selected = ''; 
				  	if( $optaMatchId == $match->getId() )
				    {
				        $selected = 'selected="selected"';
				    } 
				    echo '<option '.$selected.' value="'.$match->getId().'">'. $homeTeam->getName(). " - " .$awayTeam->getName().'</option>';
				  }
				  echo '</select>';
	            ?>
	        </td>
	        <td id="opta_matchId_err"></td>
	    </tr>
	</table>
	<?php 
} else {
    ?>
    <table border="0">
        <tr id="marge_prijs">
            <td class="left">
                <p>Koppel eerst de TeamID zie event.
            </td>
        </tr>
    </table>
    <?php  
}




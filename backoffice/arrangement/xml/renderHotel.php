﻿<?
  $koppeltabel = "arrangement";
  
  if (!@$id) 
  {
    include('../../../inc/config.php');
    $id = $_POST['id'];
    $db = new DB();  
  }
  
  $arrangement = new Arrangement();
  $arrangement->loadById( $id );
  
  $event = new Event();
  $event->loadById( $arrangement->getEventId() );
  
  $locatie = new Locatie();
  $locatie->loadById ( $event->getLocatieId() );
  $stadId = $locatie->getStadId();  
  
  $arrangement->getEventId();

  $hotels = getHotelsByKoppeling ( $koppeltabel, $id ) ;

  echo '<table border="0">';

  echo '<tr><td align="right" colspan="2"><span class="knop" onclick="openMenuWrapperHotels(event, \'hotels\', \'' . $koppeltabel . '\', ' . $id . ', ' . $stadId . ')">...</span></td></tr>';

  if ( count($hotels) )
  {
    foreach ( $hotels as $hotel )
    {
      echo '<tr><td>' . $hotel->getNaam() . '</td><td width="32"><nobr><a href="../hotels/edit.php?id=' . $hotel->getId() . '"><img src="../i/edit.png" border="0" title="bewerken"></a> <a href="javascript:X()" onclick="deleteHotelFromKoppel(' . $id . ', \'' . $koppeltabel . '\', ' . $hotel->getId() . ')"><img border="0" src="../i/del.png" /></nobr></td></tr>';
    }
  }
  else echo '<tr><td>Er zijn nog geen hotels gekoppeld aan dit arrangement.<br />Alle aan het event <a href="../event/edit.php?id=' . $event->getId() . '">' . $event->getNaam() . '</a> gekoppelde hotels zullen beschikbaar zijn.</td></tr>';

  echo '<tr><td><a href="javascript:X()" onclick="addHotelsToKoppel(' . $id . ', \'' . $koppeltabel . '\', ' . $stadId  . ')">alle hotels koppelen</a></nobr></td></tr>';


  echo '</table>';

  echo '<div class="selector" id="hotels">...</div>';  
 
?>
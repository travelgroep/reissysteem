<?
  if (!@$id) 
  {
    $id = @$_POST['id'];
    if ($id) include('../../../inc/config.php');
  }
  
  if (!$db) $db = new DB();  

  if (!$arrangement) 
  {
    $arrangement = new Arrangement();        // als arrangement nog niet is aangemaakt
    if ($id) $arrangement->loadById( $id );
  }

	$event = &$arrangement->getEvent();

  echo '<table border="0">';

  echo '<tr id="datum"><td class="left">datum</td><td><input type="text" name="datum" value="'.$arrangement->getDatum().'"></td><td id="datum_err"></td></tr>';
  echo '<tr id="heen"><td class="left">datum heen</td><td><input type="text" name="heen" value="'.$arrangement->getHeen().'"></td><td id="heen_err"></td></tr>';
  echo '<tr id="terug"><td class="left">datum terug</td><td><input type="text" name="terug" value="'.$arrangement->getTerug().'"></td><td id="terug_err"></td></tr>';
  echo '<tr id="ticket_prijs"><td class="left">ticket prijs</td><td><input id="arr_ticket_prijs" type="text" name="ticket_prijs" value="'.$arrangement->getTicketPrijs().'" onChange="calculateFare();" ></td><td id="ticket_prijs_err"></td></tr>';
  echo '<tr id="vlucht_prijs"><td class="left">vlucht prijs</td><td><input id="arr_vlucht_prijs" type="text" name="vlucht_prijs" value="'.$arrangement->getVluchtPrijs().'" onChange="calculateFare();"></td><td id="vlucht_prijs_err"></td></tr>';
  echo '<tr id="hotel_prijs"><td class="left">hotelprijs</td><td><input disabled id="arr_hotel_prijs" type="text" name="hotel_prijs" value="'.$arrangement->getHotelPrijs().'"></td><td id="hotel_prijs_err"></td></tr>';
  echo '<tr id="extra_marge"><td class="left">extra marge</td><td><input id="arr_extra_marge" onChange="calculateFare();" type="text" name="extra_marge" value="'.$arrangement->getExtraMarge().'"></td><td id="extra_marge_err"></td></tr>';
  echo '<tr id="prijs"><td class="left">prijs (incl. ' . $event->getMargePrijs() . ')</td><td><input id="arr_prijs" type="text" name="prijs" value="'.$arrangement->getPrijs( null ).'"></td><td id="prijs_err"></td></tr>';
  if ( $arrangement->getHotelId() ) 
    echo '<tr><td colspan="2" class="left">bij de prijs wordt uitgegaan van hotel \'' . $arrangement->getHotelNaam() . '\' <a href="../hotels/edit.php?id=' . $arrangement->getHotelId() . '"><img src="../i/edit.png" title="bewerken" border="0"></a></td></tr>';

  echo '<tr id="actie_code"><td class="left">actiecode</td><td><input id="arr_actie_code" type="text" name="actie_code" value="'.$arrangement->getActieCode().'"></td><td id="actie_code_err"></td></tr>';
  echo '<tr id="actie_beschrijving"><td class="left">actiebeschrijving</td><td><input id="arr_actie_beschrijving" type="text" name="actie_beschrijving" value="'.$arrangement->getActieBeschrijving().'"></td><td id="actie_beschrijving_err"></td></tr>';
  echo '<tr id="actie_bedrag"><td class="left">actiebedrag (current currency)</td><td><input id="arr_actie_bedrag" type="text" name="actie_bedrag" value="'.$arrangement->getActieBedrag().'"></td><td id="actie_bedrag_err"></td></tr>';
  echo '<tr id="actie_vervaldatum"><td class="left">actievervaldatum</td><td><input id="arr_actie_vervaldatum" type="text" name="actie_vervaldatum" value="'.$arrangement->getActieVervaldatum().'"></td><td id="actie_vervaldatum_err"></td></tr>';
  ($arrangement->getActiePp() == "t") ? $sel = 'checked' : $sel = '';
  echo '<tr><td class="left">actie pp</td><td><input class="checkbox" type="checkbox" name="actie_pp" '.$sel.'></input></td></tr>';

  echo '<tr id="vlucht_heen_tarief"><td class="left">vluchttarief heen</td><td><input type="text" name="vlucht_heen_tarief" value="'.$arrangement->getVluchtHeenTarief().'"></td><td id="vlucht_heen_tarief_err"></td></tr>';
  echo '<tr id="vlucht_terug_tarief"><td class="left">vluchttarief terug</td><td><input type="text" name="vlucht_terug_tarief" value="'.$arrangement->getVluchtTerugTarief().'"></td><td id="vlucht_terug_tarief_err"></td></tr>';
  echo '<tr id="stock_to_amount"><td class="left">aanbod Transavia (heen)</td><td><input type="text" name="stock_to_amount" value="'.$arrangement->getStockToAmount().'"></td><td id="stock_to_amount_err"></td></tr>';
  echo '<tr id="stock_return_amount"><td class="left">aanbod Transavia (terug)</td><td><input type="text" name="stock_return_amount" value="'.$arrangement->getStockReturnAmount().'"></td><td id="stock_return_amount_err"></td></tr>';

  echo '<tr id="stock_heen_prijs"><td class="left">vluchttarief Transavia heen</td><td><input type="text" name="stock_heen_prijs" value="'.$arrangement->getStockHeenPrijs().'"></td><td id="stock_heen_prijs_err"></td></tr>';
  echo '<tr id="stock_terug_prijs"><td class="left">vluchttarief Transavia terug</td><td><input type="text" name="stock_terug_prijs" value="'.$arrangement->getStockTerugPrijs().'"></td><td id="stock_terug_prijs_err"></td></tr>';

  echo '<tr id="stock_from_iata"><td class="left">IATA vertrek</td><td><input type="text" name="stock_from_iata" value="'.$arrangement->getStockFromIATA().'"></td><td id="stock_from_iata_err"></td></tr>';
  echo '<tr id="stock_heen_vertrek"><td class="left">vertrektijdstip heen</td><td><input type="text" name="stock_heen_vertrek" value="'.$arrangement->getStockHeenVertrek().'"></td><td id="stock_heen_vertrek_err"></td></tr>';
  echo '<tr id="stock_heen_aankomst"><td class="left">aankomsttijdstip heen</td><td><input type="text" name="stock_heen_aankomst" value="'.$arrangement->getStockHeenAankomst().'"></td><td id="stock_heen_aankomst_err"></td></tr>';

  echo '<tr id="stock_to_iata"><td class="left">IATA bestemming</td><td><input type="text" name="stock_to_iata" value="'.$arrangement->getStockToIATA().'"></td><td id="stock_to_iata_err"></td></tr>';
  echo '<tr id="stock_terug_vertrek"><td class="left">vertrektijdstip terug</td><td><input type="text" name="stock_terug_vertrek" value="'.$arrangement->getStockTerugVertrek().'"></td><td id="stock_terug_vertrek_err"></td></tr>';
  echo '<tr id="stock_terug_aankomst"><td class="left">aankomsttijdstip terug</td><td><input type="text" name="stock_terug_aankomst" value="'.$arrangement->getStockTerugAankomst().'"></td><td id="stock_terug_aankomst_err"></td></tr>';

  ($arrangement->actief_overboeken == "t") ? $sel = 'checked' : $sel = '';
  echo '<tr><td class="left">actief overboeken</td><td><input class="checkbox" type="checkbox" name="actief_overboeken" '.$sel.'></input></td></tr>';
  ($arrangement->actief_boeken == "t") ? $sel = 'checked' : $sel = '';
  echo '<tr><td class="left">actief boeken</td><td><input class="checkbox" type="checkbox" name="actief_boeken" '.$sel.'></input></td></tr>';
  ($arrangement->getVluchtBoeken() == "t") ? $sel = 'checked' : $sel = '';
  echo '<tr><td class="left">actief vlucht boeken</td><td><input class="checkbox" type="checkbox" name="vlucht_boeken" '.$sel.'></input></td></tr>';

  ($arrangement->getToparrangement() == "t") ? $sel = 'checked' : $sel = '';
  echo '<tr><td class="left">toparrangement</td><td><input class="checkbox" type="checkbox" name="toparrangement" '.$sel.'></input></td></tr>';


  echo '</table>';

?>
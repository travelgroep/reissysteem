<?
  require_once("../../../inc/config.php");
  
  	$db = new DB();
	$start = 0;
	$paginagrootte = PAGINAGROOTTE;
	
	/*
	 * Deze 3 velden bepalen hoe de lijst getoond wordt,
	 * zoals sortering, en welke pagina. Deze moeten
	 * altijd worden doorgegeven als naar 'PHP_SELF'
	 * wordt verwezen.
	 */
	$start = $_POST['start'];
	if ( !$sort = $_POST['sort'] ) $sort = "id";
	if ( !$ascdesc = $_POST['ascdesc'] ) $ascdesc = "desc";

  	$currentSort 	= "sort={$sort}&ascdesc={$ascdesc}&start={$start}";

  	$filter 		= $_POST['filter'];
  	$filterField 	= $_POST['filterField'];
  
  	if ( $filter && $filterField )  { $crit[] = "LOWER($filterField)" . " LIKE LOWER('%$filter%')"; }
  
  	$eventypeId = $_SESSION['eventypeId'];
  	if (isset($eventypeId)){ 	$crit []  		= "event_type_id = $eventypeId "; }
	if (isset($seasonTime)){ 	$crit []  		= $seasonTime; }
  
	$arrangementen 			= getArrangementen( $paginagrootte, $start, $sort == 'event'?'event.naam':$sort, $ascdesc, $crit );
	$aantal_arrangementen 	= countArrangementenOnEventType($crit);
	
	//print_r ( $routes ); print "<";


	print "<table class=\"overzicht\">\n";

	$fields = array( 'id', 'naam', 'event', 'beschrijving_kort', 'heen', 'datum', 'terug', 'prijs', 'ticket_prijs', 'vlucht_prijs' );
	
	// filters
	$p = "sort={$sort}&ascdesc={$ascdesc}&start={$start}";		

  echo '<tr>';
  echo '<td><img src="../i/filter.png" /></td>';
	foreach( $fields as $field )
	{
    if ( $field == 'event' )
    {
      $field = 'event.naam';
    }
    else
    {
      $field = 'arrangement.' . $field;
    }

    $value = strstr( $filterField, $field ) ? $filter : '';


		echo '<td><input value="' . $value . '" onchange="applyFilter(this, \'' . $field . '\', \'' . $p . '\')" type="text" size="8"></input></td>';
	}
  echo '</tr>';
  		
	print "	<tr>\n";

	print "		<th></th>\n";
	foreach( $fields as $field )
	{
		if( $field == $sort && $ascdesc=='desc' )
		{
			$fld_ascdesc='asc';
			$pic = "up.png";
		}
		else if ( $field == $sort )
		{
			$fld_ascdesc='desc';
			$pic = "down.png";
		}
		else 
		{
		  $fld_ascdesc = 'desc';
		  $pic = "leeg.gif";
	  }
		
		$p = "sort={$field}&ascdesc={$fld_ascdesc}&start={$start}&filter={$filter}&filterField={$filterField}";
    $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
		
		print "<th><a href=\"javascript:X()\" onclick=\"{$onclick}\">$field <img src=\"../i/$pic\" border=\"0\" /></a></th>";			
	}
	
	print "		<th><img src=\"../i/leeg.gif\" width=\"16\" /> <img src=\"../i/leeg.gif\" width=\"16\" /> <input style=\"width:20px;\" type=\"checkbox\" onclick=\"toggleAll()\"></input></th>\n";
	print "	</tr>";


	if( $arrangementen )
	{
		$odd_even = "even";
		$teller = $start;
		
		foreach( $arrangementen as $arrangement )
		{
			if ( $odd_even == 'odd' )
				$rowColor = '#B1B1F9';
			else
				$rowColor = '#ffffff';

			// $start telt vanaf 0,
			// maar wij tellen vanaf 1.
			$teller++;

	
			print "	<tr class=\"".$odd_even."\" >\n";

			print "		<td>$teller</td>\n";
			print "		<td>".$arrangement->getId()."</td>\n";
			print "		<td>".$arrangement->getNaam()."</td>\n";

      $event = new Event();
      $event->loadById( $arrangement->getEventId() );			
			
			print "		<td>".$event->getNaam()."</td>\n";
			print "		<td>".$arrangement->getBeschrijvingKort()."</td>\n";
			print "		<td>".$arrangement->getHeen('%d %b %Y')."</td>\n";
			print "		<td>".$arrangement->getDatum('%d %b %Y')."</td>\n";
			print "		<td>".$arrangement->getTerug('%d %b %Y')."</td>\n";
			print "		<td>".$arrangement->getPrijs()."</td>\n";
			print "		<td>".$arrangement->getTicketPrijs()."</td>\n";
			print "		<td>".$arrangement->getVluchtPrijs()."</td>\n";
//			print "		<td>".$arrangement->getMarge()."</td>\n";

			print "		<td><nobr><a href=\"edit.php?id=".$arrangement->getId()."\"><img src=\"../i/edit.png\" border=\"0\"/></a> <a onclick=\"deleteFromOverzicht(" . $arrangement->getId() . ", '{$currentSort}')\" href=\"javascript:X()\"><img src=\"../i/del.png\" border=\"0\"/></a> <input class=\"checkbox\" type=\"checkbox\" value=\"" . $arrangement->getId() . "\"></input></nobr></td>\n";

			print "	</tr>\n";

			$odd_even = ($odd_even == "even") ? "odd" : "even";
		}

		// Navigatie
		print "<tr><td colspan=\"11\">\n";
		
	
		// Vorige pagina
		$prev = $start - $paginagrootte;
      
		if( $prev >= 0 )
		{
  		$p = "sort={$sort}&ascdesc={$ascdesc}&start={$prev}&filter={$filter}&filterField={$filterField}";
      $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
			print "		<a onclick=\"{$onclick}\" title=\"vorige\" href=\"javascript:X()\">&lt;</a>\n";			
    }
    
		else
			print "		&lt;\n";			
		
		// Stappen
		for( $i=0; $i<$aantal_arrangementen; $i+=$paginagrootte )
		{
			if( $start != $i )
			{
  		  $p = "sort={$sort}&ascdesc={$ascdesc}&start={$i}&filter={$filter}&filterField={$filterField}";
        $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
      			  
				print "		<a onclick=\"{$onclick}\" href=\"javascript:X()\">".($i+1)."</a>\n";			
      }
			else
				print $i+1;
		}
		
		// Volgende pagina
		$next = $start + $paginagrootte;
		if( $next < $aantal_arrangementen )
		{
		  $p = "sort={$sort}&ascdesc={$ascdesc}&start={$next}&filter={$filter}&filterField={$filterField}";
      $onclick = 'Xpost(\'./xml/overzicht.php\', \'' . $p . '\', \'overzicht\', function(){})';
			print "		<a onclick=\"{$onclick}\" title=\"volgende\" href=\"javascript:X()\">&gt;</a>\n";			
    }
    
		else
			print "		&gt;\n";			


		// Toevoegen
		print "</td><td><nobr><a href=\"toevoegen_multi.php\"><img alt=\"nieuw\" src=\"../i/new.png\" title=\"toevoegen\" border=\"0\"></a> <a onclick=\"deleteAllFromOverzicht( '{$currentSort}' )\" href=\"javascript:X()\"><img alt=\"nieuw\" src=\"../i/del.png\" title=\"verwijderen\" border=\"0\"></a></nobr></td>";

		print "</tr>\n";
	}
	else
	{
		echo '<tr><td colspan="2"><i>Geen arrangementen gevonden!</i></td></tr>';		
		echo '<tr><td colspan="2"><a href="toevoegen_multi.php">Toevoegen</a></td></tr>';

	}
  print "	</table>\n";
?>
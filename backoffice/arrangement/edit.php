<?
  require("../../inc/config.php");
  $id = @$_GET['id'];
  $db = new DB();  
  
  $obj = new Arrangement();

  if ($id)
  {
    $obj->loadById( $id );
    $event = $obj->getEvent();
    if ( is_object($event) ) $locatie = $event->getLocatie();
    else die ('Het gekoppelde event bestaat niet meer.');
    if ( is_object($locatie) ) $stad = $locatie->getStad();
    else die ('De gekoppelde locatie bestaat niet meer.');    
    if ( is_object($stad) ) $land = $stad->getLand();
    else die ('De gekoppelde stad bestaat niet meer.');

    $naam = $obj->getNaam();
    $onload = 'initMenu ( function(){ syncMenu(' . $land->getId() . ',' . $stad->getId() . ',' . $locatie->getId() . ','.$event->getId().'); } );';
  }
  else
  {
    $naam = 'nieuw';
    $onload = '_currentView=\'arrangement_id\'; initMenu ( function(){ syncMenu(null, null, null, null, null) } );';
  }

  $layout = new Layout('Arrangement: ' . $naam, array('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js'), $onload );

  $layout->header();
  $layout->topMenu( 'Arrangement' );
  $layout->contentHeader();

?>

<script type="text/javascript">
<!--

function calculateFare(  )
{
	var h = document.getElementById("arr_hotel_prijs");
	var t = document.getElementById("arr_ticket_prijs");
	var v = document.getElementById("arr_vlucht_prijs");
	var m = document.getElementById("marge_prijs");
	var em = document.getElementById("arr_extra_marge");

	var total = new Number( 0 );
	
	var f = document.getElementById("arr_prijs" );
	
	total = ( parseInt(h.value) + parseInt(t.value) + parseInt(v.value) + parseInt(m.value) + parseInt(em.value));
	
	round9total = ( Math.ceil( total /10 ) * 10 ) -1;
	
	f.value = round9total.toFixed(2);
}

-->

</script>

<?

  echo '<form action="update.php" onsubmit="return update(this)">';
	if( $event )
		print "<input id=\"marge_prijs\" name=\"marge_prijs\" type=\"hidden\" value=\"".$event->getMargePrijs()."\"/></td>\n";
  
  echo '<div id="parentContent">';


  echo '<table border="0">';

  echo '<tr>';
  echo '<td class="left">land</td>';
  echo '<td id="landContent">';
  echo '</td>';
  echo '<td id="land_id_err"><td>';
  echo '</tr>';

  echo '<tr>';
  echo '<td class="left">stad</td>';
  echo '<td id="stadContent">';
  echo '</td>';
  echo '<td id="stad_id_err"><td>';
  echo '</tr>';

  echo '<tr>';
  echo '<td class="left">locatie</td>';
  echo '<td id="locatieContent">';
  echo '</td>';
  echo '<td id="locatie_id_err"><td>';
  echo '</tr>';
  
  echo '<tr>';
  echo '<td class="left">event</td>';
  echo '<td id="eventContent">';
  echo '</td>';
  echo '<td id="event_id_err"><td>';
  echo '</tr>';

  echo '</table>'; 

  echo '</div>';

  echo '<div id="webentiteitContent">';
    include("../xml/renderWebentiteit.php");
  echo '</div>';

  
  if ($id)
  {
    echo '<div id="fotoContent">';
    echo '<iframe id="uploadIframe" frameborder="0" src="../system/uploadFotoIframe.php?beschrijving_id=' . $id . '" width="100%"></iframe>';
    echo '</div>';
  }
    
  echo '<div id="optaContainer">';
  include("./xml/opta.php");
  echo '</div>';
  
  echo '<div id="beschrijvingContent">';
    include("../xml/renderBeschrijving.php");
  echo '</div>';

  echo '<div id="specificContent">';
    include("./xml/render.php");
  echo '</div>';
  
  if ($id)
  {
    echo '<div id="reisoptieContent">';
      include("../xml/renderReisoptie.php");
    echo '</div>';
  
    echo '<div id="reisoptieWaardeContent"></div>';
  }

  echo '<div id="hotelContent">';
    include ("./xml/renderHotel.php");
  echo '</div>';

  echo '<div id="submit"><input type="submit" value="WIJZIGINGEN OPSLAAN"></input></div>';

  echo '</form>';

	$layout->contentFooter();
  $layout->footer();

?>
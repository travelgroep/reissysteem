<?
  require_once("../../inc/config.php");
  $id = @$_POST['id'];

  $idList = explode('|', $id);

  $db = new DB();  

  foreach ($idList as $id)
  {
    if ($id)
    {
      $obj = new Arrangement();
      $obj->loadById( $id );
    
      $naam = $obj->getNaam();

      $obj->delete();
      echo 'Arrangement \''.$naam.'\' verwijderd!' . "\n";
    }
    else echo 'Ongeldige id: ' . $id . "\n";
  }

?>


<?
require_once ("../../inc/config.php");

$db = new DB ( );
$start = 0;
$paginagrootte = 5;

$event_id = $_POST ['event_id'];
if ($event_id) {
	$event = new Event ( );
	$event->loadById ( $event_id );
	$locatie = $event->getLocatie ();
	$stad = $locatie->getStad ();
	$land = $stad->getLand ();
	
	$onload = 'initMenu ( function(){ syncMenu(' . $land->getId () . ',' . $stad->getId () . ',' . $locatie->getId () . ',' . $event->getId () . '); } );';

}

$layout = new Layout ( 'Arrangementen', array ('/backoffice/js/ajax.js', '/backoffice/js/backoffice.js' ), $onload );

$layout->header ();
$layout->topMenu ( 'arrangement' );
$layout->contentHeader ();

?>


<script type="text/javascript">
  
function SelectAll(prefix, checkbox_state) {
	for (var i = 0; i < document.chk.elements.length; i++) {
		if ((document.chk.elements[i].name.substr(0, prefix.length) == prefix) && (document.chk.elements[i].style.visibility != 'hidden')) {
			document.chk.elements[i].checked = checkbox_state;
		}
	}
}	

function ChangeAll(prefix, val) {
	for (var i = 0; i < document.chk.elements.length; i++) {
		if ((document.chk.elements[i].name.substr(0, prefix.length) == prefix) && (document.chk.elements[i].style.visibility != 'hidden')) {
			document.chk.elements[i].value = val;
		}
	}
}	

function calculateFare( datetag )
{
	var b = document.getElementById("hotel_prijs");
	var h = document.getElementById("arr_extra_marge"+datetag);
	var t = document.getElementById("arr_ticket_prijs"+datetag);
	var v = document.getElementById("arr_vlucht_prijs"+datetag);
	var m = document.getElementById("marge_prijs");
	var total = new Number( 0 );
	 
	var f = document.getElementById("arr_prijs"+datetag);
	
	if ( !b.value ) b.value = 0;
	if ( !h.value ) h.value = 0;
	
	total = ( parseInt(t.value) + parseInt(v.value) + parseInt(m.value) + parseInt(h.value) + parseInt(b.value) );
	
	round9total = ( Math.ceil( total /10 ) * 10 ) -1;
	
	f.value = round9total.toFixed(2);
}

</script>


<?
echo '<div id="parentContent">';

$start = $_POST ['start'];
$end = $_POST ['end'];

// Als begin niet opgegeven is, nu nemen.
if (! $start){
	$start = Date ( "Y-m-d" );
}
$start_stamp = strtotime ( $start );
$start_arr = getdate ( $start_stamp );

// Als einde niet opgegeven is, 14 dagen na $start nemen
if (! $end)
	$end = Date ( "Y-m-d", mktime ( 0, 0, 0, $start_arr ['mon'], $start_arr ['mday'] + 14, $start_arr ['year'] ) );

$end_stamp = strtotime ( $end );

// Is er op de submit knop geduwd? Kunnen we arrangementen aanmaken?
if ($_POST ['toevoegen'] && $event) {
	$event = new Event ( );
	$event->loadById ( $event_id );
	
	for($i = 0; $date < $end_stamp; $i ++) {
		$date = mktime ( 0, 0, 0, $start_arr ['mon'], $start_arr ['mday'] + $i, $start_arr ['year'] );
		$datetag = date ( "dmY", $date );
		
		// Alleen toevoegen als hij geenabled is.
		if ($_POST ["arr_enable_$datetag"]) {
			
			if ($event) {
				
				$arrangement_arr ['datum'] = date ( "Y-m-d", $date );
				$arrangement_arr ['datum'] .= " " . $_POST ["arr_tijd_$datetag"];
				$arrangement_arr ['naam'] = $_POST ["arr_naam_$datetag"];
				$arrangement_arr ['beschrijving_kort'] = $_POST ["arr_beschr_kort_$datetag"];
				$arrangement_arr ['beschrijving'] = $_POST ["arr_beschr_$datetag"];
				//		$arrangement_arr['prijs'] = $_POST["arr_prijs_$datetag"];
				$arrangement_arr ['ticket_prijs'] = $_POST ["arr_ticket_prijs_$datetag"];
				$arrangement_arr ['vlucht_prijs'] = $_POST ["arr_vlucht_prijs_$datetag"];
				$arrangement_arr ['extra_marge'] = $_POST ["arr_extra_marge_$datetag"];
				
				// Heen en terug relatief tov de datum.
				$arrangement_arr ['heen'] = date ( "Y-m-d", mktime ( 0, 0, 0, $start_arr ['mon'], $start_arr ['mday'] + $i - $_POST ["arr_heen_$datetag"], $start_arr ['year'] ) );
				$arrangement_arr ['terug'] = date ( "Y-m-d", mktime ( 0, 0, 0, $start_arr ['mon'], $start_arr ['mday'] + $i + $_POST ["arr_terug_$datetag"], $start_arr ['year'] ) );
				
				//print $arrangement_arr['heen'];
				

				$arrangement_arr ['actief_overboeken'] = $_POST ["arr_overboeken_$datetag"];
				$arrangement_arr ['actief_boeken'] = $_POST ["arr_boeken_$datetag"];
				$arrangement_arr ['actief'] = $_POST ["arr_actief_$datetag"];
				$arrangement_arr ['vlucht_boeken'] = $_POST ["arr_vlucht_boeken_$datetag"];
				

				// $print = print_r($arrangement_arr,true);
				//mail("alert@travelgroep.nl",  "LOG Toevoegen arr (live (66)", "\n ALGEMEEN: =>" .$print );

    	  
				$arrangement = new Arrangement ( $event );
				$arrangement->post ( $arrangement_arr );
				
				$res = $arrangement->store ();
			} else {
				echo "<p>Event $event_id niet gevonden!</p>";
			}
			//print "<br/>Arrangement '".$arrangement->getNaam()."' op '".$arrangement->getDatum()."' toegevoegd. Result:'".$res."'\n";
		

		//print "<pre>\n";
		//print_r( $arrangement );
		//print "</pre>\n";
		

		}
	
	}
	
	?>
<script type="text/javascript">
<!--
alert('De arrangementen zijn toegevoegd.');
document.location.href='index.php' 
-->
</script>
<?
	
	die ();

}

print "<form id=\"chk\" name=\"chk\" method=\"POST\" action=\"" . $_SERVER ['PHP_SELF'] . "\">\n";

if ($event) {
	// bepaal prijs goedkoopste hotel
	if (! $hotels = getHotelsByKoppeling ( 'event', $event->getId () )) {
		if (! $hotels = getHotels ( 0, 0, "", "", "stad_id=$stadId" )) {
			$locatie = $event->getLocatie ();
			$stad = $locatie->getStad ();
			$prijs = $stad->getHotelPrijs ();
		}
	}
	if (is_object ( $hotels [0] ))
		$prijs = $hotels [0]->getBasisprijs2persKamer ();
	
	print "<input id=\"marge_prijs\" name=\"marge_prijs\" type=\"hidden\" value=\"" . $event->getMargePrijs () . "\"/></td>\n";
	print "<input id=\"hotel_prijs\" name=\"hotel_prijs\" type=\"hidden\" value=\"" . $prijs . "\"/></td>\n";
}

echo '<table border="0">';
echo '<tr>';
echo '<td>land</td>';
echo '<td>';
echo '<select name="land_id" id="land_id" onchange="getStedenFromSelect(this)">';
echo '<option value="">maak een keuze</option>';

$landen = getLanden ();
foreach ( $landen as $land_item ) {
	if ($land && $land_item->getId () == $land->getId ())
		$selected = 'selected="selected"';
	else
		unset ( $selected );
	
	echo '<option $selected value="' . $land_item->getId () . '">' . $land_item->getNaam () . '</option>';
}
echo '</select>';
echo ' <a href="javascript:newLand()"><img border="0" alt="nieuw land" src="../i/add.gif" title="nieuw land" /></a>';

echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td>stad</td>';
echo '<td id="stadContent">';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td>locatie</td>';
echo '<td id="locatieContent">';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td>event</td>';
echo '<td id="eventContent">';
echo '</td>';
echo '</tr>';

/*
  echo '<tr>';
  echo '<td>eventtype</td>';
  echo '<td>';

  echo '<select name="event_type_id" id="event_type_id">';  
  echo '<option value="">maak een keuze</option>';

  $eventtypes = getEventTypes();
  foreach ($eventtypes as $eventtype)
  {						
    echo '<option '.$selected.' value="'.$eventtype->getId().'">'.$eventtype->getNaam().'</option>';
  }
  echo '</select>'; 
  echo ' <a href="#"><img border="0" alt="nieuw land" src="../i/add.gif" title="nieuw land" /></a>';

  echo '</td>';
  echo '</tr>';
*/

echo '</table>';
echo '</div>';

echo '<div id="arrangement_multi">';

// Arrangementen voegen we alleen toe als er een event bekend is waar we ze aan toe moeten voegen.
if ($event) {
	
	print "		Start:<input class=\"date\" name=\"start\" type=\"text\" value=\"$start\" onChange=\"document.chk.submit();\" />\n";
	print "		Eind: <input class=\"date\" name=\"end\" type=\"text\" value=\"$end\" onChange=\"document.chk.submit();\" />\n";
	
	print 'Dagen uitschakelen: ';
	
	$dagen = array ('zo', 'ma', 'di', 'wo', 'do', 'vr', 'za' );
	for($i = 0; $i < 7; $i ++) {
		unset ( $checked );
		if ($_POST ['disable_' . $i])
			$checked = 'CHECKED="CHECKED"';
		
		print $dagen [$i];
		print ":<input class=\"checkbox\" name=\"disable_$i\" type=\"checkbox\" $checked onChange=\"document.chk.submit();\" />\n";
	}
	
	print "<table class=\"overzicht\">\n";
	
	print "	<tr class=\"even\" >\n";
	
	print "		<td>$teller</td>\n";
	print "		<td><input class=\"checkbox\" type=\"checkbox\" name=\"arr_checkall\" value=\"yes\" onClick=\"SelectAll('arr_enable',this.checked);\" /></td>\n";
	print "		<td></td>\n";
	print "		<td></td>\n";
	print "		<td></td>\n";
	print "		<td><input size=4 type=\"text\" onChange=\"ChangeAll('arr_tijd',this.value);\"/></td>\n";
	print "		<td><input size=2 type=\"text\" onChange=\"ChangeAll('arr_heen',this.value);\"/></td>\n";
	print "		<td><input size=2 type=\"text\" onChange=\"ChangeAll('arr_terug',this.value);\"/></td>\n";
	print "		<td><input name=\"allName\" type=\"text\" onChange=\"ChangeAll('arr_naam',this.value);\"/><!-- span class=\"knop\" onclick=\"openMenuWrapper(event, 'names', 'allName')\">...</span --></td>\n";
	print "		<td><input type=\"text\" onChange=\"ChangeAll('arr_beschr_kort',this.value);\"/></td>\n";
	print "		<td><input type=\"text\" onChange=\"ChangeAll('arr_beschr',this.value);\"/></td>\n";
	print "		<td><input size=4 type=\"text\" onChange=\"ChangeAll('arr_ticket_prijs',this.value);\"/></td>\n";
	print "		<td><input size=4 type=\"text\" onChange=\"ChangeAll('arr_vlucht_prijs',this.value);\"/></td>\n";
	//print "		<td></td>\n";
	print "		<td><input size=4 type=\"text\" onChange=\"ChangeAll('arr_extra_marge',this.value);\"/></td>\n";
	print "		<td></td>\n";
	print "		<td><input class=\"checkbox\" type=\"checkbox\" onClick=\"SelectAll('arr_overboeken',this.checked);\"/></td>\n";
	print "		<td><input class=\"checkbox\" type=\"checkbox\" onClick=\"SelectAll('arr_boeken',this.checked);\"/></td>\n";
	print "		<td><input class=\"checkbox\" checked type=\"checkbox\" onClick=\"SelectAll('arr_vlucht_boeken',this.checked);\"/></td>\n";
	print "		<td><input class=\"checkbox\" checked type=\"checkbox\" onClick=\"SelectAll('arr_actief',this.checked);\"/></td>\n";
	
	print "	</tr>\n";
	
	print "	<tr>\n";
	
	$fields = array ('voeg toe', 'dag', 'week', 'datum', 'tijd', 'heen', 'terug', 'naam', 'beschrijving_kort', 'beschrijving', 'ticket_prijs', 'vlucht_prijs', 'extra marge', 'prijs', 'overboeken', 'boeken', 'vlucht_boeken', 'actief' );
	
	print "		<th></th>\n";
	foreach ( $fields as $field ) {
		if ($field == $sort && $ascdesc == 'desc')
			$fld_ascdesc = 'asc';
		else
			$fld_ascdesc = 'desc';
		
		print "		<th>$field</th>\n";
	}
	
	print "	</tr>";
	
	$teller = 0;
	$date = $start;
	
	for($i = 0; $date < $end_stamp; $i ++) {
		$teller ++;
		
		$date = mktime ( 0, 0, 0, $start_arr ['mon'], $start_arr ['mday'] + $i, $start_arr ['year'] );
		$datetag = date ( "dmY", $date );
		
		if (! $_POST ['disable_' . date ( "w", $date )]) {
			
			if (date ( "w", $date ) == 6 || date ( "w", $date ) == 0)
				$odd_even = "odd";
			else
				$odd_even = "even";
			
			if ($odd_even == 'odd')
				$rowColor = '#B1B1F9';
			else
				$rowColor = '#ffffff';
			
			print "	<tr class=\"" . $odd_even . "\" >\n";
			
			print "		<td>$teller</td>\n";
			print "		<td><input class=\"checkbox\" type=\"checkbox\" name=\"arr_enable_$datetag\" value=\"yes\"/></td>\n";
			print "		<td>" . date ( "D", $date ) . "</td>\n";
			print "		<td>" . date ( "W", $date ) . "</td>\n";
			print "		<td>" . date ( "d-M-Y", $date ) . "</td>\n";
			print "		<td><input size=4 name=\"arr_tijd_$datetag\" type=\"text\" value=\"" . $_POST ["arr_tijd_$datetag"] . "\"/></td>\n";
			print "		<td><input size=2 name=\"arr_heen_$datetag\" type=\"text\" value=\"" . $_POST ["arr_heen_$datetag"] . "\"/></td>\n";
			print "		<td><input size=2 name=\"arr_terug_$datetag\" type=\"text\" value=\"" . $_POST ["arr_terug_$datetag"] . "\"/></td>\n";
			print "		<td><input name=\"arr_naam_$datetag\" type=\"text\" value=\"" . $_POST ["arr_naam_$datetag"] . "\"/><span class=\"knop\" onclick=\"openMenuWrapper(event, 'names', 'arr_naam_$datetag')\">...</span></td>\n";
			print "		<td><input name=\"arr_beschr_kort_$datetag\" type=\"text\" value=\"" . $_POST ["arr_beschr_kort_$datetag"] . "\"/></td>\n";
			print "		<td><input name=\"arr_beschr_$datetag\" type=\"text\" value=\"" . $_POST ["arr_beschr_$datetag"] . "\"/></td>\n";
			print "		<td><input size=4 id=\"arr_ticket_prijs_$datetag\" name=\"arr_ticket_prijs_$datetag\" type=\"text\" value=\"" . $_POST ["arr_ticket_prijs_$datetag"] . "\" onChange=\"calculateFare( '_$datetag' );\" /></td>\n";
			print "		<td><input size=4 id=\"arr_vlucht_prijs_$datetag\" name=\"arr_vlucht_prijs_$datetag\" type=\"text\" value=\"" . $_POST ["arr_vlucht_prijs_$datetag"] . "\"  onChange=\"calculateFare( '_$datetag' );\" /></td>\n";
			//print "		<td><input disabled size=4 id=\"arr_hotel_prijs_$datetag\" name=\"arr_hotel_prijs_$datetag\" type=\"text\" value=\"".$_POST["arr_hotel_prijs_$datetag"]."\"  onChange=\"calculateFare( '_$datetag' );\" /></td>\n";
			print "		<td><input size=4 id=\"arr_extra_marge_$datetag\" name=\"arr_extra_marge_$datetag\" type=\"text\" value=\"" . $_POST ["arr_extra_marge_$datetag"] . "\"  onChange=\"calculateFare( '_$datetag' );\" /></td>\n";
			print "		<td><input disabled size=4 id=\"arr_prijs_$datetag\" name=\"arr_prijs_$datetag\" type=\"text\" value=\"" . $_POST ["arr_prijs_$datetag"] . "\"/></td>\n";
			
			print "		<td><input class=\"checkbox\" name=\"arr_overboeken_$datetag\" type=\"checkbox\" value=\"yes\"/></td>\n";
			print "		<td><input class=\"checkbox\" name=\"arr_boeken_$datetag\" type=\"checkbox\" value=\"yes\"/></td>\n";
			
			print "		<td><input class=\"checkbox\" checked name=\"arr_vlucht_boeken_$datetag\" type=\"checkbox\" value=\"yes\"/></td>\n";
			print "		<td><input class=\"checkbox\" checked name=\"arr_actief_$datetag\" type=\"checkbox\" value=\"yes\"/></td>\n";
			
			print "	</tr>\n";
		}
	}
	
	// Navigatie
	print "<tr><td colspan=\"10\">\n";
	print "</td><td colspan=\"5\"><input type=\"submit\" name=\"toevoegen\" value=\"toevoegen\" /></td>";
	
	print "</tr>\n";
	
	print "	</table>\n";
	
	echo '<div id="names" class="selector">...';
	
	echo '</div>';

} else
	print "Selecteer eerst een event!";

echo '</div>';

print "</form>\n";

$layout->contentFooter ();
$layout->footer ();

?>
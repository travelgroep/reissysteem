<?php
 
/* Specifieke pagina wissen */
// http://rbs.voetbaltravel.nl/cache/clearCache.php
//unlink( CACHE_PATH . "page_29_label=contact");

//$reis_type 	= $_GET['reis_type'];
$reis_type 		= $argv[1];

$verbose_mode = false;
if ($argv[2] == "--verbose-mode")
{
	$verbose_mode 	= true;
}


$emailTo = "zsolt@travelgroep.nl, ronald@travelgroep.nl";
$dir = "";
try {
	switch ($reis_type)
	{
		case "KNVB":
		case "FBT":
		case "DKT":
		case "VBT":
			$dir = '/data1/www/live_front-end/cache/'.$reis_type;
			break;
		default:
			break;
	}
	
	if (empty($dir))
	{
		throw (new Exception("Reis type is wrong: ".$reis_type));
	}
	else
	{
		$dirCounter = 0;
		$fileCounter = 0;
	
		$msg = "Delete directory recursively: ".$dir."\n";
		$msgItems = "";
		$it = new RecursiveDirectoryIterator(
				$dir);
		$files = new RecursiveIteratorIterator($it,
				RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file){
			if ($file->isDir()){
				$dirCounter++;			
				$msgItems .= "rm dir: ".$file->getRealPath()."\n";
				rmdir($file->getRealPath());
			} else {
				$fileCounter++;			
				$msgItems .= "unlink file: ".$file->getRealPath()."\n";
				unlink($file->getRealPath());
			}
		}
		$msg .= sprintf("Deleted dir(s): %s Deleted file(s): %s\n", $dirCounter, $fileCounter);
		$msg .= "----------------------------------------------------\n";
		if ($verbose_mode)
		{
			$msg .= $msgItems;
		}
		else {
			$msg .= "--verbose mode off\n";;
		}
	}
	mail($emailTo,  "ClearFrontend cache $reis_type ",  $msg);
}
catch (Exception $e)
{
	$msg = $e->getMessage();
	mail($emailTo,  "Excetpion: ClearFrontend cache $reis_type ",  $msg);
	
}
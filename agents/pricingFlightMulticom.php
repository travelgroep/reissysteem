<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/*
 *  php pricingFlight.php FBT of VBT
	php pricingFlight.php VBT live
 * */

$arrIgnore = $argv; // "35642","35637","35151"

$absPath = array (
			"dev" => "/usr/home/ronald/public_html/dev/", 
			"test" => "/data1/www/test/", 
			"live" => "/data1/www/prod/",
		);
$rootPath = $absPath[$argv[2]];

$environment = $argv[2];

switch ($environment) {
	case 'test' : 
		$multicomEnvironment = 'test';
		break;
	case 'live' :
		$multicomEnvironment = 'production';
		break;
	case 'dev' :
		$multicomEnvironment = 'test';
		break;
	default:
		exit;
}

DEFINE ('AUTOLOAD_DONT_LOAD', true);
DEFINE ('MULTICOM_CC_FEE', 0);

//require_once( $rootPath.'share/reissysteem/inc/config.php');
//require_once( $rootPath . 'share/reissysteem/class/Arrangement.php');
require_once( $rootPath.'share/reissysteem/agents/module/ModuleMulticomPricingAgents.php');
require_once( $rootPath.'share/reissysteem/agents/module/LastSuitableMulticomOutboundFlightFilter.php');
//require_once $rootPath.'share/vendor/autoload.php';
//require_once $rootPath.'http/www.voetbaltravel.nl/config/db_config.php';
require_once $rootPath.'http/www.voetbaltravel.nl/config/bootstrap.php'; 

$vendor_prefix		= $argv[1];	

$logger = new Logger('multicomBasePriceLogger');
// Now add some handlers
$logger->pushHandler(new StreamHandler($rootPath.'logs/multicom_base_price_' . $vendor_prefix . '_' . date('Y-m-d') . '.log', Logger::DEBUG));


if(empty($GLOBALS[ 'rsdb' ])){ $GLOBALS[ 'rsdb' ] = new DB( RS_DBNAME, RS_DBUSER, RS_DBPASS, 'rsdb'); }

//  define( VENDOR_PREFIX_PRICE, "FBT" ); 	// pass this on to Pronto logger

//  $db 					= new DB();
//  $searchEventType = 'FBT';
//  $pricingAgents	= new ModulePricingAgents( $searchEventType   ); // "Voetbalreis"
//  //$array = $pricingAgents->getAllArrangementenInPresentByReisType( );
//  $array = $pricingAgents->getArrangementById(33671);
//  $pricingAgents->updatePrice($array);

// exit;

 session_start();
 
 //mail( "ronald@travelgroep.nl", "Module Pricing Agent ReisBeheer []",  "body", "header" );

$_SESSION['agent'] 	= TRUE;  // Mother Fucker - Agent mag standaard variable niet overschrijven:P

 if ( $vendor_prefix == "VBT") {
	 $searchEventType 	= "Voetbalreis";
 	 define( VENDOR_PREFIX_PRICE, "VBT" ); 	// pass this on to Pronto logger
	 unset($_SESSION ['taalcode']);
	 $_SESSION ['taalcode'] = 'nl'; 		// fase 2 en
 } else  if ( $vendor_prefix == "FBT") {
 	$searchEventType 	= "Voetbalreis"; //Football Trip
 	define( VENDOR_PREFIX_PRICE, "FBT" ); 	// nog niks mee gedaan
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'en' );
	 define( 'LOCALE_FORMAT', 'en' );	
	 $_SESSION ['taalcode'] = "en"; 		// fase 2 en
 
 } else  if ( $vendor_prefix == "DKT") {
 	$searchEventType 	= "Voetbalreis"; //Fodboldrejse
 	define( VENDOR_PREFIX_PRICE, "DKT" ); 	// nog niks mee gedaan
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'dk' );
	 define( 'LOCALE_FORMAT', 'dk' );	
	 $_SESSION ['taalcode'] = "dk"; 
	 		 
 } else  if ( $vendor_prefix == "NOT") {
    $searchEventType    = "Voetbalreis"; //Fodboldrejse
    define( VENDOR_PREFIX_PRICE, "NOT" );   // nog niks mee gedaan
     unset($_SESSION ['taalcode']);
     define( 'LOCALE_TAALCODE', 'no' );
     define( 'LOCALE_FORMAT', 'no' );   
     $_SESSION ['taalcode'] = "no";          
     	 
 } else  if ( $vendor_prefix == "SET") {
 	$searchEventType 	= "Voetbalreis"; //Fodboldrejse
 	define( VENDOR_PREFIX_PRICE, "SET" ); 	// nog niks mee gedaan
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'se' );
	 define( 'LOCALE_FORMAT', 'se' );	
	 $_SESSION ['taalcode'] = "se"; 		 
	 
 }  else  if ( $vendor_prefix == "DET") {
 	$searchEventType 	= "Voetbalreis"; //Fodboldrejse
 	define( VENDOR_PREFIX_PRICE, "DET" ); 	// nog niks mee gedaan
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'de' );
	 define( 'LOCALE_FORMAT', 'de' );	
	 $_SESSION ['taalcode'] = "de"; 		 
	 
 } else  if ( $vendor_prefix == "OPT") {
 	$searchEventType 	= "Operareis"; 
 	define( VENDOR_PREFIX_PRICE, "OPT" ); 	// nog niks mee gedaan
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'nl' );
	 define( 'LOCALE_FORMAT', 'nl' );	
	 $_SESSION ['taalcode'] = "nl"; 		// fase 2 en
 	
 } else {
 	$searchEventType = null;
 	echo " STOP ModulePricingAgents => " . $vendor_prefix;
 }
 
 
 
 $agentStartTime 		= date( "l, d F Y  H:i" );
 $db 					= new DB();
 if ( !isset( $argv[1] ) ) { echo "Gebruik argv: $argv[0] [reistype] [(String)]\n"; die;  
 } else {
            $pricingAgents = new ModuleMulticomPricingAgents( $searchEventType   );
			$pricingAgents->multicomEnvironment = $multicomEnvironment;
	       $pricingAgents->setLogger($logger);
        $array = $pricingAgents->getAllArrangementenInPresentByReisType( $arrIgnore ); // 28738
        
	$pricingAgents->updatePrice($array);
 }
$agentEndTime = date( "l, d F Y  H:i" );

		if( AGENTS_TEST == "live" ) { 		$server = "xTravel @ CJ2"; }
		elseif( AGENTS_TEST == "test" ) { 	$server = "wim @ CJ2"; }
		else { $server = "unknown"; }
		
		$searchCount 	= $pricingAgents->searchCount;
		$skipped		= $pricingAgents->skipped;
		$msg			= $pricingAgents->msg;
		$totals 		= "Agent started on ".$agentStartTime." and ended on ".$agentEndTime." system name: ".$server."\n";
		$totals 		.= "Number of 'arrangementen' processed: [".count($array)."], event type is [".$searchEventType."]\n";
		$totals 		.= "Number of searchFlights processed: [".$searchCount."], skipped [".$skipped."]\n";

		$mailbody = $totals . "\n\n" . $msg;

		$headers = "from:Module Pricing Agent <ronald@travelgroep.nl>\n";
		
		mail( "ronald@travelgroep.nl", "Module Pricing Agent ReisBeheer [".$searchEventType."]", $mailbody, $headers );

<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$absPathOpera = array (
			"test" => "/data1/www/_others/",  
			"live" => "/data1/www/others/",
		);
		
$absPathAgentsModule = array (
            "test" => "/data1/www/test/",  
            "live" => "/data1/www/prod/",
        );		
		
$rootPathOpera             = $absPathOpera[$argv[2]];
$rootPathAgentModule       = $absPathAgentsModule[$argv[2]];

		
$vendor_prefix  = $argv[1]; 		
$environment    = $argv[2];

switch ($environment) {
	case 'test' : 
		$multicomEnvironment = 'test';
		break;
	case 'live' :
		$multicomEnvironment = 'production';
		break;
	default:
		exit;
}
DEFINE ('AUTOLOAD_DONT_LOAD', true);
DEFINE ('MULTICOM_CC_FEE', 0);
 


 if ( $vendor_prefix == "ONL") {
     define( 'SITES_URL', "www.operatravel.nl" );
     define( 'VENDOR_PREFIX_PRICE', "ONL" );
	 $searchEventType 	= "Operareis";
	 unset($_SESSION ['taalcode']);
 } else  if ( $vendor_prefix == "OEN") {
 	 define( 'SITES_URL', "www.operabreak.co.uk" );
     define( 'VENDOR_PREFIX_PRICE', "OEN" );
 	$searchEventType 	= "Operareis"; // Opera Trip 
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'en' );
	 define( 'LOCALE_FORMAT', 'en' );	
 
 } else  if ( $vendor_prefix == "ODK") {
     define( 'VENDOR_PREFIX_PRICE', "ODK" );
 	$searchEventType 	= "Operareis";  
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'dk' );
	 define( 'LOCALE_FORMAT', 'dk' );	
	 
 } else  if ( $vendor_prefix == "OSE") {
     define( 'VENDOR_PREFIX_PRICE', "OSE" );
 	$searchEventType 	= "Operareis";  
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'se' );
	 define( 'LOCALE_FORMAT', 'se' );	
	 
 }  else  if ( $vendor_prefix == "ODE") {
     define( 'VENDOR_PREFIX_PRICE', "ODE" );
     define( 'SITES_URL', "www.operntrip.de" );
 	$searchEventType 	= "Operareis";  
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'de' );
	 		 
 	
 } else {
 	$searchEventType = null;
 	echo " STOP ModulePricingAgents => " . $vendor_prefix;
 }

require_once $rootPathAgentModule . 'share/reissysteem/agents/module/ModuleMulticomPricingAgents.php';
require_once $rootPathAgentModule .  'share/reissysteem/agents/module/LastSuitableMulticomOutboundFlightFilter.php';
require_once $rootPathOpera . 'http/'.SITES_URL.'/config/bootstrap.php'; 
$logger = new Logger('multicomBasePriceLogger');
$logger->pushHandler(new StreamHandler( $rootPathOpera . 'logs/multicom_base_price_' . $vendor_prefix . '_' . date('Y-m-d') . '.log', Logger::DEBUG));
if(empty($GLOBALS[ 'rsdb' ])){ $GLOBALS[ 'rsdb' ] = new DB( RS_DBNAME, RS_DBUSER, RS_DBPASS, 'rsdb'); }

$_SESSION ['taalcode'] = LOCALE_FORMAT; 
$_SESSION['agent']  = TRUE;  // Mother Fucker - Agent mag standaard variable niet overschrijven:P
 
 $agentStartTime 		= date( "l, d F Y  H:i" );
 $db 					= new DB();
 if ( !isset( $argv[1] ) ) { echo "Gebruik argv: $argv[0] [reistype] [(String)]\n"; die;  
 } else {
	
echo  "\n------------------------------Debug- \n";
echo "=> ". $vendor_prefix . " -\n";
echo "=> ". $environment . " -\n";
echo "=> ".$rootPathOpera . " -\n";
echo "=> ".$rootPathOpera . 'http/'.SITES_URL.'/config/bootstrap.php'. " -\n";

echo "=> ".$rootPathAgentModule . " -\n";
echo "=> ".$rootPathAgentModule . " - share/reissysteem/agents/module/ModuleMulticomPricingAgents.php". " -\n";
echo "=> ".$rootPathAgentModule . " - share/reissysteem/agents/module/LastSuitableMulticomOutboundFlightFilter.php". " -\n";
 
     print "\n ------ Begin -\n\n";
	 	
    $pricingAgents = new ModuleMulticomPricingAgents( $searchEventType   );
    $pricingAgents->multicomEnvironment = $multicomEnvironment;
    $pricingAgents->setLogger($logger);
    $array = $pricingAgents->getAllArrangementenInPresentByReisType( ); // 28738
     
    $pricingAgents->updatePrice($array);
 }
 
 
 
$agentEndTime = date( "l, d F Y  H:i" );

$searchCount 	= $pricingAgents->searchCount;
$skipped		= $pricingAgents->skipped;
$msg			= $pricingAgents->msg;
$totals 		= "Agent started on ".$agentStartTime." and ended on ".$agentEndTime." system name: ".$multicomEnvironment."\n";
$totals 		.= "Number of 'arrangementen' processed: [".count($array)."], event type is [".$searchEventType."]\n";
$totals 		.= "Number of searchFlights processed: [".$searchCount."], skipped [".$skipped."]\n";

$mailbody = $totals . "\n\n" . $msg;

$headers = "from:Module Pricing Agent <ronald@travelgroep.nl>\n";
mail( "ronald@travelgroep.nl", "Module Pricing Agent ReisBeheer [".$searchEventType."]", $mailbody, $headers );
 
 

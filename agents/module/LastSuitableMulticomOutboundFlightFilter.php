<?php

class LastSuitableMulticomOutboundFlightFilter extends \FilterIterator
{
    protected $lastSuitableArriveDateTime;

    public function __construct(\Iterator $iterator , $lastSuitableArriveDateTime )
    {
        parent::__construct($iterator);
        $this->lastSuitableArriveDateTime = $lastSuitableArriveDateTime;
    }        

    public function accept()
    {
        $flight = $this->getInnerIterator()->current();

        $lastSuitableArriveDateTime = $this->lastSuitableArriveDateTime;
        $flightArriveDateTime = $flight->getArrDateTime();

        if( $flightArriveDateTime < $lastSuitableArriveDateTime) {
            return true;
        }        
        return false;
    }
}

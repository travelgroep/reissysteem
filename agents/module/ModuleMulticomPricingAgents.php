<?

class ModuleMulticomPricingAgents {
  var $reis_type;
  
  var $msg;
  var $searchCount;
  var $skipped;
  
  var $clubToIATA;
  var $multicomEnvironment;
  
  var $logger;
  
	function __construct( $reis_type ) {
		$this->setReisType( $reis_type );
		$searchCount = 0;
		$skipped = 0;
		$msg = "__ ModuleMulticomPricingAgents construct \n\n";
		$this->clubToIATA = $this->initClubToIATA();
	}
	
	function updatePrice( $arrangementen ){
		
		$defaultBaseprice = money_format ( "%.2n" , 95 );
		
		$numPassenger = 2;
		
        $loginOptions = array(
			'requestMapper.options.fab.login.target' => $this->multicomEnvironment,
            'requestMapper.options.fab.login.syndicator_info.syndicator.id' => 'VoetbalTra',
            'requestMapper.options.fab.login.syndicator_info.syndicator.passwd' => 'ME7resTa'
        );
		
        $currencyConverter = $this->getCurrencyConverter();
        $numOfSuccess = 0;
        $numOfNoInbound = 0;
        $numOfNoOutbound = 0;
        $numOfNoFlights = 0;
        
		foreach( $arrangementen as $arrangement ){
			$arrangementInfo = array();
	  		$this->log( '--- updatePrice:  ' . $arrangement->getId());
			$event = $arrangement->getEvent();
			$eventId = $event->getId();
			
			if( $event ) {    
				$locatie = $event->getLocatie();
				$eventType = $event->getEventType()->getNaam();
			}
			if( $locatie ) { $stad = $locatie->getStad(); }
			
			if( $stad ) {
				$landId = $stad->land_id;
			}
			
			/* *************
             * destination IATA 
             * */
			
			if ($this->hasMoreDestIATA($eventId)) {
				$destinationIATA = $this->clubToIATA[$eventId];
			} else {		 
				$destinationIATA 	= array($stad->getIATA());
			}
			
			if ( is_array($destinationIATA) ){
				/* Change the destinationIATA by Ronald. I want more airport by the destination.  */
				if (in_array("VRN", $destinationIATA)) {
					 $destinationIATA = array('VRN','BGY');
				}
			}

			/* *************
			 * departure IATA 
			 * */
			if ( VENDOR_PREFIX_PRICE == "VBT" ||  VENDOR_PREFIX_PRICE == "ONL" ) {
				$departureIATA 		= "AMS";
			 	
			} elseif ( VENDOR_PREFIX_PRICE == "FBT") {
				$departureIATA 		= "LON";
			
			} elseif ( VENDOR_PREFIX_PRICE == "DKT" ) {
				$departureIATA 		= array('CPH', 'AAL', 'BLL');
			} elseif ( VENDOR_PREFIX_PRICE == "NOT" ) {
                $departureIATA      = array('OSL'); 	
			} elseif ( VENDOR_PREFIX_PRICE == "SET" ) {
				$departureIATA 		= array('NYO','ARN','VST');  		
			} elseif ( VENDOR_PREFIX_PRICE == "DET" ) {
				$departureIATA 		= array('BER', 'CGN', 'DUS');
			} elseif ( VENDOR_PREFIX_PRICE == "OEN") {
                $departureIATA      = array('STN', 'LGW', 'LHR'); 
                
			} elseif ( VENDOR_PREFIX_PRICE == "ODE") {
                $departureIATA      = array('HHN', 'NRN', 'SXF'); 
			} else {
				$departureIATA 		= "";	
			}
			
			
			$departureIATA = is_array($departureIATA)?$departureIATA: array($departureIATA);
			
			$exclude_suppliers = array('JTA');
			
			$this->log( '--- updatePrice START:  ' . $eventType . " , " 
					. $destinationIATA . " , " 
					. $arrangement->getVluchtBoeken() . " , " 
					. $event->getGeenVluchten()   );
			
			if( $destinationIATA 
				&& $arrangement->getVluchtBoeken() == 't' 
				&& $event->getGeenVluchten() == 'f' )  
			{
    			
				$startDate 		= $arrangement->getHeen();
    			$returnDate 	= $arrangement->getTerug();
    			
    			/*
    			 *  HEEN 
    			 * */
    			$type 			= "heen";
    				
    			$inboundParams = array
    			(
    					'departure'             => $departureIATA,
    					'destination'           => $destinationIATA,
    					'departure_date'        => $startDate,
    					'numAdt'                => $numPassenger,
    					'numChd'                => 0
    			
    			);
    			
    			$outboundParams = array
    			(
    					'departure'             => $destinationIATA,
    					'destination'           => $departureIATA,
    					'departure_date'        => $returnDate,
    					'numAdt'                => $numPassenger,
    					'numChd'                => 0
    						
    			);
    			 
    			try
    			{
    				$matchStartDateTimeString = $arrangement->getDatum();
                                
                                $inboundFlightParams = array(
									'requestMapper.options.fab.flight_rq.holiday_search_rq.response_timeout_secs' => '60',
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.num_of_adults' => $numPassenger,
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.num_of_children' => '0',
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.departure_date_range.start_date' => str_replace('-', '', $startDate),
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.departure_date_range.end_date' => str_replace('-', '', $startDate),
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.departure_airports' => $departureIATA,
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.destination_airports' => $destinationIATA,
									'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.exclude_suppliers' => $exclude_suppliers,
                                );

                                $inboundFlightParams = array_merge($inboundFlightParams, $loginOptions);

                                $inboundFlightOptions = new \Travelgroep\Multicom\FAB\WebService\FABFlightOptionable($inboundFlightParams);
                                $inboundFlightRequest = new \Travelgroep\Multicom\FAB\WebService\XMLRequest($inboundFlightOptions);
                                $inboundFlightResponse = $inboundFlightRequest->getResponse();
                                $inboundFlights = $inboundFlightResponse->getFlights();
                                
                                $arrangementInfo['OutboundFlightsBeforeFiltering'] = count($inboundFlights);
                                
                                $inboundFlights = new ArrayObject($inboundFlights);
                                
                                $calculator = new \Travelgroep\ElsyArres\LastSuitableArriveDateCalculator();
                                $knownStartTime = $calculator->calculateKnown($matchStartDateTimeString);
                                $lastSuitableArriveDateTime = $calculator->calculateDate($matchStartDateTimeString, $landId, $knownStartTime);
                                $lastSuitableArriveDateTimeString = $lastSuitableArriveDateTime->format('c');
                                $lastSuitableArriveDateTime = new DateTime($lastSuitableArriveDateTimeString);
                                $filteredFlightsIterator = new LastSuitableMulticomOutboundFlightFilter($inboundFlights->getIterator(), $lastSuitableArriveDateTime);
                                $inboundFlights = iterator_to_array($filteredFlightsIterator);
                                
    			}
    			catch (\Exception $e)
    			{
    				$inboundFlights = array();
    				print_r("\n-----------------" . $e->getTrace() . "-----------------\n");
    			}
    				 
    			try
    			{
                                $outboundFlightParams = array(
									'requestMapper.options.fab.flight_rq.holiday_search_rq.response_timeout_secs' => '60',
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.num_of_adults' => $numPassenger,
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.num_of_children' => '0',
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.departure_date_range.start_date' => str_replace('-', '', $returnDate),
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.departure_date_range.end_date' => str_replace('-', '', $returnDate),
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.departure_airports' => $destinationIATA,
                                    'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.destination_airports' => $departureIATA,
									'requestMapper.options.fab.flight_rq.holiday_search_rq.search_criteria.exclude_suppliers' => $exclude_suppliers,
                                );

                                $outboundFlightParams = array_merge($outboundFlightParams, $loginOptions);

                                $outboundFlightOptions = new \Travelgroep\Multicom\FAB\WebService\FABFlightOptionable($outboundFlightParams);
                                $outboundFlightRequest = new \Travelgroep\Multicom\FAB\WebService\XMLRequest($outboundFlightOptions);
                                $outboundFlightResponse = $outboundFlightRequest->getResponse();
                                $outboundFlights = $outboundFlightResponse->getFlights();
    			}
    			catch (\Exception $e)
    			{
    				$outboundFlights = array();
    			}
		
				
				$oldCostsPp		= 0;
				$feeMargin 		= MULTICOM_CC_FEE;//15; // De goedkoopste met een bedrag!
				
				$inboundNoBasePrice = false;
				$inboundNoFlights = false;
				$outboundNoBasePrice = false;
				$outboundNoFlights = false;
				$arrangementFlightSuccess = 'Updating arrangement was successful.';
				$arrangementInfo['ArrangementId'] = $arrangement->getId();
				$arrangementInfo['ArrangementName'] = $arrangement->getNaam();
				$arrangementInfo['DepartureAirport'] = $departureIATA;
				$arrangementInfo['DestinationAirport'] = $destinationIATA;
				$arrangementInfo['OutboundFlightDate'] = $startDate;
				$arrangementInfo['InboundFlightDate'] = $returnDate;
				$arrangementInfo['ArrangementDate'] = $arrangement->getDatum();
				$arrangementInfo['Currency'] = 'EUR';
				$arrangementInfo['OutboundFlightOldPrice'] = $arrangement->getVluchtHeenTarief();
				$arrangementInfo['InboundFlightOldPrice'] = $arrangement->getVluchtTerugTarief();
				$arrangementInfo['FlightOldPrice'] = $arrangement->getVluchtPrijs();
				
				$arrangementInfo['OutboundFlights'] = count($inboundFlights);
				
				if (count($inboundFlights) > 0 || $skip ) {
					foreach ($inboundFlights as $flight)
					{
						//$costPerPerson = $costCalculator->getCostPerPerson($flight);
                        $costPerPerson = $flight->getCostPP();
						$arrOrigCostPP[] = $costPerPerson;
						$costPerPerson = $currencyConverter->convert($costPerPerson,$flight->getCurrency(),'EUR');
						$arrCostsPp[] = money_format ( "%.2n" , $costPerPerson + $feeMargin );
					}
					
					$this->log( '--- updatePrice ('.$type.') VluchtHeenTarief(: =>  ' .$arrCostsPp[0] . ' - old, ' . $arrangement->getVluchtHeenTarief()  );
					$arrangementInfo['OutboundFlightNewOriginalPrice'] = $arrOrigCostPP[0];
                    $arrangementInfo['OutboundFlightNewOriginalCurrency'] = reset($inboundFlights)->getCurrency();
					$arrangementInfo['OutboundFlightNewPrice'] = $arrCostsPp[0];
					$arrangementInfo['OutboundFlightFlightNo'] = reset($inboundFlights)->getFlightNo();
					$arrangement->setVluchtHeenTarief($arrCostsPp[0]);
					unset($arrCostsPp);
					unset($arrOrigCostPP);
					$this->searchCount	+= 1;
				} else {
					$flightBasePrice = $arrangement->getVluchtHeenTarief();
					if (empty($flightBasePrice))
					{
						$arrangement->setVluchtHeenTarief($defaultBaseprice);
						
						$this->log( '--- updatePrice ('. $type .') vluchten INIT BASEBPRISE: '.$defaultBaseprice.' =>  ' .  $arrangement->getId());
						$this->skipped	+= 1;
						$inboundNoBasePrice = true;
					}
					else
					{
						$arrangement->setVluchtHeenTarief($flightBasePrice);
						$this->log( '--- updatePrice ('. $type .') vluchten SKIPPED(: ('.$flightBasePrice.') =>  ' .  $arrangement->getId()   );
						$this->skipped	+= 1;
						$inboundNoFlights = true;
					}
					
					$arrangementInfo['OutboundFlightNewPrice'] = $arrangement->getVluchtHeenTarief();
				}
    			 
				
    			/*
    			 *  TERUG 
    			 * */
    			$type 			= "terug";
				$oldCostsPp		= 0;
				$feeMargin 		= MULTICOM_CC_FEE; //15; // De goedkoopste met een bedrag!
				
				$arrangementInfo['InboundFlights'] = count($outboundFlights);
				
				if (count($outboundFlights) > 0 ) {
					foreach ($outboundFlights as $flight)
					{
//						$feeCalculator = new \Travelgroep\ElsyArres\SearchFlightsFeeCalculator();
//						$costCalculator = new \Travelgroep\ElsyArres\SearchFlightsCostCalculator($numPassenger, $feeCalculator);
//						$costPerPerson = $costCalculator->getCostPerPerson($flight);
					
                        $costPerPerson = $flight->getCostPP();
						$arrOrigCostPPTerug[] = $costPerPerson;
                        $costPerPerson = $currencyConverter->convert($costPerPerson,$flight->getCurrency(),'EUR');                    
						$arrCostsPpTerug[] = money_format ( "%.2n" , $costPerPerson + $feeMargin );
					}
						
					
					$this->log( '--- updatePrice ('.$type.') VluchtTerugTarief(: =>  ' .$arrCostsPpTerug[0] . ' - old, ' . $arrangement->getVluchtTerugTarief()  );
					$arrangementInfo['InboundFlightNewOriginalPrice'] = $arrOrigCostPPTerug[0];
                    $arrangementInfo['InboundFlightNewOriginalCurrency'] = reset($outboundFlights)->getCurrency();
					$arrangementInfo['InboundFlightNewPrice'] = $arrCostsPpTerug[0];
					$arrangementInfo['InboundFlightFlightNo'] = reset($outboundFlights)->getFlightNo();
					$arrangement->setVluchtTerugTarief( $arrCostsPpTerug[0] );
					unset($arrCostsPpTerug);
					unset($arrOrigCostPPTerug);
					$this->searchCount	+= 1;
				} else {
					$flightBasePrice = $arrangement->getVluchtTerugTarief();
					if (empty($flightBasePrice))
					{
						$arrangement->setVluchtTerugTarief($defaultBaseprice);
						$this->log( '--- updatePrice ('. $type .') vluchten INIT BASEBPRISE: '.$defaultBaseprice.' =>  ' .  $arrangement->getId());
						$outboundNoBasePrice = true;
					}
					else
					{
						$arrangement->setVluchtTerugTarief($flightBasePrice);
						$this->log( '--- updatePrice ('. $type .') vluchten SKIPPED(: ('.$flightBasePrice.') =>  ' .  $arrangement->getId()   );
						$outboundNoFlights = true;
					}	
						$this->skipped	+= 1;
						$arrangementInfo['InboundFlightNewPrice'] = $arrangement->getVluchtTerugTarief();
					
				}
                                
                $arrangement->store();
				$vlucht_prijs = $arrangement->getVluchtTerugTarief() + $arrangement->getVluchtHeenTarief();
				$arrangementInfo['FlightNewPrice'] = $vlucht_prijs; 
				$this->log( '--- updatePrice vlucht_prijs(: =>  ' .$vlucht_prijs  );
				
				if (($inboundNoBasePrice || $inboundNoFlights) && ($outboundNoBasePrice || $outboundNoFlights)) {
					$arrangementFlightSuccess = 'There were no flights to arrangement.';
					$numOfNoFlights++;
				} else {
					if ($inboundNoBasePrice || $inboundNoFlights) {
						$arrangementFlightSuccess = 'There were no outbound flights to arrangement.';
						$numOfNoOutbound++;
					} else if ($outboundNoBasePrice || $outboundNoFlights) {
						$arrangementFlightSuccess = 'There were no inbound flights to arrangement.';
						$numOfNoInbound++;
					} else {
						$numOfSuccess++;
					}
				}
				
				$this->logger->addInfo($arrangementFlightSuccess, $arrangementInfo);
			} else {
				$this->skipped	+= 1;
				$this->log( '--- updatePrice START:  ' . $eventType . " = " . $this->reis_type . " ,  "  
					. $destinationIATA . " , " 
					. $arrangement->getVluchtBoeken() . " = t , " 
					. $event->getGeenVluchten() . " = f "   );
					
			}
		}
		
		$this->logger->addInfo('These queries were run on the Multicom ' . $this->multicomEnvironment . ' environment.');
		$this->logger->addInfo('Arrangements with both in and outbound flights: ' . $numOfSuccess);
		$this->logger->addInfo('Arrangements without outbound flight: ' . $numOfNoOutbound);
		$this->logger->addInfo('Arrangements without inbound flight: ' . $numOfNoInbound);
		$this->logger->addInfo('Arrangements without flights: ' . $numOfNoFlights);
	}
	 
	
	function getAllArrangementenInPresentByReisType($arrIgnore ){
		$path = realpath(__DIR__.'/../');
		
		$this->log( '--- getAllArrangementenInPresentByReisType: ' . $path);
		
		if ( VENDOR_PREFIX_PRICE == "VBT" || 
		      VENDOR_PREFIX_PRICE == "ONL" || 
		      VENDOR_PREFIX_PRICE == "ODE" || 
		      VENDOR_PREFIX_PRICE == "DKT" || 
		      VENDOR_PREFIX_PRICE == "NOT" || 
		      VENDOR_PREFIX_PRICE == "SET" || 
		      VENDOR_PREFIX_PRICE == "DET") {
			$q 	= "SELECT * FROM arrangement WHERE heen > NOW() AND actief = True AND vlucht_boeken = True AND event_id " . 
					"IN (SELECT id FROM event " . 
					"WHERE event_type_id = (SELECT id FROM event_type WHERE naam = '{$this->reis_type}')) order by id desc " ;
			
		} else if ( VENDOR_PREFIX_PRICE == "FBT" ||  VENDOR_PREFIX_PRICE == "OEN"  ) {
			// Alleen Spanje en Italy 
			
			/* arrangementen op halen met de engelse bijschift*/
			$q 	= "SELECT * FROM arrangement WHERE heen > NOW() AND actief = True AND vlucht_boeken = True AND event_id IN (" . 
					"SELECT id FROM event " . 
					"WHERE event_type_id = (" .
						"SELECT id FROM event_type WHERE naam = '{$this->reis_type}')" . 
							"AND locatie_id in (" .
								"SELECT id FROM locatie WHERE stad_id IN (" .
									"SELECT id FROM stad WHERE land_id = 2 OR land_id = 15 OR land_id = 51 OR land_id = 3755 ))) order by id desc";
		}  else {
		      $this->log( '=== NO QUERY  BEKIJK ModuleMulitcomPricingAgents =>'   );
		}
	  	
		$this->log( '=== query ('.$_SESSION['taalcode'].') =>' . $q );
	    
	    $res 	= $GLOBALS['rsdb']->query( $q );
		
	    for( $i=0; $i<pg_num_rows( $res ); $i++ ) 	{
			$row 				= pg_fetch_array( $res, $i, PGSQL_ASSOC );
			
			$id = $row["id"];
			if (in_array($id, $arrIgnore, true)) {
               $this->log( '=== NO UPDATE  BEKIJK ModuleMulitcomPricingAgents =>' . $id   );
            } else {
            	$array[]       = new Arrangement(null, $row);
            }
		}
		$this->log( '=== count =>' . count($array) );
	  return $array;
	}

	
function getArrangementById( $arrId ){
		
		$this->log( '--- getArrangementById: ');
		
		$q 	= "SELECT * FROM arrangement WHERE heen > NOW() AND actief = True AND vlucht_boeken = True AND  id = {$arrId} ";
	  	
		$this->log( '=== query ('.$_SESSION['taalcode'].') =>' . $q );
	  
	    $res 	= $GLOBALS['rsdb']->query( $q );
		for( $i=0; $i<pg_num_rows( $res ); $i++ ) 	{
			$row 				= pg_fetch_array( $res, $i, PGSQL_ASSOC );
	    	$array[] 		= new Arrangement(null, $row);
		}
		$this->log( '=== count =>' . count($array) );
	  return $array;
	}
	
 	function setReisType ( $val ) { $this->reis_type = $val;   }
 	
 	function log ( $val ) {
 		$msgVal = date('Y-m-d G:i:s') . ' '. $val . "\n";
 		$this->msg .= $msgVal;
 		echo $msgVal; 
 	}
  
 	function setLogger($logger)
 	{
 		$this->logger = $logger;
 	}
 	
	public function getCurrencyConverter()
    {
    	$this->currencyList = array(
            'EUR',
            'GBP',
            'SEK', 
            'NOK', 
            'CHF', 
            'DKK', 
            'CZK'       
        );
    	
    	$currencyConverter = new \Travelgroep\Multicom\FAB\PriceCalculator\CurrencyConverter();
	     
	    $fromCurrency = 'EUR';
        $currencyList = $this->currencyList;
     
        foreach($currencyList as $toCurrency)
        {
            $exchangeRate = getMultiByCode($toCurrency);
            $currencyConverter->addExchangeRate($fromCurrency, $toCurrency, $exchangeRate);
            $currencyConverter->addExchangeRate($toCurrency, $fromCurrency, 1/$exchangeRate);    
            
        }
             
        return $currencyConverter;        
    }
	
	public function hasMoreDestIATA($eventId)
	{	
		if (array_key_exists($eventId, $this->clubToIATA)) {
			return TRUE;
		}
		
		return FALSE;
	}
	
	protected function initClubToIATA()
	{
		$barcelona_espanyol = array(
            'BCN',
            'GRO',
            'REU'
        );
        
        $real_atletico_madrid = array(
            'MAD'
        );
        
        $ac_inter_milan = array(
            'LIN',
            'MXP',
            'BGY'
        );

        $as_lazio_roma = array(
            'CIA',
            'ROM',
            'FCO'
        );
        
        $arsenal_chelsea_tottenham_hotspur_qpr = array(
            'LON'
        );
        
        $manchester_united_manchester_city_liverpool_everton = array(
            'MAN',
            'NCL',
            'LPL'
        );
        
        // eventidtoIATA
        $clubToIATA = array(
        // SSC Napoli
//            '34119' => array(),
        
        // Paris Saint-Germain
//            '34029' => array(),
        
        // Swansea City    
//            '34027' => array(),
        
        // Newcastle United
//            '33780' => array(),
        
        // M�laga CF
//            '29210' => array(),
        
        // Queens Park Rangers FC
            '28759' => $arsenal_chelsea_tottenham_hotspur_qpr,
        
        // VfB Stuttgart
//            '21679' => array(),
        
        // Bayer Leverkusen
//            '21657' => array(),
        
        // Werder Bremen
//            '21627' => array(),
        
        // FC Schalke 04
//            '21601' => array(),
        
        // Juventus FC
//            '17923' => array(),
        
        // Benfica
//            '15191' => array(),
        
        // Sevilla FC
//            '14524' => array(),
        
        // Valencia CF
//            '14500' => array(),
        
        // Stoke City
//            '14140' => array(),
        
        // FA Cup
//            '10963' => array(),
        
        // Manchester City
            '10715' => $manchester_united_manchester_city_liverpool_everton,
        
        // Atl�tico Madrid
            '8787' => $real_atletico_madrid,
        
        // RCD Espanyol
            '8738' => $barcelona_espanyol,
        
        // Glasgow Rangers
//            '7835' => array(),
        
        // Tottenham Hotspur
            '3086' => $arsenal_chelsea_tottenham_hotspur_qpr,
        
        // HSV
//            '1974' => array(),
        
        // ACF Fiorentina
//            '1696' => array(),
        
        // Lazio Roma
            '1293' => $as_lazio_roma,
        
        // Borussia Dortmund
//            '1278' => array(),
        
        // FC Bayern M�nchen        
//            '1154' => array(),
        
        // Everton FC
            '351' => $manchester_united_manchester_city_liverpool_everton,
        
        // Celtic
//            '337' => array(),
        
        // Inter Milaan
            '314' => $ac_inter_milan,
        
        // Manchester United        
            '298' => $manchester_united_manchester_city_liverpool_everton,
        
        // Liverpool FC
            '283' => $manchester_united_manchester_city_liverpool_everton,
        
        // Arsenal
            '256' => $arsenal_chelsea_tottenham_hotspur_qpr,
        
        // Aston Villa
//            '50' => array(),
        
        // Real Madrid        
            '44' => $real_atletico_madrid,
        
        // AS Roma
            '42' => $as_lazio_roma,
        
        // AC Milan
            '37' => $ac_inter_milan,
        
        // West Ham United
//            '35' => array(),
        
        // Fulham
//            '29' => array(),
        
        // Chelsea FC
            '20' => $arsenal_chelsea_tottenham_hotspur_qpr,
        // FC Barcelona     
           
            '5' => $barcelona_espanyol
        
        );
		
		return $clubToIATA;
	}
} 

<?
/*
 * Search bij namen!
 * 
 * */
class ModulePricingAgents {
  var $reis_type;
  
  var $msg;
  var $searchCount;
  var $skipped;
  
	function __construct( $reis_type ) {
		$this->setReisType( $reis_type );
		$searchCount = 0;
		$skipped = 0;
		$msg = "__ ModulePricingAgents construct \n\n";
	}
	
	function updatePrice( $arrangementen ){
		$defaultBaseprice = money_format ( "%.2n" , 95 );
//		$vbm 				= new VBM2();
		
		//$i = 3;
		
		
		$elsyArresFactory = new \Travelgroep\ElsyArres\Factory\ElsyArresFactory;
		$params = array(
				'elsy_arres_factory' => $elsyArresFactory
		);
		$bookFlightWizard = new Travelgroep\ElsyArres\Wizard\BookFlight($params);
		$numPassenger = 2;
		$feeCalculator = new \Travelgroep\ElsyArres\SearchFlightsFeeCalculator();
		$costCalculator = new \Travelgroep\ElsyArres\SearchFlightsCostCalculator($numPassenger, $feeCalculator);
		
		
		foreach( $arrangementen as $arrangement ){
			
//if ($i > 0 ) {	
	 	
	  		$this->log( '--- updatePrice:  ' . $arrangement->getId());
			$event = $arrangement->getEvent();
	  		
			//echo "<pre>"; print_r($arrangement); echo "</pre>"; 
//			$vbm->setArrangement($arrangement);
			 
		  	if( $event ) {    
		  		$locatie = $event->getLocatie();
		    	$eventType = $event->getEventType()->getNaam();
		  	}
	  		if( $locatie ) { $stad = $locatie->getStad(); }
	  		
			if( $stad ) {
				$landId = $stad->land_id;
			}
			
			$destinationIATA 	= $stad->getIATA();
			if ( VENDOR_PREFIX_PRICE == "VBT" ||  VENDOR_PREFIX_PRICE == "OPT" ) {
				$departureIATA 		= "AMS";
			 	
			} elseif ( VENDOR_PREFIX_PRICE == "FBT" ) {
				$departureIATA 		= "LON";
			} elseif ( VENDOR_PREFIX_PRICE == "DKT" ) {
				$departureIATA 		= "CPH";
						
			}else {
				$departureIATA 		= "";	
			}
			
			$this->log( '--- updatePrice START:  ' . $eventType . " , " 
					. $destinationIATA . " , " 
					. $arrangement->getVluchtBoeken() . " , " 
					. $event->getGeenVluchten()   );
			
			if( $destinationIATA 
				&& $arrangement->getVluchtBoeken() == 't' 
				&& $event->getGeenVluchten() == 'f' )  
			{
    			
				$startDate 		= $arrangement->getHeen();
    			$returnDate 	= $arrangement->getTerug();
    			
    			//$typeVlucht , $departure, $destination, $departureDate, $returnDate = '', $noAdults, $noChildren=0, $noInfants=0, $currency='EUR' 
    			//$vbm->getFlights ( 	"heen", $departure, $destination, $chooseVertrekHeendatum, null, $aantal_volwassenen, $aantal_kinderen, 0, 'EUR' );
    			/*
    			 *  HEEN 
    			 * */
    			$type 			= "heen";

    			 
    				
    			$inboundParams = array
    			(
    					'departure'             => $departureIATA,
    					'destination'           => $destinationIATA,
    					'departure_date'        => $startDate,
    					'numAdt'                => $numPassenger,
    					'numChd'                => 0
    			
    			);
    			
    			$outboundParams = array
    			(
    					'departure'             => $destinationIATA,
    					'destination'           => $departureIATA,
    					'departure_date'        => $returnDate,
    					'numAdt'                => $numPassenger,
    					'numChd'                => 0
    						
    			);
    			 
    			try
    			{
    				$matchStartDateTimeString = $arrangement->getDatum();
    				$inboundSearchFlights = $bookFlightWizard->searchFlights($inboundParams);
    				$inboundFlightsIterator = $inboundSearchFlights->getFilteredFlights($matchStartDateTimeString, $landId);
    				$inboundFlights = iterator_to_array($inboundFlightsIterator);
    			}
    			catch (\Exception $e)
    			{
    				$inboundFlights = array();
    				
//     				$elsyArres = $bookFlightWizard->getElsyArres();
//     				$lastResponse = $elsyArres->getLastResponse();
//     				print_r ($inboundParams);
//     				print_r ($outboundParams);
//     				print_r ($lastResponse);
//     				exit;
    				
    			}
    				 
    			try
    			{
    				$outboundSearchFlights = $bookFlightWizard->searchFlights($outboundParams);
    				$outboundFlights = $outboundSearchFlights->getFlights()->getFlights();
    			}
    			catch (\Exception $e)
    			{
    				$outboundFlights = array();
    			}

    			
//    			$vbm->getFlights ( 	$type, $departureIATA, $destinationIATA, $startDate, null, $numAdults = 2, 0, 0, 'EUR' );
//				$vluchten 		= $vbm->flight_data_to;
				//$out = print_r ($vluchten, true);
				//mail( "alert@travelgroep.nl", "RW_ ModulePricingAgent - ",  " " . $out   );
		
				
				$oldCostsPp		= 0;
				$feeMargin 		= 0;//15; // De goedkoopste met een bedrag!
				
				if (count($inboundFlights) > 0 || $skip ) {
// 					foreach( $vluchten as $id => $vlucht ){
// 		    			$arrCostsPp[] = money_format ( "%.2n" , $vlucht->costsPp + $feeMargin );
// 					}
					foreach ($inboundFlights as $flight)
					{
						$costPerPerson = $costCalculator->getCostPerPerson($flight);
						
						$arrCostsPp[] = money_format ( "%.2n" , $costPerPerson + $feeMargin );
					}
					
					$this->log( '--- updatePrice ('.$type.') VluchtHeenTarief(: =>  ' .$arrCostsPp[0] . ' - old, ' . $arrangement->getVluchtHeenTarief()  );
					$arrangement->setVluchtHeenTarief($arrCostsPp[0]);
					unset($arrCostsPp);
					$this->searchCount	+= 1;
				} else {
					$flightBasePrice = $arrangement->getVluchtHeenTarief();
					if (empty($flightBasePrice))
					{
						$arrangement->setVluchtHeenTarief($defaultBaseprice);
						
						$this->log( '--- updatePrice ('. $type .') vluchten INIT BASEBPRISE: '.$defaultBaseprice.' =>  ' .  $arrangement->getId());
						$this->skipped	+= 1;
					}
					else
					{
						$arrangement->setVluchtHeenTarief($flightBasePrice);
						$this->log( '--- updatePrice ('. $type .') vluchten SKIPPED(: ('.$flightBasePrice.') =>  ' .  $arrangement->getId()   );
						$this->skipped	+= 1;
					}	
				}
    			 
				
    			/*
    			 *  TERUG 
    			 * */
    			$type 			= "terug";
//    			$vbm->getFlights ( 	$type, $destinationIATA , $departureIATA, $returnDate, null, $numAdults = 2, 0, 0, 'EUR' ); 
//				$vluchten 		= $vbm->flight_data_return;
				$oldCostsPp		= 0;
				$feeMargin 		= 0; //15; // De goedkoopste met een bedrag!
				
				if (count($outboundFlights) > 0 ) {
// 					foreach( $vluchten as $id => $vlucht ){
// 		    			$arrCostsPpTerug[] = money_format ( "%.2n" ,$vlucht->costsPp + $feeMargin);
// 					}
					foreach ($outboundFlights as $flight)
					{
						$feeCalculator = new \Travelgroep\ElsyArres\SearchFlightsFeeCalculator();
						$costCalculator = new \Travelgroep\ElsyArres\SearchFlightsCostCalculator($numPassenger, $feeCalculator);
						$costPerPerson = $costCalculator->getCostPerPerson($flight);
					
						$arrCostsPpTerug[] = money_format ( "%.2n" , $costPerPerson + $feeMargin );
					}
						
					
					$this->log( '--- updatePrice ('.$type.') VluchtTerugTarief(: =>  ' .$arrCostsPpTerug[0] . ' - old, ' . $arrangement->getVluchtTerugTarief()  );
					$arrangement->setVluchtTerugTarief( $arrCostsPpTerug[0] );
					unset($arrCostsPpTerug);
					$this->searchCount	+= 1;
				} else {
					$flightBasePrice = $arrangement->getVluchtTerugTarief();
					if (empty($flightBasePrice))
					{
						$arrangement->setVluchtTerugTarief($defaultBaseprice);
						$this->log( '--- updatePrice ('. $type .') vluchten INIT BASEBPRISE: '.$defaultBaseprice.' =>  ' .  $arrangement->getId());
					}
					else
					{
						$arrangement->setVluchtTerugTarief($flightBasePrice);
						$this->log( '--- updatePrice ('. $type .') vluchten SKIPPED(: ('.$flightBasePrice.') =>  ' .  $arrangement->getId()   );
					}	
						$this->skipped	+= 1;
					
				}
				
				
				//$arrangement->storeNoLocale();
                $arrangement->store();
				
				$vlucht_prijs = $arrangement->getVluchtTerugTarief() + $arrangement->getVluchtHeenTarief(); 
				$this->log( '--- updatePrice vlucht_prijs(: =>  ' .$vlucht_prijs  );
				
				
			} else {
				$this->skipped	+= 1;
				$this->log( '--- updatePrice START:  ' . $eventType . " = " . $this->reis_type . " ,  "  
					. $destinationIATA . " , " 
					. $arrangement->getVluchtBoeken() . " = t , " 
					. $event->getGeenVluchten() . " = f "   );
					
			}
//$i  = $i-1; 
//}// one		
		} // foreach
		/**
		 * Email gegevens
		 * */
	}
	 
	
	function getAllArrangementenInPresentByReisType( ){
		
		$this->log( '--- getAllArrangementenInPresentByReisType: ');
		
		echo " test => "  . VENDOR_PREFIX_PRICE;
		
		if ( VENDOR_PREFIX_PRICE == "VBT" || VENDOR_PREFIX_PRICE == "OPT" || VENDOR_PREFIX_PRICE == "DKT") {
			$q 	= "SELECT * FROM arrangement WHERE heen > NOW() AND actief = True AND vlucht_boeken = True AND event_id " . 
					"IN (SELECT id FROM event " . 
					"WHERE event_type_id = (SELECT id FROM event_type WHERE naam = '{$this->reis_type}')) order by id desc " ;
			
		} else if ( VENDOR_PREFIX_PRICE == "FBT" ) {
			// Alleen Spanje en Italy 
			
			/* arrangementen op halen met de engelse bijschift*/
			$q 	= "SELECT * FROM arrangement WHERE heen > NOW() AND actief = True AND vlucht_boeken = True AND event_id IN (" . 
					"SELECT id FROM event " . 
					"WHERE event_type_id = (" .
						"SELECT id FROM event_type WHERE naam = '{$this->reis_type}')" . 
							"AND locatie_id in (" .
								"SELECT id FROM locatie WHERE stad_id IN (" .
									"SELECT id FROM stad WHERE land_id = 2 OR land_id = 15 ))) order by id desc";
		}  
		
	  	
		$this->log( '=== query ('.$_SESSION['taalcode'].') =>' . $q );
	  
	    $res 	= $GLOBALS['rsdb']->query( $q );
		for( $i=0; $i<pg_num_rows( $res ); $i++ ) 	{
			$row 				= pg_fetch_array( $res, $i, PGSQL_ASSOC );
	    	$array[] 		= new Arrangement(null, $row);
		}
		$this->log( '=== count =>' . count($array) );
	  return $array;
	}

	
function getArrangementById( $arrId ){
		
		$this->log( '--- getArrangementById: ');
		
		$q 	= "SELECT * FROM arrangement WHERE heen > NOW() AND actief = True AND vlucht_boeken = True AND  id = {$arrId} ";
		 
		
	  	
		$this->log( '=== query ('.$_SESSION['taalcode'].') =>' . $q );
	  
	    $res 	= $GLOBALS['rsdb']->query( $q );
		for( $i=0; $i<pg_num_rows( $res ); $i++ ) 	{
			$row 				= pg_fetch_array( $res, $i, PGSQL_ASSOC );
	    	$array[] 		= new Arrangement(null, $row);
		}
		$this->log( '=== count =>' . count($array) );
	  return $array;
	}
	
 	function setReisType ( $val ) { $this->reis_type = $val;   }
 	
 	function log ( $val ) {
 		$msgVal = date('Y-m-d G:i:s') . ' '. $val . "\n";
 		$this->msg .= $msgVal;
 		echo $msgVal; 
 	}
  
} 
?>
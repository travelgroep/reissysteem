<?php


/*
 *  php pricingFlight.php FBT of VBT
	php pricingFlight.php VBT live
 * */
$absPath = array (
			"test" => "/home/ronald/public_html/", 
			"live" => "/data1/www/",
		);
$rootPath = $absPath[$argv[2]];

require_once( $rootPath.'reissysteem/inc/config.php');
require_once( $rootPath.'reissysteem/agents/module/ModulePricingAgents.php');
require_once( $rootPath.'wizard-nieuw6/soap/VBM2.php');

 session_start();
 
 //mail( "ronald@travelgroep.nl", "Module Pricing Agent ReisBeheer []",  "body", "header" );
 
$vendor_prefix		= $argv[1];	

$_SESSION['agent'] 	= TRUE;  // Mother Fucker - Agent mag standaard variable niet overschrijven:P

 if ( $vendor_prefix == "VBT") {
	 $searchEventType 	= "Voetbalreis";
 	 define( VENDOR_PREFIX_PRICE, "VBT" ); 	// pass this on to Pronto logger
	 unset($_SESSION ['taalcode']);
	 $_SESSION ['taalcode'] = 'nl'; 		// fase 2 en
 } else  if ( $vendor_prefix == "FBT") {
 	$searchEventType 	= "Voetbalreis"; //Football Trip
 	define( VENDOR_PREFIX_PRICE, "FBT" ); 	// nog niks mee gedaan
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'en' );
	 define( 'LOCALE_FORMAT', 'en' );	
	 $_SESSION ['taalcode'] = "en"; 		// fase 2 en
 
 } else  if ( $vendor_prefix == "DKT") {
 	$searchEventType 	= "Voetbalreis"; //Fodboldrejse
 	define( VENDOR_PREFIX_PRICE, "DKT" ); 	// nog niks mee gedaan
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'dk' );
	 define( 'LOCALE_FORMAT', 'dk' );	
	 $_SESSION ['taalcode'] = "dk"; 		 
	 
 } else  if ( $vendor_prefix == "OPT") {
 	$searchEventType 	= "Operareis"; 
 	define( VENDOR_PREFIX_PRICE, "OPT" ); 	// nog niks mee gedaan
	 unset($_SESSION ['taalcode']);
	 define( 'LOCALE_TAALCODE', 'nl' );
	 define( 'LOCALE_FORMAT', 'nl' );	
	 $_SESSION ['taalcode'] = "nl"; 		// fase 2 en
 	
 } else {
 	$searchEventType = null;
 	echo " STOP ModulePricingAgents => " . $vendor_prefix;
 }
 
 
 
 $agentStartTime 		= date( "l, d F Y  H:i" );
 $db 					= new DB();
 if ( !isset( $argv[1] ) ) { echo "Gebruik argv: $argv[0] [reistype] [(String)]\n"; die;  
 } else {
	
 	$pricingAgents	= new ModulePricingAgents( $searchEventType   ); // "Voetbalreis"
	$array = $pricingAgents->getAllArrangementenInPresentByReisType( ); // 28738
	$pricingAgents->updatePrice($array);
 }
$agentEndTime = date( "l, d F Y  H:i" );

		if( AGENTS_TEST == "live" ) { 		$server = "xTravel @ CJ2"; }
		elseif( AGENTS_TEST == "test" ) { 	$server = "wim @ CJ2"; }
		else { $server = "unknown"; }
		
		$searchCount 	= $pricingAgents->searchCount;
		$skipped		= $pricingAgents->skipped;
		$msg			= $pricingAgents->msg;
		$totals 		= "Agent started on ".$agentStartTime." and ended on ".$agentEndTime." system name: ".$server."\n";
		$totals 		.= "Number of 'arrangementen' processed: [".count($array)."], event type is [".$searchEventType."]\n";
		$totals 		.= "Number of searchFlights processed: [".$searchCount."], skipped [".$skipped."]\n";

		$mailbody = $totals . "\n\n" . $msg;

		$headers = "from:Module Pricing Agent <ronald@travelgroep.nl>\n";
		
		mail( "ronald@travelgroep.nl", "Module Pricing Agent ReisBeheer [".$searchEventType."]", $mailbody, $headers );
 
 
?>
<?php

session_start();

$agentStartTime = date( "l, d F Y  H:i" );

$absPath = array (
			"test" => "/home/pheno/public_html/", 
			"live" => "/data1/www/",
		);


// set debug level
// 0 = no DEBUG;
// 1 = level 1 (all for now);
// 2 =			

define( "DEBUG" , $argv[5] );


/*
 * TEST
 * For webbased testing only!!! ALWAYS COMMENT THIS LINE BEFORE UPDATING PRODUCTION!!!
 */
 
//define( "DEBUG" , 1 ); //testing
//$searchEventType = "voetbalreis"; //testing						

/*
 * END TEST
 */
 
if ( isset($argv[1]) && isset($argv[2]) && isset($argv[3]) )
{
	$rootPath = $absPath[$argv[1]];
	$searchEventType = $argv[2];
	$searchDays = $argv[3];
	$language = $argv[4];
	$arrangement_id = $argv[6];
}
elseif ( DEBUG == 1 )
{
	$rootPath = $absPath["test"];
}
else 
{
	echo "\nusage: php <script> [test/live]] [eventType] [searchDays] [language] [DEBUG level 0/1] [arrangement_id]\n\n\n";
	die();
}

require $rootPath."wizard_v2/soap/vluchtboekingsmodule.php" ;
require_once( $rootPath.'reissysteem/db/db.php' );
require_once( $rootPath.'reissysteem/db/DBRecord.php');
require_once( $rootPath.'reissysteem/class/Beschrijving.php');
require_once( $rootPath.'reissysteem/class/Webentiteit.php');
require_once( $rootPath.'reissysteem/class/Arrangement.php');
require_once( $rootPath.'reissysteem/class/Locale2.php');
require_once( $rootPath.'reissysteem/class/LocaleValue.php');
require_once( $rootPath.'reissysteem/class/Stad.php');
require_once( $rootPath.'reissysteem/class/Locatie.php');
require_once( $rootPath.'reissysteem/class/Event.php');
require_once( $rootPath.'reissysteem/class/Land.php');
require_once( $rootPath.'reissysteem/class/EventType.php');
//require_once( '../class/Foto.php');
//require_once( '../class/Reisoptie.php');
//require_once( '../class/ReisoptieWaarde.php');

if( ! isset($GLOBALS[ 'rsdb' ]) )
{
  if( $argv[1] == "live" )
  {
    $GLOBALS[ 'rsdb' ] = new DB( 'reisdb', 'reisdb', 'blaat', 'rsdb');
  }
  else
  {
    $GLOBALS[ 'rsdb' ] = new DB( 'reisdb', 'pgsql', '', 'rsdb');
  }
}

if ( $language == 'nl' )
{
  $language = null;
}

$_SESSION['taalcode'] = $language;
$_SESSION['agent'] = TRUE;

// alleen arrangementen ophalen waarvan 'heen' in de toekomst ligt
$crit[] = "heen > '".date('Y-m-d', strtotime( '+ 1 day' ))."'";

/* dagelijks worden alleen arrangementen met 'searchEventType' opgehaald waarvan 'heen' binnen 'searchDays' dagen valt. 
 * Met uitzondering van 'EventType' voetbalreis, hiervoor worden alle arrangementen elke dag doorzocht.
 * 
 * Als 0 wordt opgegeven bij searchdays worden dus ook alle dagen doorzocht
 */
 
if( $searchDays && $searchEventType != "voetbalreis" )
{
  $crit[] = "heen < '".date('Y-m-d', strtotime( '+ '.$searchDays.' day' ))."'";
}

$crit[] = "arrangement.actief = 't' ";
//$crit[] = "arrangement.id > 14000";
//$crit = array("arrangement.id=14256");

$sortField = "heen";
$orientation = "asc";

$arrangementen_ids = $arrangement_id ? array( $arrangement_id ) : getArrangementenIds( 0, 0, $sortField, $orientation, $crit );

$vbm = new VluchtBoekingsModule();

$searchCount = 0;
$skipped = 0;
$msg = '';

foreach( $arrangementen_ids as $arrangement_id )
{ 
  $arrangement = new Arrangement();
  $arrangement->loadById( $arrangement_id );
  
  $event = $arrangement->getEvent();
  
  if( $event ) 
  {
    $locatie = $event->getLocatie();
    $eventType = strtolower( $event->getEventType()->getNaam() );
  }
  
  if( $locatie )
  {
    $stad = $locatie->getStad();
  }
  
  if( $stad )
  {
    $land = $stad->getLand(); 
  }
  
  $destinationIATA = $stad->getIATA();
  
  switch( $destinationIATA )
  {
    case 'GLA' :
    case 'MANU' :
      $departureIATA = "DUS";
    break;
    
    default:
      $departureIATA = "AMS"; 
  }

  global $language;
  if ( $language == 'en' )
  {
    $departureIATA = 'LON';
  }

  // Only get flights for matching EventTypes given through command line argument (argv[2])!
  if( $eventType == strtolower( $searchEventType ) )
  {
    $msg .= $arrangement->getId()." ) ";
    $startDate = $arrangement->getHeen();
    $returnDate = $arrangement->getTerug();
    
//    echo "\nDEBUG: departureIATA={$departureIATA} destinationIATA={$destinationIATA} startDate={$startDate} returnDate={$returnDate}"; 
//    print_r ( $land );
//		echo "\n\n";
    
    // N/REG/7886/52, N/CIT/7886/268
		$destination = $vbm->IATAtoId( $stad->getIATA() );
		$departure = 'N/REG/7886/52';
    
    $ret = $vbm->getFlights( $departure, $destination, $startDate, $returnDate, $numAdults = 1, "", $land->getISOcode() );
    $search = TRUE;
    $searchCount ++;
  }
  else
  {
    $ret = "";//$eventType." SKIPPED";
    $search = FALSE;  
    $skipped ++;
  }
  
  if( $search )
  {

		echo "terug: " . $outFlight["SearchIATADestination"];


    $msg .= "[outbound] ";
    $outFlightCosts = 0;
    $inFlightCosts = 0;
    
    if( $outFlight = $vbm->getCheapestFlight('heen', $departureIATA ) )
    {
      $outFlightCosts = $outFlight["TotalCosts"];
      $outFlightDep = $outFlight["DepartureAirport"]."(".$outFlight["SearchIATADeparture"].") ";
      $outFlightDest = $outFlight["DestinationAirport"]."(".$outFlight["SearchIATADestination"].") ";

      $arrangement->setVluchtHeenTarief( $outFlightCosts );
      
    
      $msg .= $startDate." ".$outFlightDep." - ".$outFlightDest." = � ".$outFlightCosts." ";
    }
    else
    {
      $msg .= "NOT FOUND! "; 
    }
    
    $msg .= "[inbound] ";
    
    if( $inFlight = $vbm->getCheapestFlight('terug', $outFlight["SearchIATADestination"] ) )
    {
      $inFlightCosts = $inFlight["TotalCosts"];
      $inFlightDep = $inFlight["DepartureAirport"]."(".$inFlight["SearchIATADeparture"].") ";
      $inFlightDest = $inFlight["DestinationAirport"]."(".$inFlight["SearchIATADestination"].") ";
      
      $arrangement->setVluchtTerugTarief( $inFlightCosts );
      $msg .= $returnDate." ".$inFlightDep." - ".$inFlightDest." = � ".$inFlightCosts." ";
    }
    else
    {
      $msg .= "NOT FOUND! "; 
    }
    
    $total = (int)$outFlightCosts + (int)$inFlightCosts;

    // no price found? 
    
    if ( (int)$outFlightCosts < 1 )
    {
      $arrangement->setVluchtHeenTarief( 75 );      
      $arrangement->setVluchtBoeken( false );
    }
    if ( (int)$inFlightCosts < 1 )
    {
      $arrangement->setVluchtTerugTarief( 75 );      
      $arrangement->setVluchtBoeken( false );
    }

/*    
    echo "\nid:" . $arrangement_id;
    echo "\ncosts in:" . $inFlightCosts;
    echo "\ncosts in:" . $outFlightCosts;
    echo "\n";
*/
    
    $arrangement->store(); 
   
    $msg .= "TotalCosts = � ".(int)$total;
    unset( $outFlight );
    unset( $inFlight );
    
    unset( $outFlightCosts );
    unset( $inFlightCosts );
    $msg .= "\n";
  }
  else
  {
    $msg .= $ret;
  }
}

$agentEndTime = date( "l, d F Y  H:i" );

if( $argv[1] == "live" )
{
  $server = "xTravel @ CJ2";
}
elseif( $argv[1] == "test" )
{
  $server = "wim @ CJ2";
}
else
{
  $server = "unknown";
}

$totals = "Agent started on ".$agentStartTime." and ended on ".$agentEndTime." system name: ".$server."\n";
$totals .= "Number of 'arrangementen' processed: [".count($arrangementen_ids)."], event type is [".$searchEventType."]\n";
$totals .= "Number of searchFlights processed: [".$searchCount."], skipped [".$skipped."]\n";

$mailbody = $totals . "\n\n" . $msg;

if( DEBUG == 1 )
{
  print "<pre>";
  print $mailbody;
  print "</pre>";
}
else
{
  $headers = "from:Pricing Agent<info@cj2.nl>\n";
	if( $argv[1] == "live" )
	{
		$headers .= "Bcc:info@voetbaltravel.nl\n";
	}
	$headers .= "\n";
	
	mail( "thomas@cj2.nl", "PricingAgent ReisBeheer [".$searchEventType."]", $mailbody, $headers );
}
?>